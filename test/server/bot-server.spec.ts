import BeeQueue = require('bee-queue');
import axios from 'axios';

jest.mock('axios');
jest.mock('csv-write-stream', () =>
  jest.fn(() => {
    return { pipe: jest.fn(), write: jest.fn(), end: jest.fn() };
  }),
);
const postMock = jest.fn();
axios.post = postMock;
const mockCreateConnectionSpy = jest.fn().mockReturnValue({
  getRepository: jest.fn().mockReturnValue({
    find: async (a: any) => {
      return { country: 'United states' };
    },
  }),
});

jest.mock('typeorm', () => ({
  Like: jest.fn(),
  MoreThan: jest.fn(),
  createConnection: mockCreateConnectionSpy,
  PrimaryGeneratedColumn: jest.fn(),
  PrimaryColumn: jest.fn(),
  OneToOne: jest.fn(),
  ManyToOne: jest.fn(),
  OneToMany: jest.fn(),
  ManyToMany: jest.fn(),
  JoinTable: jest.fn(),
  UpdateDateColumn: jest.fn(),
  CreateDateColumn: jest.fn(),
  Column: jest.fn(),
  Entity: jest.fn(),
  AfterLoad: jest.fn(),
  JoinColumn: jest.fn(),
  In: jest.fn(),
}));

import Il8nService from '../../src/service/Il8n.service';
import { UserService } from '../../src/service/User.service';
import { Connection } from 'typeorm/connection/Connection';
import Flight from '../../src/model/Flight';
import { FlightResponse, SpecialAlerts } from '../../src/interface/lumo';
import { FlightService } from '../../src/service/Flight.service';
import { TrackService } from '../../src/service/Track.service';
import { mockReset } from 'jest-mock-extended';
import { hasUncaughtExceptionCaptureCallback } from 'process';
import { OneToOne } from 'typeorm';
import { ConfigService } from '../../src/service';
import { AirtableFactory } from '../../src/factory/Airtable.factory';
import { FlightHelper } from '../../src/helpers/flight.helper';

const mockGetContextPath = jest.fn();
const mockGetSessionPath = jest.fn();
const mockDeleteAllContexts = jest.fn();
const mockDeleteContext = jest.fn();

FlightHelper.toShortSummary = jest.fn((a, flight) => {
  const summary = `short summary ${flight.id}`;
  return summary;
});

FlightHelper.timeUntilDeparture = jest.fn((flight) => {
  return mockTimeUntilDepartureSpy(flight.id);
});

const mockTrackSingleFlight = jest.fn((lumoRecord: FlightResponse) => {
  mockTestFlights.push({
    seq: 1,
    id: mockTestFlights.length + 1,
    toShortSummary(): string {
      return mockToShortSummarySpy(mockTestFlights.length + 1);
    },
    timeUntilArrival(): string {
      return mockTimeUntilArrivalSpy(mockTestFlights.length + 1);
    },
    timeUntilDeparture(): string {
      return mockTimeUntilDepartureSpy(mockTestFlights.length + 1);
    },
    flightNo: `${lumoRecord.flight.carrier.iata}${lumoRecord.flight.number}`,
    origin: lumoRecord.flight.origin.iata,
    destination: lumoRecord.flight.destination.iata,
    estimatedDepartureDateLocal: lumoRecord.flight.scheduled_departure.split(
      'T',
    )[0],
    scheduledDepartureDateLocal: '2020-08-31',
    scheduledDepartureTimeLocal: '10:45:00',
    scheduledDeparture: {
      toISOString: jest
        .fn()
        .mockReturnValue(lumoRecord.flight.scheduled_departure),
    },
  });
});

const mockAirTableService = AirtableFactory.create();
mockAirTableService.getValidPickups = jest
  .fn()
  .mockReturnValueOnce([])
  .mockReturnValueOnce([{ fields: { 'IATA Code': 'bubu' } }]);

const mockTestJob = {
  data: {
    To: 'whatsapp:+13235082016',
    From: 'whatsapp:+995557773417',
    source: 'whatsapp',
    human: true,
    Body: 'next_flight', //*******//
    server: 'localhost',
  },
  id: 1,
};

const mockJobWithMutedUser = {
  data: {
    To: 'whatsapp:+13235082016',
    From: 'whatsapp:+123123123123',
    source: 'whatsapp',
    human: true,
    Body: 'next_flight',
    server: 'localhost',
  },
  id: 1,
};

const mockSwitchCases = [
  'next_flight',
  'flight_time_check',
  'next_arrival_time',
  'next_departure_time',
  'all_flights',
  'terminal',
  'waivers',
  'baggage_claim',
  'safe_to_fly_to_select_number',
  'directions',
];

const mockGetDateForCity = jest.fn();

const mockStructProtoToJson = jest.fn().mockReturnValue({
  flight: {
    flight: {
      number: '300',
      carrier: {
        iata: 'AA',
      },
      origin: {
        iata: 'JFK',
      },
      scheduled_departure: '2020-08-31T10:38:28Z',
    },
  },
  flights: [
    {
      flight: {
        flight: {
          number: '300',
          carrier: {
            iata: 'AA',
          },
          origin: {
            iata: 'JFK',
          },
          scheduled_departure: '2020-08-31T10:38:28Z',
        },
      },
    },
  ],
  number: 1,
});
const mockGetFlightByData = jest.fn((number, origin, departure) => {
  return mockTestFlights.find(
    (flight) =>
      flight.flightNo === number &&
      flight.scheduledDeparture.toISOString().includes(departure) &&
      flight.origin === origin,
  );
});
const mockToShortSummarySpy = jest.fn((a) => `short summary ${a}`);
const mockTimeUntilArrivalSpy = jest.fn((a) => `time until arrival ${a.id}`);
const mockTimeUntilDepartureSpy = jest.fn(
  (a) => `time until departure ${a.id}`,
);

let mockUserTrip = [
  {
    trip_id: 1,
  },
];

let mockTrip = [
  {
    trip_id: 1,
    flight: 1,
  },
  {
    trip_id: 2,
    flight: 2,
  },
];

let mockTestFlights = [
  {
    seq: 1,
    id: 1,
    toShortSummary(): string {
      return mockToShortSummarySpy(1);
    },
    timeUntilArrival(): string {
      return mockTimeUntilArrivalSpy(1);
    },
    timeUntilDeparture(): string {
      return mockTimeUntilDepartureSpy(1);
    },
    flightNo: 'AA300',
    origin: 'JFK',
    estimatedDepartureDateLocal: '2020-08-31',
    scheduledDepartureDateLocal: '2020-08-31',
    scheduledDepartureTimeLocal: '10:45:00',

    scheduledDeparture: {
      toISOString: jest.fn().mockReturnValue('2020-08-31T10:38:28Z'),
      toJSON: jest.fn().mockReturnValue('2020-08-31T10:38:28Z'),
    },
    estimatedDeparture: {
      toISOString: jest.fn().mockReturnValue('2020-08-31T10:40:28Z'),
    },
    destination: 'TLV',
    operatingCarrier: '',
  },
  {
    seq: 1,
    id: 2,
    toShortSummary(): string {
      return mockToShortSummarySpy(2);
    },
    timeUntilArrival(): string {
      return mockTimeUntilArrivalSpy(2);
    },
    timeUntilDeparture(): string {
      return mockTimeUntilDepartureSpy(2);
    },
    flightNo: 'AA200',
    origin: 'JFK',
    destination: 'LHR',
    estimatedDepartureDateLocal: '2020-08-31',
    scheduledDepartureDateLocal: '2020-08-31',
    scheduledDepartureTimeLocal: '10:45:00',
    scheduledDeparture: {
      toISOString: jest.fn().mockReturnValue('2020-08-31T10:38:28Z'),
    },
  },
];

const mockFindOneComms = jest.fn();

const mockUserTripRepo = {
  find: jest.fn().mockReturnValue(mockUserTrip),
};

const mockTripRepo = {
  find: jest.fn().mockResolvedValue(mockTrip),
  remove: jest.fn(async (items) => {
    items.map((item: any) => {
      let index = mockTrip.indexOf(item);
      mockTrip.splice(index, 1);
    });
  }),
};

const mockBQonSpy = jest.fn();
const mockBQprocessSpy = jest
  .fn()
  .mockImplementationOnce(async (func) => {
    mockSwitchCases.map((el) => {
      const deepCopy = JSON.parse(JSON.stringify(mockTestJob));
      deepCopy.data.Body = el;
      return func(deepCopy, () => 1);
    });
  })
  .mockImplementationOnce((func) => func)
  .mockImplementationOnce(async (func) => {
    mockSwitchCases.map((el) => {
      const deepCopy = JSON.parse(JSON.stringify(mockJobWithMutedUser));
      deepCopy.data.Body = el;
      return func(deepCopy, () => 1);
    });
  });

const mockAddEvent = jest.fn();

const mockFindByMobile = jest
  .fn()
  .mockReturnValueOnce({
    firstName: 'tim',
    lastName: 'duncan',
    mobile: '995557773417',
    muteBot: false,
  })
  .mockReturnValueOnce({
    firstName: 'tim',
    lastName: 'duncan',
    mobile: '995557773417',
    muteBot: false,
  })
  .mockReturnValue({
    name: 'tim duncan',
    firstName: 'tim',
    lastName: 'duncan',
    mobile: '995557773417',
    muteBot: false,
    confirmed_messaging: false,
  });

const mockNextArrivalFlight = jest.fn(() => mockTestFlights[0]);
const mockNextDepartureFlight = jest.fn(() => mockTestFlights[0]);
const mockNextFlightByUser = jest.fn(() => mockTestFlights[0]);
const mockCreateUserFlight = jest.fn();
const mockCreateUserTrip = jest.fn(() => mockUserTrip[0]);
const mockCreateTrip = jest.fn();
const mockAllFlightsByUser = jest.fn(() => mockTestFlights);
const mockGetFlightStatusById = jest.fn(() => {
  {
    {
      ('BB8');
    }
  }
});
const mockBeeQueueService = jest.fn();

const mockGetCovidSummary = jest.fn().mockReturnValue({
  summary: {
    sitata_risk: 50,
    active_count: 100,
    case_density: 12.5,
  },
});
const mockGetRestrictions = jest.fn().mockResolvedValue({
  airline: [{ comment: 'airline' }],
  border: [{ comment: 'border' }],
  restaurants: [{ comment: `rest` }],
  curfew: [{ comment: 'curfew' }],
  masks: [
    {
      comment: `masks`,
    },
  ],
  social: [{ comment: 'open' }],
  transportation: [{ comment: 'transportation' }],
  beaches: [{ comment: 'beaches' }],
  museums: [{ comment: 'museums' }],
});

const mockGetAvailableCodes = jest
  .fn()
  .mockReturnValueOnce(['403', '410', '405', '406', '0', '1', '2', '3'])
  .mockReturnValueOnce([])
  .mockReturnValueOnce(['403', '410', '405', '406', '0', '1', '2', '3'])
  .mockReturnValueOnce([])
  .mockReturnValueOnce(['403', '410', '405', '406', '0', '1', '2', '3']);
let mockGetResponseSpy = jest.fn((a, b, c) => ({
  queryText: 'message',
  intent: {
    displayName: c,
  },
  parameters: {
    fields: {
      number: {
        numberValue: '5',
        listValue: {
          values: [],
        },
      },
      jackandferdi: {
        listValue: {
          values: [{ stringValue: 'Meditation' }],
        },
      },
      'geo-country-code': {
        structValue: {
          fields: {
            'alpha-2': {
              stringValue: 'IL',
            },
            name: {
              stringValue: 'Israel',
            },
          },
        },
      },
      'geo-city': {
        stringValue: 'New York',
      },
    },
  },
  outputContexts: [
    {
      name: 'bla',
      parameters: [],
    },
    {
      name: 'safe_to_fly_to-followup',
      parameters: {
        fields: {
          number: {
            listValue: {
              values: [
                { numberValue: 1 },
                { numberValue: 2 },
                { numberValue: 3 },
              ],
            },
          },
          'geo-country-code': {
            structValue: {
              fields: {
                'alpha-2': {
                  stringValue: 'IL',
                },
                name: {
                  stringValue: 'Israel',
                },
              },
            },
          },
        },
      },
    },
  ],
  fulfillmentText: 'fulfilling',
}));

const services = {
  db: Connection,
  bq: jest.fn(),
};

jest.mock('../../src/util/flight.utils', () => {
  return {
    structProtoToJson: mockStructProtoToJson,
    firstName: jest.fn().mockImplementation((name: string) => {
      return name.split(' ')[0];
    }),
    jsonValueToProto: mockStructProtoToJson,
    tConvert: jest.fn().mockReturnValue(73),
    getTimeFromDate: jest.fn().mockReturnValue(73),
    parseFlightNo: jest.fn((a) => {
      try {
        return {
          carrier: a.stringValue.substr(0, 2),
          flight: Number(a.stringValue.substr(2).trim()),
        };
      } catch {
        return {
          carrier: null,
          flight: null,
        };
      }
    }),
  };
});

jest.mock('../../src/service/pushNotifications.service', () => {
  return {
    pushNotificationsService: jest.fn().mockImplementation(() => {
      return {
        chrono: jest.fn(),
      };
    }),
  };
});

jest.mock('../../src/service/WeatherTime.service', () => {
  return {
    WeatherTime: jest.fn().mockImplementation(() => {
      return {
        getDateForCity: mockGetDateForCity,
      };
    }),
  };
});

jest.mock('../../src/service/User.service', () => {
  return {
    UserService: jest.fn().mockImplementation(() => {
      return {
        findByMobile: mockFindByMobile,
        userRepository: {
          save: jest.fn(),
        },
        muteBot: jest.fn(),
      };
    }),
  };
});

jest.mock('../../src/service/Flight.service', () => {
  return {
    FlightService: jest.fn().mockImplementation(() => {
      return {
        getLandedFlightsByUser: jest
          .fn()
          .mockResolvedValue([mockNextArrivalFlight()]),
        createUserFlight: mockCreateUserFlight,
        getNextFlightByUser: mockNextFlightByUser,
        getAllFlightsByUser: mockAllFlightsByUser,
        getNextArrivalFlight: mockNextArrivalFlight,
        getNextDepartureFlight: mockNextDepartureFlight,
        getFlightByData: mockGetFlightByData,
        createTrip: mockCreateTrip,
        createUserTrip: mockCreateUserTrip,
        userTripRepository: mockUserTripRepo,
        tripRepository: mockTripRepo,
      };
    }),
  };
});

const date = new Date();
const nextDayDate = new Date(date.setDate(date.getDate() + 1));
const dayAfterTomorrow = new Date(date.setDate(date.getDate() + 2));

const mockLumoSearch = jest.fn().mockReturnValue({
  results: [],
  count: 0,
});

const mockCreateContext = jest.fn();

const mockFSSearch = jest.fn().mockReturnValue({
  results: [
    {
      carrier: {
        iata: 'DL',
        name: 'DL',
      },
      origin: {
        iata: 'TLV',
      },
      departureAirport: {
        name: 'New York',
      },
      arrivalAirport: {
        name: 'Tel aviv',
      },
      flightNumber: 235,
      departureDate: {
        dateLocal: nextDayDate.toLocaleString(),
        dateUtc: nextDayDate.toLocaleString(),
      },
      arrivalDate: {
        dateLocal: dayAfterTomorrow.toLocaleString(),
        dateUtc: dayAfterTomorrow.toLocaleString(),
      },
    },
  ],
  count: 1,
});

const mockLumoStatus = jest.fn();

jest.mock('../../src/service/Lumo.service', () => {
  return {
    LumoService: jest.fn().mockImplementation(() => {
      return {
        subscribe: () => {},
        search: mockLumoSearch,
        status: mockLumoStatus,
      };
    }),
  };
});

jest.mock('../../src/service/Track.service', () => {
  return {
    TrackService: jest.fn().mockImplementation(() => {
      return {
        trackSingleFlight: mockTrackSingleFlight,
      };
    }),
  };
});
let mockMain: any;
let closeMain: any;
let mockProcessIncomingBQJob: any;
//let botserver: any;
describe('bot-server --->  main', () => {
  beforeEach(async () => {
    mockTestFlights = [
      {
        seq: null,
        id: 1,
        toShortSummary(): string {
          return mockToShortSummarySpy(1);
        },
        timeUntilArrival(): string {
          return mockTimeUntilArrivalSpy(1);
        },
        timeUntilDeparture(): string {
          return mockTimeUntilDepartureSpy(1);
        },
        flightNo: 'AA300',
        origin: 'JFK',
        estimatedDepartureDateLocal: '2020-08-31',
        scheduledDeparture: {
          toISOString: jest.fn().mockReturnValue('2020-08-31T10:38:28Z'),
          toJSON: jest.fn().mockReturnValue('2020-08-31T10:38:28Z'),
        },
        estimatedDeparture: {
          toISOString: jest.fn().mockReturnValue('2020-08-31T10:40:28Z'),
        },
        destination: 'TLV',
        operatingCarrier: '',
        scheduledDepartureDateLocal: '2020-08-31',
        scheduledDepartureTimeLocal: '10:45:00',
      },
      {
        seq: null,
        id: 2,
        toShortSummary(): string {
          return mockToShortSummarySpy(2);
        },
        timeUntilArrival(): string {
          return mockTimeUntilArrivalSpy(2);
        },
        timeUntilDeparture(): string {
          return mockTimeUntilDepartureSpy(2);
        },
        flightNo: 'AA200',
        origin: 'JFK',
        destination: 'LHR',
        estimatedDepartureDateLocal: '2020-08-31',
        scheduledDepartureDateLocal: '2020-08-31',
        scheduledDepartureTimeLocal: '10:45:00',
        scheduledDeparture: {
          toISOString: jest.fn().mockReturnValue('2020-08-31T10:38:28Z'),
        },
      },
    ];

    jest.mock('bee-queue', () => {
      return jest.fn().mockImplementation(() => ({
        on: mockBQonSpy,
        process: mockBQprocessSpy,
      }));
    });

    jest.mock('../../src/service/CommsEvent.service', () => {
      return {
        CommsEventService: jest.fn().mockImplementation(() => {
          return {
            eventRepository: {
              findOne: mockFindOneComms.mockResolvedValueOnce(null),
            },
            addEvent: mockAddEvent,
            getLastEvent: jest.fn().mockResolvedValue({
              intent: 'Default Fallback Intent',
              created: new Date(),
            }),
          };
        }),
      };
    });

    jest.mock('../../src/service/FlightStats.service', () => {
      return {
        FlightStatsService: jest.fn().mockImplementation(),
      };
    });

    //mocking the flightstats service so the code will not crash
    jest.mock('../../src/service/FlightStats.service', () => {
      return {
        FlightStatsService: jest.fn().mockImplementation(() => {
          return {
            getFlightStatusById: mockGetFlightStatusById,
          };
        }),
      };
    });

    jest.mock('../../src/service/FlightStats.service', () => {
      return {
        FlightStatsService: jest.fn().mockImplementation(() => {
          return {
            status: mockFSSearch,
            findFlightsByRoute: mockFSSearch,
          };
        }),
      };
    });

    jest.mock('../../src/service/BeeQueue.service', () => {
      return {
        BeeQueueService: jest.fn().mockImplementation(() => {
          return {
            createResponseJob: mockBeeQueueService.mockResolvedValue({
              id: 1,
            }),
          };
        }),
      };
    });
    jest.mock('../../src/service/Sitata.service', () => {
      return {
        SitataService: jest.fn().mockImplementation(() => {
          return {
            getRestrictions: mockGetRestrictions,
            getAvailableCodes: mockGetAvailableCodes,
            getCovidSummary: mockGetCovidSummary,
          };
        }),
      };
    });
    jest.mock('../../src/service/JackAndFerdi.service', () => {
      return {
        JackAndFerdiService: jest.fn().mockImplementation(() => {
          return {
            getPlaces: jest.fn((city: string, tags: Array<string>) => {
              return `${tags} in ${city}`;
            }),
            getCity: jest.fn((city: string) => {
              return city;
            }),
          };
        }),
      };
    });
    jest.mock('../../src/service/Dialogflow.service', () => {
      return {
        DialogflowService: jest.fn().mockImplementation(() => {
          return {
            getResponse: mockGetResponseSpy,
            getSessionPath: mockGetSessionPath,
            getContextPath: mockGetContextPath,
            contextClient: {
              deleteAllContexts: mockDeleteAllContexts,
              deleteContext: mockDeleteContext,
              createContext: mockCreateContext,
            },
          };
        }),
      };
    });

    // mockFindByMobile.mockResolvedValue({
    //   name: 'Bob',
    //   mobile: '1234123',
    // });

    const botserver = require('../../src/bot-server');
    mockMain = botserver.main;
    mockProcessIncomingBQJob = botserver.processIncomingBQJob;
    closeMain = botserver.terminate;
  });
  beforeAll(() => {
    AirtableFactory.create = jest.fn().mockReturnValue(mockAirTableService);
  });
  afterEach(() => {
    mockBeeQueueService.mockClear();
  });
  afterAll(() => {
    jest.clearAllMocks();
  });

  it('test', async () => {
    await mockProcessIncomingBQJob(services, '2', {
      From: 'whatsapp:+13235082016',
      To: 'whatsapp:+995557773417',
      Body: 'next_flight',
      Source: 'whatsapp',
    });
    expect(mockGetResponseSpy).toHaveBeenCalled();
    expect(mockFindByMobile).toHaveBeenCalled();
    expect(mockNextFlightByUser).toHaveBeenCalled();
    expect(FlightHelper.toShortSummary).toHaveBeenCalled();
  });
  it('restaurants', async () => {
    await mockProcessIncomingBQJob(services, '2', {
      From: 'whatsapp:+13235082016',
      To: 'whatsapp:+995557773417',
      Body: 'restaurants',
      Source: 'whatsapp',
    });

    const result = mockBeeQueueService.mock.calls[0][4];
    expect(result).toEqual('|||Restaurants:\nrest');
  });
  it('delete flight', async () => {
    await mockProcessIncomingBQJob(services, '2', {
      From: 'whatsapp:+13235082016',
      To: 'whatsapp:+995557773417',
      Body: 'delete_flight - number',
      Source: 'whatsapp',
    });
    expect(mockTrip).toEqual([
      {
        flight: 2,
        trip_id: 2,
      },
    ]);
  });
  it('blacklisted user', async () => {
    const ans = await mockProcessIncomingBQJob(services, '2', {
      From: 'whatsapp:+1244567890',
      To: 'whatsapp:+995557773417',
      Body: 'delete_flight - number',
      Source: 'whatsapp',
    });
    expect(ans).toEqual('BlockListed');
  });

  it('masks - not found', async () => {
    await mockProcessIncomingBQJob(services, '2', {
      From: 'whatsapp:+13235082016',
      To: 'whatsapp:+995557773417',
      Body: 'masks',
      Source: 'whatsapp',
    });
    const result = mockBeeQueueService.mock.calls[0][4];
    expect(result).toEqual(
      `I cant find data about Masks in my databases, but I am constantly updating so you can ask another time`,
    );
  });
  it('lost baggage(no carrier)', async () => {
    await mockProcessIncomingBQJob(services, '2', {
      From: 'whatsapp:+13235082016',
      To: 'whatsapp:+995557773417',
      Body: 'lost_baggage',
      Source: 'whatsapp',
    });
    const result = mockBeeQueueService.mock.calls[0][4];
    expect(result).toEqual(
      "Oh no! If you're still at the airport, the best thing you can do is report the bag as missing now and get a tracking number. It's much easier to get assistance when you do this in person.\n\nYou should also ask if you're entitled to compensation, sometimes the airline will agree to give you money or pay for clothes and toiletries now.",
    );
  });
  it('lost baggage(carrier)', async () => {
    mockTestFlights[0].operatingCarrier = 'AA';
    await mockProcessIncomingBQJob(services, '2', {
      From: 'whatsapp:+13235082016',
      To: 'whatsapp:+995557773417',
      Body: 'lost_baggage',
      Source: 'whatsapp',
    });
    const result = mockBeeQueueService.mock.calls[0][4];
    expect(result).toEqual(
      "Oh no! If you're still at the airport, the best thing you can do is report the bag as missing now and get a tracking number. It's much easier to get assistance when you do this in person.\n\nYou should also ask if you're entitled to compensation, sometimes the airline will agree to give you money or pay for clothes and toiletries now.\n\nFinally, you can learn more about the baggage policy of American Airlines at https://www.aa.com/i18n/travel-info/baggage/delayed-or-damaged-baggage.jsp",
    );
  });
  it('safe to fly to', async () => {
    await mockProcessIncomingBQJob(services, '2', {
      From: 'whatsapp:+13235082016',
      To: 'whatsapp:+995557773417',
      Body: 'safe_to_fly_to',
      Source: 'whatsapp',
    });
    const result = mockBeeQueueService.mock.calls[0][4];
    expect(result).toEqual(
      Il8nService.get('zenny:sitata:covidsummary', {
        templateVars: {
          country: 'Israel',
          risk: require('../../src/bot-server').getRiskWord(50),
          active_cases: 100,
          case_density: 12,
        },
      }),
    );
  });
  it('who am i?', async () => {
    await mockProcessIncomingBQJob(services, '2', {
      From: 'whatsapp:+13235082016',
      To: 'whatsapp:+995557773417',
      Body: 'who_am_i',
      Source: 'whatsapp',
    });
    const result = mockBeeQueueService.mock.calls[0][4];
    expect(result).toEqual(`tim duncan`);
  });
  it('masks', async () => {
    await mockProcessIncomingBQJob(services, '2', {
      From: 'whatsapp:+13235082016',
      To: 'whatsapp:+995557773417',
      Body: 'masks',
      Source: 'whatsapp',
    });
    const result = mockBeeQueueService.mock.calls[0][4];
    expect(result).toEqual('|||MASKS:\nmasks');
  });

  it('restaurants', async () => {
    await mockProcessIncomingBQJob(services, '2', {
      From: 'whatsapp:+13235082016',
      To: 'whatsapp:+995557773417',
      Body: 'restaurants',
      Source: 'whatsapp',
    });

    const result = mockBeeQueueService.mock.calls[0][4];
    expect(result).toEqual(
      `I cant find data about restaurants in my databases, but I am constantly updating so you can ask another time`,
    );
  });

  it('meditation', async () => {
    await mockProcessIncomingBQJob(services, '2', {
      From: 'whatsapp:+13235082016',
      To: 'whatsapp:+995557773417',
      Body: 'JnF',
      Source: 'whatsapp',
    });

    const result = mockBeeQueueService.mock.calls[0][4];
    //meditate
    expect(result).toEqual('Meditation in New York');
  });

  it('delete flight(listing the flights)', async () => {
    await mockProcessIncomingBQJob(services, '2', {
      From: 'whatsapp:+13235082016',
      To: 'whatsapp:+995557773417',
      Body: 'delete_flight',
      Source: 'whatsapp',
    });

    const result = mockBeeQueueService.mock.calls[0][4];
    expect(result).toEqual(
      "I'm currently tracking 2 flight(s) for you:\n\n1. short summary 1\n\n2. short summary 2\n\nPlease confirm the flight you want to stop tracking",
    );
  });

  it('flight_delay', async () => {
    await mockProcessIncomingBQJob(services, '2', {
      From: 'whatsapp:+13235082016',
      To: 'whatsapp:+995557773417',
      Body: 'delay_status',
      Source: 'whatsapp',
    });

    const result = mockBeeQueueService.mock.calls[0][4];
    expect(result).toEqual(
      'Your flight (AA300 to Tel Aviv-Yafo) is 2 minutes late.',
    );
  });
  it('next_flight', async () => {
    // Act
    await mockMain();
    await closeMain();

    expect(mockCreateConnectionSpy).toHaveBeenCalled();

    expect(mockBQonSpy).toHaveBeenCalled();
    expect(mockBQprocessSpy).toHaveBeenCalled();

    const [
      nextFlight,
      flightTimeCheck,
      nextArrivalTime,
      nextDepartureTime,
      allFlights,
      terminal,
      waivers,
    ] = mockGetResponseSpy.mock.results;

    // flight_time_check
    expect(flightTimeCheck.value).toStrictEqual(
      mockGetResponseSpy(0, 0, 'flight_time_check'),
    );

    // next_arrival_time
    expect(nextArrivalTime.value).toStrictEqual(
      mockGetResponseSpy(0, 0, 'next_arrival_time'),
    );
    expect(mockNextArrivalFlight).toBeCalled();

    // next_departure_time
    expect(nextDepartureTime.value).toStrictEqual(
      mockGetResponseSpy(0, 0, 'next_departure_time'),
    );

    expect(mockNextDepartureFlight).toBeCalled();

    // all_flights
    expect(allFlights.value).toStrictEqual(
      mockGetResponseSpy(0, 0, 'all_flights'),
    );

    //terminal
    expect(terminal.value).toStrictEqual(mockGetResponseSpy(0, 0, 'terminal'));
    expect(mockFindByMobile).toBeCalled;
    expect(mockNextFlightByUser).toBeCalled;
    expect(mockGetFlightStatusById).toBeCalled;

    //waivers
    expect(waivers.value).toStrictEqual(mockGetResponseSpy(0, 0, 'waivers'));
    expect(mockFindByMobile).toBeCalled;
    expect(mockNextFlightByUser).toBeCalled;
    //safe to fly to select number
    /*    expect(mockBeeQueueService).toHaveBeenNthCalledWith(
      2,
      {
        on: mockBQonSpy,
        process: mockBQprocessSpy,
      },
      'whatsapp:+13235082016',
      'whatsapp:+995557773417',
      'whatsapp',
      `ENTRY:
airlineborder`,
    );
*/
    expect(mockCreateConnectionSpy).toBeCalledTimes(2);
    // should be called 5 times, and it's actually called 5 times, but fo some
    // reason jest can't see it
  });

  it('confirm messages(2 flights no connection)', async () => {
    mockFindByMobile.mockResolvedValue({
      name: 'time duncan',
      firstName: 'tim',
      lastName: 'duncan',
      mobile: '995557773417',
      muteBot: false,
      confirmed_messaging: false,
    });
    await mockProcessIncomingBQJob(services, '2', {
      From: 'whatsapp:+13235082016',
      To: 'whatsapp:+995557773417',
      Body: 'generic_yes',
      Source: 'whatsapp',
    });

    const tempMessage = `_Outbound:_
Flight AA300, from JFK to TLV on 31 August at 10:45.
_Inbound:_
Flight AA200, from JFK to LHR on 31 August at 10:45.`;
    const splitMessageOnSubjects = mockBeeQueueService.mock.calls[0][4]
      .split('|||')
      .filter(Boolean);
    const result = splitMessageOnSubjects[0];
    const result1 = splitMessageOnSubjects[1];
    expect(result).toEqual(
      Il8nService.get('zenny:user:confirmed1', {
        templateVars: {
          name: 'tim',
        },
      }),
    );
    expect(result1).toEqual(
      Il8nService.get('zenny:user:confirmed2', {
        templateVars: {
          flights_sum: tempMessage,
        },
      }),
    );
  });

  it('confirm messages(2 flights with connection)', async () => {
    mockFindByMobile.mockResolvedValue({
      name: 'time duncan',
      firstName: 'tim',
      lastName: 'duncan',
      mobile: '995557773417',
      muteBot: false,
      confirmed_messaging: false,
    });
    mockTestFlights[0].seq = 0;
    await mockProcessIncomingBQJob(services, '2', {
      From: 'whatsapp:+13235082016',
      To: 'whatsapp:+995557773417',
      Body: 'generic_yes',
      Source: 'whatsapp',
    });

    const tempMessage = `_Outbound:_
Flight AA300, from JFK to TLV on 31 August at 10:45\nwith a connection: Flight AA200, from JFK to LHR on 31 August at 10:45.`;
    const splitMessageOnSubjects = mockBeeQueueService.mock.calls[0][4]
      .split('|||')
      .filter(Boolean);
    const result = splitMessageOnSubjects[0];
    const result1 = splitMessageOnSubjects[1];
    expect(result).toEqual(
      Il8nService.get('zenny:user:confirmed1', {
        templateVars: {
          name: 'tim',
        },
      }),
    );
    expect(result1).toEqual(
      Il8nService.get('zenny:user:confirmed2', {
        templateVars: {
          flights_sum: tempMessage,
        },
      }),
    );
  });

  it('confirm messages(3 flights with connection)', async () => {
    mockFindByMobile.mockResolvedValue({
      name: 'tim duncan',
      firstName: 'tim',
      lastName: 'duncan',
      mobile: '995557773417',
      muteBot: false,
      confirmed_messaging: false,
    });
    mockTestFlights[0].seq = 0;
    mockTestFlights.push(mockTestFlights[1]);
    await mockProcessIncomingBQJob(services, '2', {
      From: 'whatsapp:+13235082016',
      To: 'whatsapp:+995557773417',
      Body: 'generic_yes',
      Source: 'whatsapp',
    });

    const tempMessage = `_Outbound:_
Flight AA300, from JFK to TLV on 31 August at 10:45\nwith a connection: Flight AA200, from JFK to LHR on 31 August at 10:45.\n_Inbound:_\nFlight AA200, from JFK to LHR on 31 August at 10:45.`;
    const splitMessage = mockBeeQueueService.mock.calls[0][4]
      .split('|||')
      .filter(Boolean);
    const result = splitMessage[0];
    const result1 = splitMessage[1];
    expect(result).toEqual(
      Il8nService.get('zenny:user:confirmed1', {
        templateVars: {
          name: 'tim',
        },
      }),
    );
    expect(result1).toEqual(
      Il8nService.get('zenny:user:confirmed2', {
        templateVars: {
          flights_sum: tempMessage,
        },
      }),
    );
    mockTestFlights.pop();
  });

  it('Should not respond because user is muted ', async function () {
    jest.clearAllMocks();
    mockFindByMobile.mockResolvedValueOnce({
      firstName: 'tim',
      lastName: 'duncan',
      mobile: '995557773417',
      muteBot: true,
    });
    await mockMain();
    await closeMain();

    expect(mockCreateConnectionSpy).toHaveBeenCalled();

    expect(mockBQonSpy).toHaveBeenCalled();
    expect(mockBQprocessSpy).toHaveBeenCalled();

    expect(mockFindByMobile).toBeCalled();

    expect(mockNextFlightByUser).toBeCalledTimes(0);
    expect(mockTrackSingleFlight).toBeCalledTimes(0);
    expect(mockGetFlightStatusById).toBeCalledTimes(0);
    expect(mockAllFlightsByUser).toBeCalledTimes(0);
  });
  it('need help', async () => {
    await mockProcessIncomingBQJob(services, '2', {
      From: 'whatsapp:+13235082016',
      To: 'whatsapp:+995557773417',
      Body: 'need_help',
      Source: 'whatsapp',
    });
    expect(postMock).toBeCalledWith(
      ConfigService.get('SLACK_USER_ASSISTANCE'),
      { text: 'tim duncan-995557773417 has just asked for a human to help.' },
    );
    const result = mockBeeQueueService.mock.calls[0][4];
    expect(result).toEqual('fulfilling');
  });

  it('default fallback', async () => {
    await mockProcessIncomingBQJob(services, '2', {
      From: 'whatsapp:+13235082016',
      To: 'whatsapp:+995557773417',
      Body: 'Default Fallback Intent',
      Source: 'whatsapp',
    });
    const result = mockBeeQueueService.mock.calls[0][4];
    expect(result).toContain("Well... it's probably my own Jet Lag");
    expect(postMock).toBeCalledWith(
      ConfigService.get('SLACK_USER_ASSISTANCE'),
      {
        text:
          'tim duncan-995557773417 has failed to get a response from Zenny after 2 interactions.',
      },
    );
  });
  it('default fallback(second message)', async () => {
    mockFindOneComms.mockReset();
    mockFindOneComms.mockResolvedValueOnce({ message: '...Jet Lag...' });
    await mockProcessIncomingBQJob(services, '2', {
      From: 'whatsapp:+13235082016',
      To: 'whatsapp:+995557773417',
      Body: 'Default Fallback Intent',
      Source: 'whatsapp',
    });
    const result = mockBeeQueueService.mock.calls[0][4];
    expect(result).toContain("Sorry, I didn't understand what you asked.");
    expect(postMock).toBeCalledWith(
      ConfigService.get('SLACK_USER_ASSISTANCE'),
      {
        text:
          'tim duncan-995557773417 has failed to get a response from Zenny after 2 interactions.',
      },
    );
  });
  it('welcome pickup airport not supported', async () => {
    mockTestFlights[0].destination = 'LAX';
    await mockProcessIncomingBQJob(services, '2', {
      From: 'whatsapp:+13235082016',
      To: 'whatsapp:+995557773417',
      Body: 'pickup',
      Source: 'whatsapp',
    });
    const result = mockBeeQueueService.mock.calls[0][4];
    expect(result).toEqual(Il8nService.get('zenny:notfound:welcomepickup'));
  });

  it('welcome pickup airport supported', async () => {
    mockTestFlights[0].destination = 'MAD';
    await mockProcessIncomingBQJob(services, '2', {
      From: 'whatsapp:+13235082016',
      To: 'whatsapp:+995557773417',
      Body: 'pickup',
      Source: 'whatsapp',
    });
    const result = mockBeeQueueService.mock.calls[0][4];
    expect(result).toEqual(Il8nService.get('zenny:directions:welcomepickup'));
  });

  it('feedback', async () => {
    await mockProcessIncomingBQJob(services, '2', {
      From: 'whatsapp:+13235082016',
      To: 'whatsapp:+995557773417',
      Body: 'first_feedback',
      Source: 'whatsapp',
    });
    const result = mockBeeQueueService.mock.calls[0][4];
    expect(result).toEqual(
      'What can we do to improve your next travel experience? Please be brutally honest 🙂',
    );
    expect(mockCreateContext).toBeCalled();
    await mockProcessIncomingBQJob(services, '2', {
      From: 'whatsapp:+13235082016',
      To: 'whatsapp:+995557773417',
      Body: 'got_feedback',
      Source: 'whatsapp',
    });
    const result2 = mockBeeQueueService.mock.calls[1][4];
    expect(result2).toEqual('Thanks for your feedback 😎');
  });
});

describe('Helper functions', () => {
  let mainModule: any;
  const testPhoneNumber = '995557773417';
  const WhatsAppPassengerIdentifier =
    'whatsapp:+13235082016_whatsapp:+995557773417';
  const SMSpassengerIdentifier = '+13235082016_+995557773417';

  beforeAll(async () => {
    mainModule = require('../../src/bot-server');
    await mainModule.main();
  });

  it('get passenger from whatsapp', async () => {
    const fakeDB: Connection = new Connection({ type: 'postgres' });

    const result = await mainModule.getPassengerFrom(
      WhatsAppPassengerIdentifier,
      new UserService(fakeDB),
    );

    expect(result.mobile).toEqual(testPhoneNumber);
  });

  it('get passenger from sms', async () => {
    const fakeDB: Connection = new Connection({ type: 'postgres' });
    const result = await mainModule.getPassengerFrom(
      SMSpassengerIdentifier,
      new UserService(fakeDB),
    );

    expect(result.mobile).toEqual(testPhoneNumber);
  });

  it('reset context', async () => {
    mockGetSessionPath.mockReturnValue('Session');
    mockDeleteAllContexts.mockReturnValue('deleted');
    expect(await mainModule.resetContext(WhatsAppPassengerIdentifier)).toEqual(
      Il8nService.get('zenny:success:resetContext'),
    );
  });

  it('reject tracking', async () => {
    mockGetSessionPath.mockReturnValue('Context');
    mockDeleteContext.mockReturnValue('deleted');
    expect(
      await mainModule.rejectTracking(WhatsAppPassengerIdentifier),
    ).toEqual(Il8nService.get('zenny:success:rejectTracking'));
  });
  it('get Terminal without Gate', async () => {
    const flightData = new Flight();
    flightData.flightNo = 'AA';
    flightData.originTerminal = '5';
    flightData.estimatedDeparture = new Date('2020-08-20 06:25:00');
    flightData.id = 1;
    expect(await mainModule.getTerminal(flightData)).toEqual(
      await Il8nService.get('zenny:flightdata:terminal', {
        templateVars: {
          flightno: flightData.flightNo,
          timeuntildeparture: FlightHelper.timeUntilDeparture(flightData),
          terminal: flightData.originTerminal,
          airport: flightData.origin,
        },
      }),
    );
  });
  it('confirm tracking (Already tracked)', async () => {
    // Arrange
    const fakeDB: Connection = new Connection({ type: 'postgres' });
    const context = {
      name: 'found_flight_to_track',
    };

    // Act
    const result = await mainModule.confirmTracking('', [context], {
      userService: new UserService(fakeDB),
      flightService: new FlightService(fakeDB),
    });

    // Assert
    expect(result).toContain('Great news!');
  });

  it('confirm tracking', async () => {
    // Arrange
    mockStructProtoToJson.mockReturnValueOnce({
      flight: {
        flight: {
          number: '300',
          carrier: {
            iata: 'AA',
          },
          origin: {
            iata: 'JFK',
          },
          scheduled_departure: '2020-09-01T10:38:28Z',
        },
      },
      flights: [
        {
          flight: {
            flight: {
              number: '300',
              carrier: {
                iata: 'AA',
              },
              origin: {
                iata: 'JFK',
              },
              scheduled_departure: '2020-09-01T10:38:28Z',
            },
          },
        },
      ],
      number: 1,
    });
    const fakeDB: Connection = new Connection({ type: 'postgres' });
    const context = {
      name: 'found_flight_to_track',
    };
    mockAllFlightsByUser.mockReturnValueOnce(undefined);
    mockGetFlightByData.mockReturnValueOnce(undefined);
    // Act
    const result = await mainModule.confirmTracking('', [context], {
      userService: new UserService(fakeDB),
      flightService: new FlightService(fakeDB),
      trackService: new TrackService(undefined, undefined, undefined),
    });
    mockAllFlightsByUser.mockReturnValueOnce(undefined);

    await mainModule.confirmTracking('', [context], {
      userService: new UserService(fakeDB),
      flightService: new FlightService(fakeDB),
      trackService: new TrackService(undefined, undefined, undefined),
    });
    // Assert
    expect(mockTrackSingleFlight).toHaveBeenCalledTimes(1);
    expect(mockCreateTrip).toHaveBeenCalledTimes(1);
    expect(result).toContain('Done!');
  });

  it('confirm selection (Already tracked)', async () => {
    // Arrange
    const fakeDB: Connection = new Connection({ type: 'postgres' });
    const context = {
      name: 'flight_option_list',
    };

    // Act
    const result = await mainModule.confirmSelection('', [context], {
      userService: new UserService(fakeDB),
      flightService: new FlightService(fakeDB),
    });

    // Assert
    expect(result).toContain('Great news!');
  });
  it('add df params - no date in params', async () => {
    const params = {
      fields: {
        'geo-city': {
          listValue: {
            values: [{ stringValue: 'New York' }, { stringValue: 'Tel aviv' }],
          },
        },
        airport: {
          listValue: {
            values: [{}],
          },
        },
        'flight-number': {
          listValue: {
            values: [{}],
          },
        },
      },
    };
    const message = 'New York to Tel Aviv';
    const expected = {
      fields: {
        'geo-city': {
          listValue: {
            values: [{ stringValue: 'New York' }, { stringValue: 'Tel aviv' }],
          },
        },
        airport: {
          listValue: {
            values: [{}],
          },
        },
        'flight-number': {
          listValue: {
            values: [{}],
          },
        },
        origin: {
          stringValue: 'New York',
        },
      },
    };
    expect(await mainModule.addDFParameters(message, params)).toEqual(expected);
  });
  it('add df params - flight number', async () => {
    mockLumoStatus.mockResolvedValue([
      {
        prediction: {
          delay_index: 1,
        },
        flight: {
          origin: {
            city: 'Tel Aviv',
          },
        },
      },
    ]);

    const emptyArr: any[] = [];
    const params = {
      fields: {
        'geo-city': {
          listValue: {
            values: emptyArr,
          },
        },
        airport: {
          listValue: {
            values: emptyArr,
          },
        },
        'flight-number': {
          listValue: {
            values: [{ stringValue: 'DL235', kind: 'stringValue' }],
          },
        },
      },
    };
    const message = 'Tel Aviv to New York';
    const expected = {
      fields: {
        'geo-city': {
          listValue: {
            values: emptyArr,
          },
        },
        airport: {
          listValue: {
            values: emptyArr,
          },
        },
        'flight-number': {
          listValue: {
            values: [{ stringValue: 'DL235', kind: 'stringValue' }],
          },
        },
        origin: {
          stringValue: 'Tel Aviv',
        },
      },
    };

    const result = await mainModule.addDFParameters(message, params);

    expect(mockLumoStatus).toHaveBeenCalled();
    expect(result).toEqual(expected);
  });

  it('add df params - from in message', async () => {
    const params = {
      fields: {
        'geo-city': {
          listValue: {
            values: [{ stringValue: 'New York' }, { stringValue: 'Tel Aviv' }],
          },
        },
        airport: {
          listValue: {
            values: [{}],
          },
        },
        'flight-number': {
          listValue: {
            values: [{}],
          },
        },
      },
    };
    const message = 'New York from Tel Aviv';
    const expected = {
      fields: {
        'geo-city': {
          listValue: {
            values: [{ stringValue: 'Tel Aviv' }, { stringValue: 'New York' }],
          },
        },
        airport: {
          listValue: {
            values: [{}],
          },
        },
        'flight-number': {
          listValue: {
            values: [{}],
          },
        },
        origin: {
          stringValue: 'Tel Aviv',
        },
      },
    };
    expect(await mainModule.addDFParameters(message, params)).toEqual(expected);
  });

  it('add df params - one airport one city', async () => {
    const params = {
      fields: {
        'geo-city': {
          listValue: {
            values: [{ stringValue: 'New York' }],
          },
        },
        airport: {
          listValue: {
            values: [
              {
                structValue: {
                  fields: {
                    IATA: { stringValue: 'TLV' },
                    ICAO: { stringValue: 'TLV' },
                    city: { stringValue: 'Tel Aviv' },
                    country: { stringValue: 'Israel' },
                    name: { stringValue: 'Ben gurion' },
                  },
                },
              },
            ],
          },
        },
        'flight-number': {
          listValue: {
            values: [{}],
          },
        },
      },
    };
    const message = 'New York from TLV today';
    const expected = {
      fields: {
        'geo-city': {
          listValue: {
            values: [{ stringValue: 'New York' }],
          },
        },
        airport: {
          listValue: {
            values: [
              {
                structValue: {
                  fields: {
                    IATA: { stringValue: 'TLV' },
                    ICAO: { stringValue: 'TLV' },
                    city: { stringValue: 'Tel Aviv' },
                    country: { stringValue: 'Israel' },
                    name: { stringValue: 'Ben gurion' },
                  },
                },
              },
            ],
          },
        },
        'flight-number': {
          listValue: {
            values: [{}],
          },
        },
        origin: {
          stringValue: 'Tel Aviv',
        },
        day: {
          stringValue: 'today',
        },
      },
    };
    expect(await mainModule.addDFParameters(message, params)).toEqual(expected);
  });
  it('get terminal with no terminal', async () => {
    const FSdata = new Flight();
    expect(await mainModule.getTerminal(FSdata)).toEqual(
      Il8nService.get('zenny:notfound:terminal'),
    );
  });

  it('parsedfdate', async () => {
    mockGetDateForCity.mockResolvedValueOnce(
      new Date(
        new Date().toLocaleString('en-US', {
          timeZone: 'America/New_York',
        }),
      ),
    );
    const params = {
      fields: {
        origin: {
          stringValue: 'New York',
        },
        day: {
          stringValue: 'tomorrow',
        },
      },
    };
    const expected = new Date(
      new Date().toLocaleString('en-US', {
        timeZone: 'America/New_York',
      }),
    );
    expected.setDate(expected.getDate() + 1);

    expect(await mainModule.parseDfDates(params)).toHaveProperty(
      'departureDate',
      expected.toISOString().substr(0, 10),
    );
  });
  it('get Terminal with Gate', async () => {
    const flightData = new Flight();
    flightData.flightNo = 'AA';
    flightData.originTerminal = '5';
    flightData.originGate = 'F';
    flightData.estimatedDeparture = new Date('2020-08-20 06:25:00');
    flightData.id = 1;
    expect(await mainModule.getTerminal(flightData)).toEqual(
      await Il8nService.get('zenny:flightdata:terminalgate', {
        templateVars: {
          flightno: flightData.flightNo,
          timeuntildeparture: FlightHelper.timeUntilDeparture(flightData),
          terminal: flightData.originTerminal,
          gate: flightData.originGate,
        },
      }),
    );
  });

  it('get gate with Gate and no terminal', async () => {
    const flightData = new Flight();

    flightData.flightNo = 'AA';
    flightData.originTerminal = null;
    flightData.originGate = '5';
    flightData.estimatedDeparture = new Date('2020-08-20 06:25:00');
    flightData.id = 1;

    const expected =
      `AA will depart from Gate 5 in ` +
      FlightHelper.timeUntilDeparture(flightData) +
      '.';

    expect(await mainModule.getGate(flightData)).toEqual(expected);
  });

  it('get gate with Gate and terminal', async () => {
    const flightData = new Flight();
    flightData.flightNo = 'AA';
    flightData.originTerminal = '5';
    flightData.originGate = 'F';
    flightData.estimatedDeparture = new Date('2020-08-20 06:25:00');
    flightData.id = 1;
    expect(await mainModule.getGate(flightData)).toEqual(
      await Il8nService.get('zenny:flightdata:terminalgate', {
        templateVars: {
          flightno: flightData.flightNo,
          timeuntildeparture: FlightHelper.timeUntilDeparture(flightData),
          terminal: flightData.originTerminal,
          gate: flightData.originGate,
        },
      }),
    );
  });

  it('get gate with terminal and no gate', async () => {
    const flightData = new Flight();
    flightData.flightNo = 'AA';
    flightData.originTerminal = '5';
    flightData.originGate = null;
    flightData.estimatedDeparture = new Date('2020-08-20 06:25:00');
    expect(await mainModule.getGate(flightData)).toEqual(
      await Il8nService.get('zenny:notfound:gate'),
    );
  });
  it('get waivers with no special alerts(no waivers at all)', async () => {
    const lumoData = {} as FlightResponse;
    lumoData.special_alerts = [];
    expect(await mainModule.getWaivers(lumoData)).toEqual(
      Il8nService.get('zenny:notfound:waivers'),
    );
  });
  it('get waivers with special alerts but no waivers', async () => {
    const lumoData = {} as FlightResponse;
    const alert1 = {} as SpecialAlerts;
    const alert2 = {} as SpecialAlerts;
    alert1.alert = {
      type: 'vip_movement',
      summary: 'first',
      description: 'description',
      url: '',
    };
    lumoData.special_alerts = [];
    lumoData.special_alerts.push(alert1);
    expect(await mainModule.getWaivers(lumoData)).toEqual(
      Il8nService.get('zenny:notfound:waivers'),
    );
  });
  it('get waivers with 1 waiver', async () => {
    const lumoData = {} as FlightResponse;
    const alert1 = {} as SpecialAlerts;
    const alert2 = {} as SpecialAlerts;
    alert1.alert = {
      type: 'waiver',
      summary: 'first',
      description: 'description',
      url: '',
    };
    lumoData.special_alerts = [];
    lumoData.special_alerts.push(alert1);
    expect(await mainModule.getWaivers(lumoData)).toEqual(
      alert1.alert.summary + '\n\n',
    );
  });
  it('get waivers with 2 waivers', async () => {
    const lumoData = {} as FlightResponse;
    const alert1 = {} as SpecialAlerts;
    const alert2 = {} as SpecialAlerts;
    alert1.alert = {
      type: 'waiver',
      summary: 'first',
      description: 'description',
      url: '',
    };
    alert2.alert = {
      type: 'waiver',
      summary: 'second',
      description: 'description',
      url: '',
    };
    lumoData.special_alerts = [];
    lumoData.special_alerts.push(alert1);
    lumoData.special_alerts.push(alert2);
    const expected =
      Il8nService.get('zenny:flightdata:waiversopener', {
        templateVars: { number: 2 },
      }) +
      alert1.alert.summary +
      '\n\n' +
      alert2.alert.summary +
      '\n\n';
    expect(await mainModule.getWaivers(lumoData)).toEqual(expected);
  });
  it('delete flights', async () => {
    await mainModule.deleteFlight(1, [1], new FlightService(undefined));
    expect(mockTripRepo.remove).toBeCalled();
    expect(mockTrip).toEqual([]);
  });
  it('baggage claim with no data', async () => {
    const FSdata = new Flight();
    expect(await mainModule.getBaggageClaim(FSdata)).toEqual(
      Il8nService.get('zenny:notfound:baggageclaim'),
    );
  });
  it('baggage claim with  data', async () => {
    const flight = new Flight();
    flight.destinationBaggage = '5';
    expect(await mainModule.getBaggageClaim(flight)).toEqual(
      Il8nService.get('zenny:flightdata:baggageclaim', {
        templateVars: {
          reel: flight.destinationBaggage,
        },
      }),
    );
  });
  it('get risk word', async () => {
    expect(mainModule.getRiskWord(15)).toEqual('low');
    expect(mainModule.getRiskWord(30)).toEqual('moderate');
    expect(mainModule.getRiskWord(60)).toEqual('high');
  });

  it('kelvin to celsius', () => {
    expect(Math.floor(mainModule.kelvinToCelsius(300))).toEqual(26);
  });
  it('kelvin to Fahrenheit', () => {
    expect(Math.floor(mainModule.kelvinToFahrenheit(300))).toEqual(80);
  });

  it('flight stats search by carrier, flight number and airport', async () => {
    const params = {
      fields: {
        'geo-city': {
          listValue: {
            values: [{ stringValue: 'New York' }],
          },
        },
        airport: {
          listValue: {
            values: [
              {
                structValue: {
                  fields: {
                    IATA: { stringValue: 'TLV' },
                    ICAO: { stringValue: 'TLV' },
                    city: { stringValue: 'Tel Aviv' },
                    country: { stringValue: 'Israel' },
                    name: { stringValue: 'Ben gurion' },
                  },
                },
              },
            ],
          },
        },
        date: {
          listValue: {
            values: [
              {
                stringValue: nextDayDate.toString(),
                kind: 'stringValue',
              },
            ],
          },
        },
        'flight-number': {
          listValue: {
            values: [{ stringValue: 'DL235', kind: 'stringValue' }],
          },
        },
      },
    };
    const message = {
      from: 'test:+123123',
      to: 'test:+123123',
      message: 'Can you track DL235 from TLV for today?',
      source: 'test',
    };
    const fakeDb = ({
      getRepository: jest.fn().mockReturnValue({
        save: jest.fn(),
      }),
    } as unknown) as Connection;
    const services = {
      db: fakeDb,
      bq: new BeeQueue('test'),
    };
    const res = await mainModule.processFlightSearchRequest(
      params,
      message,
      services,
      '+123123',
      'Can you track DL235 from TLV for today?',
    );
    expect(mockLumoSearch).toBeCalledTimes(1);
    expect(mockLumoSearch).toBeCalledWith({
      carrier: 'DL',
      flight_number: 235,
      origin: 'TLV',
      date: new Date().toISOString().split('T')[0],
    });
    expect(mockFSSearch).toBeCalledTimes(1);
    expect(mockFSSearch).toBeCalledWith(
      {
        carrier: 'DL',
        airportType: 'dep',
        flight: 235,
        airport: 'TLV',
        date: new Date().toISOString().split('T')[0],
      },
      {
        extendedOptions: ['useInlinedReferences', 'includeNewFields'],
      },
    );
    expect(mockCreateContext).toBeCalled();
    expect(res).toContain('DL 235 is scheduled to depart from New York');
  });

  const emptyArray: any[] = [];

  it('flight stats search by carrier and flight number', async () => {
    jest.clearAllMocks();
    const params = {
      fields: {
        'geo-city': {
          listValue: {
            values: [{ stringValue: 'New York' }],
          },
        },
        airport: {
          listValue: {
            values: emptyArray,
          },
        },
        date: {
          listValue: {
            values: [
              {
                stringValue: nextDayDate.toString(),
                kind: 'stringValue',
              },
            ],
          },
        },
        'flight-number': {
          listValue: {
            values: [{ stringValue: 'BA113', kind: 'stringValue' }],
          },
        },
      },
    };
    const message = {
      from: 'test:+123123',
      to: 'test:+123123',
      message: 'Can you track BA113 from LHR for today?',
      source: 'test',
    };
    const fakeDb = ({
      getRepository: jest.fn().mockReturnValue({
        save: jest.fn(),
      }),
    } as unknown) as Connection;
    const services = {
      db: fakeDb,
      bq: new BeeQueue('test'),
    };
    const res = await mainModule.processFlightSearchRequest(
      params,
      message,
      services,
      '+123123',
      'Can you track BA113 from LHR for today?',
    );
    expect(mockLumoSearch).toBeCalledTimes(1);
    expect(mockLumoSearch).toBeCalledWith({
      carrier: 'BA',
      flight_number: 113,
      origin: '',
      date: new Date().toISOString().split('T')[0],
    });
    expect(mockFSSearch).toBeCalledTimes(1);
    expect(mockFSSearch).toBeCalledWith(
      {
        carrier: 'BA',
        airportType: 'dep',
        flight: 113,
        airport: '',
        date: new Date().toISOString().split('T')[0],
      },
      {
        extendedOptions: ['useInlinedReferences', 'includeNewFields'],
      },
    );
    expect(mockCreateContext).toBeCalled();
    expect(res).toContain(`I've found one flight which matches`);
  });

  it('flight stats search by 2 airports', async () => {
    jest.clearAllMocks();
    const params = {
      fields: {
        'geo-city': {
          listValue: {
            values: emptyArray,
          },
        },
        airport: {
          listValue: {
            values: [
              {
                structValue: {
                  fields: {
                    IATA: { stringValue: 'TLV' },
                    ICAO: { stringValue: 'TLV' },
                    city: { stringValue: 'Tel Aviv' },
                    country: { stringValue: 'Israel' },
                    name: { stringValue: 'Ben gurion' },
                  },
                },
              },
              {
                structValue: {
                  fields: {
                    IATA: { stringValue: 'LHR' },
                    ICAO: { stringValue: 'LHR' },
                    city: { stringValue: 'London' },
                    country: { stringValue: 'USA' },
                    name: { stringValue: 'Heathrow' },
                  },
                },
              },
            ],
          },
        },
        date: {
          listValue: {
            values: [
              {
                stringValue: nextDayDate.toString(),
                kind: 'stringValue',
              },
            ],
          },
        },
        'flight-number': {
          listValue: { values: emptyArray },
          kind: 'listValue',
        },
      },
    };
    const message = {
      from: 'test:+123123',
      to: 'test:+123123',
      message: 'Can you track a flight from TLV to LHR for tomorrow',
      source: 'test',
    };
    const fakeDb = ({
      getRepository: jest.fn().mockReturnValue({
        save: jest.fn(),
      }),
    } as unknown) as Connection;
    const services = {
      db: fakeDb,
      bq: new BeeQueue('test'),
    };
    const res = await mainModule.processFlightSearchRequest(
      params,
      message,
      services,
      '+123123',
      'Can you track a flight from TLV to LHR for tomorrow',
    );
    expect(mockLumoSearch).toBeCalledTimes(1);
    expect(mockLumoSearch).toBeCalledWith({
      destination: 'LHR',
      origin: 'TLV',
      date: new Date().toISOString().split('T')[0],
    });
    expect(mockFSSearch).toBeCalledTimes(1);
    expect(mockFSSearch).toBeCalledWith(
      {
        airportType: 'dep',
        arrivalAirport: 'LHR',
        departureAirport: 'TLV',
        date: new Date().toISOString().split('T')[0],
      },
      {
        extendedOptions: ['useInlinedReferences', 'includeNewFields'],
      },
    );
    expect(mockCreateContext).toBeCalled();
    expect(res).toContain(`I've found one flight which matches`);
  });

  it('flight stats search by 1 city and airport', async () => {
    jest.clearAllMocks();
    const params = {
      fields: {
        'geo-city': {
          listValue: {
            values: [{ stringValue: 'Tel Aviv-Yafo' }],
          },
        },
        airport: {
          listValue: {
            values: [
              {
                structValue: {
                  fields: {
                    IATA: { stringValue: 'LHR' },
                    ICAO: { stringValue: 'LHR' },
                    city: { stringValue: 'New York' },
                    country: { stringValue: 'USA' },
                    name: { stringValue: 'LHR' },
                  },
                },
              },
            ],
          },
        },
        date: {
          listValue: {
            values: [
              {
                stringValue: nextDayDate.toString(),
                kind: 'stringValue',
              },
            ],
          },
        },
        'flight-number': {
          listValue: { values: emptyArray },
          kind: 'listValue',
        },
      },
    };
    const message = {
      from: 'test:+123123',
      to: 'test:+123123',
      message: 'Can you track a flight from LHR to Tel-Aviv for tomorrow',
      source: 'test',
    };
    const fakeDb = ({
      getRepository: jest.fn().mockReturnValue({
        save: jest.fn(),
      }),
    } as unknown) as Connection;
    const services = {
      db: fakeDb,
      bq: new BeeQueue('test'),
    };
    const res = await mainModule.processFlightSearchRequest(
      params,
      message,
      services,
      '+123123',
      'Can you track a flight from LHR to Tel-Aviv for tomorrow',
    );
    expect(mockLumoSearch).toBeCalledTimes(1);
    expect(mockLumoSearch).toBeCalledWith([
      {
        destination: 'TLV',
        origin: 'LHR',
        date: new Date().toISOString().split('T')[0],
      },
    ]);
    expect(mockFSSearch).toBeCalledTimes(1);
    expect(mockFSSearch).toBeCalledWith(
      {
        airportType: 'dep',
        arrivalAirport: 'TLV',
        departureAirport: 'LHR',
        date: new Date().toISOString().split('T')[0],
      },
      {
        extendedOptions: ['useInlinedReferences', 'includeNewFields'],
      },
    );
    expect(mockCreateContext).toBeCalled();
    expect(res).toContain(`I've found one flight which matches`);
  });
});
