import { Connection } from 'typeorm';

jest.mock('bee-queue', () => {
  return jest.fn().mockImplementation(() => ({
    on: () => jest.fn(),
    process: () => jest.fn(),
  }));
});

// const mockLoggerError = jest.fn();
// const mockLoggerInfo = jest.fn();
// const mockLoggerWarn = jest.fn();
// const mockLoggerDebug = jest.fn();

// Supress the Pino notifications
// jest.mock('pino', () => {
//   return jest.fn().mockImplementation(() => ({
//     info: () => mockLoggerInfo,
//     warn: () => mockLoggerWarn,
//     error: () => mockLoggerError,
//     debug: () => mockLoggerDebug,
//   }));
// });

// Stop TypeORM for getting involved
jest.mock('typeorm', () => ({
  getConnectionOptions: jest.fn(),
  createConnection: jest.fn(),
  PrimaryGeneratedColumn: jest.fn(),
  PrimaryColumn: jest.fn(),
  OneToOne: jest.fn(),
  ManyToOne: jest.fn(),
  OneToMany: jest.fn(),
  ManyToMany: jest.fn(),
  JoinTable: jest.fn(),
  UpdateDateColumn: jest.fn(),
  CreateDateColumn: jest.fn(),
  Column: jest.fn(),
  Entity: jest.fn(),
  AfterLoad: jest.fn(),
  JoinColumn: jest.fn(),
}));

let mockDB: Connection;

jest.mock('../../src/service/pushNotifications.service', () => {
  return {
    pushNotificationsService: jest.fn().mockImplementation(() => {
      return {
        chrono: jest.fn(),
      };
    }),
  };
});

// Listen to attempts to write to the database
const mockEventService = jest.fn();

jest.mock('../../src/service/CommsEvent.service', () => {
  return {
    CommsEventService: jest.fn().mockImplementation(() => {
      return {
        addEvent: mockEventService.mockResolvedValue({
          id: 1,
        }),
      };
    }),
  };
});

const mockGmailService = jest.fn();

jest.mock('../../src/service/Gmail.service', () => {
  return {
    GmailService: jest.fn().mockImplementation(() => {
      return { send: mockGmailService };
    }),
  };
});

const mockWhatsAppService = jest.fn();

jest.mock('../../src/service/Twilio.service', () => {
  return {
    TwilioService: jest.fn().mockImplementation(() => {
      return { send: mockWhatsAppService };
    }),
  };
});

import comms from '../../src/comms-server';

const gmailRecord = {
  created: new Date(),
  from: 'me',
  to: 'you',
  message: 'some message',
  source: 'gmail',
  result: 'SUCCESS',
  server: 'localhost',
  subject: 'sub',
};

const whatsappRecord = {
  created: new Date(),
  from: 'me',
  to: 'you',
  message: 'some message',
  source: 'whatsapp',
  result: 'SUCCESS',
  server: 'localhost',
};

describe('Comms Dispatch Server', () => {
  beforeAll(async () => {});
  afterEach(() => {
    jest.clearAllMocks();
  });

  it('add event in db', async () => {
    // Act
    await comms.saveEventHandler(gmailRecord, mockDB);

    // Put params from our spy into an object
    const paramsForSave = mockEventService.mock.calls[0][0];

    // Assert
    expect(mockEventService).toHaveBeenCalled();
    expect(paramsForSave.subject).toEqual(gmailRecord.subject);
  });

  it('whatsapp handler', async () => {
    // Act
    await comms.twilioJobHandler(whatsappRecord);

    // Put params from our spy into an object
    const paramsForWhatsAppSend = mockWhatsAppService.mock.calls[0][0];

    // Assert
    expect(mockWhatsAppService).toHaveBeenCalled();
    expect(paramsForWhatsAppSend.to).toEqual(whatsappRecord.to);
  });

  it('gmail handler', async () => {
    // Act
    await comms.gmailJobHandler(gmailRecord);

    // Put params from our spy into an object
    const paramsForGmailSend = mockGmailService.mock.calls[0][0];

    // Assert
    expect(mockGmailService).toHaveBeenCalled();
    expect(paramsForGmailSend.subject).toEqual(gmailRecord.subject);
  });

  it('gmail error handler', async () => {
    // Mock
    mockGmailService.mockRejectedValue({});

    // Act
    const result = await comms.gmailJobHandler(gmailRecord);

    // Assert
    expect(result).toEqual('FAIL');
  });

  it('processing of a bee-queue job for gmail', async () => {
    const record: any = {
      from: 'me',
      to: 'you',
      message: 'some message',
      source: 'gmail',
      result: 'SUCCESS',
      server: 'localhost',
      subject: 'sub',
    };

    // Act gmail
    await comms.bqProcesshandler(1, record, null);

    // Put params from our spy into an object
    const paramsForGmailSend = mockGmailService.mock.calls[0][0];

    // Assert gmail
    expect(mockGmailService).toHaveBeenCalled();
    expect(paramsForGmailSend.to).toEqual(record.to);
    expect(mockWhatsAppService).not.toHaveBeenCalled();
  });

  it('processing of a bee-queue job for whatsapp', async () => {
    // Act whatsapp
    await comms.bqProcesshandler(1, whatsappRecord, null);

    // Put params from our spy into an object
    const paramsForWhatsAppSend = mockWhatsAppService.mock.calls[0][0];

    // Assert whatsApp
    expect(mockWhatsAppService).toHaveBeenCalled();
    expect(paramsForWhatsAppSend.to).toEqual(whatsappRecord.to);
    expect(mockGmailService).not.toHaveBeenCalled();
  });
});
