jest.mock('../../src/service/Config.service', () => {
  return {
    get: jest.fn((a) => process.env[a]),
  };
});

let mockBQCreateResponseJob = jest.fn();

const fakeUser = {
  id: 1,
  name: 'test',
  mobile: '1333',
  email: 'test@gmail.com',
  muteBot: false,
  flights: [{}],
};

const fakeUserDiffrentPhone = {
  id: 1,
  name: 'test',
  mobile: '13331',
  email: 'test@gmail.com',
  muteBot: false,
  flights: [{}],
};

const fakeUserDiffrentEmail = {
  id: 1,
  name: 'test',
  mobile: '13331',
  email: 'test@gmail.ai',
  muteBot: false,
  flights: [{}],
};

const mockCreateConnection = jest.fn().mockImplementation(() => {
  return {
    getConnectionOptions: jest.fn(),
    getRepository: jest.fn(),
  };
});

const mockTwilioValidatePhoneNumberService = jest.fn();

jest.mock('../../src/service/Twilio.service', () => {
  return {
    TwilioService: jest.fn().mockImplementation(() => {
      return {
        validatePhoneNumberWithTwilio: mockTwilioValidatePhoneNumberService,
      };
    }),
  };
});

const mockFindByMobile = jest.fn().mockReturnValue(fakeUser);
const mockFindByEmail = jest.fn().mockReturnValue(fakeUser);
const mockCheckIfDuplicateEmail = jest
  .fn()
  .mockReturnValue([fakeUserDiffrentPhone]);
const mockUpdateUser = jest.fn().mockReturnValue(fakeUserDiffrentEmail);
const mockMuteBot = jest.fn();
const mockGetUserWithFlights = jest
  .fn()
  .mockReturnValueOnce(fakeUser)
  .mockReturnValueOnce(fakeUser)
  .mockReturnValueOnce(undefined);
const mockProcessLumoSubscription = jest.fn();
const mockProcessAlertSubscription = jest.fn();

const mockSendMessage = jest.fn();
const mockRegisterForFlight = jest.fn(() => {
  return 0;
});
const main = require('../../src/bot-server');
main.sendMessage = mockSendMessage;

jest.mock('../../src/controllers/Flight.controller', () => {
  return jest.fn().mockImplementation(() => ({
    registerForFlight: mockRegisterForFlight,
  }));
});

jest.mock('typeorm', () => ({
  createConnection: mockCreateConnection,
  PrimaryGeneratedColumn: jest.fn(),
  PrimaryColumn: jest.fn(),
  OneToOne: jest.fn(),
  ManyToOne: jest.fn(),
  OneToMany: jest.fn(),
  ManyToMany: jest.fn(),
  JoinTable: jest.fn(),
  UpdateDateColumn: jest.fn(),
  CreateDateColumn: jest.fn(),
  Column: jest.fn(),
  Entity: jest.fn(),
  AfterLoad: jest.fn(),
  JoinColumn: jest.fn(),
}));

jest.mock('bee-queue', () => {
  return jest.fn().mockImplementation(() => ({
    on: jest.fn(),
    process: jest.fn(),
    createJob: jest.fn().mockImplementation(() => ({
      save: jest.fn().mockResolvedValue({}),
    })),
  }));
});

jest.mock('../../src/service/BeeQueue.service', () => {
  return {
    BeeQueueService: jest.fn().mockImplementation(() => ({
      createResponseJob: mockBQCreateResponseJob,
    })),
  };
});

jest.mock('../../src/service/Subscription.service', () => {
  return jest.fn().mockImplementation(() => ({
    processLumoSubscription: mockProcessLumoSubscription,
    processAlertSubscription: mockProcessAlertSubscription,
    getAlertPayload: jest.fn(),
  }));
});

jest.mock('../../src/service/pushNotifications.service', () => {
  return {
    pushNotificationsService: jest.fn().mockImplementation(() => {
      return {
        chrono: jest.fn(),
      };
    }),
  };
});
jest.mock('../../src/service/User.service', () => {
  return {
    UserService: jest.fn().mockImplementation(() => {
      return {
        findByMobile: mockFindByMobile,
        findByEmail: mockFindByEmail,
        muteBot: mockMuteBot,
        getUserWithFlightsByField: mockGetUserWithFlights,
        checkIfDuplicateEmail: mockCheckIfDuplicateEmail,
        updateUser: mockUpdateUser,
      };
    }),
  };
});

jest.mock('../../src/service/Flight.service', () => {
  return {
    FlightService: jest.fn().mockImplementation(() => {
      return {
        getUserFlightsForFrontApp: jest.fn().mockReturnValueOnce([{}]),
      };
    }),
  };
});

const fakeLumoAlert = {
  flight: {
    carrier: {
      iata: 'Test',
    },
    number: 1234,
    scheduled_departure: '2020-10-10',
  },
};

describe('webhook server', () => {
  let app: any;

  beforeAll(async () => {
    const server = require('../../src/webhook-server');
    app = server.app;
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  afterAll(() => {
    app.close();
  });

  it('Twilio endpoint should create a job', async () => {
    const response = await app.inject({
      method: 'POST',
      url: '/twilio',
      payload: {
        test: 1,
      },
    });

    expect(response.statusCode).toBe(200);
    expect(response.payload).toBe(
      '<?xml version="1.0" encoding="UTF-8"?><Response></Response>',
    );
  });

  it('Should send request to mute a bot with an invalid token', async () => {
    process.env.FRONT_PLUGIN_SECRET = '1234';
    const response = await app.inject({
      method: 'PUT',
      url: '/bot/mute/+1333',
      headers: {
        Authorization: 123,
      },
    });
    expect(response.statusCode).toBe(401);
  });

  it('Should send request to mute a bot with a valid token', async () => {
    process.env.FRONT_PLUGIN_SECRET = '123';
    const response = await app.inject({
      method: 'PUT',
      body: JSON.stringify({
        mute: true,
      }),
      url: '/bot/mute/+1333',
      headers: {
        Authorization: 123,
        'Content-Type': 'application/json',
      },
    });
    expect(mockMuteBot).toBeCalled();
    expect(mockMuteBot).toBeCalledWith('1333', true);
    expect(mockFindByMobile).toBeCalled();
    expect(mockFindByMobile).toBeCalledWith('1333');
    expect(response.statusCode).toBe(200);
  });

  it('Should send request to get user with an invalid token', async () => {
    process.env.FRONT_PLUGIN_SECRET = '1234';
    const response = await app.inject({
      method: 'GET',
      url: '/user?phone=+1333',
      headers: {
        Authorization: 123,
      },
    });
    expect(response.statusCode).toBe(401);
  });

  it('Should send request to get user by phone number', async () => {
    process.env.FRONT_PLUGIN_SECRET = '1234';
    const response = await app.inject({
      method: 'GET',
      url: '/user?phone=+1333',
      headers: {
        Authorization: 1234,
      },
    });
    expect(mockFindByMobile).toBeCalledWith('1333');
    expect(response.json()).toEqual(fakeUser);
    expect(response.statusCode).toBe(200);
  });

  it('Should send request to get user by email', async () => {
    process.env.FRONT_PLUGIN_SECRET = '1234';
    const response = await app.inject({
      method: 'GET',
      url: '/user?email=test@gmail.com',
      headers: {
        Authorization: 1234,
      },
    });

    expect(response.json()).toEqual(fakeUser);
    expect(response.statusCode).toBe(200);
  });

  it('Should not find user by phone', async () => {
    mockFindByMobile.mockReturnValueOnce(undefined);
    process.env.FRONT_PLUGIN_SECRET = '1234';
    const response = await app.inject({
      method: 'GET',
      url: '/user?phone=+1333',
      headers: {
        Authorization: 1234,
      },
    });
    expect(mockFindByMobile).toBeCalledWith('1333');
    expect(response.statusCode).toBe(404);
  });

  it('Lumo endpoint should create a lumo record', async () => {
    const response = await app.inject({
      method: 'POST',
      url: '/lumo/webhook',
      payload: {
        alert: fakeLumoAlert,
        flight: {
          flightId: 1,
        },
      },
    });
    expect(mockProcessLumoSubscription).toBeCalledTimes(1);
    expect(response.statusCode).toBe(204);
  });

  it('Sends a welcome message after you register with via the webform', async () => {
    mockTwilioValidatePhoneNumberService.mockReturnValueOnce('972533384241');
    mockFindByMobile.mockReturnValueOnce(fakeUserDiffrentPhone);

    const response = await app.inject({
      method: 'POST',
      url: '/flight/register',
      payload: {
        mobile: '972533384241',
      },
    });

    const params = {
      from: mockBQCreateResponseJob.mock.calls[0][1],
      to: mockBQCreateResponseJob.mock.calls[0][2],
      source: mockBQCreateResponseJob.mock.calls[0][3],
      message: mockBQCreateResponseJob.mock.calls[0][4],
    };

    expect(params.from).toEqual('whatsapp:+' + process.env['PRIMARY_PHONE']);
    expect(params.to).toEqual('whatsapp:+972533384241');
    expect(params.source).toEqual('whatsapp');
  });

  it('Flightstats endpoint should create an alert record', async () => {
    const response = await app.inject({
      method: 'POST',
      url: '/alerts/webhook',
      payload: {
        alert: fakeLumoAlert,
      },
    });
    expect(mockProcessAlertSubscription).toBeCalledTimes(1);
    expect(response.statusCode).toBe(204);
  });

  it('successfull validation phone number before register with via the webform', async () => {
    mockTwilioValidatePhoneNumberService.mockReturnValueOnce('972542444295');
    mockFindByMobile.mockReturnValueOnce(fakeUserDiffrentEmail);
    mockCheckIfDuplicateEmail.mockReturnValueOnce([]);
    const response = await app.inject({
      method: 'POST',
      url: '/register/user',
      payload: {
        firstName: 'uri',
        lastName: 'shein',
        email: 'shein@zenner.com',
        mobile: '972542444295',
        haveWhatsApp: false,
        name: 'uri shein',
      },
    });

    expect(mockTwilioValidatePhoneNumberService).toBeCalledTimes(1);
    expect(response.statusCode).toBe(200);
  });

  it('invalid phone number before register with via the webform', async () => {
    mockTwilioValidatePhoneNumberService.mockReturnValue(null);

    const response = await app.inject({
      method: 'POST',
      url: '/register/user',
      payload: {
        firstName: 'uri',
        lastName: 'shein',
        email: 'shein@zenner.com',
        mobile: '972542444295',
        haveWhatsApp: false,
        name: 'uri shein',
      },
    });

    expect(mockTwilioValidatePhoneNumberService).toBeCalledTimes(1);
    expect(response.statusCode).toBe(400);
    expect(response.payload).toBe('Phone is invalid');
  });

  it('email validation before register with via the webform', async () => {
    mockTwilioValidatePhoneNumberService.mockReturnValueOnce('1333');
    mockFindByMobile.mockReturnValueOnce(fakeUserDiffrentPhone);
    const response = await app.inject({
      method: 'POST',
      url: '/register/user',
      payload: fakeUser,
    });

    expect(response.statusCode).toBe(400);
    expect(response.payload).toBe('Email is already registered in the system');
  });

  it('same phone diffrent email update before register with via the webform', async () => {
    mockTwilioValidatePhoneNumberService.mockReturnValueOnce('1333');
    mockFindByMobile.mockReturnValueOnce(fakeUserDiffrentEmail);
    mockCheckIfDuplicateEmail.mockReturnValueOnce([]);
    const response = await app.inject({
      method: 'POST',
      url: '/register/user',
      payload: fakeUser,
    });

    expect(response.statusCode).toBe(200);
    expect(response.payload).toBe('');
  });
});
