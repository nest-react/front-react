const mockCreateConnection = jest.fn().mockImplementation(() => {
  return {
    getConnectionOptions: jest.fn(),
    getRepository: jest.fn().mockReturnValue({
      save: jest.fn(),
    }),
  };
});

jest.mock('../../src/service/Config.service', () => {
  return {
    get: jest.fn((a) => process.env[a]),
  };
});

jest.mock('../../src/service/WeatherTime.service', () => {
  return {
    WeatherTime: jest.fn().mockImplementation(() => {
      return {
        getTime: jest.fn((a) => '10:00 AM'),
        getWeatherDescription: jest.fn((a) => 'cold'),
        getTemperature: jest.fn().mockReturnValue(303.15),
      };
    }),
  };
});

import LandingHandler from '../../src/service/RulesEngine/handlers/Landing.handler';
import TerminalGateHandler from '../../src/service/RulesEngine/handlers/TerminalGate.handler';
import { Engine, Rule } from 'json-rules-engine';
import path from 'path';
const { promisify } = require('util');
const readdir = promisify(require('fs').readdir);
const fs = require('fs');
const flightDate = new Date();
flightDate.setHours(new Date().getHours() + 1);
const fakeDBFlight = {
  scheduledDeparture: flightDate,
  destinationBaggage: '5',
  destination: 'JFK',
  originGate: '5',
  id: 1,
  statusName: 'enroute',
  trips: [
    {
      sent_landed: false,
      seq: 1,
    },
    {
      sent_landed: false,
      seq: 1,
    },
    {
      sent_landed: false,
      seq: 1,
    },
  ],
};

const engine = new Engine();
let rules: Array<Rule> = [];

const fakeLumoPayload = {
  alert: {},
  flight: {
    flightId: 1,
  },
  status: {
    departure: {
      gate: '6',
    },
    arrival: {
      baggage_claim: '8',
    },
    text: 'Landed',
  },
};

const fakeRuleResult = {
  events: [
    {
      type: 'flight_landed',
    },
  ],
};

const fakeAlert = {
  alert: {
    flightStatus: {
      airportResources: {
        baggage: '18',
        departureGate: '9',
      },
    },
  },
};

const fakeUsers = [
  {
    id: 1,
    mobile: '123123123',
  },
  {
    id: 2,
    mobile: '512371223',
  },
];

const mockProcessLumoSubscription = jest
  .fn()
  .mockReturnValueOnce(fakeDBFlight)
  .mockReturnValueOnce(fakeDBFlight)
  .mockReturnValueOnce(null)
  .mockReturnValue(fakeDBFlight);

const mockProcessAlertSubscription = jest.fn().mockReturnValue(fakeDBFlight);

jest.mock('bee-queue', () => {
  return jest.fn().mockImplementation(() => ({
    on: jest.fn(),
    process: jest.fn(),
    createJob: jest.fn().mockImplementation(() => ({
      save: jest.fn().mockResolvedValue({}),
    })),
  }));
});

jest.mock('typeorm', () => ({
  JoinColumn: jest.fn(),
  createConnection: mockCreateConnection,
  PrimaryGeneratedColumn: jest.fn(),
  PrimaryColumn: jest.fn(),
  OneToOne: jest.fn(),
  ManyToOne: jest.fn(),
  OneToMany: jest.fn(),
  ManyToMany: jest.fn(),
  JoinTable: jest.fn(),
  UpdateDateColumn: jest.fn(),
  CreateDateColumn: jest.fn(),
  Column: jest.fn(),
  Entity: jest.fn(),
  AfterLoad: jest.fn(),
}));

const mockRunRules = jest.fn(
  async (facts): Promise<any> => {
    if (facts.oldFlight && facts.newFlight) {
      return await engine.run(facts);
    }
    return {};
  },
);

const mockGetUsersByFlight = jest.fn().mockReturnValue(fakeUsers);

jest.mock('../../src/service/Subscription.service', () => {
  return jest.fn().mockImplementation(() => ({
    processLumoSubscription: mockProcessLumoSubscription,
    processAlertSubscription: mockProcessAlertSubscription,
    getAlertPayload: jest.fn(),
  }));
});

jest.mock('../../src/service/Flight.service', () => {
  return {
    FlightService: jest.fn().mockImplementation(() => {
      return {
        flightRepository: { update: jest.fn() },
        getFlightById: jest.fn().mockResolvedValue(fakeDBFlight),
        getUsersByFlight: mockGetUsersByFlight,
        updateFlightTrip: jest.fn(),
      };
    }),
  };
});

jest.mock('../../src/service/pushNotifications.service', () => {
  return {
    pushNotificationsService: jest.fn().mockImplementation(() => {
      return {
        chrono: jest.fn(),
      };
    }),
  };
});

jest.mock('../../src/service/RulesEngine/RulesEngine', () => {
  return jest.fn().mockImplementation(() => ({
    runRules: mockRunRules,
  }));
});

describe('Rules engine in the webhook server', () => {
  let app: any;

  beforeAll(async () => {
    let fileNames: Array<string> = [];
    fileNames = await readdir(
      path.join(__dirname, '../../src/resources/rules'),
    );
    await new Promise<void>((resolve, reject) => {
      fileNames.forEach((file, index, array) => {
        const json = fs.readFileSync(
          `${path.join(__dirname, '../../src/resources/rules')}/${file}`,
        );
        const rule = new Rule(JSON.parse(json));
        engine.addRule(rule);
        rules.push(rule);
        if (index === array.length - 1) resolve();
      });
    });
    const server = require('../../src/webhook-server');
    app = server.app;
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  afterAll(() => {
    app.close();
  });

  it('should run landing rules handler and send notifications for 2 users', async () => {
    const sendMessageSpy = jest
      .spyOn(LandingHandler.prototype as any, 'sendMessage')
      .mockImplementation(() => {});
    await app.inject({
      method: 'POST',
      url: '/lumo/webhook',
      payload: fakeLumoPayload,
    });
    expect(mockProcessLumoSubscription).toBeCalled();
    expect(mockRunRules).toBeCalledTimes(1);
    expect(mockGetUsersByFlight).toBeCalled();
    expect(mockGetUsersByFlight).toBeCalledWith(1);
    expect(sendMessageSpy).toHaveBeenNthCalledWith(
      1,
      '+13235082016',
      '+123123123',
      'sms',
      expect.objectContaining({
        beeQueueService: {},
      }),
      'Welcome to John F Kennedy International Airport, New York!\nThe local time is 10:00 AM and the weather is cold with a temperature of 86°F / 30°C.',
    );
  });

  it('should run rules on lumo update', async () => {
    const response = await app.inject({
      method: 'POST',
      url: '/lumo/webhook',
      payload: fakeLumoPayload,
    });
    expect(mockProcessLumoSubscription).toBeCalled();
    expect(mockRunRules.mock.calls[0][0]).toHaveProperty(
      'oldFlight',
      fakeDBFlight,
    );
    expect(mockRunRules.mock.calls[0][0]).toHaveProperty(
      'newFlight',
      fakeLumoPayload,
    );
  });

  it('should run rules on flightstats update', async () => {
    const response = await app.inject({
      method: 'POST',
      url: '/alerts/webhook',
      payload: fakeLumoPayload,
    });
    expect(mockProcessAlertSubscription).toBeCalled();
    expect(mockRunRules.mock.calls[0][0]).toHaveProperty(
      'oldFlight',
      fakeDBFlight,
    );
    expect(mockRunRules.mock.calls[0][0]).toHaveProperty(
      'newFlight',
      fakeLumoPayload,
    );
  });

  it('should not run rules without db record', async () => {
    const response = await app.inject({
      method: 'POST',
      url: '/lumo/webhook',
      payload: fakeLumoPayload,
    });
    expect(mockProcessLumoSubscription).toBeCalled();
    expect(mockRunRules).toBeCalledTimes(0);
  });

  it('should not run landing rules handler if there is no rules', async () => {
    mockRunRules.mockReturnValueOnce(undefined);
    const sendMessageSpy = jest
      .spyOn(LandingHandler.prototype as any, 'sendMessage')
      .mockImplementation(() => {});
    await app.inject({
      method: 'POST',
      url: '/lumo/webhook',
      payload: fakeLumoPayload,
    });
    expect(mockProcessLumoSubscription).toBeCalled();
    expect(mockRunRules).toBeCalledTimes(1);
    expect(mockGetUsersByFlight).toBeCalledTimes(0);
    expect(sendMessageSpy).toBeCalledTimes(0);
  });

  it('should be getting gate rule and send messages', async () => {
    const sendMessageSpy = jest
      .spyOn(TerminalGateHandler.prototype as any, 'sendMessage')
      .mockImplementation(() => {});
    await app.inject({
      method: 'POST',
      url: '/lumo/webhook',
      payload: fakeLumoPayload,
    });
    expect(mockProcessLumoSubscription).toBeCalled();
    expect(mockRunRules).toBeCalledTimes(1);
    expect(mockGetUsersByFlight).toBeCalledTimes(2); //both handlers using this
    expect(sendMessageSpy).toBeCalledTimes(0);
  });

  it('should be getting gate rule and send messages - flight stats', async () => {
    const sendMessageSpy = jest
      .spyOn(TerminalGateHandler.prototype as any, 'sendMessage')
      .mockImplementation(() => {});
    await app.inject({
      method: 'POST',
      url: '/alerts/webhook',
      payload: fakeAlert,
    });
    expect(mockProcessAlertSubscription).toBeCalled();
    expect(mockRunRules).toBeCalledTimes(1);
    expect(mockGetUsersByFlight).toBeCalledTimes(2);
    expect(sendMessageSpy).toBeCalledTimes(2);
  });

  it("shouldn't be getting gate rule and send messages - flight stats", async () => {
    fakeAlert.alert.flightStatus.airportResources.departureGate =
      fakeDBFlight.originGate;
    const sendMessageSpy = jest
      .spyOn(TerminalGateHandler.prototype as any, 'sendMessage')
      .mockImplementation(() => {});
    await app.inject({
      method: 'POST',
      url: '/alerts/webhook',
      payload: fakeAlert,
    });
    expect(mockProcessAlertSubscription).toBeCalled();
    expect(mockRunRules).toBeCalledTimes(1);
    expect(mockGetUsersByFlight).toBeCalledTimes(1);
    expect(sendMessageSpy).toBeCalledTimes(0);
  });

  it('baggage claim - Lumo', async () => {
    fakeLumoPayload.status.departure.gate = fakeDBFlight.originGate;
    fakeLumoPayload.status.arrival.baggage_claim = '7';
    await app.inject({
      method: 'POST',
      url: '/lumo/webhook',
      payload: fakeLumoPayload,
    });
    expect(mockProcessLumoSubscription).toBeCalled();
    expect(mockRunRules).toBeCalledTimes(1);
    expect(mockGetUsersByFlight).toBeCalledTimes(2);
  });
  it('baggage claim - FlightStats', async () => {
    fakeAlert.alert.flightStatus.airportResources.departureGate =
      fakeDBFlight.originGate;
    fakeAlert.alert.flightStatus.airportResources.baggage = '7';
    await app.inject({
      method: 'POST',
      url: '/alerts/webhook',
      payload: fakeAlert,
    });
    expect(mockProcessAlertSubscription).toBeCalled();
    expect(mockRunRules).toBeCalledTimes(1);
    expect(mockGetUsersByFlight).toBeCalledTimes(1);
  });
});
