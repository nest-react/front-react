import twillliosms from '../../../src/interface/twiliosms.interface';
import { Connection } from 'typeorm';
import { ExternalServices } from '../../../src/interface/bot-server.interface';
const { struct } = require('pb-util');
jest.mock('bee-queue', () => {
  return jest.fn().mockImplementation(() => ({
    on: () => jest.fn(),
    process: () => jest.fn(),
  }));
});

jest.mock('../../../src/service/pushNotifications.service', () => {
  return {
    pushNotificationsService: jest.fn().mockImplementation(() => {
      return {
        chrono: jest.fn(),
      };
    }),
  };
});

jest.mock('typeorm', () => ({
  getConnectionOptions: jest.fn(),
  getRepository: jest.fn(),
  createConnection: jest.fn(),
  PrimaryGeneratedColumn: () => {},
  UpdateDateColumn: () => {},
  CreateDateColumn: () => {},
  Column: () => {},
  Entity: () => {},
}));

let mockDB: Connection;

const mockLoggerInfo = jest.fn();
const mockLoggerWarn = jest.fn();
const mockLoggerDebug = jest.fn();
const mockLoggerError = jest.fn();

// Supress the Pino notifications
// jest.mock('pino', () => {
//   return jest.fn().mockImplementation(() => ({
//     info: () => mockLoggerInfo,
//     warn: () => mockLoggerWarn,
//     error: () => mockLoggerError,
//     debug: () => mockLoggerDebug,
//   }));
// });

describe('Search for a flight', () => {
  // const mockLoggerInfo = jest.fn(() => 1);
  // const mockLoggerWarn = jest.fn(() => 1);
  // const mockLoggerError = jest.fn(() => 1);
  // const mockLoggerDebug = jest.fn(() => 1);

  const mockDialogFlow = jest.fn();
  const beeQueueSpy = jest.fn();
  const mockLumoSpy = jest.fn();

  beforeAll(() => {
    // jest.mock('pino', () => {
    //   return jest.fn().mockImplementation(() => ({
    //     info: () => mockLoggerInfo,
    //     warn: () => mockLoggerWarn,
    //     error: () => mockLoggerError,
    //     debug: () => mockLoggerDebug,
    //   }));
    // });

    jest.mock('../../../src/service/User.service', () => {
      return {
        UserService: jest.fn().mockImplementation(() => {
          return {
            findByMobile: jest.fn().mockResolvedValue({
              firstName: 'tim',
              lastName: 'duncan',
              mobile: '995557773417',
              muteBot: false,
            }),
          };
        }),
      };
    });
    //mocking the flightstats service so the code will not crash
    jest.mock('../../../src/service/FlightStats.service', () => {
      return {
        FlightStatsService: jest.fn().mockImplementation(() => {
          return {
            getFlightStatusById: jest.fn(),
          };
        }),
      };
    });

    jest.mock('../../../src/service/Flight.service', () => {
      return {
        FlightService: jest.fn().mockImplementation(() => {
          return {
            getNextFlightByUser: jest.fn(),
            getAllFlightsByUser: jest.fn(),
            getNextArrivalFlight: jest.fn(),
            getNextDepartureFlight: jest.fn(),
          };
        }),
      };
    });

    const mockEventService = jest.fn();

    jest.mock('../../../src/service/CommsEvent.service', () => {
      return {
        CommsEventService: jest.fn().mockImplementation(() => {
          return {
            addEvent: mockEventService.mockResolvedValue({
              id: 1,
            }),
          };
        }),
      };
    });

    jest.mock('../../../src/service/ApiAudit.service', () => {
      return {
        ApiAuditService: jest.fn().mockImplementation(() => {
          return {
            log: jest.fn(),
          };
        }),
      };
    });

    jest.mock('../../../src/service/Lumo.service', () => {
      return {
        LumoService: jest.fn().mockImplementation(() => {
          return {
            search: mockLumoSpy,
            subscribe: mockLumoSpy,
          };
        }),
      };
    });

    jest.mock('../../../src/service/FlightStats.service', () => {
      return {
        FlightStatsService: jest.fn().mockImplementation(),
      };
    });

    jest.mock('../../../src/service/Dialogflow.service', () => {
      return {
        DialogflowService: jest.fn().mockImplementation(() => {
          return {
            getResponse: mockDialogFlow,
            getSessionPath: jest
              .fn()
              .mockImplementation(
                () => 'whatsapp:+123456789_whatsapp:+987654321',
              ),
            createContext: jest.fn().mockImplementation(),
            contextClient: {
              createContext: jest.fn(),
            },
          };
        }),
      };
    });
  });

  afterEach(() => {
    // jest.resetAllMocks();
  });

  it('searches for a flight using the Trifecta', async () => {
    const bq = {
      createJob: beeQueueSpy.mockReturnValue({
        save: jest.fn(),
      }),
    };

    const mockServices: ExternalServices = {
      db: mockDB,
      bq: <any>bq,
    };

    const data: twillliosms = {
      From: 'whatsapp:+123456789',
      To: 'whatsapp:+987654321',
      Body: 'Can you track BA113 from LHR ?',
    };

    mockLumoSpy.mockResolvedValueOnce({
      count: 1,
      results: [
        {
          flight: {
            carrier: {
              name: 'British Airways',
              iata: 'BA',
            },
            origin: {
              name: 'Heathrow',
            },
            destination: {
              name: 'John F Kenenedy',
            },
            scheduled_departure: '2030-05-10T11:00:00+0',
            scheduled_arrival: '2030-05-10T14:00:00+0',
          },
          prediction: {
            delay_index: 3,
          },
        },
      ],
    });

    mockDialogFlow.mockResolvedValueOnce({
      outputContexts: [],
      intent: {
        displayName: 'slot_filled_new_flight',
      },
      parameters: struct.encode({
        date: '2030-05-10',
        'flight-number': ['BA113'],
        'geo-city': ['Heathrow'],
        airport: [
          {
            IATA: 'LHR',
            ICAO: 'EGLL',
            name: 'Heathrow',
            country: 'UK',
            city: 'Heathrow',
          },
        ],
        city: 'Heathrow',
      }),
    });
    const main = require('../../../src/bot-server');
    await main.processIncomingBQJob(mockServices, '1', data);
    const replyPleaseWait = beeQueueSpy.mock.calls[0][0];
    const replySearchResult = beeQueueSpy.mock.calls[1][0];

    expect(beeQueueSpy).toBeCalledTimes(2);
    expect(replyPleaseWait.message).toEqual(
      `Give me a second while I search for this flight.`,
    );
    expect(replySearchResult.message).toContain(`11:00 AM`);
    expect(replySearchResult.message).toContain(`2:00 PM`);
  });

  it('searches for a flight using the flight number and departure date', async () => {
    const bq = {
      createJob: beeQueueSpy.mockReturnValue({
        save: jest.fn(),
      }),
    };

    const mockServices: ExternalServices = {
      db: mockDB,
      bq: <any>bq,
    };

    const data: twillliosms = {
      From: 'whatsapp:+123456789',
      To: 'whatsapp:+987654321',
      Body: 'Can you track BA113 ?',
    };

    mockLumoSpy.mockResolvedValueOnce({
      count: 1,
      results: [
        {
          flight: {
            carrier: {
              name: 'British Airways',
              iata: 'BA',
            },
            origin: {
              name: 'Heathrow',
              iata: 'LHR',
            },
            destination: {
              name: 'John F Kenenedy',
              iata: 'TEST',
            },
            scheduled_departure: '2030-09-01',
            scheduled_arrival: '2030-09-01',
          },
          prediction: {
            delay_index: 3,
          },
        },
      ],
    });

    mockDialogFlow.mockResolvedValueOnce({
      outputContexts: [],
      intent: {
        displayName: 'slot_filled_new_flight',
      },
      parameters: struct.encode({
        date: '2030-05-10',
        'flight-number': ['BA113'],
        'geo-city': ['Heathrow'],
        airport: [],
        city: 'Heathrow',
      }),
    });

    const main = require('../../../src/bot-server');
    await main.processIncomingBQJob(mockServices, '2', data);
    const reply = beeQueueSpy.mock.calls[2][0];
    expect(beeQueueSpy).toBeCalledTimes(3);
    expect(reply.message).toContain(`I've found one flight`);
    expect(reply.message).toContain(`LHR > TEST`);
    expect(reply.message).toContain(`30% chance of delay`);
  });

  it('searches for a flights using the 2 airports', async () => {
    const bq = {
      createJob: beeQueueSpy.mockReturnValue({
        save: jest.fn(),
      }),
    };

    const mockServices: ExternalServices = {
      db: mockDB,
      bq: <any>bq,
    };

    const data: twillliosms = {
      From: 'whatsapp:+123456789',
      To: 'whatsapp:+987654321',
      Body: 'Can you track from LHR to UKH ?',
    };

    mockLumoSpy.mockResolvedValueOnce({
      count: 2,
      results: [
        {
          flight: {
            carrier: {
              name: 'British Airways',
              iata: 'BA',
            },
            origin: {
              name: 'Heathrow',
              iata: 'LHR',
            },
            destination: {
              name: 'John F Kenenedy',
              iata: 'TEST',
            },
            scheduled_departure: '2030-09-01',
            scheduled_arrival: '2030-09-01',
          },
          prediction: {
            delay_index: 3,
          },
        },
        {
          flight: {
            carrier: {
              name: 'Test carrier',
              iata: 'BA',
            },
            origin: {
              name: 'Heathrow',
              iata: 'LHR',
            },
            destination: {
              name: 'John F Kenenedy',
              iata: 'TEST',
            },
            scheduled_departure: '2030-09-01',
            scheduled_arrival: '2030-09-01',
          },
          prediction: {
            delay_index: 3,
          },
        },
      ],
    });

    mockDialogFlow.mockResolvedValueOnce({
      outputContexts: [],
      intent: {
        displayName: 'slot_filled_new_flight',
      },
      parameters: struct.encode({
        date: '2030-08-29',
        'geo-city': ['Heathrow'],
        'flight-number': [],
        airport: [
          {
            IATA: 'LHR',
            ICAO: 'EGLL',
            name: 'Heathrow',
            country: 'UK',
            city: 'Heathrow',
          },
          {
            city: 'Mukhaizna',
            IATA: 'UKH',
            ICAO: 'UKH',
            name: 'Mukhaizna Airport',
            country: 'OM',
          },
        ],
        city: 'Heathrow',
      }),
    });

    const main = require('../../../src/bot-server');

    await main.processIncomingBQJob(mockServices, '3', data);
    const reply = beeQueueSpy.mock.calls[3][0];
    expect(beeQueueSpy).toBeCalledTimes(4);
    expect(reply.message).toContain(`I found 2 flights on September 29th`);
    expect(reply.message).toContain(`# 1: British BA`);
    expect(reply.message).toContain(`There is currently a 30% chance of delay`);
    expect(reply.message).toContain(
      `Which flight # [1-2] would you like me to track for you?`,
    );
  });

  it('searches for a flights using the 2 cities', async () => {
    const bq = {
      createJob: beeQueueSpy.mockReturnValue({
        save: jest.fn(),
      }),
    };

    const mockServices: ExternalServices = {
      db: mockDB,
      bq: <any>bq,
    };

    const data: twillliosms = {
      From: 'whatsapp:+123456789',
      To: 'whatsapp:+987654321',
      Body: 'Can you track from Budapest to Kharkiv ?',
    };

    mockLumoSpy.mockResolvedValueOnce({
      count: 2,
      results: [
        {
          flight: {
            carrier: {
              name: 'British Airways',
              iata: 'BA',
            },
            origin: {
              name: 'Heathrow',
              iata: 'LHR',
            },
            destination: {
              name: 'John F Kenenedy',
              iata: 'TEST',
            },
            scheduled_departure: '2030-09-01',
            scheduled_arrival: '2030-09-01',
          },
          prediction: {
            delay_index: 3,
          },
        },
        {
          flight: {
            carrier: {
              name: 'Test carrier',
              iata: 'BA',
            },
            origin: {
              name: 'Heathrow',
              iata: 'LHR',
            },
            destination: {
              name: 'John F Kenenedy',
              iata: 'TEST',
            },
            scheduled_departure: '2030-09-01',
            scheduled_arrival: '2030-09-01',
          },
          prediction: {
            delay_index: 3,
          },
        },
      ],
    });

    mockDialogFlow.mockResolvedValueOnce({
      outputContexts: [],
      intent: {
        displayName: 'slot_filled_new_flight',
      },
      parameters: struct.encode({
        date: '2030-08-29',
        'geo-city': ['Budapest', 'Kharkiv'],
        'flight-number': [],
        airport: [],
        city: 'Heathrow',
      }),
    });

    const main = require('../../../src/bot-server');

    await main.processIncomingBQJob(mockServices, '4', data);
    const reply = beeQueueSpy.mock.calls[4][0];
    expect(beeQueueSpy).toBeCalledTimes(5);
    expect(reply.message).toContain(`I found 2 flights on September 29th`);
    expect(reply.message).toContain(`# 1: British BA`);
    expect(reply.message).toContain(`There is currently a 30% chance of delay`);
    expect(reply.message).toContain(
      `Which flight # [1-2] would you like me to track for you?`,
    );
  });

  it('searches for a flights using the 1 city and airport', async () => {
    const bq = {
      createJob: beeQueueSpy.mockReturnValue({
        save: jest.fn(),
      }),
    };

    const mockServices: ExternalServices = {
      db: mockDB,
      bq: <any>bq,
    };

    const data: twillliosms = {
      From: 'whatsapp:+123456789',
      To: 'whatsapp:+987654321',
      Body: 'Can you track from BUD to Kharkiv ?',
    };

    mockLumoSpy.mockResolvedValueOnce({
      count: 2,
      results: [
        {
          flight: {
            carrier: {
              name: 'British Airways',
              iata: 'BA',
            },
            origin: {
              name: 'Heathrow',
              iata: 'LHR',
            },
            destination: {
              name: 'John F Kenenedy',
              iata: 'TEST',
            },
            scheduled_departure: '2030-09-01',
            scheduled_arrival: '2030-09-01',
          },
          prediction: {
            delay_index: 3,
          },
        },
        {
          flight: {
            carrier: {
              name: 'Test carrier',
              iata: 'BA',
            },
            origin: {
              name: 'Heathrow',
              iata: 'LHR',
            },
            destination: {
              name: 'John F Kenenedy',
              iata: 'TEST',
            },
            scheduled_departure: '2030-09-01',
            scheduled_arrival: '2030-09-01',
          },
          prediction: {
            delay_index: 3,
          },
        },
      ],
    });

    mockDialogFlow.mockResolvedValueOnce({
      intent: {
        displayName: 'slot_filled_new_flight',
      },
      queryText: 'Can you track from BUD to Kharkiv ?',
      outputContexts: [],
      parameters: struct.encode({
        date: '2030-08-29',
        'geo-city': ['Kharkiv'],
        'flight-number': [],
        airport: [
          {
            IATA: 'BUD',
            ICAO: 'BUD',
            name: 'Budapest',
            country: 'HU',
            city: 'Budapest',
          },
        ],
        city: 'Budapest',
      }),
    });

    const main = require('../../../src/bot-server');
    await main.processIncomingBQJob(mockServices, '5', data);
    const reply = beeQueueSpy.mock.calls[5][0];
    expect(beeQueueSpy).toBeCalledTimes(6);
    expect(reply.message).toContain(`I found 2 flights on September 29th`);
    expect(reply.message).toContain(`# 1: British BA`);
    expect(reply.message).toContain(`There is currently a 30% chance of delay`);
    expect(reply.message).toContain(
      `Which flight # [1-2] would you like me to track for you?`,
    );
  });

  it('should not find a flights', async () => {
    const bq = {
      createJob: beeQueueSpy.mockReturnValue({
        save: jest.fn(),
      }),
    };

    const mockServices: ExternalServices = {
      db: mockDB,
      bq: <any>bq,
    };

    const data: twillliosms = {
      From: 'whatsapp:+123456789',
      To: 'whatsapp:+987654321',
      Body: 'Can you track BA113 ?',
    };

    mockLumoSpy.mockResolvedValueOnce({
      count: 0,
      results: [],
    });

    mockDialogFlow.mockResolvedValueOnce({
      outputContexts: [],

      intent: {
        displayName: 'slot_filled_new_flight',
      },
      parameters: struct.encode({
        date: '2030-05-10',
        'flight-number': ['BA113'],
        'geo-city': ['Heathrow'],
        airport: [],
        city: 'Heathrow',
      }),
    });

    const main = require('../../../src/bot-server');
    await main.processIncomingBQJob(mockServices, '6', data);
    const reply = beeQueueSpy.mock.calls[6][0];
    expect(beeQueueSpy).toBeCalledTimes(7);
    expect(reply.message).toEqual(
      `Sorry, I couldn't find any flights for that route on May 10th`,
    );
  });
  it('interactive flight search', async () => {
    const bq = {
      createJob: beeQueueSpy.mockReturnValue({
        save: jest.fn(),
      }),
    };

    const mockServices: ExternalServices = {
      db: mockDB,
      bq: <any>bq,
    };

    const data: twillliosms = {
      From: 'whatsapp:+123456789',
      To: 'whatsapp:+987654321',
      Body: 'Today',
    };

    mockLumoSpy.mockResolvedValueOnce({
      count: 2,
      results: [
        {
          flight: {
            carrier: {
              name: 'British Airways',
              iata: 'BA',
            },
            origin: {
              name: 'Heathrow',
              iata: 'LHR',
            },
            destination: {
              name: 'John F Kenenedy',
              iata: 'TEST',
            },
            scheduled_departure: '2030-09-01',
            scheduled_arrival: '2030-09-01',
          },
          prediction: {
            delay_index: 3,
          },
        },
        {
          flight: {
            carrier: {
              name: 'Test carrier',
              iata: 'BA',
            },
            origin: {
              name: 'Heathrow',
              iata: 'LHR',
            },
            destination: {
              name: 'John F Kenenedy',
              iata: 'TEST',
            },
            scheduled_departure: '2030-09-01',
            scheduled_arrival: '2030-09-01',
          },
          prediction: {
            delay_index: 3,
          },
        },
      ],
    });

    mockDialogFlow.mockResolvedValueOnce({
      intent: {
        displayName: 'interactive_date',
      },
      parameters: struct.encode({
        date: '2030-08-29',
        city: 'Heathrow',
      }),
      outputContexts: [
        {
          name: 'bla bla departure-followup',
          parameters: {
            fields: {
              departure: {
                stringValue: 'Heathrow',
              },
            },
          },
        },
        {
          name: 'bla bla arrival-followup',
          parameters: {
            fields: {
              arrival: {
                stringValue: 'New York',
              },
            },
          },
        },
      ],
    });

    const main = require('../../../src/bot-server');

    await main.processIncomingBQJob(mockServices, '7', data);
    const reply = beeQueueSpy.mock.calls[7][0];
    expect(reply.message).toContain(`I found 2 flights on September 29th`);
    expect(reply.message).toContain(`# 1: British BA`);
    expect(reply.message).toContain(`There is currently a 30% chance of delay`);
    expect(reply.message).toContain(
      `Which flight # [1-2] would you like me to track for you?`,
    );
  });
});
