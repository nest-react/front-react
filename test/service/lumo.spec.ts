import { Connection } from 'typeorm';

import axios from 'axios';
import qs from 'qs';

import { LumoService } from '../../src/service';
import { SearchOptions } from '../../src/interface/lumo/lumoSearch.interface';
import { ValidateRequest } from '../../src/interface/lumo';
import { LumoSubscription } from '../../src/model/LumoSubscription';

const falseDb = ({
  getRepository: jest.fn(),
} as unknown) as Connection;

const lumoService = new LumoService(falseDb);

describe('Lumo service', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  it('Request flight status with flight ID', async () => {
    const axiosGetSpy = jest.spyOn(axios, 'get').mockResolvedValue({});
    const flightID = 'testID';

    await lumoService.status(flightID);

    expect(axiosGetSpy).toHaveBeenCalledWith(`status/${flightID}`);
  });

  it('Request flight status without specific flight ID ', async () => {
    const axiosGetSpy = jest.spyOn(axios, 'get').mockResolvedValue({});

    const options = {
      carrier: '7W',
      date: '2020-06-09',
      flight_number: 163,
    };

    await lumoService.status(options);

    expect(axiosGetSpy).toHaveBeenCalledWith(`status?${qs.stringify(options)}`);
  });

  it('Request list of alternative flights with a flight ID ', async () => {
    const axiosGetSpy = jest.spyOn(axios, 'get').mockResolvedValue({});

    const flightID = 'testID';

    await lumoService.alternates(flightID);

    expect(axiosGetSpy).toHaveBeenCalledWith(`alternates/${flightID}`);
  });

  it('Retrieve flights filtered by a set of criteria', async () => {
    const axiosGetSpy = jest.spyOn(axios, 'get').mockResolvedValue({
      data: {
        results: [],
        count: 2,
      },
    });

    const options = {
      origin: 'KBP',
      date: '2020-06-09',
    };

    await lumoService.search(options);
    expect(axiosGetSpy).toBeCalled();

    expect(axiosGetSpy).toHaveBeenCalledWith(`search?${qs.stringify(options)}`);
  });

  it('Retrieve metropolitan basic flights from LON to NYC ', async () => {
    const axiosGetSpy = jest.spyOn(axios, 'get').mockResolvedValue({
      data: {
        results: [],
        count: 2,
      },
    });

    const options: SearchOptions = {
      origin: 'LON',
      destination: 'NYC',
      date: '2020-06-09',
    };

    const results = await lumoService.search(options);
    // mock request return 2 flights from each direction
    // 28 directions * 2 flight = 56 flights in total
    expect(results.count).toBe(56);

    // should request 28 times!
    // LON -> area consist of 7 airports
    // NYC -> area consist of 4 airports
    // 7 original possible airport, 4 possible destination
    // 28 combination in total
    expect(axiosGetSpy).toBeCalledTimes(28);
  });

  it('Retrieve flights from LON (metropolitan ) to JFK (single airport) ', async () => {
    const axiosGetSpy = jest.spyOn(axios, 'get').mockResolvedValue({
      data: {
        results: [],
        count: 2,
      },
    });

    const options: SearchOptions = {
      origin: 'LON',
      destination: 'JFK',
      date: '2020-06-09',
    };

    await lumoService.search(options);

    // should request 7 times!
    // LON -> area consist of 7 airports
    // JFK -> just 1 destination
    // 7 original possible airport, 1 possible destination
    // 7 combination in total
    expect(axiosGetSpy).toBeCalledTimes(7);
  });

  it('Retrieve flights from JFK (single airport ) to OSA (metropolitan) ', async () => {
    const axiosGetSpy = jest.spyOn(axios, 'get').mockResolvedValue({
      data: {
        results: [],
        count: 2,
      },
    });

    const options: SearchOptions = {
      origin: 'JFK',
      destination: 'OSA',
      date: '2020-06-09',
    };

    await lumoService.search(options);

    // OSA -> area consist of 3 airports
    // JFK -> just 1 destination
    // 1 original possible airport, 3 possible destination
    // 3 combination in total
    expect(axiosGetSpy).toBeCalledTimes(3);
  });

  it('Retrieve flights from OSA as destination (metropolitan) ', async () => {
    const axiosGetSpy = jest.spyOn(axios, 'get').mockResolvedValue({
      data: {
        results: [],
        count: 2,
      },
    });

    const options: SearchOptions = {
      destination: 'OSA',
      date: '2020-06-09',
    };

    await lumoService.search(options);

    // OSA -> area consist of 3 airports
    // 3 combination in total
    expect(axiosGetSpy).toBeCalledTimes(3);
  });

  it('Retrieve flights from NYC as original (metropolitan) ', async () => {
    const axiosGetSpy = jest.spyOn(axios, 'get').mockResolvedValue({
      data: {
        results: [],
        count: 2,
      },
    });

    const options: SearchOptions = {
      origin: 'NYC',
      date: '2020-06-09',
    };

    await lumoService.search(options);

    // NYC -> area consist of 4 airports
    // 4 combination in total
    expect(axiosGetSpy).toBeCalledTimes(4);
  });

  it('Retrieve connection risk with full flight ID', async () => {
    const axiosGetSpy = jest.spyOn(axios, 'get').mockResolvedValue({});

    const from = 'flightIDFrom';
    const to = 'flightIDTo';
    await lumoService.connection(from, to);

    expect(axiosGetSpy).toHaveBeenCalledWith(
      `connection?flights=${from},${to}`,
    );
  });

  it('Retrieve connection risk with short flight ID ', async () => {
    const axiosGetSpy = jest
      .spyOn(axios, 'get')
      .mockResolvedValueOnce({
        data: {
          count: 1,
          results: [
            {
              flight: {
                id: 'originFlightID2020201234',
                scheduled_arrival: '2020-07-23T22:30:00+00:00',
                scheduled_departure: '2020-07-23T22:30:00+00:00',
              },
            },
          ],
        },
      })
      .mockResolvedValueOnce({
        data: {
          count: 1,
          results: [
            {
              flight: {
                id: 'destinationFlightID2020201234',
                scheduled_arrival: '2020-07-23T22:30:00+00:00',
                scheduled_departure: '2020-07-24T22:30:00+00:00',
              },
            },
          ],
        },
      })
      .mockResolvedValueOnce({});

    await lumoService.connection('U9460', 'TK11', '2020-07-22');
    const [
      originRequest,
      destinationRequest,
      riskRequest,
    ] = axiosGetSpy.mock.calls;

    expect(axiosGetSpy).toBeCalledTimes(3);

    expect(originRequest[0]).toBe(
      'search?carrier=U9&flight_number=460&date=2020-07-22',
    );
    expect(decodeURIComponent(destinationRequest[0])).toBe(
      'search?carrier=TK&flight_number=11&departing_after=2020-07-23T22:30:00Z&date=2020-07-22',
    );
    expect(riskRequest[0]).toBe(
      'connection?flights=originFlightID200723,destinationFlightID200724',
    );
  });

  it('Should return true if we already track flight id', async () => {
    const flightId = 1;
    const fakeFindOne = jest.fn().mockResolvedValue({});
    const axiosPostSpy = jest.spyOn(axios, 'post');

    const falseDb = ({
      getRepository: jest.fn().mockImplementation(() => {
        return {
          findOne: fakeFindOne,
        };
      }),
    } as unknown) as Connection;

    const service = new LumoService(falseDb);

    const status = await service.subscription(flightId);
    expect(status).toBeTruthy();
    expect(axiosPostSpy).toBeCalledTimes(0);
  });

  it('Should create a lumo subscription and save it to db', async () => {
    const flightId = 1;
    const fakeFindOne = jest.fn().mockResolvedValue(undefined);
    const fakeSave = jest.fn().mockResolvedValue({});

    const axiosGetSpy = jest.spyOn(axios, 'post').mockResolvedValue({
      data: {
        itinerary_id: 'lumoID',
      },
    });

    const falseDb = ({
      getRepository: jest.fn().mockImplementation(() => {
        return {
          findOne: fakeFindOne,
          save: fakeSave,
          findByIds: jest.fn().mockReturnValue([]),
        };
      }),
    } as unknown) as Connection;

    const service = new LumoService(falseDb);

    const status = await service.subscription(flightId);
    expect(status).toBeTruthy();
    expect(axiosGetSpy).toBeCalled();

    expect(fakeSave).toBeCalled();

    expect(fakeSave).toBeCalledWith(
      new LumoSubscription({
        flightId,
        subscriptionId: 'lumoID',
      }),
    );
  });

  it('Should create a lumo subscriptions for the flight ids array and save it to db', async () => {
    const flightIds = [1, 2];
    const fakeFindOne = jest.fn().mockResolvedValue(undefined);
    const fakeFind = jest.fn().mockResolvedValue([]);
    const fakeSave = jest.fn().mockResolvedValue({});
    const axiosPostSpy = jest.spyOn(axios, 'post').mockResolvedValue({
      data: {
        itinerary_id: 'lumoID',
      },
    });
    const falseDb = ({
      getRepository: jest.fn().mockImplementation(() => {
        return {
          findOne: fakeFindOne,
          findByIds: jest.fn().mockReturnValue([]),
          find: fakeFind,
          save: fakeSave,
        };
      }),
    } as unknown) as Connection;

    const service = new LumoService(falseDb);

    const status = await service.subscription(flightIds);
    expect(status).toBeTruthy();
    expect(axiosPostSpy).toBeCalledTimes(1);
    expect(fakeFind).toBeCalledTimes(1);
    expect(fakeSave).toBeCalledTimes(2);
  });

  it('Should not create subscription for multiple flights if we already track them', async () => {
    const flightIds = [1, 2];
    const fakeFind = jest.fn().mockResolvedValue([
      {
        flightId: 1,
      },
      {
        flightId: 2,
      },
    ]);
    const fakeSave = jest.fn().mockResolvedValue({});
    const axiosPostSpy = jest.spyOn(axios, 'post').mockResolvedValue({
      data: {
        itinerary_id: 'lumoID',
      },
    });
    const falseDb = ({
      getRepository: jest.fn().mockImplementation(() => {
        return {
          find: fakeFind,
          save: fakeSave,
        };
      }),
    } as unknown) as Connection;

    const service = new LumoService(falseDb);

    const status = await service.subscription(flightIds);
    expect(status).toBeTruthy();
    expect(axiosPostSpy).toBeCalledTimes(0);
    expect(fakeFind).toBeCalledTimes(1);
    expect(fakeSave).toBeCalledTimes(0);
  });

  it("Should create subscriptions only for those that we doesn't track", async () => {
    const flightIds = [1, 2];
    const fakeFind = jest.fn().mockResolvedValue([
      {
        flightId: 1,
      },
    ]);
    const fakeSave = jest.fn().mockResolvedValue({});
    const axiosPostSpy = jest.spyOn(axios, 'post').mockResolvedValue({
      data: {
        itinerary_id: 'lumoID',
      },
    });
    const falseDb = ({
      getRepository: jest.fn().mockImplementation(() => {
        return {
          find: fakeFind,
          save: fakeSave,
          findByIds: jest.fn().mockReturnValue([]),
        };
      }),
    } as unknown) as Connection;

    const service = new LumoService(falseDb);

    const status = await service.subscription(flightIds);
    expect(status).toBeTruthy();
    expect(axiosPostSpy).toBeCalledTimes(1);
    expect(fakeFind).toBeCalledTimes(1);
    expect(fakeSave).toBeCalledTimes(1);
  });

  it('Should create subscription by flight options', async () => {
    const server = 'url';
    const ids = [1, 2];
    const options = [
      {
        carrier: 'FR',
        flight_number: 8556,
        date: '2020-07-10',
      },
      {
        carrier: 'DY',
        flight_number: 8556,
        date: '2020-07-10',
      },
    ];
    const fakeFind = jest.fn().mockResolvedValue([]);
    const fakeSave = jest.fn().mockResolvedValue({});
    const axiosPostSpy = jest.spyOn(axios, 'post').mockResolvedValue({
      data: {
        itinerary_id: 'lumoID',
      },
    });
    const falseDb = ({
      getRepository: jest.fn().mockImplementation(() => {
        return {
          find: fakeFind,
          save: fakeSave,
        };
      }),
    } as unknown) as Connection;

    const service = new LumoService(falseDb);

    const status = await service.subscribeByStatusOptions(options, server, ids);

    expect(status).toBeTruthy();
    expect(axiosPostSpy).toBeCalledTimes(1);
    expect(fakeFind).toBeCalledTimes(1);
    expect(fakeSave).toBeCalledTimes(2);
  });

  it('Should not create subscription by flight options because we already track it', async () => {
    const server = 'url';
    const id = 1;
    const options = [
      {
        carrier: 'FR',
        flight_number: 8556,
        date: '2020-07-10',
      },
    ];
    const fakeFind = jest.fn().mockResolvedValue([
      {
        flightId: 1,
      },
    ]);
    const fakeSave = jest.fn().mockResolvedValue({});
    const axiosPostSpy = jest.spyOn(axios, 'post').mockResolvedValue({
      data: {
        itinerary_id: 'lumoID',
      },
    });
    const falseDb = ({
      getRepository: jest.fn().mockImplementation(() => {
        return {
          find: fakeFind,
          findOne: fakeFind,
          save: fakeSave,
        };
      }),
    } as unknown) as Connection;

    const service = new LumoService(falseDb);

    const status = await service.subscribeByStatusOptions(options, server, id);

    expect(status).toBeTruthy();
    expect(axiosPostSpy).toBeCalledTimes(0);
    expect(fakeFind).toBeCalledTimes(1);
    expect(fakeSave).toBeCalledTimes(0);
  });

  it('Should create 1 subscription because we already track one of them', async () => {
    const server = 'url';
    const ids = [1, 2];
    const options = [
      {
        carrier: 'FR',
        flight_number: 8556,
        date: '2020-07-10',
      },
      {
        carrier: 'DY',
        flight_number: 8556,
        date: '2020-07-10',
      },
    ];
    const fakeFind = jest.fn().mockResolvedValue([
      {
        flightId: 1,
      },
    ]);
    const fakeSave = jest.fn().mockResolvedValue({});
    const axiosPostSpy = jest.spyOn(axios, 'post').mockResolvedValue({
      data: {
        itinerary_id: 'lumoID',
      },
    });
    const falseDb = ({
      getRepository: jest.fn().mockImplementation(() => {
        return {
          find: fakeFind,
          save: fakeSave,
        };
      }),
    } as unknown) as Connection;

    const service = new LumoService(falseDb);

    const status = await service.subscribeByStatusOptions(options, server, ids);

    expect(status).toBeTruthy();
    expect(axiosPostSpy).toBeCalledTimes(1);
    expect(fakeFind).toBeCalledTimes(1);
    expect(fakeSave).toBeCalledTimes(1);
  });

  it('Should validate flight', async () => {
    const axiosGetSpy = jest.spyOn(axios, 'get').mockResolvedValue({
      data: [{}],
    });

    const falseDb = ({
      getRepository: jest.fn(),
    } as unknown) as Connection;

    const service = new LumoService(falseDb);

    const options: ValidateRequest = {
      carrier: 'PS',
      flightNumber: 123,
      date: '2020-07-22',
    };

    const isValid = await service.validateFlights(options);

    expect(axiosGetSpy).toBeCalled();

    expect(isValid).toBeTruthy();
  });

  it('Does date math for finding connections', () => {
    const falseDb = ({
      getRepository: jest.fn(),
    } as unknown) as Connection;

    const service = new LumoService(falseDb);

    expect(service.getFutureDate('2020-01-01', 1)).toBe('2020-01-02');
    expect(service.getFutureDate('2019-12-31', 2)).toBe('2020-01-02');

    expect(() => {
      service.getFutureDate('2019-31-40', 2);
    }).toThrowError();
  });
});
