import { UserService } from '../../src/service/User.service';
import { Connection, Repository } from 'typeorm';
import User from '../../src/model/User';

const testData = {
  email: 'nino.g@webiz.ge',
  mobile: '1111',
  id: 1,
};

const mockRepository = new Repository<User>();

const mockUserTobeReturned = new User();
mockUserTobeReturned.email = testData.email;
mockUserTobeReturned.mobile = testData.mobile;
mockUserTobeReturned.id = testData.id;

mockRepository.findOne = jest.fn(
  async (): Promise<User> => {
    let res: User;
    await new Promise<void>((resolve) => {
      res = mockUserTobeReturned;
      resolve();
    });
    return res;
  },
);

let falseDb: Connection;

const mockUserService = new UserService(falseDb);
mockUserService.userRepository = mockRepository;

const mockfindByEmailSpy = jest.fn(mockUserService.findByEmail);
mockUserService.findByEmail = mockfindByEmailSpy;
const mockfindByMobileSpy = jest.fn(mockUserService.findByMobile);
mockUserService.findByMobile = mockfindByMobileSpy;
const mockfindByIdSpy = jest.fn(mockUserService.findById);
mockUserService.findById = mockfindByIdSpy;

describe('UserService Unit Test', () => {
  it('Should return user by Email', async () => {
    //Act

    const res = await mockUserService.findByEmail(testData.email);
    //Assert
    expect(mockfindByEmailSpy).toBeCalled();
    expect(res).toBe(mockUserTobeReturned);
  });

  it('Should return User by Mobile', async () => {
    //Act
    const res = await mockUserService.findByMobile(testData.mobile);
    //Assert
    expect(mockfindByMobileSpy).toHaveBeenCalled();
    expect(res).toBe(mockUserTobeReturned);
  });

  it('Should return User by Id', async () => {
    //Act
    const res = await mockUserService.findById(testData.id);
    //Assert
    expect(mockfindByIdSpy).toHaveBeenCalled();
    expect(res).toBe(mockUserTobeReturned);
  });
});
