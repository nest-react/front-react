import AirportService, {
  AirportType,
  sortByAirportSize,
} from '../../src/service/Airport.service';

describe('Airport service', () => {
  it('Should find airport by city name', () => {
    const cityName = 'London';
    const results = AirportService.findByCityName(cityName, 5);
    expect(results).toHaveLength(5);
  });

  it('Should find metropolitan airport  code', () => {
    const cityName = 'New York';
    const results = AirportService.findByCityName(cityName, 5);
    expect(results).toHaveLength(1);
    expect(results[0].iata_code).toBe('NYC');
  });

  it('Sort function should sort by airport size', () => {
    const list: AirportType[] = [
      {
        type: 'large_airport',
      },
      {
        type: 'medium_airport',
      },
      {
        type: 'small_airport',
      },
      {
        type: 'large_airport',
      },
      {
        type: 'large_airport',
      },
    ];

    list.sort(sortByAirportSize);

    expect(list[0].type).toBe('large_airport');
    expect(list[1].type).toBe('large_airport');
    expect(list[2].type).toBe('large_airport');
    expect(list[3].type).toBe('medium_airport');
    expect(list[4].type).toBe('small_airport');
  });
});
