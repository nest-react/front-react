import { TwilioService } from '../../src/service/Twilio.service';
let whatsAppService: TwilioService;

const mockCreateSpy = jest.fn();

const testData = {
  from: 'whatsapp:13235082016',
  to: 'whatsapp:995591978104',
  body: 'Greetings from whatsapp',
};

const testDataLongMessage = {
  from: 'whatsapp:13235082016',
  to: 'whatsapp:995591978104',
  body: `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas id efficitur magna, at posuere nibh. Proin justo diam, vestibulum vitae urna vel, dictum iaculis risus. Vivamus quis feugiat tellus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Fusce sodales sapien velit, a egestas lorem ornare eu. Phasellus commodo arcu nisi, eget tincidunt lectus porttitor nec. Mauris rhoncus eu purus nec posuere. Sed velit velit, feugiat ac lacinia in, porttitor eu felis.

  Sed venenatis sodales orci, vel auctor dolor vehicula in. Fusce vel erat finibus, hendrerit sem vitae, imperdiet magna. Nunc lacinia ultrices metus, at rhoncus risus elementum nec. Vestibulum et mauris vel diam facilisis accumsan. Proin quis aliquet eros. Maecenas maximus pharetra eros a pulvinar. Nam at massa eget tellus volutpat aliquet sit amet et sapien. Vestibulum tempus metus arcu, non condimentum arcu cursus eget. Fusce quis lacus et dolor iaculis imperdiet vel in metus.
  
  Curabitur at nisi tempor, egestas nisl in, placerat ligula. Vivamus ultrices eleifend purus, id interdum urna. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Morbi aliquet, metus vel venenatis commodo, nulla ipsum ullamcorper magna, vel vestibulum libero odio luctus tortor. Donec ultricies lacus ligula, nec tristique nibh ultricies sed. Praesent at justo vel enim aliquam semper. Quisque a felis quis nunc eleifend sodales nec a ante. Nulla urna dolor, posuere et consequat quis, aliquam laoreet libero. Mauris eget eros sodales, lobortis tellus in, lobortis donec.`,
};

const testDataLongMessageWithSubjects = {
  from: 'whatsapp:13235082016',
  to: 'whatsapp:995591978104',
  body: `First subject: this is the body and will not be split 
  |||Second subject: Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas id efficitur magna, at posuere nibh. Proin justo diam, vestibulum vitae urna vel, dictum iaculis risus. Vivamus quis feugiat tellus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Fusce sodales sapien velit, a egestas lorem ornare eu. Phasellus commodo arcu nisi, eget tincidunt lectus porttitor nec. Mauris rhoncus eu purus nec posuere. Sed velit velit, feugiat ac lacinia in, porttitor eu felis.

   Sed venenatis sodales orci, vel auctor dolor vehicula in. Fusce vel erat finibus, hendrerit sem vitae, imperdiet magna. Nunc lacinia ultrices metus, at rhoncus risus elementum nec. Vestibulum et mauris vel diam facilisis accumsan. Proin quis aliquet eros. Maecenas maximus pharetra eros a pulvinar. Nam at massa eget tellus volutpat aliquet sit amet et sapien. Vestibulum tempus metus arcu, non condimentum arcu cursus eget. Fusce quis lacus et dolor iaculis imperdiet vel in metus.
  
  Third Curabitur at nisi tempor, egestas nisl in, placerat ligula. Vivamus ultrices eleifend purus, id interdum urna. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Morbi aliquet, metus vel venenatis commodo, nulla ipsum ullamcorper magna, vel vestibulum libero odio luctus tortor. Donec ultricies lacus ligula, nec tristique nibh ultricies sed. Praesent at justo vel enim aliquam semper. Quisque a felis quis nunc eleifend sodales nec a ante. Nulla urna dolor, posuere et consequat quis, aliquam laoreet libero. Mauris eget eros sodales, lobortis tellus in, lobortis donec.
  
  |||Third subject: this is the body and will not be split 
  
  |||Fourth subject: this is the body and will not be split `,
};

describe('WhatsApp (Twilio) unit tests', () => {
  beforeEach(() => {
    jest.mock('twilio', () => () => ({
      messages: {
        create: mockCreateSpy,
      },
    }));
    const { TwilioService } = require('../../src/service/Twilio.service');
    whatsAppService = new TwilioService();

    mockCreateSpy.mockResolvedValue({ testData });
  });

  beforeAll(() => {});

  afterEach(() => {
    mockCreateSpy.mockClear();
    jest.resetModules();
  });

  it('Check number is formatted correctly', async () => {
    // Act
    await whatsAppService.send(testData);

    // Assert
    expect(mockCreateSpy).toHaveBeenCalled();
    expect(mockCreateSpy.mock.calls[0][0]).toEqual(testData);
  });

  it('Switches number for staging', async () => {
    // Act
    await whatsAppService.send(testData);

    // Assert
    expect(mockCreateSpy).toHaveBeenCalled();
    expect(mockCreateSpy.mock.calls[0][0]).toEqual(testData);
  });

  it('Split a long message', async () => {
    // Act
    await whatsAppService.send(testDataLongMessage);

    // Assert

    expect(mockCreateSpy).toHaveBeenCalledTimes(2);
    expect(mockCreateSpy.mock.calls[0][0].body.length).toBeLessThanOrEqual(
      1500,
    );
  });
});

it('Split a long message with subjects', async () => {
  // Act
  await whatsAppService.send(testDataLongMessageWithSubjects);

  // Assert

  expect(mockCreateSpy).toHaveBeenCalledTimes(5);
  expect(mockCreateSpy.mock.calls[0][0].body.length).toBeLessThanOrEqual(1500);
});
