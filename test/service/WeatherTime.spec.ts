jest.mock('../../src/service/Config.service', () => {
  return {
    get: jest.fn((a) => process.env[a]),
  };
});
import axios from 'axios';

import { WeatherTime } from '../../src/service/WeatherTime.service';

const openWeatherService = new WeatherTime();

describe('Openweather service', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should receive temperature when given city', async () => {
    const axiosGetSpy = jest.spyOn(axios, 'get').mockResolvedValue({
      data: {
        weather: [{}],
        main: {
          temp: {},
        },
      },
    });

    await openWeatherService.getTemperature('ashdod');

    expect(axiosGetSpy).toBeCalled();

    expect(axiosGetSpy).toBeCalledWith(
      'https://api.openweathermap.org/data/2.5/weather?appid=undefined&q=ashdod',
    );
  });

  it('should receive weather description when given city', async () => {
    const axiosGetSpy = jest.spyOn(axios, 'get').mockResolvedValue({
      data: {
        weather: [{}],
      },
    });

    await openWeatherService.getWeatherDescription('ashdod');

    expect(axiosGetSpy).toBeCalled();

    expect(axiosGetSpy).toBeCalledWith(
      'https://api.openweathermap.org/data/2.5/weather?appid=undefined&q=ashdod',
    );
  });

  it('Should recieve local time with given city', async () => {
    const axiosGetSpy = jest.spyOn(axios, 'get').mockResolvedValue({
      data: {
        results: [
          {
            geometry: {
              location: {
                lat: {},
                lng: {},
              },
            },
          },
        ],
      },
    });

    await openWeatherService.getTime('ashdod');

    expect(axiosGetSpy).toBeCalledTimes(2);
  });

  it('Get date of city', async () => {
    const axiosGetSpy = jest
      .spyOn(axios, 'get')
      .mockResolvedValueOnce({
        data: {
          results: [
            {
              geometry: {
                location: {
                  lat: {},
                  lng: {},
                },
              },
            },
          ],
        },
      })
      .mockResolvedValueOnce({ data: { timeZoneId: 'America/New_York' } });

    expect(await openWeatherService.getDateForCity('new York')).toEqual(
      new Date(
        new Date().toLocaleString('en-US', {
          timeZone: 'America/New_York',
        }),
      ),
    );
    expect(axiosGetSpy).toBeCalledTimes(2);
  });
});
