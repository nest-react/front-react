import axios from 'axios';
import SpyInstance = jest.SpyInstance;
import { Contact } from '../../../src/service/front/Contact';

describe('FrontContact', () => {
  let contact: Contact;
  let axiosSpy: SpyInstance;

  beforeEach(() => {
    contact = new Contact();
    axiosSpy = jest.spyOn(axios, 'request').mockResolvedValue({
      data: {},
    });
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('Update contact by email', async () => {
    const email = 'email';
    const data = {};
    const alias = contact.createAlias({
      key: 'email',
      value: email,
    });
    const response = await contact.updateByEmail(email, data);
    expect(axiosSpy).toBeCalled();
    expect(axiosSpy).toHaveBeenCalledWith({
      method: 'PATCH',
      url: `contacts/${alias}`,
      data,
    });
    expect(response.error).toBeNull();
  });

  it('Update contact by phone', async () => {
    const phone = 'phone';
    const data = {};
    const alias = contact.createAlias({
      key: 'phone',
      value: phone,
    });
    const response = await contact.updateByPhone(phone, data);
    expect(axiosSpy).toBeCalled();
    expect(axiosSpy).toHaveBeenCalledWith({
      method: 'PATCH',
      url: `contacts/${alias}`,
      data,
    });
    expect(response.error).toBeNull();
  });
});
