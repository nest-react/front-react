import axios from 'axios';
import SpyInstance = jest.SpyInstance;
import { FrontBase } from '../../../src/service/front/FrontBase';

class FrontBaseMock extends FrontBase<any> {
  protected readonly TARGET: string = 'entities';
}

describe('FrontBase', () => {
  let frontBase: FrontBase<any>;
  let axiosSpy: SpyInstance;

  beforeEach(() => {
    frontBase = new FrontBaseMock();
    axiosSpy = jest.spyOn(axios, 'request').mockResolvedValue({
      data: {},
    });
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('Find all entities', async () => {
    const response = await frontBase.findAll();
    expect(axiosSpy).toBeCalled();
    expect(axiosSpy).toHaveBeenCalledWith({
      method: 'GET',
      url: 'entities',
    });
    expect(response.error).toBeNull();
  });

  it('Find entity by id', async () => {
    const id = 'id';
    const response = await frontBase.findById(id);
    expect(axiosSpy).toBeCalled();
    expect(axiosSpy).toHaveBeenCalledWith({
      method: 'GET',
      url: `entities/${id}`,
    });
    expect(response.error).toBeNull();
  });

  it('Create entity', async () => {
    const data = {};
    const response = await frontBase.create(data);
    expect(axiosSpy).toBeCalled();
    expect(axiosSpy).toHaveBeenCalledWith({
      method: 'POST',
      url: 'entities',
      data,
    });
    expect(response.error).toBeNull();
  });

  it('Update entity by id', async () => {
    const id = 'id';
    const data = {};
    const response = await frontBase.update(id, data);
    expect(axiosSpy).toBeCalled();
    expect(axiosSpy).toHaveBeenCalledWith({
      method: 'PATCH',
      url: `entities/${id}`,
      data,
    });
    expect(response.error).toBeNull();
  });

  it('Get all entities with exception', async () => {
    axiosSpy = jest.spyOn(axios, 'request').mockRejectedValue({
      response: {
        data: {
          _error: '',
        },
      },
    });
    const response = await frontBase.findAll();
    expect(axiosSpy).toBeCalled();
    expect(response.success).toBeFalsy();
  });

  it('Create alias', () => {
    const response = frontBase.createAlias({
      key: 'email',
      value: '',
    });
    expect(typeof response).toBe('string');
  });
});
