const BeeQueue = require('bee-queue');
const { BeeQueueService } = require('../../src/service/BeeQueue.service');

const mockBQService = new BeeQueueService();
mockBQService.createResponseJob = jest.fn(mockBQService.createResponseJob);

interface MockJob {
  from: string;
  to: string;
  source: string;
  message: string;
  save: () => void;
}

const mocksaveJobSpy = jest.fn();
const mockCreateJobSpy = jest.fn(
  (a): MockJob => ({
    from: a.from,
    to: a.to,
    source: a.source,
    message: a.message,
    save: mocksaveJobSpy,
  }),
);
const bqt = new BeeQueue('some');
bqt.createJob = mockCreateJobSpy;

const testData = {
  from: 'whatsapp:13235082016',
  to: 'whatsapp:995591978104',
  source: 'whatsapp',
  message: 'Greetings from whatsapp',
};

describe('BeeQueue unit tests', () => {
  afterAll(() => {
    bqt.close();
  });

  it('Check createResponceJob', async () => {
    // Act
    await mockBQService.createResponseJob(
      bqt,
      testData.from,
      testData.to,
      testData.source,
      testData.message,
    );

    // Assert
    expect(mockBQService.createResponseJob).toHaveBeenCalled();
    expect(mockCreateJobSpy).toHaveBeenCalled();

    expect(mockCreateJobSpy).toHaveReturnedWith({
      ...testData,
      save: mocksaveJobSpy,
    });
    expect(mocksaveJobSpy).toHaveBeenCalled();
  });
});
