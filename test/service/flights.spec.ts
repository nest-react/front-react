import { FlightService } from '../../src/service/Flight.service';
import { Connection, Repository } from 'typeorm';
import Flight from '../../src/model/Flight';
import User from '../../src/model/User';
import { UserTrip } from '../../src/model/UserTrip';
import { Trip } from '../../src/model/Trip';
import { FlightResponse } from '../../src/interface/lumo';
import { FlightHelper } from '../../src/helpers/flight.helper';

const fakeLumoRecord: FlightResponse = {
  alert: {
    change: 'Some change',
    itinerary_id: '1234',
    timestamp: new Date().toISOString(),
  },
  flight: {
    aircraft_type: {
      generic: 'Fl',
    },
    carrier: {
      iata: 'BA',
      name: 'British Airlines',
    },
    destination: {
      iata: 'JFK',
      name: 'JFK',
      city: 'New York',
    },
    id: 'BB113',
    number: 113,
    origin: {
      iata: 'LHR',
      city: 'London',
      name: 'LHR',
    },
    uuid: '123123',
    scheduled_departure: new Date().toISOString(),
    scheduled_arrival: new Date().toISOString(),
  },
  status: {
    cancelled: false,
    text: 'Scheduled',
    arrival: {
      scheduled: new Date().toISOString(),
      latest: 'abc',
      type: 'scheduled',
    },
    departure: {
      scheduled: new Date().toISOString(),
      latest: 'abc',
      type: 'scheduled',
    },
    source: 'flightview',
    last_updated: new Date().toISOString(),
  },
  prediction: {
    delay_index: 1,
    risk: 'LOW',
    distribution: [1],
    causes: ['flight-cancelled'],
    is_override: false,
    connections: [
      {
        airport: 'LHR',
        prob_miss: 1,
        connection_time: 2,
      },
    ],
  },
};
const testFlight1 = new Flight();
testFlight1.id = 12;
testFlight1.departureDate = new Date('11.02.2020');
testFlight1.scheduledArrivalDateLocal = '12.02.2020';
testFlight1.estimatedDeparture = new Date('11.02.2020');

const testFlight2 = new Flight();
testFlight2.id = 13;
testFlight2.departureDate = new Date('12.02.2020');
testFlight2.scheduledArrivalDateLocal = '11.02.2020';
testFlight2.estimatedDeparture = new Date('12.02.2020');

const testFlight3 = new Flight();
testFlight3.id = 14;
testFlight3.departureDate = new Date('10.02.2020');
testFlight3.scheduledArrivalDateLocal = '10.02.2020';
testFlight3.isOutOfDate = true;
testFlight3.estimatedDeparture = new Date('10.02.2020');

const testUserTrip = new UserTrip({});
testUserTrip.trip_id = 1;
testUserTrip.user_id = 2;

const testTrip = new Trip({});
testTrip.flight = testFlight1;
testTrip.trip_id = 1;
testTrip.seq = 0;
testTrip.flight_trip_id = 0;
testFlight1.trips = [testTrip];

const testTrip1 = new Trip({});
testTrip1.flight = testFlight2;
testTrip1.trip_id = 1;
testTrip1.seq = 0;
testTrip1.flight_trip_id = 0;

const mockrepositoryUserTrip = new Repository<UserTrip>();
const mockrepositoryTrip = new Repository<Trip>();
const mockrepositoryUser = new Repository<User>();
const mockrepositoryFlight = new Repository<Flight>();

mockrepositoryUser.findOne = jest.fn(
  async (record: any): Promise<User> => {
    const result = new User();
    result.firestoreId = record.where.id;
    return result;
  },
);

mockrepositoryUser.find = jest.fn(
  async (record: any): Promise<User[]> => {
    const result = new User();
    result.firestoreId = record.where.id;
    return [result];
  },
);
mockrepositoryUser.findByIds = jest.fn(async (ids: any[]) => {
  return await mockrepositoryUser.find({ where: { id: ids[0] } });
});

mockrepositoryFlight.find = jest.fn(
  async (record: any): Promise<Flight[]> => {
    const flightIds = record.where.id._value;
    let flights = [];
    for (let i = 0; i < flightIds.length; i++) {
      if (flightIds[i] === testFlight1.id) {
        flights.push(testFlight1);
      }
      if (flightIds[i] === testFlight2.id) {
        flights.push(testFlight2);
      }
      if (flightIds[i] === testFlight3.id) {
        flights.push(testFlight3);
      }
    }
    return flights;
  },
);

mockrepositoryFlight.findOne = jest.fn(
  async (record: any) =>
    (
      await mockrepositoryFlight.find({
        where: { id: { _value: [record.where.id] } },
      })
    )[0],
);
mockrepositoryUserTrip.find = jest.fn(
  async (record: any): Promise<UserTrip[]> => {
    if (record.where.user_id == 2) {
      return [testUserTrip];
    }
    return [];
  },
);

mockrepositoryTrip.find = jest.fn(
  async (record: any): Promise<Trip[]> => {
    const tripIds = record.where.trip_id._value;
    if (tripIds.length === 1 && tripIds[0] === 1) {
      return [testTrip, testTrip1];
    }
    return [];
  },
);
let falseDb: Connection;
const mockFlightService = new FlightService(falseDb);
mockFlightService.userRepository = mockrepositoryUser;
mockFlightService.flightRepository = mockrepositoryFlight;
mockFlightService.userTripRepository = mockrepositoryUserTrip;
mockFlightService.tripRepository = mockrepositoryTrip;

const mockGetAllFlightsByUserSpy = jest.fn(
  mockFlightService.getAllFlightsByUser,
);
mockFlightService.getAllFlightsByUser = mockGetAllFlightsByUserSpy;

const mockGetNextFlightByUserSpy = jest.fn(
  mockFlightService.getNextFlightByUser,
);
mockFlightService.getNextFlightByUser = mockGetNextFlightByUserSpy;

const mockGetPassengerIdForUserSpy = jest.fn(
  mockFlightService.getPassengerIdForUser,
);
mockFlightService.getPassengerIdForUser = mockGetPassengerIdForUserSpy;

describe('FlightService', () => {
  it('getPassengerIdForUser', async () => {
    const res = await mockFlightService.getPassengerIdForUser(0);

    //expect(mockrepositoryUser.findOne).toBeCalled();
    //old firebase user
    expect(res).toBe(0);
  });

  it('getAllFlightsByUser --- no flights', async () => {
    const res = await mockFlightService.getAllFlightsByUser(0);

    expect(mockGetPassengerIdForUserSpy).toBeCalled();
    expect(mockGetAllFlightsByUserSpy).toBeCalled();
    expect(res[0]).toBe(undefined);
  });

  it('getAllFlightsByUser --- 2 flights', async () => {
    const res = await mockFlightService.getAllFlightsByUser(2);

    expect(mockGetAllFlightsByUserSpy).toBeCalled();
    expect(res).toHaveLength(2);
    expect(res[0]).toBe(testFlight1);
    expect(res[1]).toBe(testFlight2);
  });

  it('getNextFlightByUser', async () => {
    const res = await mockFlightService.getNextFlightByUser(2);

    expect(mockGetPassengerIdForUserSpy).toBeCalled();
    expect(mockGetNextFlightByUserSpy).toBeCalled();
    expect(res).toBe(testFlight1);
  });

  it('getNextArrivalFlight', async () => {
    const res = await mockFlightService.getNextArrivalFlight(2);

    expect(mockGetPassengerIdForUserSpy).toBeCalled();
    expect(mockGetAllFlightsByUserSpy).toBeCalled();
    expect(res).toBe(testFlight1);
  });

  it('getNextDepartureFlight', async () => {
    const res = await mockFlightService.getNextDepartureFlight(2);

    expect(mockGetPassengerIdForUserSpy).toBeCalled();
    expect(mockGetNextFlightByUserSpy).toBeCalled();
    expect(res).toBe(testFlight1);
  });

  it('getUsersByFlight', async () => {
    const res = await mockFlightService.getUsersByFlight(12);
    expect(mockrepositoryFlight.findOne).toBeCalled();
    expect(mockrepositoryUserTrip.find).toBeCalled();
    expect(mockrepositoryUser.findByIds).toBeCalled;
  });
});

describe('flight model', () => {
  it('get city of a flight with lumo data', () => {
    const flight = new Flight();
    flight.initialLumoData = fakeLumoRecord;
    expect(FlightHelper.getOriginCity(flight)).toEqual('London');
  });
  it('get city of a flight without lumo data', () => {
    const flight = new Flight();
    flight.origin = 'LHR';
    expect(FlightHelper.getOriginCity(flight)).toEqual('London');
  });
});
