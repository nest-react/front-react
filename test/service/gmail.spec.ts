import { GmailService } from '../../src/service/Gmail.service';

const testDataSend = {
  to: 'bob@bob.com',
  subject: 'some subject',
  text: 'some text',
};
const testDataSendElad = {
  to: 'elad@gozenner.com',
  subject: 'some subject',
  text: 'some text',
};
const testDataSendDaniel = {
  to: 'daniel@gozenner.com',
  subject: 'some subject',
  text: 'some text',
};

let gmailService: GmailService;
const mockSendSpy = jest.fn((args) => args.bcc);

describe('GMail unit tests', () => {
  const initialProcessEnv = process.env;
  beforeAll(() => {
    jest.mock('nodemailer', () => ({
      createTransport: () => ({
        sendMail: mockSendSpy,
      }),
    }));
    const { GmailService } = require('../../src/service/Gmail.service');
    gmailService = new GmailService();
    process.env = Object.assign(initialProcessEnv, {
      GMAIL_SERVICE_CLIENT: 'not real client',
    });
  });

  beforeEach(() => {});

  afterEach(() => {
    mockSendSpy.mockClear();
  });

  afterAll(() => {
    jest.restoreAllMocks();
    process.env = initialProcessEnv;
  });

  it('send a message', async () => {
    // Act
    await gmailService.send(testDataSend);

    // Assert
    expect(mockSendSpy).toHaveBeenCalled();
    expect(mockSendSpy).toHaveReturnedWith(
      'elad@gozenner.com, daniel@gozenner.com',
    );
  });

  it('send a message to Elad', async () => {
    // Act
    await gmailService.send(testDataSendElad);

    // Assert
    expect(mockSendSpy).toHaveBeenCalled();
    expect(mockSendSpy).toHaveReturnedWith(
      'Daniel Green <daniel@gozenner.com>',
    );
  });

  it('send a message to Daniel', async () => {
    // Act
    await gmailService.send(testDataSendDaniel);

    // Assert
    expect(mockSendSpy).toHaveBeenCalled();
    expect(mockSendSpy).toHaveReturnedWith('Elad Schaffer <elad@gozenner.com>');
  });
});
