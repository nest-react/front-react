import { mock } from 'jest-mock-extended';
import Airtable from 'airtable';
import { ConfigService } from '../../../src/service';
import { AirtableService } from '../../../src/service/airtable/Airtable';
import {
  IAirportMessage,
  IPassengerMessage,
  Direction,
} from '../../../src/interface/airtable/message.interface';

const queryMock = mock<Airtable.Query<any>>();

jest.mock('airtable', () =>
  jest.fn(() => ({
    base: () =>
      jest.fn().mockReturnValue({
        select: () => queryMock,
      }),
  })),
);

describe('AirtableService', () => {
  let airtableService: AirtableService;

  beforeAll(() => {
    airtableService = new AirtableService(new Airtable(), ConfigService);
  });

  it('Getting airport messages', async () => {
    queryMock.all.mockResolvedValue([
      {
        id: '',
        fields: {
          code: '',
          terminal: '',
          direction: Direction.arrive,
          date: '',
          active: true,
          message: '',
        },
      },
    ]);
    const response: Airtable.Records<IAirportMessage> = await airtableService.getMessageForAirport(
      {
        airportCode: '',
      },
    );
    expect(queryMock.all).toBeCalled();
    expect(response).toBeDefined();
  });

  it('Getting airport messages with exception', async () => {
    queryMock.all.mockRejectedValue(new Error());
    const response: Airtable.Records<IAirportMessage> = await airtableService.getMessageForAirport(
      {
        airportCode: '',
      },
    );
    expect(queryMock.all).toBeCalled();
    expect(response).toBeNull();
  });

  it('Getting passenger messages', async () => {
    queryMock.all.mockResolvedValue([
      {
        id: '',
        fields: {
          email: '',
          airportCode: '',
          eventName: '',
          active: true,
          sent: true,
          message: '',
        },
      },
    ]);
    const response: Airtable.Records<IPassengerMessage> = await airtableService.getMessageForPassenger(
      {
        airportCode: '',
      },
    );
    expect(queryMock.all).toBeCalled();
    expect(response).toBeDefined();
  });

  it('Getting passenger messages with exception', async () => {
    queryMock.all.mockRejectedValue(new Error());
    const response: Airtable.Records<IPassengerMessage> = await airtableService.getMessageForPassenger(
      {
        airportCode: '',
      },
    );
    expect(queryMock.all).toBeCalled();
    expect(response).toBeNull();
  });
});
