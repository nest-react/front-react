import Il8nService from '../../src/service/Il8n.service';

describe('Internationalization (il8n) Service', () => {
  it('Should return null on no match in any language', () => {
    expect(Il8nService.get('testing:shouldfail')).toBe(null);
  });

  it('Should find other languages', () => {
    expect(Il8nService.get('testing:test1', { language: 'es' })).toBe(
      'This line should always be present in a language file (ES)',
    );
  });

  it('Should fallback to English', () => {
    expect(Il8nService.get('testing:test2', { language: 'es' })).toBe(
      'This line should only be present in the ENGLISH language file',
    );
  });

  it('Should support Handlebar variables', () => {
    expect(
      Il8nService.get('testing:test3', {
        language: 'es',
        templateVars: { vartext: 'template' },
      }),
    ).toBe('This has a template in it');
  });

  it('Should ignore irrelevant Handlebar variables', () => {
    expect(
      Il8nService.get('testing:test2', {
        language: 'es',
        templateVars: { vartext: 'template' },
      }),
    ).toBe('This line should only be present in the ENGLISH language file');
  });

  it('Should ignore empty variables', () => {
    expect(Il8nService.get('testing:test3', { language: 'es' })).toBe(
      'This has a {{vartext}} in it',
    );
  });

  it('Should never contain a non-breaking space', () => {
    expect(
      Il8nService.get('testing:test3', { language: 'es' }).indexOf('\u00A0'),
    ).toBeLessThan(0);
  });
});
