import { CommsEventService } from '../../src/service/CommsEvent.service';
import { Connection, Repository } from 'typeorm';
import CommsEvent from '../../src/model/CommsEvent';

const testEvent = new CommsEvent();
testEvent.created = new Date();
testEvent.from = 'whatsapp:+13235082016';
testEvent.to = 'whatsapp:+995557773417';
testEvent.source = 'whatsapp';
testEvent.human = false;
testEvent.message = 'some message';
testEvent.server = 'localhost';
testEvent.result = 'SUCCESS';

const mockrepository = new Repository<CommsEvent>();

mockrepository.save = jest.fn(async (options: any) => {
  return options;
});

const falseDb = new Connection({ type: 'postgres' });
const getRepositorySpy = jest.fn();
falseDb.getRepository = getRepositorySpy;

const mockCommsEventService = new CommsEventService(falseDb);
mockCommsEventService.eventRepository = mockrepository;

const mockAddEventSpy = jest.fn(mockCommsEventService.addEvent);
mockCommsEventService.addEvent = mockAddEventSpy;

describe('CommsEventService', () => {
  it('addEvent', async () => {
    const res = await mockCommsEventService.addEvent(testEvent);

    expect(mockAddEventSpy).toBeCalled();
    expect(getRepositorySpy).toBeCalled();
    expect(mockrepository.save).toBeCalled();
    expect(res.created).toBe(testEvent.created);
    expect(res.from).toBe(testEvent.from);
    expect(res.to).toBe(testEvent.to);
    expect(res.message).toBe(testEvent.message);
  });
});
