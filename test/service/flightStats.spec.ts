import axios from 'axios';
import { FlightStatsService } from '../../src/service/FlightStats.service';
import {
  AirportOrigin,
  FlightStatusByRouteOptions,
  FlightStatusesOptions,
} from '../../src/interface/flightStats';
import { Connection } from 'typeorm';

const fakeDB = ({
  getRepository: jest.fn(),
} as unknown) as Connection;

const flightStatsService = new FlightStatsService(fakeDB);

describe('FlightStats service', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  const fakeAlert: any = {
    id: '1360976293',
    carrierFsCode: 'FR',
    flightNumber: '4124',
    departureAirportFsCode: 'HRK',
    arrivalAirportFsCode: 'BUD',
    departure: '2020-08-10T18:55:00.000',
    arrival: '2020-08-10T20:05:00.000',
    name: 'Default',
    ruleEvents: {
      type: 'ALL_CHANGES',
    },
    nameValues: [],
    delivery: {
      format: 'JSON',
      destination: 'url',
    },
  };
  const fakeType = {
    type: 'INITIAL',
  };

  const fakeFindOne = jest.fn().mockResolvedValue(undefined);
  const fakeSave = jest.fn().mockResolvedValue({});
  const fakeInsert = jest.fn().mockResolvedValue({});

  const fakeIds = [1360976293, 1360976181, 1360970791, 1360809975, 1360793633];

  it('Request flight stats by ID', async () => {
    const axiosGetSpy = jest.spyOn(axios, 'get').mockResolvedValue({
      data: {
        flightStatus: {},
      },
    });

    const id = 1038417465;
    const results = await flightStatsService.getFlightStatusById(id);
    expect(axiosGetSpy).toBeCalled();
    expect(axiosGetSpy).toHaveBeenCalledWith(
      'flightstatus/rest/v2/json/flight/status/' + String(id),
    );
    expect(results.error).toBeFalsy();
  });

  it('Request flight stats with invalid ID', async () => {
    const axiosGetSpy = jest.spyOn(axios, 'get').mockResolvedValue({
      data: {
        error: {
          errorMessage: 'Unable to find a flight with the ID given',
        },
      },
    });

    const id = 123123;
    const response = await flightStatsService.getFlightStatusById(id);
    expect(axiosGetSpy).toBeCalled();
    expect(axiosGetSpy).toHaveBeenCalledWith(
      'flightstatus/rest/v2/json/flight/status/' + String(id),
    );

    expect(response.results).toBeNull();
    expect(response.message).toBe('Unable to find a flight with the ID given');
    expect(response.error).toBeTruthy();
  });

  it('Request arrival flight stats by carrier + flight ID + date', async () => {
    const axiosGetSpy = jest.spyOn(axios, 'get').mockResolvedValue({
      data: {
        flightStatuses: {},
      },
    });

    const options: FlightStatusesOptions = {
      airportType: AirportOrigin.arrival,
      carrier: 'AI',
      date: '2020-06-15',
      flight: 1927,
    };

    await flightStatsService.status(options);

    expect(axiosGetSpy).toHaveBeenCalledWith(
      'flightstatus/rest/v2/json/flight/status/AI/1927/arr/2020/6/15',
      {
        params: options,
      },
    );
  });

  it('Request alert flight stats by flight info and departure date', async () => {
    const axiosGetSpy = jest.spyOn(axios, 'get').mockResolvedValue({
      data: {
        rule: fakeAlert,
        event: fakeType,
      },
    });

    const fakeDB = ({
      getRepository: jest.fn().mockImplementation(() => {
        return {
          findOne: fakeFindOne,
          save: fakeSave,
          insert: fakeInsert,
        };
      }),
    } as unknown) as Connection;

    const service = new FlightStatsService(fakeDB);

    const options = {
      carrier: 'FR',
      flightNumber: 4124,
      arrivalAirport: 'BUD',
      departureAirport: 'HRK',
      date: '2020-08-10',
    };
    const alert = await service.createAlertByDeparture(options, 1);
    expect(axiosGetSpy).toHaveBeenCalledWith(
      'alerts/rest/v1/json/create/FR/4124/from/HRK/departing/2020/8/10',
      {
        params: options,
      },
    );
    expect(alert.results.rule).toEqual(fakeAlert);
    expect(alert.message).toBeUndefined();
    expect(fakeFindOne).toBeCalled();
    expect(fakeFindOne).toBeCalledWith({
      where: {
        departureDate: '2020-08-10',
        flightNo: 'FR4124',
      },
    });
    expect(fakeInsert).toBeCalled();
    expect(alert.error).toBeUndefined();
    expect(axiosGetSpy).toBeCalled();
  });

  it('Request alert flight by flight info and arrival date', async () => {
    const axiosGetSpy = jest.spyOn(axios, 'get').mockResolvedValue({
      data: {
        rule: fakeAlert,
        event: fakeType,
      },
    });

    const fakeDB = ({
      getRepository: jest.fn().mockImplementation(() => {
        return {
          findOne: fakeFindOne,
          save: fakeSave,
          insert: fakeInsert,
        };
      }),
    } as unknown) as Connection;

    const service = new FlightStatsService(fakeDB);

    const options = {
      carrier: 'FR',
      flightNumber: 4124,
      arrivalAirport: 'BUD',
      date: '2020-08-10',
    };
    const alert = await service.createAlertByArrival(options, 1);
    expect(axiosGetSpy).toHaveBeenCalledWith(
      'alerts/rest/v1/json/create/FR/4124/to/BUD/arriving/2020/8/10',
      {
        params: options,
      },
    );
    expect(alert.results.rule).toEqual(fakeAlert);
    expect(alert.message).toBeUndefined();
    expect(alert.error).toBeUndefined();
    expect(fakeFindOne).toBeCalled();
    expect(fakeFindOne).toBeCalledWith({
      where: {
        departureDate: '2020-08-10',
        flightNo: 'FR4124',
      },
    });
    expect(fakeInsert).toBeCalled();
    expect(axiosGetSpy).toBeCalled();
  });

  it('Request alert flight by flight info + route + arrival date', async () => {
    const axiosGetSpy = jest.spyOn(axios, 'get').mockResolvedValue({
      data: {
        rule: fakeAlert,
        event: fakeType,
      },
    });

    const fakeDB = ({
      getRepository: jest.fn().mockImplementation(() => {
        return {
          findOne: fakeFindOne,
          save: fakeSave,
          insert: fakeInsert,
        };
      }),
    } as unknown) as Connection;

    const service = new FlightStatsService(fakeDB);

    const options = {
      carrier: 'FR',
      flightNumber: 4124,
      arrivalAirport: 'BUD',
      departureAirport: 'HRK',
      date: '2020-08-10',
    };
    const alert = await service.createAlertForRouteByArrival(options, 1);
    expect(axiosGetSpy).toHaveBeenCalledWith(
      'alerts/rest/v1/json/create/FR/4124/from/HRK/to/BUD/arriving/2020/8/10',
      {
        params: options,
      },
    );
    expect(alert.results.rule).toEqual(fakeAlert);
    expect(alert.message).toBeUndefined();
    expect(alert.error).toBeUndefined();
    expect(fakeFindOne).toBeCalled();
    expect(fakeFindOne).toBeCalledWith({
      where: {
        departureDate: '2020-08-10',
        flightNo: 'FR4124',
      },
    });
    expect(fakeInsert).toBeCalled();
    expect(axiosGetSpy).toBeCalled();
  });

  it('Request alert flight by flight info + route + departure date', async () => {
    const axiosGetSpy = jest.spyOn(axios, 'get').mockResolvedValue({
      data: {
        rule: fakeAlert,
        event: fakeType,
      },
    });

    const options = {
      carrier: 'FR',
      flightNumber: 4124,
      arrivalAirport: 'BUD',
      departureAirport: 'HRK',
      date: '2020-08-10',
    };
    const fakeDB = ({
      getRepository: jest.fn().mockImplementation(() => {
        return {
          findOne: fakeFindOne,
          save: fakeSave,
          insert: fakeInsert,
        };
      }),
    } as unknown) as Connection;

    const service = new FlightStatsService(fakeDB);

    const alert = await service.createAlertForRouteByDeparture(options, 1);
    expect(axiosGetSpy).toHaveBeenCalledWith(
      'alerts/rest/v1/json/create/FR/4124/from/HRK/to/BUD/departing/2020/8/10',
      {
        params: options,
      },
    );
    expect(alert.results.rule).toEqual(fakeAlert);
    expect(alert.message).toBeUndefined();
    expect(alert.error).toBeUndefined();
    expect(fakeFindOne).toBeCalled();
    expect(fakeFindOne).toBeCalledWith({
      where: {
        departureDate: '2020-08-10',
        flightNo: 'FR4124',
      },
    });
    expect(fakeInsert).toBeCalled();
    expect(axiosGetSpy).toBeCalled();
  });

  it('Deletes a flight alert by alert id', async () => {
    const axiosGetSpy = jest.spyOn(axios, 'get').mockResolvedValue({
      data: {
        rule: fakeAlert,
      },
    });
    const fakeDelete = jest.fn().mockReturnValue(undefined);
    const id = '1361130975';
    const fakeDB = ({
      getRepository: jest.fn().mockImplementation(() => {
        return {
          findOne: fakeFindOne,
          delete: fakeDelete,
        };
      }),
    } as unknown) as Connection;

    const service = new FlightStatsService(fakeDB);
    const alert = await service.deleteAlertById(id);
    expect(axiosGetSpy).toHaveBeenCalledWith(
      `alerts/rest/v1/json/delete/${id}`,
    );
    expect(alert.results.rule).toEqual(fakeAlert);
    expect(alert.message).toBeUndefined();
    expect(alert.error).toBeUndefined();
    expect(fakeDelete).toBeCalled();
    expect(fakeDelete).toBeCalledWith({
      subscriptionId: id,
    });
    expect(axiosGetSpy).toBeCalled();
  });

  it('Deletes a flight alert by invalid flight id', async () => {
    const axiosGetSpy = jest.spyOn(axios, 'get').mockResolvedValue({
      data: {
        error: {
          httpStatusCode: 400,
          errorId: '6a8abbfc-1853-4d0d-93c2-6ae922e5e561',
          errorMessage: 'The rule ID specified did not match an active rule',
        },
      },
    });

    const id = '1360933152';
    const response = await flightStatsService.deleteAlertById(id);

    expect(axiosGetSpy).toHaveBeenCalledWith(
      'alerts/rest/v1/json/delete/1360933152',
    );
    expect(response.results).toBeNull();
    expect(response.error).toBeTruthy();
    expect(response.message).toBe(
      'The rule ID specified did not match an active rule',
    );
  });

  it('Get a flight alert by alert id', async () => {
    const axiosGetSpy = jest.spyOn(axios, 'get').mockResolvedValue({
      data: {
        rule: fakeAlert,
      },
    });

    const id = 1360933936;
    const alert = await flightStatsService.getAlertById(id);
    expect(axiosGetSpy).toHaveBeenCalledWith(`alerts/rest/v1/json/get/${id}`);
    expect(alert.results.rule).toEqual(fakeAlert);
    expect(alert.message).toBeUndefined();
    expect(alert.error).toBeUndefined();
    expect(axiosGetSpy).toBeCalled();
  });

  it('Get a flight alerts by account', async () => {
    const axiosGetSpy = jest.spyOn(axios, 'get').mockResolvedValue({
      data: {
        request: {
          url: 'https://api.flightstats.com/flex/alerts/rest/v1/json/list',
          ruleFilter: {
            interpreted: 'all',
          },
        },
        ruleIds: fakeIds,
      },
    });

    const alertIds = await flightStatsService.getAlertIds();
    expect(axiosGetSpy).toHaveBeenCalledWith('alerts/rest/v1/json/list');
    expect(alertIds.results.ruleIds).toEqual(fakeIds);
    expect(alertIds.message).toBeUndefined();
    expect(alertIds.error).toBeUndefined();
    expect(axiosGetSpy).toBeCalled();
  });

  it('Get a flight alerts by account that less than given alert id', async () => {
    const alertId = 1360935158;
    const axiosGetSpy = jest.spyOn(axios, 'get').mockResolvedValue({
      data: {
        request: {
          url: `https://api.flightstats.com/flex/alerts/rest/v1/json/${alertId}`,
          ruleFilter: {
            interpreted: 'all',
          },
        },
        ruleIds: fakeIds,
      },
    });

    const alertIds = await flightStatsService.getAlertIdsLesserThanId(alertId);
    expect(axiosGetSpy).toHaveBeenCalledWith(
      `alerts/rest/v1/json/list/${alertId}`,
    );
    expect(alertIds.results.ruleIds).toEqual(fakeIds);
    expect(alertIds.message).toBeUndefined();
    expect(alertIds.error).toBeUndefined();
    expect(axiosGetSpy).toBeCalled();
  });

  it('Request arrival flight stats with invalid carrier + flight ID + date', async () => {
    const axiosGetSpy = jest.spyOn(axios, 'get').mockResolvedValue({
      data: {
        error: {
          errorMessage: 'Invalid value for carrier',
        },
      },
    });

    const options: FlightStatusesOptions = {
      airportType: AirportOrigin.arrival,
      carrier: 'invalidCarrierID',
      date: '2020-06-15',
      flight: 1927,
    };

    const response = await flightStatsService.status(options);
    expect(axiosGetSpy).toHaveBeenCalledWith(
      'flightstatus/rest/v2/json/flight/status/invalidCarrierID/1927/arr/2020/6/15',
      {
        params: options,
      },
    );

    expect(response.results).toBeNull();
    expect(response.error).toBeTruthy();
    expect(response.message).toBe('Invalid value for carrier');
  });

  it('Request departure flight stats by carrier + flight ID + date', async () => {
    const axiosGetSpy = jest.spyOn(axios, 'get').mockResolvedValue({
      data: {
        flightStatuses: {},
      },
    });

    const options: FlightStatusesOptions = {
      airportType: AirportOrigin.departure,
      carrier: 'TT',
      date: '2020-07-08',
      flight: 123,
      airport: 'KBP',
    };

    const response = await flightStatsService.status(options);

    expect(axiosGetSpy).toHaveBeenCalledWith(
      'flightstatus/rest/v2/json/flight/status/TT/123/dep/2020/7/8',
      {
        params: options,
      },
    );

    expect(response.error).toBeFalsy();
  });

  it('Request flight status between two airport for specific date', async () => {
    const axiosGetSpy = jest.spyOn(axios, 'get').mockResolvedValue({
      data: {},
    });

    const options: FlightStatusByRouteOptions = {
      airportType: AirportOrigin.departure,
      departureAirport: 'WAW',
      arrivalAirport: 'SZZ',
      date: '2020-06-15',
    };

    const response = await flightStatsService.findFlightsByRoute(options);
    expect(axiosGetSpy).toHaveBeenCalledWith(
      'flightstatus/rest/v2/json/route/status/WAW/SZZ/dep/2020/6/15',
      {
        params: {},
      },
    );
    expect(response.error).toBeFalsy();
  });

  it('Request flight status between metropolitan airports', async () => {
    const axiosGetSpy = jest.spyOn(axios, 'get').mockResolvedValue({
      data: {
        flightStatuses: [],
      },
    });

    const options: FlightStatusByRouteOptions = {
      airportType: AirportOrigin.departure,
      departureAirport: 'LON',
      arrivalAirport: 'NYC',
      date: '2020-06-15',
    };

    const response = await flightStatsService.findFlightsByRoute(options);

    expect(axiosGetSpy).toBeCalledTimes(28);
    expect(response.error).toBeFalsy();
  });

  it('Request flight status between airport and metropolitan airport', async () => {
    const axiosGetSpy = jest.spyOn(axios, 'get').mockResolvedValue({
      data: {
        flightStatuses: [],
      },
    });

    const options: FlightStatusByRouteOptions = {
      airportType: AirportOrigin.departure,
      departureAirport: 'WAW',
      arrivalAirport: 'NYC',
      date: '2020-06-15',
    };

    const response = await flightStatsService.findFlightsByRoute(options);

    // NYC area consists of 4 airport
    expect(axiosGetSpy).toBeCalledTimes(4);
    expect(response.error).toBeFalsy();
  });

  it('Request flight status between two airport with invalid airport code', async () => {
    const axiosGetSpy = jest.spyOn(axios, 'get').mockResolvedValue({
      data: {
        error: {
          errorMessage: 'Invalid value for departureAirport',
        },
      },
    });

    const options: FlightStatusByRouteOptions = {
      airportType: AirportOrigin.departure,
      departureAirport: '123',
      arrivalAirport: 'WAW',
      date: '2020-06-15',
    };

    const response = await flightStatsService.findFlightsByRoute(options);

    expect(axiosGetSpy).toHaveBeenCalledWith(
      'flightstatus/rest/v2/json/route/status/123/WAW/dep/2020/6/15',
      {
        params: {},
      },
    );

    expect(response.results).toBeNull();
    expect(response.error).toBeTruthy();
    expect(response.message).toBe('Invalid value for departureAirport');
  });

  it('Search flights by airport code', async () => {
    const axiosGetSpy = jest.spyOn(axios, 'get').mockResolvedValue({
      data: {
        flightStatuses: {},
      },
    });

    const response = await flightStatsService.findFlightsByAirport({
      airport: 'KBP',
      hourOfDay: 12,
      date: '2020-06-13',
      maxFlights: 10,
      airportType: AirportOrigin.arrival,
    });

    expect(axiosGetSpy).toHaveBeenCalledWith(
      'flightstatus/rest/v2/json/airport/status/KBP/arr/2020/6/13/12',
      {
        params: {
          maxFlights: 10,
        },
      },
    );

    expect(response.error).toBeFalsy();
  });

  it('Search flights by invalid airport code', async () => {
    const axiosGetSpy = jest.spyOn(axios, 'get').mockResolvedValue({
      data: {
        error: {
          errorMessage: 'Invalid value for airport',
        },
      },
    });

    const response = await flightStatsService.findFlightsByAirport({
      airport: 'KBP',
      hourOfDay: 12,
      date: '2020-06-13',
      maxFlights: 10,
      airportType: AirportOrigin.arrival,
    });

    expect(axiosGetSpy).toHaveBeenCalledWith(
      'flightstatus/rest/v2/json/airport/status/KBP/arr/2020/6/13/12',
      {
        params: {
          maxFlights: 10,
        },
      },
    );

    expect(response.results).toBeNull();
    expect(response.error).toBeTruthy();
    expect(response.message).toBe('Invalid value for airport');
  });
});
