import { mock } from 'jest-mock-extended';
import {
  Client,
  Status,
  TravelMode,
} from '@googlemaps/google-maps-services-js/dist';
import { DirectionsResponseData } from '@googlemaps/google-maps-services-js/dist/directions';
import { DistanceMatrixResponseData } from '@googlemaps/google-maps-services-js/dist/distance';
import { DistanceMatrixRowElement } from '../../src/interface/DistanceMatrixRowElement.interface';
import { DirectionService } from '../../src/service/Direction.service';

const directionsResponseData: DirectionsResponseData = {
  geocoded_waypoints: [],
  routes: [],
  available_travel_modes: [],
  status: Status.OK,
  error_message: '',
};

const distanceMatrixResponseData: DistanceMatrixResponseData = {
  origin_addresses: [],
  destination_addresses: [],
  rows: [
    {
      elements: [
        {
          duration: {
            value: 0,
            text: '',
          },
          duration_in_traffic: {
            value: 0,
            text: '',
          },
          distance: {
            value: 0,
            text: '',
          },
          fare: {
            currency: '',
            value: 0,
            text: '',
          },
          status: Status.OK,
        },
      ],
    },
  ],
  status: Status.OK,
  error_message: '',
};

const clientMock = mock<Client>();

jest.mock('@googlemaps/google-maps-services-js', () => ({
  Client: jest.fn(() => clientMock),
  TravelMode: {},
  Status: {},
  UnitSystem: {},
}));

describe('Direction service', () => {
  let directionService: DirectionService;

  beforeAll(() => {
    directionService = new DirectionService(new Client(), '');
  });

  it('Request getting directions', async () => {
    clientMock.directions.mockResolvedValue({
      config: {},
      headers: {},
      data: directionsResponseData,
      status: 200,
      statusText: Status.OK,
    });
    const response: DirectionsResponseData = await directionService.getDirection(
      '',
      '',
    );
    expect(clientMock.directions).toBeCalled();
    expect(response).toBeDefined();
  });

  it('Request getting fastest way', async () => {
    clientMock.distancematrix.mockResolvedValue({
      config: {},
      headers: {},
      data: distanceMatrixResponseData,
      status: 200,
      statusText: Status.OK,
    });
    const response: DistanceMatrixRowElement = await directionService.getFastestWay(
      '',
      '',
    );
    expect(clientMock.distancematrix).toBeCalled();
    expect(response).toBeDefined();
  });

  it('Request getting fastest way of modes', async () => {
    clientMock.distancematrix.mockResolvedValue({
      config: {},
      headers: {},
      data: distanceMatrixResponseData,
      status: 200,
      statusText: Status.OK,
    });
    const response: DistanceMatrixRowElement[] = await directionService.getFastestWayOfModes(
      '',
      '',
      [TravelMode.driving],
    );
    expect(clientMock.distancematrix).toBeCalled();
    expect(response).toBeDefined();
  });
});
