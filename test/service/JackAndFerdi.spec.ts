import { JackAndFerdiService } from '../../src/service/JackAndFerdi.service';
import axios from 'axios';
import Il8nService from '../../src/service/Il8n.service';
import { Connection, createConnection } from 'typeorm';
jest.mock('axios');
const mockedAxios = axios as jest.Mocked<typeof axios>;
let temp: any = {};
let db: Connection;
Object.keys(Connection).map((a) => (temp[a] = undefined));
db = temp as Connection;
db.getRepository = jest.fn().mockReturnValue({
  findOne: () => {
    return { city_ascii: 'bla' };
  },
});
const jackandferdiService = new JackAndFerdiService(db);

describe('jack and freddie services', () => {
  afterEach(() => {
    mockedAxios.get.mockReset();
    jest.clearAllMocks();
  });

  it('meditation places in new york(one result)', async () => {
    const params = {
      search: 'new york'.toLowerCase(),
      tags: 'meditation',
    };
    mockedAxios.get.mockResolvedValueOnce({ data: [{ name: 'new york' }] });
    mockedAxios.get.mockResolvedValueOnce({
      data: {
        results: [
          {
            city_ref: {
              name: 'new york',
            },
            title: 'place',
            tagline: 'sum',
            tags: ['Meditation', 'Outdoors', 'Stunning'],
          },
        ],
      },
    });
    expect(
      await jackandferdiService.getPlaces('new york', ['meditation']),
    ).toEqual(
      Il8nService.get('zenny:jackandferdi:place', {
        templateVars: {
          title: 'place',
          sum: 'sum',
        },
      }),
    );
    expect(axios.get).toHaveBeenCalledWith(`place/search/`, {
      params: params,
    });
  });

  it('drinks places in new york(two results)', async () => {
    const params = {
      search: 'new york'.toLowerCase(),
      tags: 'drinks',
    };
    mockedAxios.get.mockResolvedValueOnce({ data: [{ name: 'New York' }] });
    mockedAxios.get.mockResolvedValueOnce({
      data: {
        results: [
          {
            city_ref: {
              name: 'new york',
            },
            title: 'place',
            tagline: 'sum',
            tags: ['Drinks'],
          },
          {
            city_ref: {
              name: 'new york',
            },
            title: 'place',
            tagline: 'sum',
            tags: ['Drinks'],
          },
        ],
      },
    });
    let data = [];
    data.push(
      Il8nService.get('zenny:jackandferdi:place', {
        templateVars: {
          title: 'place',
          sum: 'sum',
        },
      }),
    );
    data.push(data[0]);
    let expected =
      Il8nService.get('zenny:jackandferdi:drinkscount', {
        templateVars: {
          counter: 2,
          city: 'New York',
        },
      }) + data.map((line, index) => `|||${index + 1}. ${line}`).join('');
    expect(await jackandferdiService.getPlaces('new york', ['drinks'])).toEqual(
      expected,
    );
    expect(mockedAxios.get).toHaveBeenNthCalledWith(2, `place/search/`, {
      params: params,
    });
  });
  it('food places in new york(no results)', async () => {
    const params = {
      search: 'new york'.toLowerCase(),
      tags: 'food',
    };
    mockedAxios.get.mockResolvedValueOnce({ data: [{ name: 'New York' }] });
    mockedAxios.get.mockResolvedValueOnce({
      data: {
        results: [],
      },
    });
    let expected = Il8nService.get('zenny:jackandferdi:notfound', {
      templateVars: { city: 'New York' },
    });
    expect(await jackandferdiService.getPlaces('new york', ['food'])).toEqual(
      expected,
    );
    expect(mockedAxios.get).toHaveBeenNthCalledWith(2, `place/search/`, {
      params: params,
    });
  });
  it('meditation place with no city', async () => {
    const params = {
      search: 'new york'.toLowerCase(),
      tags: 'meditation',
    };
    mockedAxios.get.mockResolvedValueOnce({ data: [{ name: 'New York' }] });
    mockedAxios.get.mockResolvedValueOnce({
      data: {
        results: [],
      },
    });
    let expected = Il8nService.get('zenny:notfound:city');
    expect(await jackandferdiService.getPlaces('bla', ['meditation'])).toEqual(
      expected,
    );
  });
  it('get city id ', async () => {
    mockedAxios.get.mockResolvedValue({
      data: [
        {
          name: 'New York',
          id: 100,
        },
      ],
    });
    expect(await jackandferdiService.getCityId('new york')).toEqual(100);
  });
  it('get fun fact ', async () => {
    mockedAxios.get
      .mockResolvedValueOnce({
        data: [
          {
            name: 'New York',
            id: 100,
          },
        ],
      })
      .mockResolvedValueOnce({
        data: [
          {
            name: 'New York',
            id: 100,
          },
        ],
      });
    mockedAxios.get.mockResolvedValueOnce({
      data: {
        funfacts: [{ category: 'FUN', description: 'Fun Fact' }],
      },
    });
    expect(await jackandferdiService.getFunFact('new york')).toEqual(
      'Fun Fact',
    );
  });
  it('get fun fact(with no results) ', async () => {
    mockedAxios.get
      .mockResolvedValueOnce({
        data: [
          {
            name: 'New York',
            id: 100,
          },
        ],
      })
      .mockResolvedValueOnce({
        data: [
          {
            name: 'New York',
            id: 100,
          },
        ],
      });
    mockedAxios.get.mockResolvedValueOnce({
      data: {
        funfacts: [],
      },
    });
    expect(await jackandferdiService.getFunFact('new york')).toEqual(
      Il8nService.get('zenny:notfound:funfact'),
    );
  });

  it('manners(1) ', async () => {
    mockedAxios.get
      .mockResolvedValueOnce({
        data: [
          {
            name: 'New York',
            id: 100,
          },
        ],
      })
      .mockResolvedValueOnce({
        data: [
          {
            name: 'New York',
            id: 100,
          },
        ],
      });
    mockedAxios.get.mockResolvedValueOnce({
      data: {
        manners: [{ description: 'Manner' }],
      },
    });
    expect(
      await jackandferdiService.getCityData('new york', 'manners'),
    ).toEqual('Manner');
  });
  it('manners(2) ', async () => {
    mockedAxios.get
      .mockResolvedValueOnce({
        data: [
          {
            name: 'New York',
            id: 100,
          },
        ],
      })
      .mockResolvedValueOnce({
        data: [
          {
            name: 'New York',
            id: 100,
          },
        ],
      });
    mockedAxios.get.mockResolvedValueOnce({
      data: {
        manners: [{ description: 'Manner' }, { description: 'Manner' }],
      },
    });
    expect(
      await jackandferdiService.getCityData('new york', 'manners'),
    ).toEqual(
      'Here are some business manners you should consider in new york:\n\nManner\nManner',
    );
  });
  it('working places', async () => {
    const params = {
      search: 'new york'.toLowerCase(),
      type: 'WORK',
    };
    mockedAxios.get.mockResolvedValueOnce({ data: [{ name: 'New York' }] });
    mockedAxios.get.mockResolvedValueOnce({
      data: {
        results: [
          {
            city_ref: {
              name: 'new york',
            },
            title: 'place',
            tagline: 'sum',
            type: 'WORK',
            tags: ['Coffee'],
          },
          {
            city_ref: {
              name: 'New york',
            },
            title: 'place',
            tagline: 'sum',
            type: 'WORK',
            tags: ['Drinks'],
          },
        ],
      },
    });
    let data = [];
    data.push(
      Il8nService.get('zenny:jackandferdi:place', {
        templateVars: {
          title: 'place',
          sum: 'sum',
        },
      }),
    );
    data.push(data[0]);
    let expected =
      Il8nService.get('zenny:jackandferdi:workcount', {
        templateVars: {
          counter: 2,
          city: 'New York',
        },
      }) + data.join('\n');
    expect(await jackandferdiService.getWorkPlaces('new york')).toEqual(
      expected,
    );
    expect(mockedAxios.get).toHaveBeenNthCalledWith(2, `place/search/`, {
      params: params,
    });
  });
});
