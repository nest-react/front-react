import { HttpClient } from '../../src/service/HttpClient';

jest.unmock('axios');
import axios from 'axios';

import { LumoService } from '../../src/service/Lumo.service';
import { Connection } from 'typeorm';

describe('HttpClient', () => {
  beforeEach(() => {
    const instance = axios.create({
      // replace default fetch method with our custom implementation
      adapter: (config) => {
        return Promise.resolve({
          data: {
            test: 1,
            results: [],
          },
          status: 200,
          statusText: 'OK',
          headers: {},
          config,
        });
      },
    });

    jest.spyOn(axios, 'create').mockImplementation(() => instance);
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('onResponse event should appear after request  ', async () => {
    const httpClient = new HttpClient();
    const replay = jest.fn();

    httpClient.on('onResponse', replay);

    const http = httpClient.create();

    await http.get('https://google.com/');

    expect(replay).toBeCalled();
  });

  it('LumoService should emit onResponse events after each request ', async () => {
    const fakeFind = jest.fn().mockResolvedValue([]);
    const fakeSave = jest.fn().mockResolvedValue({});
    const fakeDb = ({
      getRepository: jest.fn().mockImplementation(() => {
        return {
          find: fakeFind,
          save: fakeSave,
        };
      }),
    } as unknown) as Connection;
    const lumoService = new LumoService(fakeDb);

    const replay = jest.fn();
    lumoService.on('onResponse', replay);

    await lumoService.status('testId');
    await lumoService.status('testId2');
    await lumoService.status('testId3');
    await lumoService.search({
      origin: 'KBP',
      date: '2020-06-17',
    });

    expect(replay).toBeCalledTimes(4);
  });
});
