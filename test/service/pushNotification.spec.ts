import { pushNotificationsService } from '../../src/service/pushNotifications.service';
import {
  createConnection,
  Repository,
  SaveOptions,
  Connection,
  DeepPartial,
} from 'typeorm';
import Flight from '../../src/model/Flight';
import { Trip } from '../../src/model/Trip';
import { UserTrip } from '../../src/model/UserTrip';
import User from '../../src/model/User';
import { FlightResponse, FlightInfo } from '../../src/interface/lumo';
const mockGetPlacesInAirport = jest.fn();
const mockGetNextFlight = jest.fn();

const mockGetCityName = jest.fn(async (a) => 'Tel Aviv');

jest.mock('@googlemaps/google-maps-services-js', () => ({
  Client: jest.fn().mockImplementation(() => {
    return {
      distancematrix: jest.fn().mockResolvedValue({
        status: 'OK',
        duration: {
          text: '2 hours',
        },
      }),
    };
  }),
  TravelMode: {},
}));

jest.mock('../../src/service/Dialogflow.service', () => {
  return {
    DialogflowService: jest.fn().mockImplementation(() => {
      return {
        getSessionPath: jest
          .fn()
          .mockImplementation(() => 'whatsapp:+123456789_whatsapp:+987654321'),
        createContext: jest.fn().mockImplementation(),
        contextClient: {
          createContext: jest.fn(),
        },
      };
    }),
  };
});
jest.mock(
  '../../src/service/Direction.service',
  jest.fn().mockImplementation(() => {
    return {
      DirectionService: jest.fn().mockImplementation(() => {
        return {
          getFastestWay: jest.fn().mockResolvedValue({
            status: 'OK',
            duration: {
              text: '2 hours',
            },
          }),
        };
      }),
    };
  }),
);
let mockGetDescription = jest.fn().mockReturnValue('few clouds');
jest.mock(
  '../../src/service/WeatherTime.service',
  jest.fn().mockImplementation(() => {
    return {
      WeatherTime: jest.fn().mockImplementation(() => {
        return {
          getTemperature: jest.fn().mockReturnValue(303.15),
          getWeatherDescription: mockGetDescription,
        };
      }),
    };
  }),
);

jest.mock('../../src/service/JackAndFerdi.service', () => {
  return {
    JackAndFerdiService: jest.fn().mockImplementation(() => {
      return {
        getPlacesInAirport: mockGetPlacesInAirport,
        getPlaces: jest.fn((city: string, tags: Array<string>) => {
          return `${tags} in ${city}`;
        }),
        getCity: jest.fn((city: string) => {
          return city;
        }),
        getCityName: mockGetCityName,
      };
    }),
  };
});

jest.mock(
  '../../src/service/Flight.service',
  jest.fn().mockImplementation(() => {
    return {
      FlightService: jest.fn().mockImplementation(() => {
        return {
          getNextFlightByUser: mockGetNextFlight.mockResolvedValue(null),
          getNextDepartureFlight: jest.fn(async () => flight1),
        };
      }),
    };
  }),
);

jest.mock(
  '../../src/bot-server',
  jest.fn().mockImplementation(() => {
    return {
      kelvinToFahrenheit: jest.fn((a) => (a * 9) / 5 - 459.67),
      kelvinToCelsius: jest.fn((a) => a - 273.15),
      sendMessage: jest.fn(),
      getRiskWord: jest.fn((risk) => {
        if (risk < 20) {
          return 'low';
        }
        if (risk < 60) {
          return 'moderate';
        }
        if (risk < 100) {
          return 'high';
        }
        /*istanbul ignore next*/
        return 'severe';
      }),
    };
  }),
);

import { sendMessage } from '../../src/bot-server';
import { AirtableFactory } from '../../src/factory/Airtable.factory';
import { AirtableService } from '../../src/service/airtable/Airtable';
import { allowedNodeEnvironmentFlags } from 'process';
import { FlightStatus } from '../../src/interface/flightStats';
import { doesNotMatch } from 'assert';
import CarrierService from '../../src/service/Carrier.service';

let spy = jest.spyOn({ sendMessage }, 'sendMessage');

const trip1 = new Trip({
  flight_trip_id: 1,
  trip_id: 1,
  sent_24hrs: false,
  sent_4hrs: false,
  sent_landed: false,
  sent_directions: false,
  sent_feedback: false,
});

const mockedTrips = [trip1];

const user1 = new User({
  name: 'nati',
  firstName: 'nati',
  id: 5,
  mobile: '012smile',
  whatsapp: '012smile',
  firestoreData: '{"has_whatsapp" : true}',
  confirmed_messaging: true,
});

const usertrip = new UserTrip({
  trip_id: 1,
  user_id: 5,
});

const flight1 = new Flight();
flight1.destination = 'TLV';
flight1.scheduledDeparture = new Date();
flight1.scheduledDeparture.setDate(flight1.scheduledDeparture.getDate() + 1);
const flightData = {
  origin: {
    iata: 'JFK',
    city: 'New york',
    name: 'John F. Kennedy International Airport',
  },
  destination: {
    iata: 'TLV',
    city: 'Tel Aviv',
    name: 'Ben gurion',
  },
} as FlightInfo;
const weatherData = { destination: { summary: 'Cloudy' } };
flight1.initialLumoData = {
  flight: flightData,
  weather: weatherData,
} as FlightResponse;

flight1.initialFlightStatsData = {
  departureAirport: {
    name: 'JFK',
    countryName: 'United states',
  },
  arrivalAirport: {
    name: 'Ben Gurion',
    iata: 'TLV',
  },
} as FlightStatus;
flight1.lumoRisk = '5';
flight1.origin = 'JFK';
flight1.originGate = '5';
flight1.flightNo = 'ua90';
flight1.trips = mockedTrips;
const mockedUsers = [user1];
const mockedFlights = [flight1];
const mockedUserTrips = [usertrip];

const mockrepositoryFlight = new Repository<Flight>();
mockrepositoryFlight.find = jest.fn(
  async (): Promise<Flight[]> => {
    return mockedFlights;
  },
);

const mockrepositoryUserTrip = new Repository<UserTrip>();
mockrepositoryUserTrip.find = jest.fn(
  async (options: any): Promise<UserTrip[]> => {
    return mockedUserTrips.filter(
      (usertrip) => options.where.trip_id == usertrip.trip_id,
    );
  },
);

const mockrepositoryUser = new Repository<User>();
mockrepositoryUser.find = jest.fn(
  async (options: any): Promise<User[]> => {
    return mockedUsers.filter((user) => {
      let flag = true;
      Object.keys(options.where).map((item) => {
        if (item === 'id' && !options.where[item]._value.includes(user.id)) {
          flag = false;
        }
        if (
          item === 'confirmed_messaging' &&
          !options.where[item] === user.confirmed_messaging
        ) {
          flag = false;
        }
      });
      return flag;
    });
  },
);
mockrepositoryUser.save = jest.fn();

const mockrepositoryTrip = new Repository<Trip>();
mockrepositoryTrip.save = async <T extends DeepPartial<Trip>>(
  entity: T,
  options: SaveOptions,
): Promise<T> => {
  if (entity.flight_trip_id === mockedTrips[0].flight_trip_id) {
    mockedTrips[0] = entity as Trip;
  }
  return mockedTrips[0] as T;
};
let falseDb: Connection;

/*
jest.mock('../../src/factory/Airtable.factory',() =>{
    AirtableFactory: {
      create: jest.fn().mockResolvedValue()
    }
  }
)
*/

const mockAirTableService = AirtableFactory.create();
mockAirTableService.getMessageForAirport = jest.fn();
mockAirTableService.getValidPickups = jest
  .fn()
  .mockReturnValueOnce([{ fields: { 'IATA Code': 'bubu' } }])
  .mockReturnValueOnce({ fields: {} });
mockAirTableService.getAirlinesWebsitesLink = jest
  .fn()
  .mockReturnValue([{ fields: { Link: 'https://airlinelink.com' } }]);

let pushNotiService: pushNotificationsService;
describe('Push notification service', () => {
  beforeAll(async () => {
    pushNotiService = new pushNotificationsService(falseDb);
    pushNotiService.flightRepository = mockrepositoryFlight;
    pushNotiService.userRepository = mockrepositoryUser;
    pushNotiService.userTripRepository = mockrepositoryUserTrip;
    pushNotiService.tripRepository = mockrepositoryTrip;
    AirtableFactory.create = jest.fn().mockReturnValue(mockAirTableService);
    CarrierService.search = jest.fn().mockReturnValue([{ name: 'DL' }]);
  });
  beforeEach(() => {
    spy.mockClear();
    spy = jest.spyOn({ sendMessage }, 'sendMessage');
  });

  it('24 hr noti', async () => {
    await pushNotiService.dayBeforeNotifications(
      await pushNotiService.getFlights(),
      new Date(),
    );
    expect(spy).toBeCalledTimes(3);
    expect(spy).toHaveBeenNthCalledWith(
      1,
      'whatsapp:+' + user1.mobile,
      'whatsapp:+' + process.env.PRIMARY_PHONE,
      'whatsapp',
      `nati, your flight to Tel Aviv is in only 24 hours, how exciting! ✈️\n\nI’ve done some work behind the scenes (just a quick analysis of thousands of unique data points regarding your flight, no biggie) and your flight looks fine and I will let you know if anything changes.`,
      pushNotiService.services,
    );
    expect(spy).toHaveBeenNthCalledWith(
      2,
      'whatsapp:+' + user1.mobile,
      'whatsapp:+' + process.env.PRIMARY_PHONE,
      'whatsapp',
      `BTW, don’t forget to *check in*!\nUse this link for the DL website: https://airlinelink.com`,
      pushNotiService.services,
    );
    expect(spy).toHaveBeenNthCalledWith(
      3,
      'whatsapp:+' + user1.mobile,
      'whatsapp:+' + process.env.PRIMARY_PHONE,
      'whatsapp',
      `The weather in Tel Aviv is cloudy with a temperature of 86°F / 30°C.\nDon’t forget to take your scarf 🧣`,
      pushNotiService.services,
    );
  });

  it('24 hr noti with unknown description', async () => {
    flight1.scheduledDeparture = new Date();
    flight1.scheduledDeparture.setDate(
      flight1.scheduledDeparture.getDate() + 1,
    );
    trip1.sent_24hrs = false;
    mockGetDescription.mockReturnValue('bla');
    await pushNotiService.dayBeforeNotifications(
      await pushNotiService.getFlights(),
      new Date(),
    );
    expect(spy).toBeCalledTimes(3);
    expect(spy).toHaveBeenNthCalledWith(
      1,
      'whatsapp:+' + user1.mobile,
      'whatsapp:+' + process.env.PRIMARY_PHONE,
      'whatsapp',
      `nati, your flight to Tel Aviv is in only 24 hours, how exciting! ✈️\n\nI’ve done some work behind the scenes (just a quick analysis of thousands of unique data points regarding your flight, no biggie) and your flight looks fine and I will let you know if anything changes.`,
      pushNotiService.services,
    );
    expect(spy).toHaveBeenNthCalledWith(
      2,
      'whatsapp:+' + user1.mobile,
      'whatsapp:+' + process.env.PRIMARY_PHONE,
      'whatsapp',
      `BTW, don’t forget to *check in*!\nUse this link for the DL website: https://airlinelink.com`,
      pushNotiService.services,
    );

    expect(spy).toHaveBeenNthCalledWith(
      3,
      'whatsapp:+' + user1.mobile,
      'whatsapp:+' + process.env.PRIMARY_PHONE,
      'whatsapp',
      `The weather in Tel Aviv is bla with a temperature of 86°F / 30°C.`,
      pushNotiService.services,
    );
  });
  it('sends a message 4 hours before departure', async () => {
    flight1.scheduledDeparture = new Date();

    flight1.scheduledDeparture.setHours(
      flight1.scheduledDeparture.getHours() + 4,
    );

    flight1.estimatedDeparture = new Date();
    flight1.estimatedDeparture.setHours(
      flight1.estimatedDeparture.getHours() + 4,
    );
    flight1.estimatedDeparture.setMinutes(
      flight1.estimatedDeparture.getMinutes() - 3,
    );

    await pushNotiService.predepartureMessages(
      await pushNotiService.getFlights(),
      new Date(),
    );

    expect(spy).toBeCalledTimes(2);

    const departureMessage = {
      to: spy.mock.calls[0][0],
      from: spy.mock.calls[0][1],
      source: spy.mock.calls[0][2],
      message: spy.mock.calls[0][3],
    };

    const gateMessage = spy.mock.calls[1][3];

    expect(departureMessage.to).toEqual('whatsapp:+' + user1.mobile);
    expect(departureMessage.from).toEqual(
      'whatsapp:+' + process.env.PRIMARY_PHONE,
    );
    expect(departureMessage.source).toEqual('whatsapp');
    expect(departureMessage.message).toContain("It's time to fly! ✈️");
    expect(departureMessage.message).toMatch(/3 minutes/);
    expect(departureMessage.message).toMatch(/Cloudy/);
    expect(departureMessage.message).toMatch(/www.google.com/);

    expect(gateMessage).toEqual('Your flight will depart from gate 5');
  });

  it('2 days before notification', async () => {
    flight1.trips[0].sent_48hrs = false;
    (flight1.scheduledDeparture = new Date()),
      flight1.scheduledDeparture.setDate(
        flight1.scheduledDeparture.getDate() + 2,
      );

    await pushNotiService.twoDaysBeforeNotifications(
      await pushNotiService.getFlights(),
      new Date(),
    );
    expect(spy).toBeCalledTimes(1);
    expect(spy).toHaveBeenNthCalledWith(
      1,
      'whatsapp:+' + user1.mobile,
      'whatsapp:+' + process.env.PRIMARY_PHONE,
      'whatsapp',
      'Hi nati, have you planned your arrival at TLV? I can help you pre-book a taxi from the airport to your final destination. Our partners at WelcomePickups provide a professional, safe and clean transfer service that will wait for you upon arrival. You can check it out at https://transfers.zenner.ai/.',
      pushNotiService.services,
    );
  });
  it('2 days before not supported pickup airport (no notification)', async () => {
    await pushNotiService.twoDaysBeforeNotifications(
      await pushNotiService.getFlights(),
      new Date(),
    );
    expect(spy).not.toBeCalled();
  });
  it('7 hours (no notification)', async () => {
    (flight1.scheduledDeparture = new Date()),
      flight1.scheduledDeparture.setHours(
        flight1.scheduledDeparture.getHours() + 7,
      );
    await pushNotiService.chrono();
    expect(spy).not.toBeCalled();
  });
  it('3 hr noti(both gate and terminal)', async () => {
    flight1.trips[0].sent_4hrs = false;
    flight1.scheduledDeparture = new Date();

    flight1.scheduledDeparture.setHours(
      flight1.scheduledDeparture.getHours() + 4,
    );
    flight1.originTerminal = '7';
    flight1.estimatedDeparture = new Date();
    flight1.estimatedDeparture.setHours(
      flight1.estimatedDeparture.getHours() + 4,
    );
    flight1.estimatedDeparture.setMinutes(
      flight1.estimatedDeparture.getMinutes() - 3,
    );

    await pushNotiService.predepartureMessages(
      await pushNotiService.getFlights(),
      new Date(),
    );
    expect(spy).toBeCalledWith(
      'whatsapp:+' + user1.mobile,
      'whatsapp:+' + process.env.PRIMARY_PHONE,
      'whatsapp',
      'Your flight will depart from terminal 7 gate 5',
      pushNotiService.services,
    );
  });
  it('3 hr noti(terminal)', async () => {
    flight1.trips[0].sent_4hrs = false;
    flight1.scheduledDeparture = new Date();

    flight1.scheduledDeparture.setHours(
      flight1.scheduledDeparture.getHours() + 4,
    );
    flight1.originGate = null;
    flight1.originTerminal = '7';
    flight1.estimatedDeparture = new Date();
    flight1.estimatedDeparture.setHours(
      flight1.estimatedDeparture.getHours() + 4,
    );
    flight1.estimatedDeparture.setMinutes(
      flight1.estimatedDeparture.getMinutes() - 3,
    );
    await pushNotiService.predepartureMessages(
      await pushNotiService.getFlights(),
      new Date(),
    );

    expect(spy).toBeCalledWith(
      'whatsapp:+' + user1.mobile,
      'whatsapp:+' + process.env.PRIMARY_PHONE,
      'whatsapp',
      'Your flight will depart from terminal 7',
      pushNotiService.services,
    );
  });

  it('baggage', async () => {
    flight1.trips[0].sent_baggage = false;
    flight1.estimatedArrival = new Date();
    flight1.statusName = 'landed';
    flight1.estimatedArrival.setMinutes(
      flight1.estimatedArrival.getMinutes() - 2,
    );
    flight1.destinationBaggage = '2';
    await pushNotiService.baggageNotification(
      await pushNotiService.getFlights(),
      new Date(),
    );

    expect(spy).toBeCalledWith(
      'whatsapp:+' + user1.mobile,
      'whatsapp:+' + process.env.PRIMARY_PHONE,
      'whatsapp',
      'If you need to collect bags, head over to baggage claim 2.\nI will keep you updated if there is any change.',
      pushNotiService.services,
    );
  });
  it('Directions', async () => {
    flight1.trips[0].sent_directions = false;
    flight1.estimatedArrival = new Date();
    flight1.statusName = 'landed';
    flight1.estimatedArrival.setMinutes(
      flight1.estimatedArrival.getMinutes() - 15,
    );
    await pushNotiService.directionsNotification(
      await pushNotiService.getFlights(),
      new Date(),
    );
    expect(spy).toBeCalledTimes(1);
    expect(spy).toBeCalledWith(
      'whatsapp:+' + user1.mobile,
      'whatsapp:+' + process.env.PRIMARY_PHONE,
      'whatsapp',
      'Getting from Ben Gurion to Tel Aviv should take you now approximately 2 hours by car or taxi 🚕.\nWith public transportation it would take you 2 hours.\nWatch the journey here: https://www.google.com/maps/dir/?api=1&origin=Ben%20Gurion(TLV)&destination=Tel%20Aviv,%20undefined',
      pushNotiService.services,
    );
  });
  it('JNF', async () => {
    flight1.estimatedArrival = new Date();
    flight1.statusName = 'landed';
    flight1.estimatedArrival.setMinutes(
      flight1.estimatedArrival.getMinutes() - 16,
    );
    await pushNotiService.JNFNotification(
      await pushNotiService.getFlights(),
      new Date(),
    );
    expect(spy).toBeCalledWith(
      'whatsapp:+' + user1.mobile,
      'whatsapp:+' + process.env.PRIMARY_PHONE,
      'whatsapp',
      'BTW, I have some great tips about Tel Aviv, such as:\n\nTips for staying healthy 🏃‍♂️\nPlaces to meditate/unwind 🧘‍♂️\nLocal business manners 👔\nDuring your stay in the city you can ask me about it by writing something like: “Where can I meditate in Tel Aviv?“',
      pushNotiService.services,
    );
  });

  it('JNF - city not found', async () => {
    mockGetCityName.mockResolvedValue(undefined);
    flight1.trips[0].sent_JNF = false;
    flight1.estimatedArrival = new Date();
    flight1.statusName = 'landed';
    flight1.estimatedArrival.setMinutes(
      flight1.estimatedArrival.getMinutes() - 16,
    );
    await pushNotiService.JNFNotification(
      await pushNotiService.getFlights(),
      new Date(),
    );
    expect(spy).not.toBeCalled();
  });

  it('FeedBack', async () => {
    flight1.estimatedArrival = new Date();
    flight1.statusName = 'landed';
    flight1.estimatedArrival.setMinutes(
      flight1.estimatedArrival.getMinutes() - 60,
    );
    await pushNotiService.FeedBackNotifications(
      await pushNotiService.getFlights(),
      new Date(),
    );
    expect(spy).toBeCalledWith(
      'whatsapp:+' + user1.mobile,
      'whatsapp:+' + process.env.PRIMARY_PHONE,
      'whatsapp',
      "nati, as I always want to improve myself, I'd greatly appreciate your feedback 💃\nHow would you rate the overall experience with Zenny as your travel assistant? Please rate it from 1 (worst) to 5 (best)?",
      pushNotiService.services,
    );
  });

  it('FeedBack should not be sent(because the current time is not between 60-70 minutes after the flight arrived)', async () => {
    trip1.sent_feedback = false;
    flight1.estimatedArrival = new Date();
    flight1.statusName = 'landed';
    flight1.estimatedArrival.setMinutes(
      flight1.estimatedArrival.getMinutes() - 10,
    );
    await pushNotiService.FeedBackNotifications(
      await pushNotiService.getFlights(),
      new Date(),
    );
    expect(spy).not.toBeCalled();
    flight1.estimatedArrival = new Date();
    flight1.statusName = 'landed';
    flight1.estimatedArrival.setMinutes(
      flight1.estimatedArrival.getMinutes() - 100,
    );
    await pushNotiService.FeedBackNotifications(
      await pushNotiService.getFlights(),
      new Date(),
    );
    expect(spy).not.toBeCalled();
  });
  it('feedback when user has another flight in more than 7 days', async () => {
    trip1.sent_feedback = false;
    flight1.scheduledDeparture = new Date();
    flight1.scheduledDeparture.setDate(new Date().getDate() + 10);
    mockGetNextFlight.mockResolvedValue(flight1);
    flight1.estimatedArrival = new Date();
    flight1.statusName = 'landed';
    flight1.estimatedArrival.setMinutes(
      flight1.estimatedArrival.getMinutes() - 60,
    );
    await pushNotiService.FeedBackNotifications(
      await pushNotiService.getFlights(),
      new Date(),
    );
    expect(spy).toBeCalledWith(
      'whatsapp:+' + user1.mobile,
      'whatsapp:+' + process.env.PRIMARY_PHONE,
      'whatsapp',
      "nati, as I always want to improve myself, I'd greatly appreciate your feedback 💃\nHow would you rate the overall experience with Zenny as your travel assistant? Please rate it from 1 (worst) to 5 (best)?",
      pushNotiService.services,
    );
  });

  it('feedback when user has another flight in less than 7 days', async () => {
    trip1.sent_feedback = false;
    flight1.scheduledDeparture = new Date();
    flight1.scheduledDeparture.setDate(new Date().getDate() + 5);
    mockGetNextFlight.mockResolvedValue(flight1);
    flight1.estimatedArrival = new Date();
    flight1.statusName = 'landed';
    flight1.estimatedArrival.setMinutes(
      flight1.estimatedArrival.getMinutes() - 60,
    );
    await pushNotiService.FeedBackNotifications(
      await pushNotiService.getFlights(),
      new Date(),
    );
    expect(spy).not.toBeCalled();
  });
  it('24 hrs after user havent replied ', async () => {
    user1.confirmed_messaging = false;
    user1.sent_24afterRegister = false;
    user1.created = new Date();
    user1.created.setDate(new Date().getDate() - 1);
    user1.created.setHours(user1.created.getHours() - 1);
    await pushNotiService.dayAfterRegisterNotification(
      await pushNotiService.getUsersThatNotConfirmed(),
      new Date(),
    );

    expect(spy.mock.calls[0][3]).toContain("Hey, don't leave me hanging");
  });

  it('24 hrs before flight, user havent replied ', async () => {
    user1.confirmed_messaging = false;
    user1.sent_24beforeFlight = false;
    user1.created = new Date();
    user1.created.setDate(new Date().getDate() - 1);
    flight1.estimatedDeparture = new Date();
    flight1.estimatedDeparture.setDate(new Date().getDate() + 1);
    flight1.scheduledDeparture = new Date();
    flight1.scheduledDeparture.setDate(new Date().getDate() + 1);
    await pushNotiService.dayBeforeFlightUserNotConfirmed(
      await pushNotiService.getUsersThatNotConfirmed(),
      new Date(),
    );

    expect(spy.mock.calls[0][3]).toContain('As JRR Tolkien once wrote:');
  });

  it('24 hrs before flight, user havent replied and the time difference between the creation of the user and the departure of the flight is less then 27 hours ', async () => {
    const today = new Date();
    user1.confirmed_messaging = false;
    user1.sent_24afterRegister = false;
    user1.sent_24beforeFlight = false;
    user1.created = new Date();
    user1.created.setHours(new Date().getHours() - 2);
    flight1.estimatedDeparture = new Date();
    flight1.estimatedDeparture.setDate(new Date().getDate() + 1);
    flight1.scheduledDeparture = new Date();
    flight1.scheduledDeparture.setDate(new Date().getDate() + 1);
    today.setMinutes(today.getMinutes() + 5);
    await pushNotiService.dayBeforeFlightUserNotConfirmed(
      await pushNotiService.getUsersThatNotConfirmed(),
      today,
    );

    expect(spy.mock.calls[0][3]).toContain("Hey, don't leave me hanging");
  });

  it('T-2 , origin airport is not support by JNF', async () => {
    mockGetPlacesInAirport.mockResolvedValueOnce(
      "Sorry, I couldn't find this type of place in JFK",
    );
    const today = new Date();
    trip1.sent_JNFAirport = false;
    user1.confirmed_messaging = true;
    flight1.estimatedDeparture = new Date();
    flight1.estimatedDeparture.setHours(new Date().getHours() + 2);
    flight1.scheduledDeparture = new Date();
    flight1.scheduledDeparture.setHours(new Date().getHours() + 2);
    today.setMinutes(today.getMinutes() + 5);
    await pushNotiService.airportPlacesNotification(
      await pushNotiService.getFlights(),
      today,
    );
    expect(spy).not.toBeCalled();
  });
  it('T-2 , origin airport is support by JNF', async () => {
    mockGetPlacesInAirport.mockResolvedValueOnce('#airport places#');
    const today = new Date();
    trip1.sent_JNFAirport = false;
    user1.confirmed_messaging = true;
    flight1.estimatedDeparture = new Date();
    flight1.estimatedDeparture.setHours(new Date().getHours() + 2);
    flight1.scheduledDeparture = new Date();
    flight1.scheduledDeparture.setHours(new Date().getHours() + 2);
    today.setMinutes(today.getMinutes() + 5);
    await pushNotiService.airportPlacesNotification(
      await pushNotiService.getFlights(),
      today,
    );
    expect(spy.mock.calls[0][3]).toEqual(
      `Hey, I have suggestions for things to do in the airport. If you'd like to know more, just 👍 or ask me "what can I do in JFK?"`,
    );
  });
  it('T-2 , origin airport is support by JNF but this is 3 hours before the flight', async () => {
    mockGetPlacesInAirport.mockResolvedValueOnce('#airport places#');
    const today = new Date();
    trip1.sent_JNFAirport = false;
    user1.confirmed_messaging = true;
    flight1.estimatedDeparture = new Date();
    flight1.estimatedDeparture.setHours(new Date().getHours() + 3);
    flight1.scheduledDeparture = new Date();
    flight1.scheduledDeparture.setHours(new Date().getHours() + 3);
    today.setMinutes(today.getMinutes() + 5);
    await pushNotiService.airportPlacesNotification(
      await pushNotiService.getFlights(),
      today,
    );
    expect(spy).not.toBeCalled();
  });
});
