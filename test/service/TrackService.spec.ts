import { TrackService } from '../../src/service/Track.service';
import { LumoService } from '../../src/service';
import { Connection } from 'typeorm/index';
import { FlightService } from '../../src/service/Flight.service';
import { FlightStatsService } from '../../src/service/FlightStats.service';

import {
  DelayIndex,
  FlightResponse,
  PredictionRisk,
} from '../../src/interface/lumo';
import User from '../../src/model/User';
import { Trip } from '../../src/model/Trip';
import { UserTrip } from '../../src/model/UserTrip';

jest.mock('typeorm', () => ({
  createConnection: jest.fn(),
  PrimaryGeneratedColumn: jest.fn(),
  PrimaryColumn: jest.fn(),
  OneToOne: jest.fn(),
  ManyToOne: jest.fn(),
  OneToMany: jest.fn(),
  ManyToMany: jest.fn(),
  JoinTable: jest.fn(),
  UpdateDateColumn: jest.fn(),
  CreateDateColumn: jest.fn(),
  Column: jest.fn(),
  Entity: jest.fn(),
  AfterLoad: jest.fn(),
  JoinColumn: jest.fn(),
}));

const fakeDb = ({
  getRepository: jest.fn().mockReturnValue({
    findOne: jest.fn().mockReturnValue({
      trip_id: 1,
      user_id: 1,
    }),
    find: jest.fn().mockReturnValue({
      trip_id: 1,
      user_id: 1,
    }),
    save: jest.fn(),
  }),
  findOne: jest.fn(),
} as unknown) as Connection;
const fakeDBFlight = {
  scheduledDeparture: new Date(),
  destinationBaggage: '5',
  destination: 'JFK',
  originGate: '5',
  id: 1,
  statusName: 'enroute',
  trips: [
    {
      sent_landed: false,
      seq: 1,
    },
    {
      sent_landed: false,
      seq: 1,
    },
    {
      sent_landed: false,
      seq: 1,
    },
  ],
};
const lumoService = new LumoService(fakeDb);
const flightStatsService = new FlightStatsService(fakeDb);
const flightService = new FlightService(fakeDb);
flightService.getAllFlightsByUser = jest
  .fn()
  .mockResolvedValue([fakeDBFlight, fakeDBFlight]);

const delayIndex: DelayIndex = 1;
const risk: PredictionRisk = 'LOW';

const fakeLumoRecord: FlightResponse = {
  alert: {
    change: 'Some change',
    itinerary_id: '1234',
    timestamp: new Date().toISOString(),
  },
  flight: {
    aircraft_type: {
      generic: 'Fl',
    },
    carrier: {
      iata: 'BA',
      name: 'British Airlines',
    },
    destination: {
      iata: 'JFK',
      name: 'JFK',
      city: 'New York',
    },
    id: 'BB113',
    number: 113,
    origin: {
      iata: 'LHR',
      city: 'London',
      name: 'LHR',
    },
    uuid: '123123',
    scheduled_departure: new Date().toISOString(),
    scheduled_arrival: new Date().toISOString(),
  },
  status: {
    cancelled: false,
    text: 'Scheduled',
    arrival: {
      scheduled: new Date().toISOString(),
      latest: 'abc',
      type: 'scheduled',
    },
    departure: {
      scheduled: new Date().toISOString(),
      latest: 'abc',
      type: 'scheduled',
    },
    source: 'flightview',
    last_updated: new Date().toISOString(),
  },
  prediction: {
    delay_index: delayIndex,
    risk,
    distribution: [1],
    causes: ['flight-cancelled'],
    is_override: false,
    connections: [
      {
        airport: 'LHR',
        prob_miss: 1,
        connection_time: 2,
      },
    ],
  },
};
const fakePassenger: User = {
  mutedAt: new Date(),
  lastMessageDate: new Date(),
  id: 1,
  created: new Date(),
  name: 'test',
  firstName: 'test',
  lastName: 'test',
  email: 'test@asdsa.cc',
  mobile: '213123123',
  whatsapp: 'true',
  firestoreId: '1',
  firestoreData: 'data',
  flights: [],
  muteBot: false,
  confirmed_messaging: true,
  sent_24beforeFlight: false,
  sent_24afterRegister: false,
};

describe('Track service test', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  it('Should track single flight', async () => {
    const trackService = new TrackService(
      lumoService,
      flightStatsService,
      flightService,
    );
    const flightStatsSpy = jest
      .spyOn(flightStatsService, 'status')
      .mockImplementation();
    const lumoServiceSpy = jest
      .spyOn(lumoService, 'status')
      .mockImplementation();
    const saveFlightSpy = jest.spyOn(flightService, 'saveFlight');
    const createUserFlightSpy = jest
      .spyOn(flightService, 'createUserTrip')
      .mockResolvedValue(new UserTrip({ trip_id: 1 }));
    const createTripSpy = jest.spyOn(flightService, 'createTrip');
    await trackService.trackSingleFlight(fakeLumoRecord, fakePassenger);
    expect(flightStatsSpy).toBeCalled();
    expect(lumoServiceSpy).toBeCalled();
    expect(saveFlightSpy).toBeCalled();
    expect(createTripSpy).toBeCalled();
    expect(createUserFlightSpy).toBeCalled();
  });

  it('should track connection flights', async () => {
    const trackService = new TrackService(
      lumoService,
      flightStatsService,
      flightService,
    );
    const flightStatsSpy = jest
      .spyOn(flightStatsService, 'status')
      .mockImplementation();
    const lumoServiceSpy = jest
      .spyOn(lumoService, 'status')
      .mockImplementation();
    const saveFlightSpy = jest.spyOn(flightService, 'saveFlight');
    const createUserFlightSpy = jest
      .spyOn(flightService, 'createUserTrip')
      .mockResolvedValue(new UserTrip({ trip_id: 1 }));
    const createTripSpy = jest.spyOn(flightService, 'createTrip');
    await trackService.trackConnectionFlight(
      [fakeLumoRecord, fakeLumoRecord],
      fakePassenger,
    );
    expect(flightStatsSpy).toBeCalledTimes(2);
    expect(lumoServiceSpy).toBeCalledTimes(2);
    expect(saveFlightSpy).toBeCalledTimes(2);
    expect(createTripSpy).toBeCalledTimes(2);
    expect(createUserFlightSpy).toBeCalled();
  });

  it('should track 3 connecting flights', async () => {
    const trackService = new TrackService(
      lumoService,
      flightStatsService,
      flightService,
    );
    const flightStatsSpy = jest
      .spyOn(flightStatsService, 'status')
      .mockImplementation();
    const lumoServiceSpy = jest
      .spyOn(lumoService, 'status')
      .mockImplementation();
    const saveFlightSpy = jest.spyOn(flightService, 'saveFlight');
    const createUserFlightSpy = jest
      .spyOn(flightService, 'createUserTrip')
      .mockResolvedValue(new UserTrip({ trip_id: 1 }));
    const createTripSpy = jest.spyOn(flightService, 'createTrip');
    await trackService.trackConnectionFlight(
      [fakeLumoRecord, fakeLumoRecord, fakeLumoRecord],
      fakePassenger,
    );
    expect(flightStatsSpy).toBeCalledTimes(3);
    expect(lumoServiceSpy).toBeCalledTimes(3);
    expect(saveFlightSpy).toBeCalledTimes(3);
    expect(createTripSpy).toBeCalledTimes(3);
    expect(createUserFlightSpy).toBeCalled();
  });

  it('should track a flight without its connection', async () => {
    const trackService = new TrackService(
      lumoService,
      flightStatsService,
      flightService,
    );
    const flightStatsSpy = jest
      .spyOn(flightStatsService, 'status')
      .mockImplementation();
    const lumoServiceSpy = jest
      .spyOn(lumoService, 'status')
      .mockImplementation();
    const saveFlightSpy = jest.spyOn(flightService, 'saveFlight');
    const createUserFlightSpy = jest
      .spyOn(flightService, 'createUserTrip')
      .mockResolvedValue(new UserTrip({ trip_id: 1 }));
    const createTripSpy = jest.spyOn(flightService, 'createTrip');
    await trackService.trackConnectionFlight([fakeLumoRecord], fakePassenger);
    expect(flightStatsSpy).toBeCalledTimes(1);
    expect(lumoServiceSpy).toBeCalledTimes(1);
    expect(saveFlightSpy).toBeCalledTimes(1);
    expect(createTripSpy).toBeCalledTimes(1);
    expect(createUserFlightSpy).toBeCalled();
  });
});
