import redis from 'redis';

const mockCreateClientSpy = jest.fn();
const mockUseAdapterSpy = jest.fn();

describe('Redis Service', () => {
  redis.createClient = mockCreateClientSpy;
  beforeAll(() => {
    jest.mock('@type-cacheable/redis-adapter', () => ({
      useAdapter: mockUseAdapterSpy,
    }));
  });

  it('Create Client', () => {
    require('../../src/service/Redis.service');

    expect(mockCreateClientSpy).toHaveBeenCalled();
    expect(mockUseAdapterSpy).toHaveBeenCalled();
  });
});
