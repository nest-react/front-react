import axios from 'axios';

import { SitataService } from '../../src/service/Sitata.service';

const sitataService = new SitataService();

describe('Sitata service', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should receive covid summary with correct country ID', async () => {
    const axiosGetSpy = jest.spyOn(axios, 'get').mockResolvedValue({
      data: {},
    });

    await sitataService.getCovidSummary('UA');

    expect(axiosGetSpy).toBeCalled();

    expect(axiosGetSpy).toBeCalledWith('UA/covid19_summary');
  });

  it('covid summary should be null with incorrect country id', async () => {
    const axiosGetSpy = jest.spyOn(axios, 'get').mockRejectedValue(null);

    const result = await sitataService.getCovidSummary('incorrectID');

    expect(axiosGetSpy).toBeCalled();

    expect(axiosGetSpy).toBeCalledWith('incorrectID/covid19_summary');

    expect(result).toBeNull();
  });

  it('should receive country restrictions with correct country ID', async () => {
    const dummyData: any = [
      {
        origin_country_id: 'b50ca34a-2998-47ed-b5da-fc2e0b174328',
        references: [],
        start: '2020-07-03T17:06:11.087587Z',
        type: 3,
        updated_at: '2020-07-03T17:06:11.109434Z',
        value: 11,
      },
      {
        origin_country_id: 'b50ca34a-2998-47ed-b5da-fc2e0b174328',
        references: [],
        start: '2020-07-03T17:06:11.087587Z',
        type: 1,
        updated_at: '2020-07-03T17:06:11.109434Z',
        value: 11,
      },
    ];

    const axiosGetSpy = jest.spyOn(axios, 'get').mockResolvedValue({
      data: dummyData,
    });

    await sitataService.getRestrictions('UA');
    expect(axiosGetSpy).toBeCalled();

    expect(axiosGetSpy).toBeCalledWith('UA/travel_restrictions');
  });

  it('country restrictions should be null with incorrect country ID', async () => {
    const axiosGetSpy = jest.spyOn(axios, 'get').mockRejectedValue({});

    const result = await sitataService.getRestrictions('incorrectID');

    expect(axiosGetSpy).toBeCalled();

    expect(axiosGetSpy).toBeCalledWith('incorrectID/travel_restrictions');

    expect(result).toBeNull();
  });

  it('should receive country travel status with correct country ID', async () => {
    const axiosGetSpy = jest.spyOn(axios, 'get').mockResolvedValue({
      data: {},
    });

    await sitataService.getTravelStatus('UA');
    expect(axiosGetSpy).toBeCalled();

    expect(axiosGetSpy).toBeCalledWith('UA/travel_status');
  });

  it('country travel status should be null with incorrect country ID', async () => {
    const axiosGetSpy = jest.spyOn(axios, 'get').mockRejectedValue({});

    const response = await sitataService.getTravelStatus('incorrectID');

    expect(axiosGetSpy).toBeCalled();

    expect(axiosGetSpy).toBeCalledWith('incorrectID/travel_status');

    expect(response).toBeNull();
  });
});
