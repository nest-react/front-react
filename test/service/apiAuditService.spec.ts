import {
  ApiAuditService,
  AuditRecord,
} from '../../src/service/ApiAudit.service';
import { HttpClient, onResponseMeta } from '../../src/service/HttpClient';
jest.unmock('axios');
import axios from 'axios';

const record: AuditRecord = {
  method: '',
  outgoing: false,
  duration: 0,
  hostname: '',
  request: {
    arg: 1,
  },
  response: {
    res: 1,
  },
  serviceClass: '',
  url: '',
};

describe('Api Audit service', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  it('Create api audit service instance', async () => {
    const falseDb: any = {
      getRepository: jest.fn(),
    };

    new ApiAuditService(falseDb);
    expect(falseDb.getRepository).toBeCalled();
  });

  it('ApiAudit create record successful', async () => {
    const falseDb: any = {
      getRepository: jest.fn().mockImplementation(() => {
        return {
          save: jest.fn().mockResolvedValue({}),
        };
      }),
    };

    const apiAudit = new ApiAuditService(falseDb);

    const result = await apiAudit.log(record);
    expect(result).toBeTruthy();
  });

  it('ApiAudit create record fail', async () => {
    const falseDb: any = {
      getRepository: jest.fn().mockImplementation(() => {
        return {
          save: jest.fn().mockRejectedValue({}),
        };
      }),
    };

    const apiAudit = new ApiAuditService(falseDb);

    const result = await apiAudit.log(record);
    expect(result).toBeFalsy();
  });

  it('ApiAudit service should be able to log HttpClient events', async () => {
    const responseData = {
      test: 1,
    };
    const instance = axios.create({
      adapter: (config) => {
        return Promise.resolve({
          data: responseData,
          status: 200,
          statusText: 'OK',
          headers: {},
          config,
        });
      },
    });

    jest.spyOn(axios, 'create').mockImplementation(() => instance);

    const falseDb: any = {
      getRepository: jest.fn().mockImplementation(() => {
        return {
          save: jest.fn().mockResolvedValue({}),
        };
      }),
    };

    const apiAudit = new ApiAuditService(falseDb);

    const httpClient = new HttpClient();
    const onResponsePromise = new Promise<onResponseMeta>((resolve) => {
      httpClient.on('onResponse', async (httpRequest: onResponseMeta) => {
        resolve(httpRequest);
      });
    });

    const http = httpClient.create();
    await http.get('https://google.com/');

    const httpRequestMeta = await onResponsePromise;
    const result = await apiAudit.log(httpRequestMeta);
    expect(result).toBeTruthy();
  });
});
