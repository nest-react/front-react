import { DialogflowService } from '../../src/service/Dialogflow.service';
import twillliosms from '../../src/interface/twiliosms.interface';

const testData: twillliosms = {
  From: 'whatsapp:13235082016',
  To: 'whatsapp:995591978104',
  Body: 'hi',
};

const testResult = [{ queryResult: 'the most relevant intent' }];

let mockDialogflowService: DialogflowService;
let mockgetResponceSpy = jest.fn();

const mockProjectAgentSessionPath = jest.fn();
const mockDetectIntentSpy = jest.fn(() => testResult);

describe('Dialogflow unit tests', () => {
  beforeAll(() => {
    jest.mock('@google-cloud/dialogflow', () => ({
      v2: {
        SessionsClient: jest.fn(() => ({
          sessionPath: jest.fn(),
          projectAgentSessionPath: mockProjectAgentSessionPath,
          detectIntent: mockDetectIntentSpy,
        })),
        ContextsClient: jest.fn(() => ({
          contextPath: jest.fn(),
          projectAgentSessionPath: mockProjectAgentSessionPath,
        })),
      },
    }));
    const {
      DialogflowService,
    } = require('../../src/service/Dialogflow.service');
    mockDialogflowService = new DialogflowService();
    mockgetResponceSpy = jest.fn(mockDialogflowService.getResponse);
    mockDialogflowService.getResponse = mockgetResponceSpy;
  });

  it('Dialogflow Unit test', async () => {
    // Act
    await mockDialogflowService.getResponse(
      testData.From,
      testData.To,
      testData.Body,
      'whatsapp',
      'test-new-flight',
    );

    // Assert
    expect(mockgetResponceSpy).toHaveBeenCalled();
    expect(mockProjectAgentSessionPath).toBeCalled();
    expect(mockDetectIntentSpy).toBeCalled();
    expect(mockDetectIntentSpy).toHaveReturnedWith(testResult);
    // expect(mockgetResponceSpy).toHaveReturnedWith('the most relevant intent');   // ---have no idea why it fails
  });
});
