describe('Date Time helpers', () => {
  const DT = require('../../src/util/datetime.utils');

  beforeAll(() => {});

  afterEach(() => {});

  afterAll(() => {});

  it('Gets the number of minutes correctly', async () => {
    const current = new Date();
    const departure = new Date(current.getTime() + 20 * 60 * 1000);
    const arrival = new Date(departure.getTime() + 20 * 60 * 1000);

    expect(DT.getDiffMinutes(departure, current)).toEqual(20); // Current should be before departure
    expect(DT.getDiffMinutes(departure, arrival)).toEqual(-20); // Departure should be after arrival
  });

  it('Prints a date string nicely', async () => {
    const d = new Date();
    let mins = d.getMinutes() < 10 ? '0' : '';
    mins += d.getMinutes();
    const referenceString =
      d.getHours() +
      ':' +
      mins +
      ', ' +
      d.getDate() +
      '-' +
      (d.getMonth() + 1) +
      '-' +
      d.getFullYear();

    expect(DT.getNiceTimeDateString(d.toISOString())).toEqual(referenceString);
  });

  it('Formats minutes into a string', async () => {
    expect(DT.getNiceMinsDiffString(1570)).toEqual('1 day and 2 hours');
    expect(DT.getNiceMinsDiffString(1510)).toEqual('1 day and 1 hour');
    expect(DT.getNiceMinsDiffString(2940)).toEqual('2 days and 1 hour');
    expect(DT.getNiceMinsDiffString(3000)).toEqual('2 days and 2 hours');
    expect(DT.getNiceMinsDiffString(1440)).toEqual('1 day');
    expect(DT.getNiceMinsDiffString(4320)).toEqual('3 days');
    expect(DT.getNiceMinsDiffString(70)).toEqual('1 hour and 10 minutes');
    expect(DT.getNiceMinsDiffString(130)).toEqual('2 hours and 10 minutes');
    expect(DT.getNiceMinsDiffString(120)).toEqual('2 hours');
    expect(DT.getNiceMinsDiffString(60)).toEqual('1 hour');
    expect(DT.getNiceMinsDiffString(61)).toEqual('1 hour and 1 minute');
    expect(DT.getNiceMinsDiffString(121)).toEqual('2 hours and 1 minute');
    expect(DT.getNiceMinsDiffString(1)).toEqual('1 minute');
    expect(DT.getNiceMinsDiffString(10)).toEqual('10 minutes');
  });
});
