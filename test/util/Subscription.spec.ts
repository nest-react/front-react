import {
  gateChanged,
  gateHasntChanged,
} from '../../src/util/Subscription.util';

describe('Subscription Utilities', () => {
  it('Gate number given but we already showed the terminal', () => {
    expect(gateHasntChanged('G07', '7')).toEqual(true);
    expect(gateHasntChanged('G07', '07')).toEqual(true);
    expect(gateHasntChanged('07', 'G07')).toEqual(true);
  });

  it('Same gate number shown but with leading zeros', () => {
    expect(gateHasntChanged('07', '7')).toEqual(true);
    expect(gateHasntChanged('7', '07')).toEqual(true);
  });

  it('New letter in the Gate, could mean a new terminal', () => {
    expect(gateHasntChanged('G07', 'H07')).toEqual(false);
  });

  it('Handle partial gate matches', () => {
    expect(gateHasntChanged('01', '11')).toEqual(false);
    expect(gateHasntChanged('1', '1')).toEqual(true);
  });

  it('Handle nulls and empties', () => {
    expect(gateHasntChanged(null, '1')).toEqual(false);
    expect(gateHasntChanged('', '1')).toEqual(false);
    expect(gateHasntChanged(null, null)).toEqual(true);
    expect(gateHasntChanged('1', null)).toEqual(true);
  });

  it('Inverse function', () => {
    expect(gateChanged(null, '1')).toEqual(true);
    expect(gateChanged('', '1')).toEqual(true);
    expect(gateChanged('', '')).toEqual(false);
    expect(gateChanged(null, null)).toEqual(false);
    expect(gateChanged('1', null)).toEqual(false);
  });
});
