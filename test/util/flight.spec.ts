import { firstName, tConvert } from '../../src/util/flight.utils';

describe('flight-util', () => {
  it('firstName should be expected', () => {
    expect(firstName('Al Bal')).toBe('Al');
    expect(firstName('adam smith morgan')).toBe('adam');
  });

  it('should return converted flight date', () => {
    expect(tConvert('14:00')).toBe('2:00 PM');
    expect(tConvert('10:00')).toBe('10:00 AM');
  });
});
