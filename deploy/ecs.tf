resource "aws_ecs_cluster" "main" {
  name = "${local.prefix}-cluster"

  tags = local.common_tags
}

resource "aws_iam_policy" "task_execution_role_policy" {
  name        = "${local.prefix}-task-exec-role-policy"
  path        = "/"
  description = "Allow retrieving images and adding to logs"
  policy      = file("./templates/ecs/task-exec-role.json")
}

resource "aws_iam_role" "task_execution_role" {
  name               = "${local.prefix}-task-exec-role"
  assume_role_policy = file("./templates/ecs/assume-role-policy.json")
}

resource "aws_iam_role_policy_attachment" "task_execution_role" {
  role       = aws_iam_role.task_execution_role.name
  policy_arn = aws_iam_policy.task_execution_role_policy.arn
}

resource "aws_iam_role" "app_iam_role" {
  name               = "${local.prefix}-core-task"
  assume_role_policy = file("./templates/ecs/assume-role-policy.json")

  tags = local.common_tags
}

resource "aws_cloudwatch_log_group" "ecs_task_logs" {
  name = "${local.prefix}-core"

  tags = local.common_tags
}

data "template_file" "core_container_definitions" {
  template = file("templates/ecs/container-definitions.json.tpl")

  vars = {
    allowed_hosts             = aws_route53_record.app.fqdn
    app_image                 = var.ecr_image_core
    db_host                   = aws_db_instance.main.address
    db_name                   = aws_db_instance.main.name
    db_user                   = aws_db_instance.main.username
    db_pass                   = aws_db_instance.main.password
    redis_host                = aws_elasticache_cluster.redis.cache_nodes.0.address
    log_group_name            = aws_cloudwatch_log_group.ecs_task_logs.name
    log_group_region          = data.aws_region.current.name
    node_env                  = var.node_env
    twilio_sid                = var.twilio_sid
    twilio_token              = var.twilio_token
    openweatehrmap_api_key    = var.openweatehrmap_api_key
    front_plugin_secret       = var.front_plugin_secret
    front_twilio              = var.front_twilio
    front_api_token           = var.front_api_token
    front_api_key             = var.front_api_key
    slack_flightevents_shu    = var.slack_flightevents_shu
    slack_dialogflow_shu      = var.slack_dialogflow_shu
    slack_onboarding_shu      = var.slack_onboarding_shu
    jackandferdi_token        = var.jackandferdi_token
    dialog_maps_api           = var.dialog_maps_api
    google_api_key            = var.google_api_key
    google_distancematrix_key = var.google_distancematrix_key
    sitata_organization       = var.sitata_organization
    sitata_authorization      = var.sitata_authorization
    lumo_token                = var.lumo_token
    df_private_key_id         = var.df_private_key_id
    df_private_key            = var.df_private_key
    flightstats_app_key       = var.flightstats_app_key
    airtable_api_key          = var.airtable_api_key
    gmail_private_key         = var.gmail_private_key
  }
}

resource "aws_ecs_task_definition" "core" {
  family                   = "${local.prefix}-core"
  container_definitions    = data.template_file.core_container_definitions.rendered
  requires_compatibilities = ["FARGATE"]
  network_mode             = "awsvpc"
  cpu                      = 1024
  memory                   = 2048
  execution_role_arn       = aws_iam_role.task_execution_role.arn
  task_role_arn            = aws_iam_role.app_iam_role.arn
  volume {
    name = "logs"
  }

  tags = local.common_tags
}

resource "aws_security_group" "ecs_service" {
  description = "Access for the ECS service"
  name        = "${local.prefix}-ecs-service"
  vpc_id      = aws_vpc.main.id

  egress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port = 5432
    to_port   = 5432
    protocol  = "tcp"
    cidr_blocks = [
      aws_subnet.private_a.cidr_block,
      aws_subnet.private_b.cidr_block,
    ]
  }

  egress {
    from_port = 6379
    to_port   = 6379
    protocol  = "tcp"
    cidr_blocks = [
      aws_subnet.private_a.cidr_block,
      aws_subnet.private_b.cidr_block,
    ]
  }

  ingress {
    from_port = 8080
    to_port   = 8080
    protocol  = "tcp"
    security_groups = [
      aws_security_group.lb.id
    ]
  }

  tags = local.common_tags
}

resource "aws_ecs_service" "core" {
  name                 = "${local.prefix}-core"
  cluster              = aws_ecs_cluster.main.name
  task_definition      = aws_ecs_task_definition.core.family
  desired_count        = 1
  launch_type          = "FARGATE"
  depends_on           = [aws_lb_listener.core_https]
  force_new_deployment = true

  network_configuration {
    subnets = [
      aws_subnet.private_a.id,
      aws_subnet.private_b.id,
    ]
    security_groups = [aws_security_group.ecs_service.id]
  }
  load_balancer {
    target_group_arn = aws_lb_target_group.core.arn
    container_name   = "core"
    container_port   = 8080
  }
}
