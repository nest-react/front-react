[
    {
        "name": "core",
        "image": "${app_image}",
        "essential": true,
        "memoryReservation": 1024,
        "environment": [
            {"name": "DB_HOST", "value": "${db_host}"},
            {"name": "DB_NAME", "value": "${db_name}"},
            {"name": "DB_USER", "value": "${db_user}"},
            {"name": "DB_PASS", "value": "${db_pass}"},
            {"name": "REDIS_HOST", "value": "${redis_host}"},
            {"name": "REDIS_PORT", "value": "6379"},
            {"name": "NODE_ENV", "value": "${node_env}"},
            {"name": "TWILIO_SID", "value": "${twilio_sid}"},
            {"name": "TWILIO_TOKEN", "value": "${twilio_token}"},
            {"name": "OPENWEATEHRMAP_API_KEY", "value": "${openweatehrmap_api_key}"},
            {"name": "FRONT_PLUGIN_SECRET", "value": "${front_plugin_secret}"},
            {"name": "FRONT_TWILIO", "value": "${front_twilio}"},
            {"name": "FRONT_API_TOKEN", "value": "${front_api_token}"},
            {"name": "FRONT_API_KEY", "value": "${front_api_key}"},
            {"name": "SLACK_FLIGHTEVENTS_SHU", "value": "${slack_flightevents_shu}"},
            {"name": "SLACK_DIALOGFLOW_SHU", "value": "${slack_dialogflow_shu}"},
            {"name": "SLACK_ONBOARDING_SHU", "value": "${slack_onboarding_shu}"},
            {"name": "JACKANDFERDI_TOKEN", "value": "${jackandferdi_token}"},
            {"name": "DIALOG_MAPS_API", "value": "${dialog_maps_api}"},
            {"name": "GOOGLE_API_KEY", "value": "${google_api_key}"},
            {"name": "GOOGLE_DISTANCEMATRIX_KEY", "value": "${google_distancematrix_key}"},
            {"name": "SITATA_ORGANIZATION", "value": "${sitata_organization}"},
            {"name": "SITATA_AUTHORIZATION", "value": "${sitata_authorization}"},
            {"name": "LUMO_TOKEN", "value": "${lumo_token}"},
            {"name": "DF_PRIVATE_KEY_ID", "value": "${df_private_key_id}"},
            {"name": "DF_PRIVATE_KEY", "value": "${df_private_key}"},
            {"name": "FLIGHTSTATS_APP_KEY", "value": "${flightstats_app_key}"},
            {"name": "AIRTABLE_API_KEY", "value": "${airtable_api_key}"},
            {"name": "GMAIL_PRIVATE_KEY", "value": "${gmail_private_key}"}
        ],
        "logConfiguration": {
            "logDriver": "awslogs",
            "options": {
                "awslogs-group": "${log_group_name}",
                "awslogs-region": "${log_group_region}",
                "awslogs-stream-prefix": "core"
            }
        },
        "portMappings": [
            {
                "containerPort": 8080,
                "hostPort": 8080
            }
        ],
        "mountPoints": [
            {
                "readOnly": false,
                "containerPath": "/usr/src/app/logs",
                "sourceVolume": "logs"
            }
        ]
    }
]
