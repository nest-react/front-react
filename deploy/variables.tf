variable "prefix" {
  default = "sac"
}

variable "project" {
  default = "shu-app-core"
}

variable "contact" {
  default = "alex@itsyndicate.org"
}

variable "bastion_key_name" {
  default = "alex-key"
}

variable "db_name" {
  description = "Name for PostgreSQL RDS instance"
  default     = "zennerdb"
}

variable "db_username" {
  description = "Username for PostgeSQL RDS instance"
  default     = "zenneruser"
}

variable "db_password" {
  description = "Password for PostgreSQL RDS instance"
  default     = "zennerpass"
}

variable "node_env" {
  description = "NodeJS Environment"
  default     = "test"
}

variable "ecr_image_core" {
  description = "ECR Image for CORE"
  default     = "323847718990.dkr.ecr.us-east-1.amazonaws.com/shu-app-core:latest"
}

variable "dns_zone_name" {
  description = "Domain name"
  default     = "services.zenner.ai"
}

variable "subdomain" {
  description = "Subdomain per environment"
  type        = map(string)
  default = {
    production = "api"
    staging    = "api.staging"
    default    = "api.dev"
  }
}

variable "twilio_sid" {
  description = "TWILIO_SID"
  default     = "TWILIO_SID-placaholder"
}

variable "twilio_token" {
  description = "TWILIO_TOKEN"
  default     = "TWILIO_TOKEN-placaholder"
}

variable "openweatehrmap_api_key" {
  description = "OPENWEATEHRMAP_API_KEY"
  default     = "OPENWEATEHRMAP_API_KEY-placaholder"
}

variable "front_plugin_secret" {
  description = "FRONT_PLUGIN_SECRET"
  default     = "FRONT_PLUGIN_SECRET-placaholder"
}

variable "front_twilio" {
  description = "FRONT_TWILIO"
  default     = "FRONT_TWILIO-placaholder"
}

variable "front_api_token" {
  description = "FRONT_API_TOKEN"
  default     = "FRONT_API_TOKEN-placaholder"
}

variable "front_api_key" {
  description = "FRONT_API_KEY"
  default     = "FRONT_API_KEY-placaholder"
}

variable "slack_flightevents_shu" {
  description = "SLACK_FLIGHTEVENTS_SHU"
  default     = "SLACK_FLIGHTEVENTS_SHU-placaholder"
}

variable "slack_dialogflow_shu" {
  description = "SLACK_DIALOGFLOW_SHU"
  default     = "SLACK_DIALOGFLOW_SHU-placaholder"
}

variable "slack_onboarding_shu" {
  description = "SLACK_ONBOARDING_SHU"
  default     = "SLACK_ONBOARDING_SHU-placaholder"
}

variable "jackandferdi_token" {
  description = "JACKANDFERDI_TOKEN"
  default     = "JACKANDFERDI_TOKEN-placaholder"
}

variable "dialog_maps_api" {
  description = "DIALOG_MAPS_API"
  default     = "DIALOG_MAPS_API-placaholder"
}

variable "google_api_key" {
  description = "GOOGLE_API_KEY"
  default     = "GOOGLE_API_KEY-placaholder"
}

variable "google_distancematrix_key" {
  description = "GOOGLE_DISTANCEMATRIX_KEY"
  default     = "GOOGLE_DISTANCEMATRIX_KEY-placaholder"
}

variable "sitata_organization" {
  description = "SITATA_ORGANIZATION"
  default     = "SITATA_ORGANIZATION-placaholder"
}

variable "sitata_authorization" {
  description = "SITATA_AUTHORIZATION"
  default     = "SITATA_AUTHORIZATION-placaholder"
}

variable "lumo_token" {
  description = "LUMO_TOKEN"
  default     = "LUMO_TOKEN-placaholder"
}

variable "df_private_key_id" {
  description = "DF_PRIVATE_KEY_ID"
  default     = "DF_PRIVATE_KEY_ID-placaholder"
}

variable "df_private_key" {
  description = "DF_PRIVATE_KEY"
  default     = "DF_PRIVATE_KEY-placaholder"
}

variable "flightstats_app_key" {
  description = "FLIGHTSTATS_APP_KEY"
  default     = "FLIGHTSTATS_APP_KEY-placaholder"
}

variable "airtable_api_key" {
  description = "AIRTABLE_API_KEY"
  default     = "AIRTABLE_API_KEY-placaholder"
}

variable "gmail_private_key" {
  description = "GMAIL_PRIVATE_KEY"
  default     = "GMAIL_PRIVATE_KEY-placaholder"
}
