output "db_host" {
  value = aws_db_instance.main.address
}

output "redis_host" {
  value = aws_elasticache_cluster.redis.cache_nodes.0.address
}

output "core_endpoint" {
  value = aws_route53_record.app.fqdn
}

output "bastion_host" {
  value = aws_instance.bastion.public_dns
}
