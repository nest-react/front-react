terraform {
  backend "s3" {
    bucket         = "shu-app-core-tfstate"
    key            = "shu-app-core.tfstate"
    region         = "us-east-1"
    encrypt        = true
    dynamodb_table = "shu-app-core-tf-state-lock"
  }
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

provider "aws" {
  region = "us-east-1"
}

locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    DevOps      = var.contact
    ManagedBy   = "Terraform"
  }
}

data "aws_region" "current" {}
