FROM node:14-alpine

WORKDIR /usr/src/app

COPY package.json ./

RUN npm install

COPY ./src ./src

COPY newrelic.js tsconfig.json ormconfig.js.tpl .env.tpl ./
COPY ./entrypoint.sh /entrypoint.sh

RUN chmod +x /entrypoint.sh

RUN mkdir -p /usr/src/app/src/public

RUN apk add gettext

CMD ["/entrypoint.sh"]
