/**
 * The bot processing microservice
 * @file index.ts
 */
import * as dotenv from 'dotenv'; // Environment vars
import * as qs from 'qs';
import axios from 'axios';
import { EventEmitter } from 'events';
import BQ, { DoneCallback } from 'bee-queue';
import { Connection, createConnection, In, Like, MoreThan } from 'typeorm';
import { google } from '@google-cloud/dialogflow/build/protos/protos';
import { Client, TravelMode } from '@googlemaps/google-maps-services-js/dist';
import ConfigService from './service/Config.service';
import { FlightHelper } from './helpers/flight.helper';

// Load env vars
dotenv.config();

// Services
import { LumoService } from './service';
import Il8nService from './service/Il8n.service';
// import { firestore } from './service/FirebaseAdmin';
import { UserService } from './service/User.service';
import { TrackService } from './service/Track.service';
import { FlightService } from './service/Flight.service';
import { SitataService } from './service/Sitata.service';
import { WeatherTime } from './service/WeatherTime.service';
import { BeeQueueService } from './service/BeeQueue.service';
import { DirectionService } from './service/Direction.service';

// Removed 27 October 2020 as a precursor to dropping Epomeno
// import { FirestoreService } from './service/Firestore.service';

import { client as redisClient } from './service/Redis.service';
import { CommsEventService } from './service/CommsEvent.service';
import { DialogflowService } from './service/Dialogflow.service';
import { FlightStatsService } from './service/FlightStats.service';
import AirportService, { AirportMeta } from './service/Airport.service';
// Interfaces
import {
  SearchOptions,
  SearchResponse,
} from './interface/lumo/lumoSearch.interface';
import { FlightResponse } from './interface/lumo';
import twiliosms from './interface/twiliosms.interface';
import {
  AirportsPayload,
  BlockList,
  DateStracture,
  DEPARTURE_DAY_FROM,
  DEPARTURE_DAY_LENGTH,
  DEPARTURE_MONTH_FROM,
  DEPARTURE_MONTH_LENGTH,
  DialogflowAirport,
  ExternalServices,
  FlightNumber,
  MONTH_NAMES,
  ProcessAndCacheOptions,
  RADIX,
  SearchRequest,
  TrackedFlight,
  TrackServices,
} from './interface/bot-server.interface';
// Models
import User from './model/User';
import Flight from './model/Flight';
// Utils
import {
  firstName,
  getTimeFromDate,
  jsonValueToProto,
  parseFlightNo,
  structProtoToJson,
} from './util/flight.utils';
import { createLogger } from './util/logger';
import {
  getDayWithSuffix,
  getDiffMinutes,
  getNiceDateString,
  getNiceHourString,
  getNiceTimeDiffString,
} from './util/datetime.utils';
import { parsePhoneNumber } from './util/botServer.utils';
import { JackAndFerdiService } from './service/JackAndFerdi.service';
import { AirtableFactory } from './factory/Airtable.factory';
import {
  AirportOrigin,
  FlightStatsResults,
  FlightStatus,
} from './interface/flightStats';

// Load env vars
dotenv.config();

if (process.env.NODE_ENV === 'production') {
  require('newrelic');
}
import * as fs from 'fs';
const csvWriter = require('csv-write-stream');

const writer = csvWriter();
const csv = require('csv-parser');
// Logging
const logger = createLogger('zenner', 'bot', 'zenner-bot');

import IStruct = google.protobuf.IStruct;
import IValue = google.protobuf.IValue;
import IContext = google.cloud.dialogflow.v2beta1.IContext;
import { CityTable } from './model/CityTable';
import path = require('path');

// import { RegisterDto, RegisterPayload } from './interface/firestore.interface';

//Config
const redisConfig = {
  host: ConfigService.get('REDIS_HOST'),
  port: Number(ConfigService.get('REDIS_HOST')),
};

const IS_UNDEFINED = 'undefined';

const dialogflowService = new DialogflowService();
const beeQueueService = new BeeQueueService();

const sitataService = new SitataService();
const emitter = new EventEmitter();
let jackandferdiService: JackAndFerdiService;

// const firestoreService = new FirestoreService(firestore);

let lumoService: LumoService;

let flightStatsService: FlightStatsService;

function LumoFlightReponseMessageHeader(lumo: FlightResponse): string {
  const flightNo = lumo.flight.carrier.iata + ' ' + lumo.flight.number;
  const departureTime = getTimeFromDate(lumo.flight.scheduled_departure);
  const arrivalTime = getTimeFromDate(lumo.flight.scheduled_arrival);

  return (
    `${flightNo} is scheduled to depart from ${lumo.flight.origin.name} @ ${departureTime}.\n` +
    `You will land at ${lumo.flight.destination.name} around ${arrivalTime}.\n` +
    `This flight is operated by ${lumo.flight.carrier.name}.`
  );
}

async function getLostBaggageData(carrier: string): Promise<string> {
  let messageTobeSent: string;
  let link: any;
  let airline: any;
  return await new Promise((resolve, reject) => {
    fs.createReadStream('src/resources/Airlines Baggague Policies.csv')
      .pipe(csv())
      .on('data', async (row: any) => {
        if (row['Airline Acronym'] === carrier) {
          link = row['Lost Baggage Information '];
          airline = row['Airline'];
        }
      })
      .on('error', () => {
        reject();
      })
      .on('end', () => {
        messageTobeSent = Il8nService.get('zenny:lost_baggage:base');
        if (link && airline) {
          messageTobeSent += Il8nService.get('zenny:lost_baggage:link', {
            templateVars: {
              airline: airline,
              link: link,
            },
          });
        }
        resolve(messageTobeSent);
      });
  });
}

async function userPhoneSearch(
  userService: UserService,
  message: twiliosms,
): Promise<User> | null {
  const currUser = await userService.findByMobile(
    message.From.replace(/\D/g, ''),
  );
  /* istanbul ignore next */
  if (!currUser) {
    return null;
  }
  return currUser;
}

function FlightStatsResponseMessageHeader(status: FlightStatus): string {
  const flightNo = status.carrier.iata + ' ' + status.flightNumber;
  const departureTime = getTimeFromDate(status.departureDate.dateLocal);
  const arrivalTime = getTimeFromDate(status.arrivalDate.dateLocal);
  return (
    `${flightNo} is scheduled to depart from ${status.departureAirport.name} @ ${departureTime}.\n` +
    `You will land at ${status.arrivalAirport.name} around ${arrivalTime}.\n` +
    `This flight is operated by ${status.carrier.name}.`
  );
}

function LumoFlightResponseRiskResponse(lumo: FlightResponse): string {
  const risk = lumo.prediction.delay_index + '0%';
  return `*There is currently a ${risk} risk of disruption.*`;
}

function LumoFlightResponseToMessage(lumo: FlightResponse): string {
  return `${LumoFlightReponseMessageHeader(
    lumo,
  )}\n${LumoFlightResponseRiskResponse(lumo)}`;
}

function FlightStatsResponseToMessage(status: FlightStatus): string {
  return FlightStatsResponseMessageHeader(status);
}

export function getRiskWord(
  risk: number,
): 'high' | 'moderate' | 'low' | 'severe' {
  if (risk < 20) {
    return 'low';
  }
  if (risk < 60) {
    return 'moderate';
  }
  if (risk < 100) {
    return 'high';
  }

  /*istanbul ignore next*/
  return 'severe';
}

function cleanLumoResults(
  searchResults: SearchResponse<FlightResponse>,
): SearchResponse<FlightResponse> {
  const now = new Date();
  const filteredResults = searchResults.results.filter((value) => {
    const depmins = getDiffMinutes(
      value.flight.scheduled_departure,
      now.toISOString(),
    );
    return depmins > 0;
  });

  return {
    count: filteredResults.length,
    results: filteredResults,
  };
}

function cleanFlightStatsResults(
  searchResults: SearchResponse<FlightStatus>,
): SearchResponse<FlightStatus> {
  const now = new Date();
  const filteredResults = searchResults.results.filter((value) => {
    const depmins = getDiffMinutes(
      value.departureDate.dateUtc,
      now.toISOString(),
    );
    return depmins > 0;
  });

  return {
    count: filteredResults.length,
    results: filteredResults,
  };
}

export async function deleteFlight(
  userId: number,
  flightIds: Array<number>,
  flightService: FlightService,
): Promise<string> {
  try {
    const tripIds = (
      await flightService.userTripRepository.find({
        where: {
          user_id: userId,
        },
      })
    ).map((item) => item.trip_id);
    const tripsToDelete = await flightService.tripRepository.find({
      where: {
        trip_id: In(tripIds),
        flight: In(flightIds),
      },
    });
    await flightService.tripRepository.remove(tripsToDelete);
    return Il8nService.get('zenny:flight:stopTracking');
  } catch (e) /*istanbul ignore next*/ {
    logger.error("Couldn't delete flight - ", e);
    return Il8nService.get('zenny:flight:failStopTracking');
  }
}

function flightStatsSearchResultToMessage(
  flightObj: FlightStatus,
  hideRoute = false,
) {
  const name = firstName(flightObj.carrier.name);
  let reply = `${name} ${flightObj.carrier.iata} ${flightObj.flightNumber}`;
  if (!hideRoute) {
    reply += ` (${flightObj.departureAirport.iata} > ${flightObj.arrivalAirport.iata}) `;
  }

  const departureTime = getTimeFromDate(flightObj.departureDate.dateLocal);

  reply += `@ ${departureTime}.`;
  return reply;
}

function flightSearchResultToMessage(
  flightObj: FlightResponse,
  hideRoute = false,
) {
  const now = new Date();
  const departureMinutes = getDiffMinutes(
    flightObj.flight.scheduled_departure,
    now.toISOString(),
  );
  const arrivalMinutes = getDiffMinutes(
    flightObj.flight.scheduled_arrival,
    now.toISOString(),
  );

  const name = firstName(flightObj.flight.carrier.name);
  let reply = `${name} ${flightObj.flight.carrier.iata} ${flightObj.flight.number}`;
  if (!hideRoute) {
    reply += ` (${flightObj.flight.origin.iata} > ${flightObj.flight.destination.iata}) `;
  }

  const departureTime = getTimeFromDate(flightObj.flight.scheduled_departure);

  reply += `@ ${departureTime}.`;

  if (departureMinutes > 0) {
    const predictionPercent = flightObj.prediction['delay_index'];
    reply +=
      `\nThere is currently a ${predictionPercent}` +
      `0% chance of delay or cancellation.`;
  } else if (arrivalMinutes > 0) {
    reply += '\nFlight has already departed';
  }
  return reply;
}

async function cacheLumoResult(
  identifier: string,
  flightResult: FlightResponse,
) {
  const sessionPath = dialogflowService.getSessionPath(identifier);
  const request = {
    parent: sessionPath,
    context: {
      name: sessionPath + '/contexts/found_flight_to_track',
      lifespanCount: 2,
      parameters: {
        fields: {
          lumo_id: {
            stringValue: String(flightResult.flight.id),
            kind: 'stringValue',
          },
          flight: jsonValueToProto(flightResult),
        },
      },
    },
  };
  await dialogflowService.contextClient.createContext(request);
}

async function cacheFlightStatsResult(
  identifier: string,
  flightResult: FlightStatus,
) {
  const sessionPath = dialogflowService.getSessionPath(identifier);
  const request = {
    parent: sessionPath,
    context: {
      name: sessionPath + '/contexts/found_flight_to_track',
      lifespanCount: 2,
      parameters: {
        fields: {
          flightStatsId: {
            stringValue: String(flightResult.flightId),
            kind: 'stringValue',
          },
          flight: jsonValueToProto(flightResult),
        },
      },
    },
  };
  await dialogflowService.contextClient.createContext(request);
}

async function cacheLumoFlights(
  identifier: string,
  flightResults: SearchResponse<FlightResponse>,
) {
  const flightArray = flightResults.results.map((flight, i) => ({
    flight,
    lumo_id: flight.flight.id,
    flight_number: flight.flight.number,
    flight_carrier: flight.flight.carrier.iata,
    no: i + 1,
  }));

  const sessionPath = dialogflowService.getSessionPath(identifier);
  const request = {
    parent: sessionPath,
    context: {
      name: sessionPath + '/contexts/flight_option_list',
      lifespanCount: 2,
      parameters: {
        fields: {
          flights: jsonValueToProto(flightArray),
        },
      },
    },
  };
  await dialogflowService.contextClient.createContext(request);
}

async function cacheFlightStatsFlights(
  identifier: string,
  flightResults: SearchResponse<FlightStatus>,
) {
  const flightArray = flightResults.results.map((flight, i) => ({
    flight,
    flightStatsId: flight.flightId,
    flight_number: flight.flightNumber,
    flight_carrier: flight.carrier.iata,
    no: i + 1,
  }));

  const sessionPath = dialogflowService.getSessionPath(identifier);
  const request = {
    parent: sessionPath,
    context: {
      name: sessionPath + '/contexts/flight_option_list',
      lifespanCount: 2,
      parameters: {
        fields: {
          flights: jsonValueToProto(flightArray),
        },
      },
    },
  };
  await dialogflowService.contextClient.createContext(request);
}

export async function predictCity(
  currUser: User,
  flightService: FlightService,
): Promise<string[] | string> {
  let messageTobeSent;
  /* istanbul ignore next */
  if (!currUser) {
    messageTobeSent = Il8nService.get('zenny:notfound:user');
  }
  const nextDepartureFlight = await flightService.getNextDepartureFlight(
    currUser.id,
  );

  const destinationFlight = nextDepartureFlight;
  const destinationAirport =
    destinationFlight?.initialLumoData.flight.destination.name ||
    destinationFlight?.destination;
  const destinationCity =
    destinationFlight?.initialLumoData.flight.destination.city ?? undefined;

  const destinationIATAAirport =
    destinationFlight?.initialLumoData.flight.destination.iata;

  const originFlight = (
    await flightService.getLandedFlightsByUser(currUser.id)
  )[0];
  const originAirport =
    originFlight?.initialLumoData.flight.origin.name || originFlight?.origin;
  const originCity =
    originFlight?.initialLumoData.flight.origin.city ?? undefined;

  const originIATAAirport = originFlight?.initialLumoData.flight.origin.iata;

  //basic functionality tested in directions testing.
  /* istanbul ignore next */
  if (destinationFlight && originFlight) {
    if (destinationFlight.estimatedDeparture > originFlight.estimatedArrival) {
      messageTobeSent = [
        destinationCity,
        destinationAirport,
        destinationIATAAirport,
      ];
    } else {
      messageTobeSent = [originCity, originAirport, originIATAAirport];
    }
  } else if (destinationFlight) {
    messageTobeSent = [
      destinationCity,
      destinationAirport,
      destinationIATAAirport,
    ];
  } else if (originFlight) {
    messageTobeSent = [originCity, originAirport, originIATAAirport];
  } else {
    return undefined;
  }
  return messageTobeSent;
}

async function searchAndCacheFlight(
  flightId: FlightNumber,
  departureDate: string,
  origin: DialogflowAirport,
  dfIdentifier: string,
) {
  const lumoSearchResults = await lumoService.search({
    flight_number: flightId.flight,
    carrier: flightId.carrier,
    date: departureDate.substr(0, 10),
    origin: origin.iata ?? '',
  });

  if (lumoSearchResults.count) {
    if (lumoSearchResults.count === 0) {
      return `Sorry, I couldn't find flight ${flightId.carrier}${
        flightId.flight
      } on the ${getDayWithSuffix(
        parseInt(departureDate.split('-')[2]),
      )} of ${new Date(departureDate).toLocaleString('default', {
        month: 'long',
      })}.`;
    }
    const flightResult = lumoSearchResults.results[0];
    await cacheLumoResult(dfIdentifier, flightResult);
    return LumoFlightResponseToMessage(flightResult);
  }
  const fsSearchResults = await flightStatsSearch({
    flight_number: flightId.flight,
    date: departureDate,
    carrier: flightId.carrier,
    origin: origin.iata,
  });
  if (fsSearchResults.count === 0) {
    return `Sorry, I couldn't find flight ${flightId.carrier}${
      flightId.flight
    } on the ${getDayWithSuffix(
      parseInt(departureDate.split('-')[2]),
    )} of ${new Date(departureDate).toLocaleString('default', {
      month: 'long',
    })}.`;
  }
  const flightResult = fsSearchResults.results[0];
  await cacheFlightStatsResult(dfIdentifier, flightResult);
  return FlightStatsResponseToMessage(flightResult);
}

function sortLumoByDepartureDate(
  searchResponse: SearchResponse<FlightResponse>,
): SearchResponse<FlightResponse> {
  const flights = searchResponse.results.sort((prevFlight, nextFlight) => {
    return (
      new Date(prevFlight.flight.scheduled_departure).getTime() -
      new Date(nextFlight.flight.scheduled_departure).getTime()
    );
  });

  return { results: flights, count: flights.length };
}

function sortFlightStatsByDepartureDate(
  searchResponse: SearchResponse<FlightStatus>,
): SearchResponse<FlightStatus> {
  const flights = searchResponse.results.sort((prevFlight, nextFlight) => {
    return (
      new Date(prevFlight.departureDate.dateUtc).getTime() -
      new Date(nextFlight.departureDate.dateUtc).getTime()
    );
  });

  return { results: flights, count: flights.length };
}

async function lumoSearch(options: SearchRequest | SearchRequest[]) {
  try {
    const searchResult = await lumoService.search(options);
    return sortLumoByDepartureDate(cleanLumoResults(searchResult));
  } catch (e) {
    logger.error('Failed Lumo search', { error: e });
    return null;
  }
}

async function flightStatsSearch(options: SearchRequest) {
  try {
    const res = await flightStatsService.status(
      {
        carrier: options.carrier,
        flight: options.flight_number,
        date: options.date,
        airport: options.origin,
        airportType: AirportOrigin.departure,
      },
      { extendedOptions: ['useInlinedReferences', 'includeNewFields'] },
    );
    return sortFlightStatsByDepartureDate(
      cleanFlightStatsResults({
        results: res.results,
        count: res.results.length,
      }),
    );
  } catch (e) {
    logger.error('flightStatsSearch() threw error', {
      options: options,
      error: e,
    });
    return {
      results: [],
      count: 0,
    };
  }
}

function mergeSearchResponse(
  response: FlightStatsResults<FlightStatus[]>[],
): SearchResponse<FlightStatus> {
  return response.reduce(
    (searchResponse, { results }) => {
      searchResponse.results.push(...results);
      searchResponse.count += results.length;
      return searchResponse;
    },
    { count: 0, results: [] } as SearchResponse<FlightStatus>,
  );
}

async function flightStatsAirportSearch(
  options: SearchRequest | SearchRequest[],
) {
  try {
    if (!Array.isArray(options)) options = [options];
    const promises = options.map<Promise<FlightStatsResults<FlightStatus[]>>>(
      (option) => {
        return flightStatsService.findFlightsByRoute(
          {
            departureAirport: option.origin,
            arrivalAirport: option.destination,
            airportType: AirportOrigin.departure,
            date: option.date,
          },
          { extendedOptions: ['useInlinedReferences', 'includeNewFields'] },
        );
      },
    );
    const results = await Promise.all(promises);
    const mergedResults = mergeSearchResponse(results);
    return sortFlightStatsByDepartureDate(
      cleanFlightStatsResults(mergedResults),
    );
  } catch (e) {
    logger.error('flightStatsAirportSearch() threw error', {
      options: options,
      error: e,
    });

    return {
      results: [],
      count: 0,
    };
  }
}

export async function getCity(
  services: ExternalServices,
  city: string,
): Promise<CityTable> {
  const cityTable = services.db.getRepository(CityTable);
  const cityT =
    (await cityTable.findOne({
      where: {
        city: city,
      },
      order: {
        ranking: 'ASC',
        density: 'DESC',
      },
    })) ||
    (await cityTable.findOne({
      where: {
        city_ascii: city,
      },
      order: {
        ranking: 'ASC',
        density: 'DESC',
      },
    })) ||
    (await cityTable.findOne({
      where: { admin_name: city },
      order: { ranking: 'ASC', density: 'DESC' },
    }));
  return cityT;
}

export async function addDFParameters(
  message: string,
  params: IStruct,
): Promise<IStruct> {
  let airportParams: IValue[] = [];
  let cityParams: IValue[] = [];
  if (message.toLowerCase().includes('tomorrow')) {
    params.fields['day'] = { stringValue: 'tomorrow' };
  }
  if (message.toLowerCase().includes('today')) {
    params.fields['day'] = { stringValue: 'today' };
  }
  let origin = '';
  const { flightId, airports, cities } = parseDfParams(params);
  if (flightId && flightId.carrier && flightId.carrier) {
    const today = new Date();
    const flightOptions = {
      flight_number: flightId.flight,
      carrier: flightId.carrier, //[0] is the carrier
      date: `${today.getFullYear()}-${today.getMonth() + 1}-${today.getDate()}`,
    };
    let flightStatus;
    try {
      flightStatus = await lumoService.status(flightOptions);
      origin = flightStatus[0].flight.origin.city;
      params.fields['origin'] = { stringValue: origin };
      return params;
    } catch (Error) {
      logger.error(Error);
    }
  }

  let temp = message.toLowerCase();
  if (message.toLowerCase().includes('from')) {
    temp = message.split('from')[1].toLowerCase();
  }
  if (cities.length === 2) {
    if (temp.indexOf(cities[0].toLowerCase()) < 0) {
      origin = cities[1];
    } else if (temp.indexOf(cities[1].toLowerCase()) < 0) {
      /*istanbul ignore next*/ origin = cities[0];
    } else {
      origin =
        temp.indexOf(cities[0]) > temp.indexOf(cities[1])
          ? cities[1]
          : cities[0];
    }
    cityParams = [
      {
        stringValue: origin,
      },
      {
        stringValue: origin === cities[0] ? cities[1] : cities[0],
      },
    ];
    params.fields['geo-city'].listValue.values = cityParams;
  } else if (airports.length === 2) {
    if (temp.indexOf(airports[0].iata.toLowerCase()) < 0) {
      /*istanbul ignore next*/ origin = airports[1].city;
    } else if (temp.indexOf(airports[1].iata.toLowerCase()) < 0) {
      /*istanbul ignore next*/ origin = airports[0].city;
    } else {
      origin =
        temp.indexOf(airports[0].iata.toLowerCase()) >
        temp.indexOf(airports[1].iata.toLowerCase())
          ? airports[1].city
          : airports[0].city;
    }
    airportParams = [
      origin === airports[0].city
        ? airportToDFStruct(airports[0])
        : airportToDFStruct(airports[1]),
      origin === airports[0].city
        ? airportToDFStruct(airports[1])
        : airportToDFStruct(airports[0]),
    ];
    params.fields['airport'].listValue.values = airportParams;
  } else {
    try {
      const city = cities[0].toLowerCase();
      const airport = airports[0].iata.toLowerCase();
      if (temp.indexOf(city) < 0) {
        origin = airports[0].city;
      } else if (temp.indexOf(airport) < 0) {
        /*istanbul ignore next*/ origin = cities[0];
      } else {
        origin =
          temp.indexOf(city) > temp.indexOf(airport)
            ? airports[0].city
            : cities[0];
      }
    } catch (err) {
      return params;
    }
  }
  params.fields['origin'] = { stringValue: origin };
  return params;
}

async function processAndCacheLumoResults(
  options: ProcessAndCacheOptions<FlightResponse>,
) {
  const { flightResults, month, day, dfIdentifier } = options;
  if (flightResults.count === 0) {
    return `Sorry, I couldn't find any flights for that route on ${MONTH_NAMES[month]} ${day}`;
  }

  const scheduledDeparture =
    flightResults.results[0].flight.scheduled_departure;
  const departureMonth = scheduledDeparture.substr(
    DEPARTURE_MONTH_FROM,
    DEPARTURE_MONTH_LENGTH,
  );
  const flightDate = `${MONTH_NAMES[parseInt(departureMonth) - 1]} ${day}`;
  if (flightResults.count === 1) {
    const flightResult = flightResults.results[0];
    await cacheLumoResult(dfIdentifier, flightResult);
    return (
      `I've found one flight which matches your ${flightDate} request:\n\n${flightSearchResultToMessage(
        flightResult,
      )}` + '\n\nWould you like me to track this flight?'
    );
  } else {
    const flightList = flightResults.results
      .map((flight, index) => {
        return `\n\n# ${index + 1}: ${flightSearchResultToMessage(flight)}`;
      })
      .join('');
    await cacheLumoFlights(dfIdentifier, flightResults);

    return `I found ${flightResults.count} flights on ${flightDate}: ${flightList} \n\nWhich flight # [1-${flightResults.count}] would you like me to track for you?`;
  }
}

async function processAndCacheFSResults(
  options: ProcessAndCacheOptions<FlightStatus>,
) {
  const { flightResults, month, day, dfIdentifier } = options;
  if (flightResults.count === 0) {
    return `Sorry, I couldn't find any flights for that route on ${MONTH_NAMES[month]} ${day}`;
  }

  const scheduledDeparture = flightResults.results[0].departureDate.dateLocal;
  const departureMonth = scheduledDeparture.substr(
    DEPARTURE_MONTH_FROM,
    DEPARTURE_MONTH_LENGTH,
  );
  const flightDate = `${MONTH_NAMES[parseInt(departureMonth) - 1]} ${day}`;
  if (flightResults.count === 1) {
    const flightResult = flightResults.results[0];
    await cacheFlightStatsResult(dfIdentifier, flightResult);
    return (
      `I've found one flight which matches your ${flightDate} request:\n\n${flightStatsSearchResultToMessage(
        flightResult,
      )}` + '\n\nWould you like me to track this flight?'
    );
  } else {
    const flightList = flightResults.results
      .map((flight, index) => {
        return `\n\n# ${index + 1}: ${flightStatsSearchResultToMessage(
          flight,
        )}`;
      })
      .join();
    await cacheFlightStatsFlights(dfIdentifier, flightResults);

    return `I found ${flightResults.count} flights on ${flightDate}: ${flightList} \n\nWhich flight # [1-${flightResults.count}] would you like me to track for you?`;
  }
}

export function kelvinToCelsius(kelvin: number): number {
  return kelvin - 273.15;
}

export function kelvinToFahrenheit(kelvin: number): number {
  return (kelvin * 9) / 5 - 459.67;
}

function transformAirport(airport: DialogflowAirport): Partial<AirportMeta> {
  return {
    iata_code: airport.iata,
  };
}

function prepareAirportsForSearch(
  airports: DialogflowAirport | Partial<AirportMeta>[],
): AirportMeta[] {
  return (Array.isArray(airports)
    ? airports
    : [transformAirport(airports)]) as AirportMeta[];
}

async function searchByAirports(
  origin: Partial<AirportMeta>[] | DialogflowAirport,
  destination: Partial<AirportMeta>[] | DialogflowAirport,
  departureDate: string,
  isLumoSearch: boolean,
) {
  const options: SearchOptions[] = [];

  const originAirports = prepareAirportsForSearch(origin);
  const destinationAirports = prepareAirportsForSearch(destination);

  originAirports.forEach((source) => {
    destinationAirports.forEach((dest) => {
      options.push({
        origin: source.iata_code,
        destination: dest.iata_code,
        date: departureDate.substr(0, 10),
      });
    });
  });

  if (isLumoSearch) {
    return lumoSearch(options);
  }

  return flightStatsAirportSearch(options);
}

const sitataFormat = (message: string): string => {
  message = message.replace('\n\n ', '\n\n'); //removing space withour asterix
  message = message.split('\n\n\n').join('\n'); ///removing triple \n
  message = message.replace(':\n ', ':\n'); //removing space at the beginning of a field

  message = message.replace('.*', '.\n*'); //adding \n between records
  const arr = message.split('\n*');
  const unique = [...new Set(arr)];
  message = unique.join('\n*');
  return message;
};

const getAirports = (
  phrase: string,
  cities: string[],
  airports: DialogflowAirport[],
): AirportsPayload => {
  let match = phrase.match(/(?<=from)(.*)(?=to)/);

  if (!match) {
    match = phrase.match(/(?<=to)(.*)(?=from)/);
    if (!match) {
      return {
        originAirports: [],
        destinationAirports: [],
      };
    } else {
      const destination = match[0].trim();
      const isCityDestination = !!cities.filter((city) => city === destination)
        .length;

      if (!isCityDestination) {
        return {
          originAirports: AirportService.findByCityName(cities[0]),
          destinationAirports: airports[0],
        };
      }
      return {
        originAirports: airports[0],
        destinationAirports: AirportService.findByCityName(cities[0]),
      };
    }
  }

  const origin = match[0].trim();
  const isCityOrigin = !!cities.filter((city) => city === origin).length;

  if (!isCityOrigin) {
    return {
      originAirports: airports[0],
      destinationAirports: AirportService.findByCityName(cities[0]),
    };
  }

  return {
    originAirports: AirportService.findByCityName(cities[0]),
    destinationAirports: airports[0],
  };
};

export async function parseDfDates(params: IStruct): Promise<DateStracture> {
  let departureDate;
  try {
    departureDate = params.fields['date'].stringValue;
    if ('day' in params.fields) {
      departureDate = null;
    }
  } catch /*istanbul ignore next*/ {
    departureDate = null;
  }
  if (!departureDate) {
    try {
      departureDate = await new WeatherTime().getDateForCity(
        params.fields['origin'].stringValue,
      );
      if (
        'day' in params.fields &&
        params.fields['day'].stringValue == 'tomorrow'
      ) {
        departureDate.setDate(departureDate.getDate() + 1);
        departureDate.setHours(0);
        departureDate.setMinutes(0);
      }
      departureDate = departureDate.toLocaleDateString();
      const splitted = departureDate.split('/');
      let month = parseInt(splitted[0]);
      const dayStr = splitted[1];
      const day = getDayWithSuffix(parseInt(dayStr));
      const year = splitted[2];
      departureDate =
        year +
        '-' +
        (month >= 10 ? month : '0' + month) +
        '-' +
        (parseInt(dayStr) >= 10 ? dayStr : '0' + dayStr);
      month = month - 1;
      return {
        month,
        day,
        departureDate,
      };
    } catch (err) {
      logger.error(err);
    }
    const now = new Date();
    departureDate = now.toISOString().substr(0, 10);
    logger.info(
      `Got a flight tracking request without a date, will assume ${departureDate}`,
    );
  }

  const month =
    parseInt(
      departureDate.substr(DEPARTURE_MONTH_FROM, DEPARTURE_MONTH_LENGTH),
      RADIX,
    ) - 1;
  const day = getDayWithSuffix(
    parseInt(
      departureDate.substr(DEPARTURE_DAY_FROM, DEPARTURE_DAY_LENGTH),
      RADIX,
    ),
  );

  return {
    month,
    day,
    departureDate,
  };
}

function loadContext(outputContexts: any, name: string) {
  for (const i in outputContexts) {
    /* istanbul ignore next */
    if (!outputContexts.hasOwnProperty(i)) {
      continue;
    }

    if (outputContexts[i].name.indexOf(name) < 0) {
      continue;
    }

    return outputContexts[i].parameters;
  }

  return null;
}

async function processInteractiveTracking(
  departure: any,
  arrival: any,
  params: IStruct,
  identifier: string,
) {
  const { departureDate, month, day } = await parseDfDates(params);
  const originAirports =
    typeof departure === 'string'
      ? AirportService.findByCityName(departure)
      : parseDfAirport(departure);
  const destinationAirports =
    typeof arrival === 'string'
      ? AirportService.findByCityName(arrival)
      : parseDfAirport(arrival);

  const flightResults = await searchByAirports(
    originAirports,
    destinationAirports,
    departureDate,
    true,
  );

  return processAndCacheLumoResults({
    flightResults: flightResults as SearchResponse<FlightResponse>,
    dfIdentifier: identifier,
    day,
    month,
  });
}

function parseDfParams(params: IStruct) {
  if (params.fields.length === 0) {
    throw new Error(`Got empty flight search`);
  }
  const cityParams: IValue[] = params.fields['geo-city'].listValue.values;
  const cities: string[] = cityParams.map((cityParam) =>
    cityParam.stringValue.replace('İ', 'I'),
  );

  const airport: IValue[] = params.fields['airport'].listValue.values;
  const airports: DialogflowAirport[] = airport.map((dfAirport) =>
    parseDfAirport(dfAirport),
  );
  const flightNumber: IValue[] =
    params.fields['flight-number'].listValue.values;
  const flightId = parseFlightNo(flightNumber[0]);
  return {
    cities,
    flightNumber,
    airports,
    originAirport: airports[0],
    flightId,
  };
}

function airportToDFStruct(airport: DialogflowAirport): IValue {
  const DFstruct = {
    structValue: {
      fields: {
        city: {
          stringValue: airport.city,
        },
        IATA: {
          stringValue: airport.iata,
        },
        ICAO: {
          stringValue: airport.icao,
        },
        country: {
          stringValue: airport.country,
        },
        name: {
          stringValue: airport.name,
        },
      },
    },
  } as IValue;
  return DFstruct;
}

function parseDfAirport(airport: IValue): DialogflowAirport {
  if (!(airport && airport.structValue)) {
    return null;
  }

  return {
    city: airport.structValue.fields.city.stringValue,
    iata: airport.structValue.fields.IATA.stringValue,
    icao: airport.structValue.fields.ICAO.stringValue,
    country: airport.structValue.fields.country.stringValue,
    name: airport.structValue.fields.name.stringValue,
  };
}

/**
 * Search for a flight based on a text result
 * @param params {IStruct} entities extracted by DialogFlow
 * @param message twilio message
 * @param services
 * @param identifier session identifier
 * @param originalPhrase phrase provided by user
 * @returns {Promise<string>} a message to be displayed to the user
 */
export async function processFlightSearchRequest(
  params: IStruct,
  message: twiliosms,
  services: ExternalServices,
  identifier: string,
  originalPhrase: string,
): Promise<string> {
  // Function inside function to make code more readable
  function sendReply(msg: string) {
    return sendMessage(message.From, message.To, message.Source, msg, services);
  }

  try {
    const { departureDate, month, day } = await parseDfDates(params);
    const {
      flightNumber,
      flightId,
      airports,
      originAirport,
      cities,
    } = parseDfParams(params);
    if (departureDate && flightNumber.length > 0 && airports.length > 0) {
      // Trifecta!
      // Search by flight number, departure date and origin airport
      await sendReply(Il8nService.get('zenny:flight:searching'));
      return await searchAndCacheFlight(
        flightId,
        departureDate,
        originAirport,
        identifier,
      );
    }

    if (departureDate && flightId.flight > 0) {
      // Search by flight number and departure date
      logger.info(
        `Got ${flightId.flight} and ${departureDate} from DialogFlow -- do search`,
      );
      const flightResults = await lumoSearch({
        flight_number: flightId.flight,
        carrier: flightId.carrier ?? '',
        date: departureDate.substr(0, 10),
        origin: originAirport?.iata ?? '',
      });
      if (flightResults.count >= 1) {
        return await processAndCacheLumoResults({
          flightResults,
          dfIdentifier: identifier,
          month,
          day,
        });
      } else {
        const searchResults = await flightStatsSearch({
          flight_number: flightId.flight,
          carrier: flightId.carrier ?? '',
          date: departureDate.substr(0, 10),
          origin: originAirport?.iata ?? '',
        });
        return await processAndCacheFSResults({
          flightResults: searchResults,
          dfIdentifier: identifier,
          month,
          day,
        });
      }
    }

    if (departureDate && airports.length === 2) {
      // Search by 2 airports
      if (airports.length >= 2) {
        const flightResults = await lumoSearch({
          date: departureDate.substr(0, 10),
          origin: originAirport.iata,
          destination: airports[1].iata,
        });
        if (flightResults.count >= 1) {
          return await processAndCacheLumoResults({
            flightResults,
            dfIdentifier: identifier,
            day,
            month,
          });
        } else {
          const searchResults = await flightStatsAirportSearch({
            date: departureDate.substr(0, 10),
            origin: originAirport.iata,
            destination: airports[1].iata,
          });
          return await processAndCacheFSResults({
            flightResults: searchResults,
            dfIdentifier: identifier,
            day,
            month,
          });
        }
      }
    }

    if (departureDate && cities.length === 2 && airports.length === 0) {
      // Search by 2 cities
      const originAirports = AirportService.findByCityName(cities[0]);
      const destinationAirports = AirportService.findByCityName(cities[1]);

      const flightResults = await searchByAirports(
        originAirports,
        destinationAirports,
        departureDate.substr(0, 10),
        true,
      );
      if (flightResults.count !== 0) {
        return processAndCacheLumoResults({
          flightResults: flightResults as SearchResponse<FlightResponse>,
          dfIdentifier: identifier,
          day,
          month,
        });
      }
      const fsResults = await searchByAirports(
        originAirports,
        destinationAirports,
        departureDate.substr(0, 10),
        false,
      );
      return processAndCacheFSResults({
        flightResults: fsResults as SearchResponse<FlightStatus>,
        dfIdentifier: identifier,
        day,
        month,
      });
    }

    if (departureDate && cities.length === 1 && airports.length === 1) {
      // Search by 1 city and 1 airport
      const { originAirports, destinationAirports } = getAirports(
        originalPhrase,
        cities,
        airports,
      );
      const flightResults = await searchByAirports(
        originAirports,
        destinationAirports,
        departureDate.substr(0, 10),
        true,
      );
      if (flightResults.count !== 0) {
        return processAndCacheLumoResults({
          flightResults: flightResults as SearchResponse<FlightResponse>,
          dfIdentifier: identifier,
          day,
          month,
        });
      }
      const fsResults = await searchByAirports(
        originAirports,
        destinationAirports,
        departureDate.substr(0, 10),
        false,
      );
      return processAndCacheFSResults({
        flightResults: fsResults as SearchResponse<FlightStatus>,
        dfIdentifier: identifier,
        day,
        month,
      });
    }
  } catch (e) /* istanbul ignore next */ {
    logger.error(`Flight search error`, { error: e });
    return Il8nService.get('zenny:flight:searchError');
  }
  return Il8nService.get('zenny:flight:searchFail');
}

export async function trackSingleFlight(
  record: FlightResponse | FlightStatus,
  passenger: User,
  trackService: TrackService,
): Promise<void> {
  try {
    await trackService.trackSingleFlight(record, passenger);
  } catch (e) {
    logger.error(`Track single flight error`, { error: e });
  }
}

export async function trackConnectionFlight(
  lumoRecord: FlightResponse[],
  passenger: User,
  trackService: TrackService,
): Promise<void> {
  try {
    await trackService.trackConnectionFlight(lumoRecord, passenger);
  } catch (e) {
    logger.error(`Track Connecting flight error ${e}`);
  }
}

export async function confirmTracking(
  identifier: string,
  outputContexts: IContext[],
  services: TrackServices,
): Promise<string> {
  try {
    for (const context of outputContexts) {
      if (context.name.indexOf('found_flight_to_track') < 0) {
        continue;
      }
      const previousQuery = structProtoToJson(context.parameters);
      const passenger = await getPassengerFrom(
        identifier,
        services.userService,
      );
      if (!passenger) {
        return Il8nService.get('zenny:notfound:user');
      }
      const allFlights = await services.flightService.getAllFlightsByUser(
        passenger.id,
      );
      let record = previousQuery.flight;
      if (!record) {
        return Il8nService.get('zenny:fail:confirmTracking');
      }

      let flight;
      let flightNumber: string;
      if (previousQuery.flight.hasOwnProperty('airportResources')) {
        // flight stats record
        record = previousQuery.flight as FlightStatus;
        flightNumber = `${record.carrier.iata}${record.flightNumber}`;
        if (
          allFlights &&
          allFlights.some(
            (flight) =>
              flight.flightNo.includes(flightNumber) &&
              flight.estimatedDepartureDateLocal ===
                record.arrivalDate.dateLocal.split('T')[0] &&
              flight.origin === record.departureAirport.iata,
          )
        ) {
          //check if a user already tracked the flight
          return Il8nService.get('zenny:flightdata:flighttracked');
        }
        flight = await services.flightService.getFlightByData(
          flightNumber,
          record.departureAirport.iata,
          record.departureDate.dateLocal.split('T')[0],
        );
      } else {
        record = previousQuery.flight as FlightResponse;
        flightNumber = `${record.flight.carrier.iata}${record.flight.number}`;
        if (
          allFlights &&
          allFlights.some(
            (flight) =>
              flight.flightNo.includes(flightNumber) &&
              flight.estimatedDepartureDateLocal ===
                record.flight.scheduled_departure.split('T')[0] &&
              flight.origin === record.flight.origin.iata,
          )
        ) {
          //check if a user already tracked the flight
          return Il8nService.get('zenny:flightdata:flighttracked');
        }
        flight = await services.flightService.getFlightByData(
          flightNumber,
          record.flight.origin.iata,
          record.flight.scheduled_departure.split('T')[0],
        );
      }
      if (flight) {
        const userTripId = await services.flightService.createUserTrip(
          passenger.id,
        );
        await services.flightService.createTrip(
          userTripId.trip_id,
          null,
          flight.id,
          null,
          null,
        );
      } else {
        await trackSingleFlight(record, passenger, services.trackService);
      }

      await dialogflowService.contextClient.deleteContext({
        name: context.name,
      });

      /*
      // Removed because we're not doing FireStore

      let additionalPayload = {
        carrier: '',
        flightNumber: '',
        airport: '',
      };

      if (record.hasOwnProperty('airportResources')) {
        additionalPayload = {
          carrier: record.carrier.iata,
          flightNumber: record.flightNumber,
          airport: record.departureAirport.iata,
        };
      } else {
        additionalPayload = {
          carrier: record.flight.carrier.iata,
          flightNumber: String(record.flight.number),
          airport: record.flight.origin.iata,
        };
      }
      const formattedDate = record.hasOwnProperty('airportResources')
        ? record.departureDate.dateLocal
        : record.flight.scheduled_departure;
      const values = {
        firstName: passenger.firstName,
        lastName: passenger.lastName,
        email: passenger.email,
        phone: passenger.mobile,
        haveFlight: 'ticket',
        haveWhatsApp: true,
        has_whatsapp: true,
        haveConnecting: false,
        haveReturnFlight: false,
        departureAirport: '',
        date: new Date(),
        airportFrom: '',
        airportTo: '',
        connectingCarrier: '',
        connectingFlightNumber: '',
        departingAirport: '',
        returnCarrier: '',
        returnFlightNumber: '',
        returnAirport: '',
        returnDate: '',
      };


      const payload = {
        ...values,
        ...additionalPayload,
        mobile: passenger.mobile,
        date: formattedDate.split('T')[0],
      };
      */
      await dialogflowService.contextClient.deleteContext({
        name: context.name,
      });

      // Removed 27 October 2020 as a precursor to dropping Epomeno
      /*
      await firestoreService.queueFlight(
        payload,
        passenger.firestoreId,
        lumoService,
      );
      */
      return 'Done! You should now receive updates and notifications for this flight.';
    }
    return Il8nService.get('zenny:fail:confirmTracking');
  } catch (e) {
    logger.error('Confirm tracking error', { e });
    return null;
  }
}

export async function getPassengerFrom(
  identifier: string,
  userService: UserService,
): Promise<User> {
  const phoneNumber = parsePhoneNumber(identifier);
  return userService.findByMobile(phoneNumber);
}

export async function rejectTracking(identifier: string): Promise<string> {
  try {
    const context = 'found_flight_to_track';
    const contextPath = dialogflowService.getContextPath(identifier, context);
    await dialogflowService.contextClient.deleteContext({ name: contextPath });
    return Il8nService.get('zenny:success:rejectTracking');
  } catch (e) /* istanbul ignore next */ {
    logger.error(`Reject tracking error`, { error: e });
    return Il8nService.get('zenny:fail:confirmTracking');
  }
}

export function getBaggageClaim(flight: Flight): string {
  if (!flight.destinationBaggage) {
    return Il8nService.get('zenny:notfound:baggageclaim');
  }

  return Il8nService.get('zenny:flightdata:baggageclaim', {
    templateVars: {
      reel: flight.destinationBaggage,
    },
  });
}

export function getGate(flight: Flight): string {
  if (!flight.originGate) {
    return Il8nService.get('zenny:notfound:gate');
  }

  if (!flight.originTerminal) {
    return Il8nService.get('zenny:flightdata:gate', {
      templateVars: {
        gate: flight.originGate,
        flightno: flight.flightNo,
        timeuntildeparture: FlightHelper.timeUntilDeparture(flight),
      },
    });
  }

  return Il8nService.get('zenny:flightdata:terminalgate', {
    templateVars: {
      terminal: flight.originTerminal,
      flightno: flight.flightNo,
      timeuntildeparture: FlightHelper.timeUntilDeparture(flight),
      gate: flight.originGate,
    },
  });
}

export function getTerminal(flight: Flight): string {
  if (!flight.originTerminal) {
    return Il8nService.get('zenny:notfound:terminal');
  }

  if (!flight.originGate) {
    return Il8nService.get('zenny:flightdata:terminal', {
      templateVars: {
        terminal: flight.originTerminal,
        flightno: flight.flightNo,
        timeuntildeparture: FlightHelper.timeUntilDeparture(flight),
      },
    });
  }

  return Il8nService.get('zenny:flightdata:terminalgate', {
    templateVars: {
      terminal: flight.originTerminal,
      flightno: flight.flightNo,
      timeuntildeparture: FlightHelper.timeUntilDeparture(flight),
      gate: flight.originGate,
    },
  });
}

export async function confirmSelection(
  identifier: string,
  outputContexts: IContext[],
  services: TrackServices,
): Promise<string> {
  /* eslint-disable no-await-in-loop */
  let previousQuery: any;
  let passenger: User;
  let record: any;
  let flight: Flight;
  let flightNumber: string;
  let i: any;

  for (i in outputContexts) {
    if (!outputContexts.hasOwnProperty(i)) {
      continue;
    }

    if (outputContexts[i].name.indexOf('flight_option_list') < 0) {
      continue;
    }
    previousQuery = structProtoToJson(outputContexts[i].parameters);
    passenger = await getPassengerFrom(identifier, services.userService);
    if (!passenger) {
      return Il8nService.get('zenny:notfound:user');
    }
    if (previousQuery.number && previousQuery.number !== '') {
      const num = Number(previousQuery.number) - 1;
      const allFlights = await services.flightService.getAllFlightsByUser(
        passenger.id,
      );
      if (previousQuery.flights.hasOwnProperty(num)) {
        if (
          previousQuery.flights[num].flight.hasOwnProperty('airportResources')
        ) {
          //fs record
          record = previousQuery.flights[num].flight as FlightStatus;
          flightNumber = `${record.carrier.iata}${record.flightNumber}`;
          if (
            allFlights &&
            allFlights.some(
              (flight) =>
                flight.flightNo.includes(flightNumber) &&
                flight.estimatedDepartureDateLocal ===
                  record.arrivalDate.dateLocal.split('T')[0] &&
                flight.origin === record.departureAirport.iata,
            )
          ) {
            //check if a user already tracked the flight
            return Il8nService.get('zenny:flightdata:flighttracked');
          }
          flight = await services.flightService.getFlightByData(
            flightNumber,
            record.departureAirport.iata,
            record.departureDate.dateLocal.split('T')[0],
          );
        } else {
          record = previousQuery.flights[num].flight as FlightResponse;
          flightNumber = `${record.flight.carrier.iata}${record.flight.number}`;
          if (
            allFlights &&
            allFlights.some(
              (flight) =>
                flight.flightNo.includes(flightNumber) &&
                flight.estimatedDepartureDateLocal ===
                  record.flight.scheduled_departure.split('T')[0] &&
                flight.origin === record.flight.origin.iata,
            )
          ) {
            //check if a user already tracked the flight
            return Il8nService.get('zenny:flightdata:flighttracked');
          }
          flight = await services.flightService.getFlightByData(
            flightNumber,
            record.flight.origin.iata,
            record.flight.scheduled_departure.split('T')[0],
          );
        }

        if (flight) {
          const tripId = await services.flightService.createUserTrip(
            passenger.id,
          );
          await services.flightService.createTrip(
            tripId.trip_id,
            null,
            flight.id,
            null,
            null,
          );
        } else {
          await trackSingleFlight(record, passenger, services.trackService);
        }
        await dialogflowService.contextClient.deleteContext({
          name: outputContexts[i].name,
        });

        // Removed 27 October 2020 as a precursor to dropping Epomeno
        /*

        let additionalPayload = {
          carrier: '',
          flightNumber: '',
          airport: '',
        };

        if (record.hasOwnProperty('airportResources')) {
          additionalPayload = {
            carrier: record.carrier.iata,
            flightNumber: record.flightNumber,
            airport: record.departureAirport.iata,
          };
        } else {
          additionalPayload = {
            carrier: record.flight.carrier.iata,
            flightNumber: String(record.flight.number),
            airport: record.flight.origin.iata,
          };
        }
        const formattedDate = record.hasOwnProperty('airportResources')
          ? record.departureDate.dateLocal
          : record.flight.scheduled_departure;

        const values = {
          firstName: passenger.firstName,
          lastName: passenger.lastName,
          email: passenger.email,
          phone: passenger.mobile,
          haveFlight: 'ticket',
          haveWhatsApp: true,
          has_whatsapp: true,
          haveConnecting: false,
          haveReturnFlight: false,
          departureAirport: '',
          date: new Date(),
          airportFrom: '',
          airportTo: '',
          connectingCarrier: '',
          connectingFlightNumber: '',
          departingAirport: '',
          returnCarrier: '',
          returnFlightNumber: '',
          returnAirport: '',
          returnDate: '',
        };


        const payload = {
          ...values,
          ...additionalPayload,
          mobile: passenger.mobile,
          date: formattedDate.split('T')[0],
        };

        await firestoreService.queueFlight(
            payload,
            passenger.firestoreId,
            lumoService,
          );
        */
        return Il8nService.get('zenny:flight:confirmTracking', {
          templateVars: {
            carrier: previousQuery.flights[num].flight_carrier,
            flightNumber: previousQuery.flights[num].flight_number,
          },
        });
      }
    }
    return Il8nService.get('zenny:flight:searchFail');
  }

  const flightArr: TrackedFlight[] = Array.from(previousQuery.flights);

  for (const flight of flightArr) {
    if (
      Number(previousQuery['flight-number']) === Number(flight.flight_number) ||
      String(previousQuery['flight-number']) ===
        String(flight.flight_carrier + flight.flight_number) ||
      String(previousQuery['flight-number']) ===
        String(flight.flight_carrier + ' ' + flight.flight_number)
    ) {
      const lumoRecord = flight.flight;
      const flightNumber = `${lumoRecord.flight.carrier.iata}${lumoRecord.flight.number}`;
      const allFlight = await services.flightService.getAllFlightsByUser(
        passenger.id,
      );
      if (
        allFlight &&
        allFlight.some(
          (flight) =>
            flight.flightNo.includes(flightNumber) &&
            flight.estimatedDepartureDateLocal ===
              lumoRecord.flight.scheduled_departure.split('T')[0] &&
            flight.origin === lumoRecord.flight.origin.iata,
        )
      ) {
        //check if a user already tracked the flight
        return Il8nService.get('zenny:flightdata:flighttracked');
      }
      const TrackedFlight = await services.flightService.getFlightByData(
        flightNumber,
        lumoRecord.flight.origin.iata,
        lumoRecord.flight.scheduled_departure.split('T')[0],
      );
      if (TrackedFlight) {
        const tripId = await services.flightService.createUserTrip(
          passenger.id,
        );
        await services.flightService.createTrip(
          tripId.trip_id,
          null,
          TrackedFlight.id,
          null,
          null,
        );
      } else {
        await trackSingleFlight(lumoRecord, passenger, services.trackService);
      }
      await dialogflowService.contextClient.deleteContext({
        name: outputContexts[i].name,
      });

      return Il8nService.get('zenny:flight:confirmTracking', {
        templateVars: {
          carrier: flight.flight_carrier,
          flightNumber: flight.flight_number,
        },
      });
    }
  }
}

export function getWaivers(lumoData: FlightResponse): string {
  let message = '';
  let counter = 0;
  let alert: any;
  if (lumoData.special_alerts.length > 0) {
    for (let i = 0; i < lumoData.special_alerts.length; i++) {
      alert = lumoData.special_alerts[i];
      if (alert.alert.type != 'waiver') {
        continue;
      }
      message += alert.alert.summary;
      message += '\n\n';
      counter++;
    }
  }

  if (!message) {
    return Il8nService.get('zenny:notfound:waivers');
  }

  if (counter > 1) {
    return (
      Il8nService.get('zenny:flightdata:waiversopener', {
        templateVars: {
          number: counter,
        },
      }) + message
    );
  }

  return message;
}

export async function sendMessage(
  from: string,
  to: string,
  source: string,
  message: string,
  services: ExternalServices,
): Promise<void> {
  const commsEventService = new CommsEventService(services.db);
  await beeQueueService.createResponseJob(
    services.bq,
    to,
    from,
    source,
    message,
  );

  const addedEvent = await commsEventService.addEvent({
    from: to,
    to: from,
    message,
    server: require('os').hostname(),
    source: source,
    created: new Date(),
    result: 'success',
  });

  logger.debug('Logged message to audit DB', addedEvent);
}

export async function resetContext(identifier: string): Promise<string> {
  try {
    const sessionPath = dialogflowService.getSessionPath(identifier);
    await dialogflowService.contextClient.deleteAllContexts({
      parent: sessionPath,
    });
    return Il8nService.get('zenny:success:resetContext');
  } catch (e) /* istanbul ignore next */ {
    logger.error(`Reset context error`, { error: e });
    return 'An error occurred';
  }
}

export async function processIncomingBQJob(
  services: ExternalServices,
  id: string,
  data: twiliosms,
): Promise<string> {
  const commsEventService = new CommsEventService(services.db);
  const userService = new UserService(services.db);
  const flightService = new FlightService(services.db);
  const trackService = new TrackService(
    lumoService,
    flightStatsService,
    flightService,
  );
  const trackingServices = {
    userService,
    trackService,
    flightService,
  };
  logger.info(`Processing message ${id} from comms in bot server`, {
    ...data,
    queue: ConfigService.get('QUEUE_NAME_TWILIO'),
  });
  const blockList: BlockList = JSON.parse(
    fs
      .readFileSync(path.resolve('./src/resources', './blockList.json'))
      .toString(),
  );

  const message = <twiliosms>data;
  if (
    blockList.platform.whatsapp.numbers.includes(
      message.From.slice(message.From.indexOf(':') + 1),
    )
  ) {
    return 'BlockListed';
  }
  /* istanbul ignore next */
  if (message.To.includes('whatsapp')) {
    message.Source = 'whatsapp';
  } else if (message.To.includes('test')) {
    message.Source = 'test';
  } else {
    message.Source = 'sms';
  }

  if (ConfigService.get('FRONT_TWILIO') && message.Source !== 'test') {
    try {
      await axios({
        method: 'post',
        url: ConfigService.get('FRONT_TWILIO'),
        data: qs.stringify(data),
        headers: {
          'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
        },
      });
    } catch (err) /* istanbul ignore next */ {
      logger.error(`Failed to post a message to Front`, { error: err });
    }
  }
  const identifier = `${message.To}_${message.From}`;

  const passenger = await getPassengerFrom(identifier, userService);

  const dfResult = await dialogflowService.getResponse(
    message.From,
    message.To,
    message.Body,
    message.Source,
    identifier,
  );

  logger.info(
    `Got ${dfResult.intent.displayName} from DialogFlow`,
    dfResult.intent,
  );

  let intentName: string = dfResult.intent.displayName;

  if (passenger && passenger.muteBot) {
    const addedEvent = await commsEventService.addEvent({
      from: message.From,
      to: message.To,
      message: message.Body,
      server: require('os').hostname(),
      source: message.Source,
      created: new Date(),
      result: 'success',
      intent: intentName,
    });
    logger.debug('Logged message to audit DB', addedEvent);
    logger.info('Not replying to last mode - bot is muted');
    return;
  }
  let messageTobeSent = '';
  try {
    const currUser = await userPhoneSearch(userService, message);
    if (currUser.confirmed_messaging === false && intentName !== 'generic_no') {
      currUser.confirmed_messaging = true;
      await userService.userRepository.save(currUser);
      if (intentName === 'generic_yes') {
        messageTobeSent = Il8nService.get('zenny:user:confirmed1', {
          templateVars: {
            name: currUser.firstName,
          },
        });
        let tempMessage = '';
        const allFlights = await flightService.getAllFlightsByUser(currUser.id);
        if (allFlights && allFlights.length !== 0) {
          let index = 0;
          tempMessage += '_Outbound:_\n';
          while (index < allFlights.length) {
            if ([1, 2].includes(index)) {
              tempMessage += '\n_Inbound:_\n';
            }
            const currFlight = allFlights[index];
            index += 1;
            tempMessage += Il8nService.get('zenny:flightdata:flightsum', {
              templateVars: {
                flight_number: currFlight.flightNo,
                departure_airport: currFlight.origin,
                arrival_airport: currFlight.destination,
                time: getNiceHourString(currFlight.scheduledDepartureTimeLocal),
                date: getNiceDateString(currFlight.scheduledDepartureDateLocal),
              },
            });

            if (currFlight.seq === 0) {
              tempMessage += '\nwith a connection: ';
              const connFlight = allFlights[index];
              index += 1;
              tempMessage += Il8nService.get('zenny:flightdata:flightsum', {
                templateVars: {
                  flight_number: connFlight.flightNo,
                  departure_airport: connFlight.origin,
                  arrival_airport: connFlight.destination,
                  time: getNiceHourString(
                    connFlight.scheduledDepartureTimeLocal,
                  ),
                  date: getNiceDateString(
                    connFlight.scheduledDepartureDateLocal,
                  ),
                },
              });
            }
            tempMessage += '.';
          }
          if (tempMessage !== '') {
            messageTobeSent +=
              '|||' +
              Il8nService.get('zenny:user:confirmed2', {
                templateVars: {
                  flights_sum: tempMessage,
                },
              });
          }
        }
      }
    }
  } catch (err) {
    logger.error(err);
  }
  if (
    dfResult.outputContexts.some((context) =>
      context.name.includes('second_feedback'),
    )
  ) {
    intentName = 'got_feedback';
  }
  switch (intentName) {
    case 'Default Fallback Intent': {
      const lastEvent = await commsEventService.getLastEvent(
        message.From,
        message.To,
      );
      const lastIntent = lastEvent.intent;
      if (
        lastIntent === 'Default Fallback Intent' &&
        new Date().getTime() - lastEvent.created.getTime() <
          60000 * 15 /*15 minutes*/
      ) {
        const yesterday = new Date();
        yesterday.setDate(yesterday.getDate() - 1);
        const mutedInTheLastDay = await commsEventService.eventRepository.findOne(
          {
            where: {
              from: message.To,
              message: Like('%Jet Lag%'),
              created: MoreThan(yesterday),
            },
          },
        );
        if (mutedInTheLastDay) {
          messageTobeSent = Il8nService.get('zenny:secondDefault2');
        } else {
          messageTobeSent = Il8nService.get('zenny:secondDefault1');
        }
        await userService.muteBot(message.From.replace(/\D/g, ''), true);
        const user = await userPhoneSearch(userService, message);
        let name = 'Name Not found';
        let mobile = message.From.replace(/\D/g, '');
        if (user) {
          name = user.name;
          mobile = user.mobile;
        }
        const username = name + '-' + mobile;
        const slackmsg =
          username +
          ` has failed to get a response from Zenny after 2 interactions.`;
        await axios.post(ConfigService.get('SLACK_USER_ASSISTANCE'), {
          text: slackmsg,
        });
      }
      break;
    }
    case 'need_help': {
      await userService.muteBot(message.From.replace(/\D/g, ''), true);
      const user = await userPhoneSearch(userService, message);
      let name = 'Name Not found';
      let mobile = message.From.replace(/\D/g, '');
      if (user) {
        name = user.name;
        mobile = user.mobile;
      }
      const username = name + '-' + mobile;

      const slackmsg = username + ` has just asked for a human to help.`;
      await axios.post(ConfigService.get('SLACK_USER_ASSISTANCE'), {
        text: slackmsg,
      });
      break;
    }
    case 'next_flight': {
      const currUser = await userPhoneSearch(userService, message);
      /* istanbul ignore next */
      if (!currUser) {
        messageTobeSent = Il8nService.get('zenny:notfound:user');
        break;
      }

      let nextFlight;

      try {
        nextFlight = await flightService.getNextFlightByUser(currUser.id);
      } catch (e) {
        logger.error('Failed to get next flight', { error: e });
      }

      /* istanbul ignore next */
      if (!nextFlight) {
        messageTobeSent = Il8nService.get('zenny:notfound:upcomingflights');
        break;
      }
      messageTobeSent = Il8nService.get('zenny:flightdata:nextflight', {
        templateVars: {
          summary: FlightHelper.toShortSummary(true, nextFlight),
        },
      });
      break;
    }
    case 'arrival_terminal': {
      const currUser = await userPhoneSearch(userService, message);
      /* istanbul ignore next */
      if (!currUser) {
        messageTobeSent = Il8nService.get('zenny:notfound:user');
        break;
      }
      const nextArrivalFlight = await flightService.getNextArrivalFlight(
        currUser.id,
      );
      if (nextArrivalFlight.destinationTerminal) {
        messageTobeSent = Il8nService.get('zenny:flightdata:arrivalterminal', {
          templateVars: {
            terminal: nextArrivalFlight.destinationTerminal,
          },
        });
      } else {
        messageTobeSent = Il8nService.get('zenny:notfound:terminal');
      }
      break;
    }
    case 'tips': {
      const airtabelService = AirtableFactory.create();
      let terminal;
      let airport =
        dfResult.parameters.fields['airport']?.listValue?.values[0]?.structValue
          ?.fields.IATA.stringValue ?? null;
      if (airport == null) {
        const currUser = await userPhoneSearch(userService, message);
        /* istanbul ignore next */
        if (!currUser) {
          messageTobeSent = 'Please specify which airport you are looking for';
          break;
        }
        airport =
          (await flightService.getNextFlightByUser(currUser.id))?.origin ??
          null;
        if (!airport) {
          messageTobeSent = 'Please specify which airport you are looking for';
          break;
        }
      }
      try {
        terminal = dfResult.parameters.fields['number'].numberValue.toString();
      } catch {
        terminal = null;
      }

      let tips = await airtabelService.getMessageForAirport({
        airportCode: airport,
        active: true,
        terminal: terminal,
      });
      if (terminal !== null) {
        tips = tips.concat(
          await airtabelService.getMessageForAirport({
            airportCode: airport,
            active: true,
            terminal: '_',
          }),
        );
      }
      if (tips[0] !== undefined) {
        messageTobeSent = `Here are the tips for ${airport} ${
          !terminal ? '' : `in terminal ${terminal}`
        }: \n`;
        tips.forEach((tip: any) => {
          messageTobeSent += `* ${tip.fields.message}\n`;
        });
      } else {
        messageTobeSent = 'No tips were found for this Query';
      }

      break;
    }
    case 'flight_time_check': {
      // This doesn't need an account
      // Just find the answer and return it
      break;
    }
    case 'next_arrival_time': {
      const currUser = await userPhoneSearch(userService, message);
      /* istanbul ignore next */
      if (!currUser) {
        messageTobeSent = Il8nService.get('zenny:notfound:user');
        break;
      }
      const nextArrivalFlight = await flightService.getNextArrivalFlight(
        currUser.id,
      );
      /* istanbul ignore next */
      if (!nextArrivalFlight) {
        messageTobeSent = Il8nService.get('zenny:notfound:upcomingflights');
        break;
      }
      messageTobeSent = Il8nService.get('zenny:flightdata:nextarrival', {
        templateVars: {
          flightno: nextArrivalFlight.flightNo,
          origin: nextArrivalFlight.origin,
          destination: nextArrivalFlight.destination,
          hour: getNiceHourString(nextArrivalFlight.estimatedArrivalTimeLocal),
          date: getNiceDateString(nextArrivalFlight.estimatedArrivalDateLocal),
        },
      });
      break;
    }
    case 'next_departure_time': {
      const currUser = await userPhoneSearch(userService, message);
      /* istanbul ignore next */
      if (!currUser) {
        messageTobeSent = Il8nService.get('zenny:notfound:user');
        break;
      }
      const nextDepartureFlight = await flightService.getNextDepartureFlight(
        currUser.id,
      );
      /* istanbul ignore next */
      if (!nextDepartureFlight) {
        messageTobeSent = Il8nService.get('zenny:notfound:upcomingflights');
        break;
      }
      messageTobeSent = Il8nService.get('zenny:flightdata:nextdepart', {
        templateVars: {
          time_until_departure: getNiceTimeDiffString(
            nextDepartureFlight.estimatedDeparture.toISOString(),
            new Date().toISOString(),
          ),
          flightno: nextDepartureFlight.flightNo,
          origin: nextDepartureFlight.origin,
          destination: nextDepartureFlight.destination,
          hour: getNiceHourString(
            nextDepartureFlight.estimatedDepartureTimeLocal,
          ),
          date: getNiceDateString(
            nextDepartureFlight.estimatedDepartureDateLocal,
          ),
        },
      });
      break;
    }
    case 'lost_baggage': {
      const currUser = await userPhoneSearch(userService, message);
      logger.info(`Looking up all flights for ${currUser.name}`);
      /* istanbul ignore next */
      if (!currUser) {
        messageTobeSent = Il8nService.get('zenny:notfound:user');
        break;
      }
      const LandedFlights = await flightService.getLandedFlightsByUser(
        currUser.id,
      );
      /* istanbul ignore next */
      if (!LandedFlights || !LandedFlights.length) {
        messageTobeSent = Il8nService.get('zenny:notfound:upcomingflights');
        break;
      }
      const carrier =
        LandedFlights[0].operatingCarrier || LandedFlights[0].marketedCarrier;
      messageTobeSent = await getLostBaggageData(carrier);
      break;
    }
    case 'all_flights': {
      const currUser = await userPhoneSearch(userService, message);
      logger.info(`Looking up all flights for ${currUser.name}`);
      /* istanbul ignore next */
      if (!currUser) {
        messageTobeSent = Il8nService.get('zenny:notfound:user');
        break;
      }
      const upcomingFlights = await flightService.getAllFlightsByUser(
        currUser.id,
      );
      /* istanbul ignore next */
      if (!upcomingFlights || !upcomingFlights.length) {
        messageTobeSent = Il8nService.get('zenny:notfound:upcomingflights');
        break;
      }
      /* istanbul ignore next */
      if (upcomingFlights.length === 1) {
        let templatePath: string;

        // If the flight is in the air, call it your current flight instead
        if (FlightHelper.isBeforeDeparture(upcomingFlights[0])) {
          templatePath = 'zenny:flightdata:nextflight';
        } else {
          templatePath = 'zenny:flightdata:currentflight';
        }

        messageTobeSent = Il8nService.get(templatePath, {
          templateVars: {
            summary: FlightHelper.toShortSummary(true, upcomingFlights[0]),
          },
        });
        break;
      }
      const flightTexts: Array<string> = upcomingFlights.map((flight, i) => {
        return `${i + 1}. ${FlightHelper.toShortSummary(true, flight)}`;
      });

      messageTobeSent = Il8nService.get('zenny:flightdata:allflights', {
        templateVars: {
          length: flightTexts.length,
          flights: flightTexts.join('\n\n'),
        },
      });
      break;
    }
    case 'interactive_date': {
      const departure = loadContext(
        dfResult.outputContexts,
        'departure-followup',
      );
      const arrival = loadContext(dfResult.outputContexts, 'arrival-followup');
      const date = dfResult.parameters;
      messageTobeSent = await processInteractiveTracking(
        departure.fields.departure.stringValue === ''
          ? departure.fields.departure_airport
          : departure.fields.departure.stringValue,
        arrival.fields.arrival.stringValue === ''
          ? arrival.fields.arrival_airport
          : arrival.fields.arrival.stringValue,
        date,
        identifier,
      );
      break;
    }
    case 'delete_flight - number': {
      const numbers = dfResult.parameters.fields['number'].listValue.values.map(
        (item) => item.numberValue,
      );
      const currUser = await userPhoneSearch(userService, message);
      let flag = false;
      logger.info(`Looking up all flights for ${currUser.name}`);
      /* istanbul ignore next */
      if (!currUser) {
        messageTobeSent = Il8nService.get('zenny:notfound:user');
        break;
      }
      const upcomingFlights = await flightService.getAllFlightsByUser(
        currUser.id,
      );
      /* istanbul ignore next */
      if (!upcomingFlights || !upcomingFlights.length) {
        messageTobeSent = Il8nService.get('zenny:notfound:upcomingflights');
        break;
      }

      let numberMap: void[] = numbers.map((number) => {
        if (number > upcomingFlights.length || number <= 0) {
          flag = true;
        }
      });
      if (flag) {
        messageTobeSent = 'You picked an invalid number.';
        break;
      }
      const selectedFlights: Flight[] = [];

      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      numberMap = numbers.map((number) => {
        selectedFlights.push(upcomingFlights[number - 1]);
      });
      messageTobeSent = await deleteFlight(
        currUser.id,
        selectedFlights.map((flight) => flight.id),
        flightService,
      );
      break;
    }
    /*istanbul ignore next*/
    case 'delete_flight - yes': {
      const currUser = await userPhoneSearch(userService, message);
      logger.info(`Looking up all flights for ${currUser.name}`);
      /* istanbul ignore next */
      if (!currUser) {
        messageTobeSent = Il8nService.get('zenny:notfound:user');
        break;
      }
      const upcomingFlights = await flightService.getAllFlightsByUser(
        currUser.id,
      );
      /* istanbul ignore next */
      if (!upcomingFlights || !upcomingFlights.length) {
        messageTobeSent = Il8nService.get('zenny:notfound:upcomingflights');
        break;
      }
      /* istanbul ignore next */
      if (upcomingFlights.length === 1) {
        messageTobeSent = 'Sure, I have stopped tracking this flight!';
        deleteFlight(currUser.id, [upcomingFlights[0].id], flightService);
        break;
      }
      messageTobeSent =
        'You need to pick the number of the flight you want to stop tracking for confirmation.';
      break;
    }
    /* istanbul ignore next */
    //the only function used is getting a user which is testsd
    case 'flight_length': {
      const currUser = await userPhoneSearch(userService, message);
      logger.info(`Looking up all flights for ${currUser.name}`);
      /* istanbul ignore next */
      if (!currUser) {
        messageTobeSent = Il8nService.get('zenny:notfound:user');
        break;
      }
      const upcomingFlights = await flightService.getAllFlightsByUser(
        currUser.id,
      );
      /* istanbul ignore next */
      if (!upcomingFlights || !upcomingFlights.length) {
        messageTobeSent = Il8nService.get('zenny:notfound:upcomingflights');
        break;
      }
      messageTobeSent = `Your flight will take ${getNiceTimeDiffString(
        upcomingFlights[0].estimatedArrival.toISOString(),
        upcomingFlights[0].estimatedDeparture.toISOString(),
      )}`;
      break;
    }
    case 'delete_flight': {
      const currUser = await userPhoneSearch(userService, message);
      logger.info(`Looking up all flights for ${currUser.name}`);
      /* istanbul ignore next */
      if (!currUser) {
        messageTobeSent = Il8nService.get('zenny:notfound:user');
        break;
      }
      const upcomingFlights = await flightService.getAllFlightsByUser(
        currUser.id,
      );
      /* istanbul ignore next */
      if (!upcomingFlights || !upcomingFlights.length) {
        messageTobeSent = Il8nService.get('zenny:notfound:upcomingflights');
        break;
      }
      /* istanbul ignore next */
      if (upcomingFlights.length === 1) {
        let templatePath: string;

        // If the flight is in the air, call it your current flight instead
        if (FlightHelper.isBeforeDeparture(upcomingFlights[0])) {
          templatePath = 'zenny:flightdata:nextflight';
        } else {
          templatePath = 'zenny:flightdata:currentflight';
        }

        messageTobeSent =
          Il8nService.get(templatePath, {
            templateVars: {
              summary: FlightHelper.toShortSummary(true, upcomingFlights[0]),
            },
          }) + '\n\nis this the flight you wanted to stop tracking?';
        break;
      }
      const flightTexts: Array<string> = upcomingFlights.map((flight, i) => {
        return `${i + 1}. ${FlightHelper.toShortSummary(true, flight)}`;
      });

      messageTobeSent =
        Il8nService.get('zenny:flightdata:allflights', {
          templateVars: {
            length: flightTexts.length,
            flights: flightTexts.join('\n\n'),
          },
        }) + '\n\nPlease confirm the flight you want to stop tracking';
      break;
    }
    case 'when_should_i_leave': {
      messageTobeSent = Il8nService.get('zenny:TTL');

      break;
    }
    // Remove this when it's working
    /* istanbul ignore next */
    case 'safe_to_fly_to': {
      try {
        let country: IStruct =
          dfResult.parameters.fields['geo-country-code']?.structValue ?? null;
        let countryCode = country?.fields['alpha-2']?.stringValue ?? null;
        const countryName = country?.fields['name']?.stringValue ?? null;
        let city = dfResult.parameters.fields['geo-city']?.stringValue ?? null;
        let bestResult: CityTable = null;
        let resendText;
        if (dfResult.queryText.toLowerCase().includes('tonga')) {
          countryCode = 'TO';
        } else if (!city && countryName === 'Tonga') {
          resendText = dfResult.queryText.toLowerCase().split('to').join('');
          const ans = dialogflowService.getResponse(
            'whatsapp:' + message.From.replace(/\D/g, ''),
            'whatsapp:' + ConfigService.get('PRIMARY_PHONE'),
            resendText,
            'city-res',
            'city-res',
          );
          country =
            (await ans).parameters.fields['geo-country-code']?.structValue ??
            null;
          countryCode = country?.fields['alpha-2']?.stringValue ?? null;
          city = (await ans).parameters.fields['geo-city']?.stringValue ?? null;
          if (city && !country) {
            bestResult = await getCity(services, city); //biggest city temp
            countryCode = bestResult.iso2;
          }
        } else if (city && (!country || countryName === 'Tonga')) {
          bestResult = await getCity(services, city); //biggest city temp
          countryCode = bestResult.iso2;
        }
        const covidSummary = await sitataService.getCovidSummary(countryCode);

        if (!covidSummary) {
          messageTobeSent = Il8nService.get('zenny:notfound:country');
          break;
        }

        messageTobeSent = Il8nService.get('zenny:sitata:covidsummary', {
          templateVars: {
            country:
              bestResult?.country || country?.fields['name']?.stringValue,
            city: bestResult?.city ? `${bestResult?.city}, ` : '',
            risk: getRiskWord(covidSummary.summary.sitata_risk),
            active_cases: covidSummary.summary.active_count,
            case_density: Math.floor(covidSummary.summary.case_density),
          },
        });
      } catch (err) {
        logger.error(err);
        messageTobeSent = Il8nService.get('zenny:notfound:country');
      }
      if (messageTobeSent.length === 0) {
        messageTobeSent = Il8nService.get('zenny:notfound:country');
      }
      break;
    }
    case 'safe_to_fly_to_select_number': {
      const params = loadContext(
        dfResult.outputContexts,
        'safe_to_fly_to-followup',
      );
      const country: IStruct =
        params.fields['geo-country-code']?.structValue ?? null;
      let countryCode = country?.fields['alpha-2']?.stringValue ?? null;
      const city = params.fields['geo-city']?.stringValue ?? null;
      let bestResult: CityTable = null;
      if (city && !country) {
        bestResult = await getCity(services, city); //biggest city temp

        countryCode = bestResult.iso2;
      }

      if (!params) {
        /*istanbul ignore next */
        messageTobeSent = `Sorry, I wasn't able to find that information`;
        break;
      }

      const options = [];
      for (
        let i = 0;
        i < params.fields['number'].listValue.values.length;
        i++
      ) {
        options.push(
          String(params.fields['number'].listValue.values[i].numberValue),
        );
      }

      for (
        let i = 0;
        i < params.fields['sitata'].listValue.values.length;
        i++
      ) {
        const val: string =
          params.fields['sitata'].listValue.values[i].stringValue;
        options.push(
          String(
            val.toLowerCase() == 'masks'
              ? 2
              : val.toLowerCase() == 'social'
              ? 3
              : 1,
          ),
        );
      }

      let option = '';

      const travelStatus = await sitataService.getRestrictions(countryCode);

      const availableTravelRestrictions = sitataService.getAvailableCodes(
        travelStatus,
      );
      if (options.includes('1')) {
        messageTobeSent += '|||ENTRY:\n';
        if (availableTravelRestrictions.indexOf('0') >= 0) {
          messageTobeSent += '\n\nAirline:\n';
          messageTobeSent += travelStatus.airline[0].comment;
        }
        if (availableTravelRestrictions.indexOf('1') >= 0) {
          messageTobeSent += '\n\nBorders:\n';
          messageTobeSent += travelStatus.border[0].comment;
        }
        if (availableTravelRestrictions.indexOf('201') >= 0) {
          messageTobeSent += '\n\nBorder Application::\n';
          /*istanbul ignore next */
          messageTobeSent += travelStatus.borderApp[0].comment;
        }

        if (typeof travelStatus.curfew[0] === IS_UNDEFINED) {
          /*istanbul ignore next*/
          option = 'Entry';
          messageTobeSent += `I cant find data about ${option} in my databases, but I am constantly updating so you can ask another time`;
        }
      }
      if (options.includes('2')) {
        messageTobeSent += '|||MASKS:\n';
        if (availableTravelRestrictions.indexOf('410') >= 0) {
          messageTobeSent += travelStatus.masks[0].comment;
        }
        if (availableTravelRestrictions.indexOf('3') >= 0) {
          messageTobeSent += '\n\nSocial:\n';
          messageTobeSent += travelStatus.social[0].comment;
        }
        if (availableTravelRestrictions.indexOf('2') >= 0) {
          messageTobeSent += '\n\nCurfew:\n';
          /*istanbul ignore next */
          messageTobeSent += travelStatus.curfew[0].comment;
        }
        if (typeof travelStatus.masks[0] === IS_UNDEFINED) {
          /*istanbul ignore next*/
          option = 'Masks';
          messageTobeSent += `I cant find data about ${option} in my databases, but I am constantly updating so you can ask another time`;
        }
      }
      if (options.includes('3')) {
        messageTobeSent += '|||SOCIAL:\n';
        if (availableTravelRestrictions.indexOf('402') >= 0) {
          messageTobeSent += '\nAccommodations:\n';
          /*istanbul ignore next */
          messageTobeSent += travelStatus.accommodation[0].comment;
        }
        if (availableTravelRestrictions.indexOf('4') >= 0) {
          messageTobeSent += '\n\nTransportation:\n';
          messageTobeSent += travelStatus.transportation[0].comment;
        }
        if (availableTravelRestrictions.indexOf('409') >= 0) {
          messageTobeSent += '\n\nEvents:\n';
          /*istanbul ignore next */
          messageTobeSent += travelStatus.events[0].comment;
        }
        if (availableTravelRestrictions.indexOf('403') >= 0) {
          messageTobeSent += '\n\nRestaurants:\n';
          messageTobeSent += travelStatus.restaurants[0].comment;
        }
        if (availableTravelRestrictions.indexOf('404') >= 0) {
          messageTobeSent += '\n\nBars and Cafes:\n';
          /*istanbul ignore next */
          messageTobeSent += travelStatus.barsAndCafes[0].comment;
        }
        if (availableTravelRestrictions.indexOf('405') >= 0) {
          messageTobeSent += '\n\nBeaches:\n';
          messageTobeSent += travelStatus.beaches[0].comment;
        }
        if (availableTravelRestrictions.indexOf('406') >= 0) {
          messageTobeSent += '\n\nMuseums:\n';
          messageTobeSent += travelStatus.museums[0].comment;
        }
        if (availableTravelRestrictions.indexOf('407') >= 0) {
          messageTobeSent += '\n\nPersonal care:\n';
          /*istanbul ignore next */
          messageTobeSent += travelStatus.personalCare[0].comment;
        }
        if (availableTravelRestrictions.indexOf('408') >= 0) {
          messageTobeSent += '\n\nPlaces of worship:\n';
          /*istanbul ignore next */
          messageTobeSent += travelStatus.placesOfWorship[0].comment;
        }
        if (typeof travelStatus.border === IS_UNDEFINED) {
          /*istanbul ignore next*/
          option = 'Social';
          messageTobeSent += `I cant find data about ${option} in my databases, but I am constantly updating so you can ask another time`;
        }
      }

      messageTobeSent = sitataFormat(messageTobeSent);
      break;
    }

    case 'entry': {
      try {
        const country: IStruct =
          dfResult.parameters.fields['geo-country-code']?.structValue ?? null;
        let countryCode = country?.fields['alpha-2']?.stringValue ?? null;
        const city =
          dfResult.parameters.fields['geo-city']?.stringValue ?? null;
        let bestResult: CityTable = null;
        if (city && !country) {
          bestResult = await getCity(services, city);
          countryCode = bestResult.iso2;
        }
        const travelStatus = await sitataService.getRestrictions(countryCode);
        const availableTravelRestrictions = sitataService.getAvailableCodes(
          travelStatus,
        );

        messageTobeSent += '|||ENTRY:\n';
        if (availableTravelRestrictions.indexOf('0') >= 0) {
          messageTobeSent += '\nAirline:\n';
          messageTobeSent += travelStatus.airline[0].comment;
        }
        if (availableTravelRestrictions.indexOf('1') >= 0) {
          messageTobeSent += '\n\nBorders:\n';
          messageTobeSent += travelStatus.border[0].comment;
        }
        if (availableTravelRestrictions.indexOf('201') >= 0) {
          messageTobeSent += '\n\nBorder Application::\n';
          /*istanbul ignore next */
          messageTobeSent += travelStatus.borderApp[0].comment;
        }

        if (typeof travelStatus.curfew[0] === IS_UNDEFINED) {
          /*istanbul ignore next*/
          messageTobeSent += Il8nService.get('zenny:notfound:entry');
        }
      } catch (err) {
        logger.error(err);
        messageTobeSent = Il8nService.get('zenny:notfound:country');
      }
      if (messageTobeSent.length === 0) {
        messageTobeSent = Il8nService.get('zenny:notfound:country');
      }

      break;
    }
    case 'jnf_airport_confirmed': {
      const airport = (
        await flightService.getNextFlightByUser(
          (await userPhoneSearch(userService, message)).id,
        )
      )?.origin;
      messageTobeSent = airport
        ? await jackandferdiService.getPlacesInAirport(airport)
        : Il8nService.get('zenny:jackandferdi:airportnotfound');
      break;
    }
    case 'jnf_airport': {
      const airport =
        dfResult.parameters.fields['airport']?.structValue?.fields?.IATA
          .stringValue ??
        (
          await flightService.getNextFlightByUser(
            (await userPhoneSearch(userService, message)).id,
          )
        )?.origin;
      messageTobeSent = airport
        ? await jackandferdiService.getPlacesInAirport(airport)
        : Il8nService.get('zenny:jackandferdi:airportnotfound');
      break;
    }
    case 'JnF': {
      const city = dfResult.parameters.fields['geo-city'].stringValue;
      /*istanbul ignore next*/
      if (!city) {
        messageTobeSent = Il8nService.get('zenny:notfound:city');
        break;
      }
      messageTobeSent = await jackandferdiService.getPlaces(
        city,
        dfResult.parameters.fields['jackandferdi'].listValue.values.map(
          (a) => a.stringValue,
        ),
      );
      break;
    }
    case 'funfact': {
      const city = dfResult.parameters.fields['geo-city'].stringValue;
      /*istanbul ignore next*/
      if (!city) {
        messageTobeSent = Il8nService.get('zenny:notfound:city');
        break;
      }
      messageTobeSent = await jackandferdiService.getFunFact(city);
      break;
    }
    case 'first_feedback': {
      let data;
      try {
        data = fs.readFileSync('reviews_sum.json').toString();
      } catch {
        fs.writeFileSync('reviews_sum.json', '{}');
        data = '{}';
      }
      const reviews = JSON.parse(data);
      reviews[dfResult.parameters.fields['number'].numberValue.toString()]
        ? (reviews[
            dfResult.parameters.fields['number'].numberValue.toString()
          ] += 1)
        : (reviews[
            dfResult.parameters.fields['number'].numberValue.toString()
          ] = 1);
      fs.writeFileSync('reviews_sum.json', JSON.stringify(reviews));
      const sessionPath = dialogflowService.getSessionPath(identifier);
      const request = {
        parent: sessionPath,
        context: {
          name: sessionPath + '/contexts/second_feedback',
          lifespanCount: 2,
        },
      };
      await dialogflowService.contextClient.createContext(request);
      messageTobeSent = Il8nService.get('zenny:feedback:second');
      break;
    }
    case 'got_feedback': {
      messageTobeSent = Il8nService.get('zenny:feedback:third');

      const currUser = await userPhoneSearch(userService, message);

      /* istanbul ignore next */
      if (!currUser) {
        messageTobeSent = Il8nService.get('zenny:notfound:user');
        break;
      }
      const flights = await flightService.getLandedFlightsByUser(currUser.id);
      writer.pipe(fs.createWriteStream('feedbacks.csv'));
      writer.write({
        user: currUser.name,
        feedback: message.Body,
        flights: flights
          .map((flight) => flight.origin + ' ==> ' + flight.destination)
          .join(','),
      });
      writer.end();
      break;
    }
    case 'delay_status': {
      const currUser = await userPhoneSearch(userService, message);

      /* istanbul ignore next */
      if (!currUser) {
        messageTobeSent = Il8nService.get('zenny:notfound:user');
        break;
      }
      const nextFlight = await flightService.getNextFlightByUser(currUser.id);
      /* istanbul ignore next */
      if (!nextFlight) {
        messageTobeSent = Il8nService.get('zenny:notfound:upcomingflights');
        break;
      }
      const minutesDiff = getDiffMinutes(
        nextFlight.scheduledDeparture.toJSON(),
        nextFlight.estimatedDeparture.toISOString(),
      );
      const status =
        minutesDiff == 0
          ? 'on time'
          : minutesDiff < 0
          ? -minutesDiff + ' minutes late'
          : minutesDiff + ' minutes early';
      let city = '';
      try {
        city = nextFlight.initialLumoData.flight.destination.city;
      } catch /*istanbul ignore next*/ {
        city = AirportService.search(nextFlight.destination)[0].municipality;
      }
      messageTobeSent = `Your flight (${nextFlight.flightNo} to ${city}) is ${status}.`;
      break;
    }
    /* istanbul ignore next */
    case 'health_tips': {
      const city = dfResult.parameters.fields['geo-city'].stringValue;
      /*istanbul ignore next*/
      if (!city) {
        messageTobeSent = Il8nService.get('zenny:notfound:city');
        break;
      }
      messageTobeSent = await jackandferdiService.getCityData(
        city,
        'healthytricks',
      );
      break;
    }
    /* istanbul ignore next */
    case 'manners': {
      const city = dfResult.parameters.fields['geo-city'].stringValue;
      /*istanbul ignore next*/
      if (!city) {
        messageTobeSent = Il8nService.get('zenny:notfound:city');
        break;
      }
      messageTobeSent = await jackandferdiService.getCityData(city, 'manners');
      break;
    }
    /* istanbul ignore next */
    case 'work_places': {
      const city = dfResult.parameters.fields['geo-city'].stringValue;
      /*istanbul ignore next*/
      if (!city) {
        messageTobeSent = Il8nService.get('zenny:notfound:city');
        break;
      }
      messageTobeSent = await jackandferdiService.getWorkPlaces(city);
      break;
    }
    /* istanbul ignore next */
    case 'time_in_location': {
      const city =
        dfResult.parameters.fields['geo-city'].listValue.values[0].stringValue;
      const time = new WeatherTime();
      try {
        messageTobeSent = `The time in ${city} is ${await time.getTime(city)}`;
      } catch (err) {
        logger.error(`Failed to get the time in ${city}`, { error: err });
      }
      break;
    }
    /* istanbul ignore next */
    case 'weather_for_location': {
      try {
        const city =
          dfResult.parameters.fields['geo-city'].listValue.values[0]
            .stringValue;

        // I don't know why we need to do this on PRD
        const weatherAPIKey = ConfigService.get('OPENWEATEHRMAP_API_KEY');
        const weatherURL =
          'https://api.openweathermap.org/data/2.5/weather?q=' +
          city +
          '&appid=' +
          weatherAPIKey;

        const weather = (await axios.get(weatherURL)).data;

        messageTobeSent = Il8nService.get('zenny:weather:display', {
          templateVars: {
            city: city,
            description: weather.weather[0].description,
            ftemp: Math.round(kelvinToFahrenheit(weather.main.temp)),
            ctemp: Math.round(kelvinToCelsius(weather.main.temp)),
          },
        });
      } catch (err) {
        messageTobeSent = Il8nService.get('zenny:notfound:city');
      }
      /* istanbul ignore next */
      if (messageTobeSent.length === 0) {
        messageTobeSent = Il8nService.get('zenny:notfound:city');
      }
      break;
    }
    case 'masks': {
      try {
        const country: IStruct =
          dfResult.parameters.fields['geo-country-code']?.structValue ?? null;
        let countryCode = country?.fields['alpha-2']?.stringValue ?? null;
        const city =
          dfResult.parameters.fields['geo-city']?.stringValue ?? null;
        let bestResult: CityTable = null;
        if (city && !country) {
          bestResult = await getCity(services, city);
          countryCode = bestResult.iso2;
        }
        const travelStatus = await sitataService.getRestrictions(countryCode);
        const availableTravelRestrictions = sitataService.getAvailableCodes(
          travelStatus,
        );

        let reply = '|||MASKS:\n';
        if (availableTravelRestrictions.indexOf('410') >= 0) {
          reply += travelStatus.masks[0].comment;
        }
        if (reply === '|||MASKS:\n') {
          messageTobeSent = `I cant find data about Masks in my databases, but I am constantly updating so you can ask another time`;
          break;
        }
        messageTobeSent = sitataFormat(reply);
      } catch (err) {
        logger.error(err);
        messageTobeSent = Il8nService.get('zenny:notfound:country');
      }
      if (messageTobeSent.length === 0) {
        messageTobeSent = Il8nService.get('zenny:notfound:country');
      }
      break;
    }
    // All lines of this case are covered by other tests
    /* istanbul ignore next */
    case 'next_connection_duration': {
      const currUser = await userPhoneSearch(userService, message);

      /* istanbul ignore next */
      if (!currUser) {
        messageTobeSent = Il8nService.get('zenny:notfound:user');
        break;
      }
      const userTrip: any = await services.db
        .getRepository('user_trip')
        .findOne({
          where: {
            user_id: currUser.id,
          },
        });

      const nextFlight = await flightService.getNextFlightByUser(currUser.id);
      const trip: any = await services.db.getRepository('trip').findOne({
        where: {
          trip_id: userTrip.trip_id,
          flight: nextFlight.id,
        },
      });

      messageTobeSent =
        trip && trip.conn_time
          ? `Your connection time is ${trip.conn_time} minutes`
          : 'Your flight does not have a connection';

      break;
    }

    case 'pickup': {
      try {
        const airtableService = AirtableFactory.create();
        let airtableRes;
        const currUser = await userPhoneSearch(userService, message);
        let destination;
        if (!currUser) {
          messageTobeSent = Il8nService.get('zenny:notfound:user');
        } else {
          destination =
            (await flightService.getNextFlightByUser(currUser.id))
              ?.destination ?? null;
          if (!destination) {
            messageTobeSent = Il8nService.get('zenny:directions:welcomepickup');
          } else {
            airtableRes = await airtableService.getValidPickups({
              'IATA Code': destination,
            });
            if (airtableRes[0]?.fields['IATA Code']) {
              messageTobeSent = Il8nService.get(
                'zenny:directions:welcomepickup',
              );
            } else {
              messageTobeSent = Il8nService.get('zenny:notfound:welcomepickup');
            }
          }
        }
      } catch (err) {
        logger.error(err);
        messageTobeSent = Il8nService.get('zenny:directions:welcomepickup');
      }
      break;
    }

    case 'connection_tracking': {
      const flightNumbers: IValue[] =
        dfResult.parameters.fields['flight-number'].listValue.values;

      const currUser = await userPhoneSearch(userService, message);

      /* istanbul ignore next */
      if (!currUser) {
        messageTobeSent = Il8nService.get('zenny:notfound:user');
        break;
      }
      const { day, month, departureDate } = await parseDfDates(
        dfResult.parameters,
      );

      const lumoRecords: SearchResponse<FlightResponse>[] = [];
      flightNumbers.map(async (flightNumber) => {
        const flightId = parseFlightNo(flightNumber);
        logger.info(
          `Got ${flightId.flight} and ${departureDate} from DialogFlow -- do search`,
        );
        const flightResult = await lumoSearch({
          flight_number: flightId.flight,
          carrier: flightId.carrier ?? '',
          date: departureDate.substr(0, 10),
        });
        lumoRecords.push(flightResult);
        await processAndCacheLumoResults({
          flightResults: flightResult,
          dfIdentifier: identifier,
          month,
          day,
        });

        await trackConnectionFlight(
          lumoRecords.map((lumoRecord) => lumoRecord.results[0]),
          currUser,
          trackService,
        );
      });
      messageTobeSent = 'We are tracking your connection';
      break;
    }

    case 'restaurants': {
      try {
        const country: IStruct =
          dfResult.parameters.fields['geo-country-code']?.structValue ?? null;
        let countryCode = country?.fields['alpha-2']?.stringValue ?? null;
        const city =
          dfResult.parameters.fields['geo-city']?.stringValue ?? null;
        let bestResult: CityTable = null;
        if (city && !country) {
          bestResult = await getCity(services, city);
          countryCode = bestResult.iso2;
        }
        const travelStatus = await sitataService.getRestrictions(countryCode);
        const availableTravelRestrictions = sitataService.getAvailableCodes(
          travelStatus,
        );
        let reply = '|||Restaurants:\n';
        if (availableTravelRestrictions.includes('403')) {
          reply += travelStatus.restaurants[0].comment;
        }
        if (reply === '|||Restaurants:\n') {
          messageTobeSent = `I cant find data about restaurants in my databases, but I am constantly updating so you can ask another time`;
          break;
        }

        messageTobeSent = sitataFormat(reply);
      } catch (err) {
        logger.error(err);
        messageTobeSent = Il8nService.get('zenny:notfound:country');
      }
      if (messageTobeSent.length === 0) {
        messageTobeSent = Il8nService.get('zenny:notfound:country');
      }
      break;
    }

    case 'flight_status': {
      const flightID: string =
        dfResult.parameters.fields['flight-number'].stringValue;
      const today = new Date();

      const flightOptions = {
        flight_number: parseInt(flightID.split(' ')[1]), //Dialogflow returns {{carrier}} {{flight number}}
        carrier: flightID.split(' ')[0].trim(), //[0] is the carrier
        date: `${today.getFullYear()}-${
          today.getMonth() + 1
        }-${today.getDate()}`,
      };

      let flightStatus;
      try {
        flightStatus = await lumoService.status(flightOptions);
      } catch (Error) {
        flightStatus = null;
      }
      messageTobeSent =
        flightStatus == null
          ? Il8nService.get('zenny:notfound:upcomingflights')
          : Il8nService.get('zenny:flight:status', {
              templateVars: {
                status: flightStatus[0].status.text,
              },
            });

      break;
    }
    // All lines of this case are covered by other tests
    /* istanbul ignore next */
    case 'directions': {
      const distance = new DirectionService(
        new Client(),
        ConfigService.get('GOOGLE_API_KEY'),
      );
      const currUser = await userPhoneSearch(userService, message);

      if (!currUser) {
        messageTobeSent = Il8nService.get('zenny:notfound:user');
        break;
      }
      /* istanbul ignore next */
      try {
        let city =
          dfResult.parameters.fields['geo-city'].listValue.values[0]
            ?.stringValue ?? (await predictCity(currUser, flightService))[0];

        const airport =
          dfResult.parameters.fields['airport']?.listValue.values[0]
            ?.structValue.fields.name.stringValue ??
          (await predictCity(currUser, flightService))[1];

        if (
          dfResult.parameters.fields['geo-city'].listValue.values[0]
            ?.stringValue == null &&
          dfResult.parameters.fields['airport']?.listValue.values[0]
            ?.structValue.fields.IATA.stringValue != null
        ) {
          const airPortCity =
            dfResult.parameters.fields['airport']?.listValue.values[0]
              ?.structValue.fields.city.stringValue;
          if (airPortCity) {
            city = airPortCity;
          }
        }

        const iataAirport =
          dfResult.parameters.fields['airport']?.listValue.values[0]
            ?.structValue.fields.IATA.stringValue ??
          (await predictCity(currUser, flightService))[2];

        let origin: string = airport;
        let destination: string = city;
        const lowerCaseBody = message.Body.toLowerCase();
        if (
          lowerCaseBody.indexOf(city.toLowerCase()) ||
          lowerCaseBody.indexOf(iataAirport.toLowerCase())
        ) {
          origin =
            lowerCaseBody.indexOf(city.toLowerCase()) >
            lowerCaseBody.indexOf(iataAirport.toLowerCase())
              ? airport
              : city;
          destination = origin === city ? airport : city;
        }

        const [car, transit] = await Promise.all([
          distance.getFastestWay(origin, destination, TravelMode.driving),
          distance.getFastestWay(origin, destination, TravelMode.transit),
        ]);
        let directionMessage = '';
        if (car.status === 'OK' || transit.status === 'OK') {
          directionMessage = Il8nService.get('zenny:directions:heading', {
            templateVars: {
              airport_name: origin,
              city_name: destination,
            },
          });
        }

        if (car.status === 'OK') {
          directionMessage += Il8nService.get('zenny:directions:car', {
            templateVars: {
              duration: car.duration.text,
            },
          });
        }

        if (transit.status === 'OK') {
          directionMessage += Il8nService.get(
            'zenny:directions:pubilctransport',
            {
              templateVars: {
                duration: transit.duration.text,
              },
            },
          );
        }
        directionMessage += Il8nService.get('zenny:directions:link', {
          templateVars: {
            origin: origin,
            destination: destination,
            link: `https://www.google.com/maps/dir/?api=1&origin=${encodeURI(
              origin,
            )}&destination=${encodeURI(destination)}`,
          },
        });

        messageTobeSent = directionMessage;
      } catch (err) {
        logger.error(`Could not get directions data ${err}`, { error: err });
        messageTobeSent =
          'Please specify the airport of origin and city of destination ';
      }
      break;
    }
    case 'usage': {
      messageTobeSent = Il8nService.get('zenny:usage');
      break;
    }
    // All lines of this case are covered by other tests
    /* istanbul ignore next */
    case 'flight_delay': {
      const currUser = await userPhoneSearch(userService, message);

      if (!currUser) {
        messageTobeSent = Il8nService.get('zenny:notfound:user');
        break;
      }
      const nextFlight = await flightService.getNextFlightByUser(currUser.id);
      if (!nextFlight) {
        messageTobeSent = Il8nService.get('zenny:notfound:upcomingflights');
        break;
      }
      const flight = await lumoService.status(nextFlight.lumoId);
      messageTobeSent = LumoFlightResponseRiskResponse(flight);

      break;
    }

    case 'slot_filled_new_flight': {
      dfResult.parameters = await addDFParameters(
        message.Body,
        dfResult.parameters,
      );
      messageTobeSent = await processFlightSearchRequest(
        dfResult.parameters,
        message,
        services,
        identifier,
        dfResult.queryText,
      );
      break;
    }
    case 'waivers': {
      const currUser = await userPhoneSearch(userService, message);
      if (!currUser) {
        /* istanbul ignore next */
        messageTobeSent = Il8nService.get('zenny:notfound:user');
        break;
      }
      const nextFlight = await flightService.getNextFlightByUser(currUser.id);
      if (!nextFlight) {
        /* istanbul ignore next */
        messageTobeSent = Il8nService.get('zenny:notfound:upcomingflights');
        break;
      }
      const flight = await lumoService.status(nextFlight.lumoId);
      messageTobeSent = getWaivers(flight);
      break;
    }
    case 'baggage_claim': {
      const currUser = await userPhoneSearch(userService, message);
      if (!currUser) {
        /* istanbul ignore next */
        messageTobeSent = Il8nService.get('zenny:notfound:user');
        break;
      }
      const nextFlight = await flightService.getNextFlightByUser(currUser.id);
      const previousFlight = await flightService.getLandedFlightsByUser(
        currUser.id,
      );
      if (!nextFlight && !previousFlight[0]) {
        /* istanbul ignore next */
        messageTobeSent = Il8nService.get('zenny:notfound:upcomingflights');
        break;
      }
      messageTobeSent = getBaggageClaim(
        nextFlight ? nextFlight : previousFlight[0],
      );
      break;
    }
    /*istanbul ignore next */
    case 'gate': {
      const currUser = await userPhoneSearch(userService, message);
      if (!currUser) {
        /* istanbul ignore next */
        messageTobeSent = Il8nService.get('zenny:notfound:user');
        break;
      }
      const nextFlight = await flightService.getNextFlightByUser(currUser.id);
      if (!nextFlight) {
        /* istanbul ignore next */
        messageTobeSent = Il8nService.get('zenny:notfound:upcomingflights');
        break;
      }
      messageTobeSent = getGate(nextFlight);
      break;
    }
    case 'terminal': {
      const currUser = await userPhoneSearch(userService, message);
      if (!currUser) {
        /* istanbul ignore next */
        messageTobeSent = Il8nService.get('zenny:notfound:user');
        break;
      }
      const nextFlight = await flightService.getNextFlightByUser(currUser.id);
      if (!nextFlight) {
        /* istanbul ignore next */
        messageTobeSent = Il8nService.get('zenny:notfound:upcomingflights');
        break;
      } else {
        messageTobeSent = getTerminal(nextFlight);
      }
      break;
    }
    case 'lost_bags': {
      Il8nService.get('zenny:notfound:bags');
      break;
    }
    /* istanbul ignore next */
    case 'confirm_tracking': {
      messageTobeSent = await confirmTracking(
        identifier,
        dfResult.outputContexts,
        trackingServices,
      );
      break;
    }
    /* istanbul ignore next */
    case 'select_flight_from_list': {
      messageTobeSent = await confirmSelection(
        identifier,
        dfResult.outputContexts,
        trackingServices,
      );
      break;
    }
    case 'generic_mask': {
      messageTobeSent = Il8nService.get('zenny:masks');
      break;
    }

    /* istanbul ignore next */
    case 'baggage_allowance': {
      messageTobeSent = Il8nService.get('zenny:baggage_allowance');
      break;
    }

    case 'who_am_i': {
      const currUser = await userPhoneSearch(userService, message);
      if (!currUser) {
        /* istanbul ignore next */
        messageTobeSent = Il8nService.get('zenny:notfound:user');
        break;
      }
      messageTobeSent = currUser.firstName + ' ' + currUser.lastName;
      break;
    }
    /* istanbul ignore next */
    case 'reject_tracking': {
      messageTobeSent = await rejectTracking(identifier);
      break;
    }
    /* istanbul ignore next */
    case 'waiting_times': {
      messageTobeSent = Il8nService.get('zenny:waiting');
      break;
    }
    /* istanbul ignore next */

    case 'start_over': {
      messageTobeSent = await resetContext(identifier);
      break;
    }
  }
  // logger.info(
  //   `Should reply with ${messageTobeSent || dfResult.fulfillmentText}`,
  //   dfResult,
  // );
  try {
    const addedEvent = await commsEventService.addEvent({
      from: message.From,
      to: message.To,
      message: message.Body,
      server: require('os').hostname(),
      source: message.Source,
      created: new Date(),
      result: 'success',
      intent: intentName,
    });
    logger.debug('Logged message to audit DB', addedEvent);
  } catch (err) {
    logger.error(`Logged messageerror commsEventService addEvent ${err}`);
  }

  if (messageTobeSent === '' && dfResult.fulfillmentText === '') {
    //if there is no answer at all from zenny
    messageTobeSent = Il8nService.get('zenny:notfound:question');
  }

  await sendMessage(
    message.From,
    message.To,
    message.Source,
    messageTobeSent || dfResult.fulfillmentText,
    services,
  );

  return 'SUCCESS';
}

export async function terminate(): Promise<void> {
  emitter.emit('CLOSEBOT');
}

/**
 * Main function
 * Needed in order to have the benefit of top-level async/await for database connection
 */
export async function main(): Promise<void> {
  logger.info('Starting Bot Server');

  let bqIncoming: BQ; // Messages received via WhatsApp and/or SMS
  let bqOutgoing: BQ; // Send messages for dispatch

  // db connection
  let db: Connection;
  try {
    db = await createConnection();
    logger.info('Database connected');
  } catch (err) /* istanbul ignore next */ {
    logger.error('SEVERE: TypeORM failed to connect to DB', { error: err });
    process.exit(1);
  }

  jackandferdiService = new JackAndFerdiService(db);

  try {
    bqIncoming = new BQ('INCOMING_TWILIO', {
      isWorker: true,
      redis: redisConfig,
    });
    bqOutgoing = new BQ('OUTGOING_MESSAGE', {
      isWorker: false,
      redis: redisConfig,
    });
  } catch (err) /* istanbul ignore next */ {
    logger.error('SEVERE: Could not connect to Bee-Queue', { error: err });
    process.exit(2);
  }

  lumoService = new LumoService(db);

  flightStatsService = new FlightStatsService(db);

  // --------------------------------------------------------------for synthetic event--
  bqOutgoing.on('ready', async () => {
    logger.info('BQ for outgoing messages ready');
    // const job = bqIncoming.createJob({
    //   To: 'whatsapp:+16465063056',
    //   From: 'whatsapp:+972523686125',
    //   source: 'whatsapp',
    //   human: true,
    //   Body: 'reset',
    //   server: 'localhost',
    // });
    //
    // await job.save();
    //
    // const job2 = bqIncoming.createJob({
    //   To: 'whatsapp:+16465063056',
    //   From: 'whatsapp:+972523686125',
    //   source: 'whatsapp',
    //   human: true,
    //   Body: 'Can you track BA113 from LHR for tomorrow?',
    //   server: 'localhost',
    // });
    //
    // await job2.save();
    //
    // const job3 = bqIncoming.createJob({
    //   To: 'whatsapp:+16465063056',
    //   From: 'whatsapp:+972523686125',
    //   source: 'whatsapp',
    //   human: true,
    //   Body: 'yes',
    //   server: 'localhost',
    // });
    //
    // await job3.save();
  });
  // -----------------------------------------------------------------------------------

  /* istanbul ignore next */
  bqOutgoing.on('ready', async () => {
    logger.info('Queue now ready', {
      queue: ConfigService.get('QUEUE_NAME_OUTGOING'),
    });
  });

  bqIncoming.process(async (job: any, done: DoneCallback<any>) => {
    const services = {
      db,
      bq: bqOutgoing,
    };
    try {
      const result = await processIncomingBQJob(services, job.id, job.data);
      return done(null, result);
    } catch (err) {
      return done(err, null);
    }
  });

  // This code is placed in a separate function to make it easier to test

  // Someone pressed CTRL-C attempt graceful shutdown
  /* istanbul ignore next */
  process.on('SIGINT', async function shutdown() {
    logger.warn('Caught interrupt signal');
    await bqIncoming.close(3000);
    logger.info(`Disconnecting from ${ConfigService.get('QUEUE_NAME_TWILIO')}`);
    await bqOutgoing.close(3000);
    logger.info(`Disconnecting from ${ConfigService.get('QUEUE_NAME_TWILIO')}`);
    await db.close();
    logger.info('Disconnected from DB');
    process.exit();
  });

  emitter.on('CLOSEBOT', async function closebot() {
    await redisClient.quit();
  });
}

// Run the main function
main().then((r) => {
  // This might be thrown at the end of the main loop but the Redis connection should keep running.
  /* istanbul ignore next */
  if (typeof r !== 'undefined') {
    logger.error(r);
  }
});
