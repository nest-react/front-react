import fp from 'fastify-plugin';
import { createConnection } from 'typeorm';
import { createLogger } from '../util/logger';

const logger = createLogger('zenner', 'db', 'zenner-common');

export default fp(async (fastify) => {
  try {
    const connection = await createConnection();
    fastify.decorate('db', {
      connection,
    });
  } catch (error) {
    logger.error(error);
  }
});
