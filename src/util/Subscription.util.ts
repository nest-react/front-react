/**
 * Compares two gates and returns true if it appears that the new gate is just a variant of the old one
 * @param currentGate the gate in the flight record
 * @param newGate the gate in the update payload
 * @returns {boolean} if the gate has changed
 */
export function gateHasntChanged(
  currentGate: string,
  newGate: string,
): boolean {
  // There is no new gate, no need to continue
  if (newGate === null || newGate === '') {
    return true;
  }

  // We don't know the gate yet, any valid value is fine
  if ((currentGate === null || currentGate === '') && newGate !== null) {
    if (newGate === '') {
      return true;
    } else {
      return false;
    }
  }

  if (String(currentGate) === String(newGate)) {
    return true;
  }

  const matchTerminalGate = /[a-zA-Z]+|[0-9]+/g;
  currentGate = currentGate
    .match(matchTerminalGate)
    .map((item) => (Number(item) ? Number(item).toString() : item))
    .join('');
  newGate = newGate
    .match(matchTerminalGate)
    .map((item) => (Number(item) ? Number(item).toString() : item))
    .join('');

  return (
    currentGate.match(matchTerminalGate).some((item) => {
      return item === newGate;
    }) ||
    newGate.match(matchTerminalGate).some((item) => {
      return item === currentGate;
    }) ||
    currentGate.toLowerCase() === newGate.toLowerCase()
  );
}

export function gateChanged(currentGate: string, newGate: string): boolean {
  return !gateHasntChanged(currentGate, newGate);
}
