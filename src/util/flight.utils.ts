// import { getDiffMinutes, getNiceMinsDiffString } from './datetime.utils';

import {
  FlightNumber,
  JSON_SIMPLE_TYPE_TO_PROTO_KIND_MAP,
  JSON_SIMPLE_VALUE_KINDS,
} from '../interface/bot-server.interface';
import { google } from '@google-cloud/dialogflow/build/protos/protos';
import IValue = google.protobuf.IValue;
import { AlertPayload } from '../interface/flightStats/alert';
import { FlightResponse } from '../interface/lumo';
import { FlightStatus } from '../interface/flightStats';

export function firstName(name: string): string {
  return name.split(' ')[0];
}

export function tConvert(time: number | any[] | string): string {
  // Check correct time format and split into components
  /* istanbul ignore next */
  time = time.toString().match(/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [
    time,
  ];

  /* istanbul ignore next */
  if (time.length > 1) {
    // If time format correct
    time = time.slice(1); // Remove full string match value
    time[5] = Number(time[0]) < 12 ? ' AM' : ' PM'; // Set AM/PM
    time[0] = Number(time[0]) % 12 || 12; // Adjust hours
  }
  return time.join(''); // return adjusted time or original string
}

export const jsonToStructProto = (json: any): any => {
  const fields: any = {};
  for (const k in json) {
    fields[k] = jsonValueToProto(json[k]);
  }
  return { fields };
};

const valueProtoToJson = (proto: any) => {
  if (!proto || !proto.kind) {
    return null;
  }

  if (JSON_SIMPLE_VALUE_KINDS.has(proto.kind)) {
    return proto[proto.kind];
  } else if (proto.kind === 'nullValue') {
    return null;
  } else if (proto.kind === 'listValue') {
    if (!proto.listValue || !proto.listValue.values) {
      console.warn('Invalid JSON list value proto: ', JSON.stringify(proto));
    }
    return proto.listValue.values.map(valueProtoToJson);
  } else if (proto.kind === 'structValue') {
    return structProtoToJson(proto.structValue);
  } else {
    console.warn('Unsupported JSON value proto kind: ', proto.kind);
    return null;
  }
};

export const structProtoToJson = (proto: any): any => {
  if (!proto || !proto.fields) {
    return {};
  }
  const json: any = {};
  for (const k in proto.fields) {
    json[k] = valueProtoToJson(proto.fields[k]);
  }
  return json;
};

export function getAlertPayload(
  payload: AlertPayload,
): {
  realFlightNo: string;
  operatingCarrier: string;
  cancelled: boolean;
  diverted: boolean;
  tailNo: string;
  originTerminal: string;
  originGate: string;
  destinationTerminal: string;
  destinationBaggage: string;
} {
  return {
    realFlightNo:
      payload.flightStatus.operatingCarrierFsCode + payload.rule.flightNumber,
    operatingCarrier: payload.flightStatus.operatingCarrierFsCode,
    cancelled: payload.flightStatus.status === 'C',
    diverted: payload.flightStatus.status === 'D',
    tailNo: payload.flightStatus.flightEquipment.tailNumber,
    originTerminal: payload.flightStatus.airportResources.departureTerminal,
    originGate: payload.flightStatus.airportResources.departureGate,
    destinationTerminal: payload.flightStatus.airportResources.arrivalTerminal,
    destinationBaggage: payload.flightStatus.airportResources.baggage,
  };
}

export function getTimeFromDate(dateTime: string): string {
  return tConvert(dateTime.substr(11, 5));
}

export function getLumoPayload(
  payload: FlightResponse,
): {
  statusName: string;
  lumoRisk: string;
  originGate: string;
  destinationGate: string;
  origin: string;
  destination: string;
  scheduledDeparture: Date;
  estimatedDeparture: Date;
  scheduledArrival: Date;
  estimatedArrival: Date;
  estimatedDepartureDateLocal: string;
  estimatedDepartureTimeLocal: string;
  estimatedArrivalDateLocal: string;
  estimatedArrivalTimeLocal: string;
  scheduledDepartureDateLocal: string;
  scheduledDepartureTimeLocal: string;
  scheduledArrivalDateLocal: string;
  scheduledArrivalTimeLocal: string;
  destinationBaggage: string;
} {
  return {
    statusName: payload.status.text.toLowerCase(),
    lumoRisk: payload.prediction.delay_index.toString(),
    originGate: payload.status?.departure?.gate,
    destinationGate: payload.status?.arrival?.gate,
    origin: payload.flight.origin.iata,
    destination: payload.flight.destination.iata,
    scheduledDeparture: new Date(payload.status.departure.scheduled),
    estimatedDeparture: new Date(payload.status.departure.latest),
    scheduledArrival: new Date(payload.status.arrival.scheduled),
    estimatedArrival: new Date(payload.status.arrival.latest),
    estimatedDepartureDateLocal: payload.status.departure.latest.substr(0, 10),
    estimatedDepartureTimeLocal: payload.status.departure.latest.substr(11, 8),
    estimatedArrivalDateLocal: payload.status.arrival.latest.substr(0, 10),
    estimatedArrivalTimeLocal: payload.status.arrival.latest.substr(11, 8),
    scheduledDepartureDateLocal: payload.status.departure.scheduled.substr(
      0,
      10,
    ),
    scheduledDepartureTimeLocal: payload.status.departure.scheduled.substr(
      11,
      8,
    ),
    scheduledArrivalDateLocal: payload.status.arrival.scheduled.substr(0, 10),
    scheduledArrivalTimeLocal: payload.status.arrival.scheduled.substr(11, 8),
    destinationBaggage: payload.status?.arrival?.baggage_claim,
  };
}

enum FlightStatusStatuses {
  L = 'landed',
  A = 'expected',
  C = 'cancelled',
  D = 'diverted',
  DN = 'data-source-needed',
  NO = 'not-operational',
  R = 'redirected',
  S = 'scheduled',
  U = 'unknown',
}

export function getAdditionalAlertPayload(payload: FlightStatus) {
  return {
    statusName: FlightStatusStatuses[payload.status],
    destinationGate: payload.airportResources?.arrivalGate,
    origin: payload.departureAirportFsCode,
    destination: payload.arrivalAirportFsCode,
    scheduledDeparture: new Date(payload.departureDate.dateUtc),
    estimatedDeparture: new Date(payload.departureDate.dateUtc),
    scheduledArrival: new Date(payload.arrivalDate.dateUtc),
    estimatedArrival: new Date(payload.arrivalDate.dateUtc),
    estimatedDepartureDateLocal: payload.departureDate.dateLocal.substr(0, 10),
    estimatedDepartureTimeLocal: payload.departureDate.dateLocal.substr(11, 8),
    estimatedArrivalDateLocal: payload.arrivalDate.dateLocal.substr(0, 10),
    estimatedArrivalTimeLocal: payload.arrivalDate.dateLocal.substr(11, 8),
    scheduledDepartureDateLocal: payload.departureDate.dateLocal.substr(0, 10),
    scheduledDepartureTimeLocal: payload.departureDate.dateLocal.substr(11, 8),
    scheduledArrivalDateLocal: payload.arrivalDate.dateLocal.substr(0, 10),
    scheduledArrivalTimeLocal: payload.arrivalDate.dateLocal.substr(11, 8),
  };
}

export function parseFlightNo(flightProto: IValue): FlightNumber {
  if (flightProto) {
    const flightString = valueProtoToJson(flightProto);
    const carrier = flightString.substr(0, 2);
    const flight = Number(flightString.substr(2).trim());
    return { carrier, flight };
  }
  return {
    carrier: null,
    flight: null,
  };
}

export const jsonValueToProto = (value: any): any => {
  const valueProto: any = {};
  if (value === null) {
    valueProto.kind = 'nullValue';
    valueProto.nullValue = 'NULL_VALUE';
  } else if (value instanceof Array) {
    valueProto.kind = 'listValue';
    valueProto.listValue = { values: value.map(jsonValueToProto) };
  } else if (typeof value === 'object') {
    valueProto.kind = 'structValue';
    valueProto.structValue = jsonToStructProto(value);
  } else if (typeof value in JSON_SIMPLE_TYPE_TO_PROTO_KIND_MAP) {
    const kind = JSON_SIMPLE_TYPE_TO_PROTO_KIND_MAP[typeof value];
    valueProto.kind = kind;
    valueProto[kind] = value;
  } else {
    //console.warn('Unsupported value type ', typeof value);
  }
  return valueProto;
};

// export function flightToMessage(
//   flightObj: any,
//   hideDate = false,
//   hideRoute = false,
// ) {
//   const now = new Date();
//   const depmins = getDiffMinutes(
//     flightObj.status.departure.latest,
//     now.toISOString(),
//   );
//   const arrmins = getDiffMinutes(
//     flightObj.status.arrival.latest,
//     now.toISOString(),
//   );

//   //-----------------------------------------------------[Temporary]-----------------------------------------------------//
//   const flight = {
//     flight_no: 367,
//     from_airport: 'from Airp',
//     to_airport: 'to Airp',
//     estimated_departure_date: new Date('04.06.2020'),
//     estimated_departure_time: new Date('04.06.2020').getTime(),
//   };
//   //-----------------------------------------------------[Temporary]-----------------------------------------------------//

//   let reply = firstName(flightObj.flight.carrier.name) + ' ' + flight.flight_no;

//   if (!hideRoute) {
//     reply += ' (' + flight.from_airport + ' > ' + flight.to_airport + ')';
//   }

//   reply += ' departs ' + tConvert(flight.estimated_departure_time);

//   if (!hideDate) {
//     reply += ' on ' + flight.estimated_departure_date;
//   }

//   reply += '.';

//   if (depmins > 0) {
//     reply +=
//       '\nThere is a ' +
//       flightObj.prediction['delay_index'] +
//       '0% chance of disruption.';
//     reply +=
//       '\nYou have ' + getNiceMinsDiffString(depmins) + ' until departure.';
//   } else if (arrmins > 0) {
//     reply += ' You have ' + getNiceMinsDiffString(arrmins) + ' until arrival.';
//   }
//   return reply;
// }
