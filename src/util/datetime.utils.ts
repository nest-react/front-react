import { Moment } from 'moment-timezone';

const moment = require('moment');
moment.locale('en');

/**
 * Pretty up a datetime string
 * @param {string} dateStr
 * @returns {string} human readable date string
 */

export const getNiceHourString = function (hoursStr: string): string {
  return hoursStr.slice(0, 5);
};
export const getNiceTimeDateString = function (dateStr: string): string {
  const d = new Date(dateStr);

  return String(
    d.getHours() +
      ':' +
      (d.getMinutes().toString().length === 1
        ? '0' + d.getMinutes()
        : d.getMinutes()) +
      ', ' +
      d.getDate() +
      '-' +
      (d.getMonth() + 1) +
      '-' +
      d.getFullYear(),
  );
};

export const getNiceDateString = function (dateStr: string): string {
  const dateArr = dateStr.split('-');
  const months = moment.months();
  return dateArr[2] + ' ' + months[parseInt(dateArr[1]) - 1];
};
/**
 * Calculate the difference in minutes between two dates
 * @param dateString1
 * @param dateString2
 * @returns {number} minutes between dates
 */
export const getDiffMinutes = function (
  dateString1: string,
  dateString2: string,
): number {
  return Math.floor(
    (new Date(dateString1).getTime() - new Date(dateString2).getTime()) / 60000,
  );
};

export const getMomentDiffInMinutes = function (
  date1: Moment,
  date2: Moment,
): number {
  return date1.diff(date2, 'minutes');
};

/* istanbul ignore next */
export const getNiceTimeDiffString = function (
  dt1: string,
  dt2: string,
  inLocalTime = false,
): string {
  if (!inLocalTime) return getNiceMinsDiffString(getDiffMinutes(dt1, dt2));
  const localDate1 = moment(dt1).local(true);
  const localDate2 = moment(dt2).local(true);
  return getNiceMinsDiffString(getMomentDiffInMinutes(localDate1, localDate2));
};

export const getDayWithSuffix = (day: number): string => {
  const firstNumeral = day % 10;
  const decimal = day % 100;

  if (firstNumeral === 1 && decimal !== 11) return day + 'st';
  if (firstNumeral === 2 && decimal !== 12) return day + 'nd';
  if (firstNumeral === 3 && decimal !== 13) return day + 'rd';

  return day + 'th';
};

export const getNiceMinsDiffString = function (mins: number): string {
  const difMins = Math.abs(mins);

  if (difMins === 60) {
    return '1 hour';
  }
  if (difMins < 60) {
    if (difMins === 1) {
      return '1 minute';
    }
    return String(difMins + ' minutes');
  } else if (difMins < 120) {
    if (difMins === 61) {
      return String('1 hour and 1 minute');
    }
    return String('1 hour and ' + (difMins - 60) + ' minutes');
  } else if (difMins >= 1440) {
    const days = Math.floor(difMins / 1440);
    const hours = Math.floor((difMins % 1440) / 60);

    if (hours === 0) {
      if (days === 1) {
        return '1 day';
      }
      return String(days + ' days');
    } else {
      if (days === 1) {
        if (hours === 1) {
          return String('1 day and 1 hour');
        }
        return String('1 day and ' + hours + ' hours');
      }
      if (hours === 1) {
        return String(days + ' days and 1 hour');
      }
      return String(days + ' days and ' + hours + ' hours');
    }
  } else {
    const hours = Math.floor(difMins / 60);
    const minutes = Math.floor(difMins % 60);

    if (minutes === 0) {
      return String(hours + ' hours');
    } else {
      if (minutes === 1) {
        return String(hours + ' hours and 1 minute');
      }
      return String(hours + ' hours and ' + minutes + ' minutes');
    }
  }
};
