import { AxiosResponse } from 'axios';

export const parseData = (res: AxiosResponse): any => res.data;
