export function parsePhoneNumber(identifier: string): string {
  const phoneNumber = identifier.substr(identifier.indexOf('_') + 1);
  const isWhatsAppMessage = phoneNumber.includes('whatsapp');
  const isTestCommand = phoneNumber.includes('test');
  // phone is whatsapp:+9720523686125
  if (isWhatsAppMessage) return phoneNumber.substr(10);
  if (isTestCommand)
    return phoneNumber.substr(phoneNumber.indexOf('test') + 'test:+'.length);
  // phone is +9720523686125
  if (phoneNumber.includes('+')) return phoneNumber.replace(/\+/g, '');
  // phone is 9720523686125
  if (phoneNumber.includes('9720')) return phoneNumber.replace('9720', '972');
  // phone is 972523686125
  return phoneNumber;
}
