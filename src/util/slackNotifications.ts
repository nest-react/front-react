import { ConfigService } from '../service';
import { FlightResponse } from '../interface/lumo';
import { IncomingWebhook } from '@slack/webhook';

const monthNames = [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December',
];

function niceLocalTimeString(dtString: string): string {
  return (
    dtString.substr(8, 2) +
    ' ' +
    monthNames[Number(dtString.substr(5, 2)) - 1] +
    '@' +
    dtString.substr(11, 5)
  );
}

export async function lumoUpdateToSlack(
  payload: FlightResponse,
): Promise<void> {
  const url = ConfigService.get('SLACK_FLIGHTEVENTS_SHU');

  if (url === undefined || url === '') {
    return;
  }

  const slackChannel = new IncomingWebhook(url);

  await slackChannel
    .send({
      blocks: [
        {
          type: 'section',
          text: {
            type: 'mrkdwn',
            text:
              'We have received an update from *Lumo*\n\n' +
              payload.summaries.short.message,
          },
        },
        {
          type: 'section',
          fields: [
            {
              type: 'mrkdwn',
              text: '*Event:*\n' + payload.status.text,
            },
            {
              type: 'mrkdwn',
              text:
                '*Flight:*\n' +
                payload.flight.carrier.name +
                ' ' +
                payload.flight.carrier.iata +
                ' ' +
                payload.flight.number,
            },
            {
              type: 'mrkdwn',
              text:
                '*Origin:*\n' +
                payload.flight.origin.name +
                ' (' +
                payload.flight.origin.iata +
                ')',
            },
            {
              type: 'mrkdwn',
              text:
                '*Destination:*\n' +
                payload.flight.destination.name +
                ' (' +
                payload.flight.destination.iata +
                ')',
            },
            {
              type: 'mrkdwn',
              text:
                '*Departure:*\n' +
                niceLocalTimeString(payload.status.departure.latest),
            },
            {
              type: 'mrkdwn',
              text:
                '*Arrival:*\n' +
                niceLocalTimeString(payload.status.arrival.latest),
            },
          ],
        },
      ],
    })
    .catch(() => {
      // DO NOTHING
    });
}
