import * as winston from 'winston';
const newrelicFormatter = require('@newrelic/winston-enricher');

/* istanbul ignore next */
export function createLogger(
  service: string,
  module: string,
  label: string,
): winston.Logger {
  if (process.env.NODE_ENV === 'production') {
    return winston.createLogger({
      level: 'info',
      format: winston.format.combine(
        winston.format.label({ label: label }),
        newrelicFormatter(),
      ),
      defaultMeta: { service: service, module: module },
      transports: [
        new winston.transports.File({
          filename: './logs/error.log',
          level: 'error',
        }),
        new winston.transports.File({ filename: './logs/combined.log' }),
        new winston.transports.Console({ level: 'info' }),
      ],
    });
  } else {
    return winston.createLogger({
      level: 'debug',
      transports: [new winston.transports.Console({ level: 'debug' })],
    });
  }
}
