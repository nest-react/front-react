export interface MetropolitanAirportCode {
  [key: string]: string[];
}

export const getMetropolitanAirports = (airportCode: string): string[] => {
  return metropolitanCodes[airportCode];
};

export const isMetropolitanAirport = (airportCode: string): boolean => {
  const code: undefined | string[] = metropolitanCodes[airportCode];
  return code && Array.isArray(code) && Boolean(code.length);
};

export const metropolitanCodes: MetropolitanAirportCode = {
  BKK: ['BKK', 'DMK'],
  BJS: ['PEK', 'NAY'],
  OSA: ['KIX', 'ITM', 'UKB'],
  SPK: ['CTS', 'OKD'],
  SEL: ['ICN', 'GMP'],
  SHA: ['SHA', 'PVG'],
  TYO: ['NRT', 'HND'],
  JKT: ['CGK', 'HLP'],
  BUH: ['OTP', 'BBU'],
  BRU: ['BRU', 'CRL'],
  LON: ['BQH', 'LCY', 'LGW', 'LTN', 'LHR', 'SEN', 'STN'],
  MIL: ['BGY', 'MXP', 'LIN', 'PMF'],
  MOW: ['SVO', 'DME', 'VKO', 'BKA'],
  OSL: ['OSL', 'TRF', 'RYG'],
  PAR: ['CDG', 'ORY', 'LBG'],
  REK: ['KEF', 'RKV'],
  ROM: ['FCO', 'CIA'],
  STO: ['ARN', 'NYO', 'BMA', 'VST'],
  TCI: ['TFN', 'TFS'],
  CHI: ['ORD', 'MDW', 'RFD'],
  QDF: ['DAL', 'DFW'],
  DTT: ['DTW', 'DET', 'YIP'],
  YEA: ['YEG'],
  IAH: ['IAH', 'HOU'],
  LAX: ['LAX'],
  QMI: ['MIA', 'FLL', 'PBI'],
  YMQ: ['YUL', 'YMY'],
  NYC: ['JFK', 'EWR', 'LGA', 'HPN'],
  YTO: ['YYZ', 'YTZ', 'YKF'],
  WAS: ['IAD', 'DCA', 'BWI'],
  BHZ: ['CNF', 'PLU'],
  BUE: ['EZE', 'AEP'],
  RIO: ['GIG', 'SDU'],
  SAO: ['GRU', 'CGH', 'VCP'],
};
