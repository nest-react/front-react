export interface Config {
  get(key: string, defaultValue?: any): string;
}
