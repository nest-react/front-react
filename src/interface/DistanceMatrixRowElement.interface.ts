import {
  DistanceMatrixRowElement as BaseDistanceMatrixRowElement,
  TravelMode,
} from '@googlemaps/google-maps-services-js/dist';

export interface DistanceMatrixRowElement extends BaseDistanceMatrixRowElement {
  mode: TravelMode;
}
