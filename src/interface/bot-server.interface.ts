import { Connection } from 'typeorm';
import BQ from 'bee-queue';
import moment from 'moment-timezone';
import { FlightResponse } from './lumo';
import { SearchResponse } from './lumo/lumoSearch.interface';
import { AirportMeta } from '../service/Airport.service';
import { FlightService } from '../service/Flight.service';
import { FlightStatsService } from '../service/FlightStats.service';
import { LumoService } from '../service';
import { UserService } from '../service/User.service';
import { TrackService } from '../service/Track.service';

export interface FlightNumber {
  carrier: string;
  flight: number;
}

export interface DialogflowAirport {
  city: string;
  iata: string;
  icao: string;
  country: string;
  name: string;
}

export interface ExternalServices {
  db: Connection;
  bq: BQ;
}

export interface SubscriptionServices {
  lumoService: LumoService;
  flightStatsService: FlightStatsService;
}

export interface TrackServices {
  userService: UserService;
  trackService: TrackService;
  flightService: FlightService;
}

export interface FlightSubscriptionServices extends SubscriptionServices {
  flightService: FlightService;
  userService: UserService;
}

export interface SubscriptionIds {
  lumoId: string;
  flightStatsId: string;
}

export interface FlightPayload {
  flightNo: string;
  statusName?: string;
  active: boolean;
  lumoId?: string;
  departureDate: string;
}

export interface SearchRequest {
  flight_number?: number;
  date?: string;
  carrier?: string;
  origin?: string;
  destination?: string;
}

export interface SearchRequestWithCache extends SearchRequest {
  day: string;
  month: number;
  flight: FlightNumber;
}

export interface ProcessAndCacheOptions<T> {
  flightResults: SearchResponse<T>;
  dfIdentifier: string;
  month: number;
  day: string;
}

export interface BlockList {
  platform: {
    whatsapp: {
      numbers: string[];
    };
    messenger: {
      ids: string[];
    };
    email: {
      adresses: string[];
    };
  };
}

export interface TrackedFlight {
  no: number;
  flight: FlightResponse;
  lumo_id: string;
  flight_carrier: string;
  flight_number: string;
}

export const MONTH_NAMES = moment.months();

export const JSON_SIMPLE_TYPE_TO_PROTO_KIND_MAP = {
  [typeof 0]: 'numberValue',
  [typeof '']: 'stringValue',
  [typeof false]: 'boolValue',
};

export interface AirportsPayload {
  originAirports: AirportMeta[] | DialogflowAirport;
  destinationAirports: AirportMeta[] | DialogflowAirport;
}

export const JSON_SIMPLE_VALUE_KINDS = new Set([
  'numberValue',
  'stringValue',
  'boolValue',
]);

export interface DateStracture {
  month: number;
  day: string;
  departureDate: string;
}

export const DEPARTURE_MONTH_FROM = 5;

export const DEPARTURE_MONTH_LENGTH = 2;

export const RADIX = 10;

export const DEPARTURE_DAY_FROM = 8;

export const DEPARTURE_DAY_LENGTH = 2;

export const PHONE_NUMBER_FROM = 10;
