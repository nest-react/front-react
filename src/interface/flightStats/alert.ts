import { FlightStatsBaseResponse } from './index';

export interface RuleEvent {
  type: string;
}

interface Delivery {
  format: string;
  destination: string;
}

export interface OperationalTime {
  dateUtc: string;
  dateLocal: string;
}

interface UpdatedTextField {
  field: string;
  newText: string;
}

interface UpdatedDateField {
  field: string;
  newDateLocal: string;
  newDateUtc: string;
}

interface BasicFlightStatsAlert {
  carrierFsCode: string;
  flightNumber: string;
  departureAirportFsCode: string;
  arrivalAirportFsCode: string;
}

export interface FlightStatsAlert extends BasicFlightStatsAlert {
  id: string;
  departure: string;
  arrival: string;
  name: string;
  ruleEvents: [RuleEvent];
  delivery: Delivery;
}

export interface InitialFlightStatsAlert extends BasicFlightStatsAlert {
  departureDate: any;
  arrivalDate: any;
  status: string;
  schedule: any;
  operationalTimes: any;
  codeshares: any;
  delays: any;
  arrivalAirport?: any;
  departureAirport?: any;
  flightDurations: any;
  airportResources: any;
  flightEquipment: any;
}

interface CodeShare {
  fsCode: string;
  flightNumber: string;
  relationship: string;
}

interface FlightStatusUpdate {
  updatedAt: {
    dateUtc: string;
  };
  source: string;
  updatedTextFields?: {
    updatedTextField: [UpdatedTextField];
  };
  updatedDateFields?: {
    updatedDateField: [UpdatedDateField];
  };
}

interface FlightStatus {
  flightId: string;
  carrierFsCode: string;
  operatingCarrierFsCode: string;
  primaryCarrierFsCode: string;
  flightNumber: string;
  departureAirportFsCode: string;
  arrivalAirportFsCode: string;
  departureDate: OperationalTime;
  arrivalDate: OperationalTime;
  status: string;
  schedule: {
    flightType: string;
    serviceClasses: string;
    restrictions: string;
  };
  operationalTimes: {
    publishedDeparture: OperationalTime;
    scheduledGateDeparture: OperationalTime;
    estimatedGateDeparture: OperationalTime;
    actualGateDeparture: OperationalTime;
    publishedArrival: OperationalTime;
    scheduledGateArrival: OperationalTime;
    estimatedGateArrival: OperationalTime;
    estimatedRunwayArrival: OperationalTime;
  };
  codeshares: {
    codeshare: [CodeShare];
  };
  delays: {
    departureGateDelayMinutes: string;
  };
  flightDurations: {
    scheduledBlockMinutes: string;
  };
  airportResources: {
    departureTerminal: string;
    departureGate: string;
    arrivalTerminal: string;
    baggage: string;
  };
  flightEquipment: {
    scheduledEquipmentIataCode: string;
    actualEquipmentIataCode: string;
    tailNumber: string;
  };
  flightStatusUpdates: {
    flightStatusUpdate: [FlightStatusUpdate];
  };
}

export interface AlertPayload {
  rule: FlightStatsAlert;
  flightStatus?: FlightStatus;
  event?: RuleEvent;
  dataSource?: string;
  dateTimeRecorded?: string;
}

export interface AlertResponse {
  alert: AlertPayload;
}

export interface FlightStatsAlertResponse extends FlightStatsBaseResponse {
  rule: FlightStatsAlert;
}

export interface AlertIdsResponse extends FlightStatsBaseResponse {
  ruleIds: number[];
}

export interface AlertOptions {
  // Example: carrier=DL
  // The airline operating the flight
  carrier: string;
  // Example: flightNumber=100
  // The flight number of the flight
  flightNumber: number;
  // Example: arrivalAirport=LHR
  // The arrival airport for the flight
  arrivalAirport: string;
  // Example: 2020/07/29 | 2020-08-20 | any valid date
  // Departure date
  date: string;
}

export interface AlertRouteOptions extends AlertOptions {
  // Example: arrivalAirport=LHR
  // The departure airport for the flight
  departureAirport: string;
  events?: string;
  status?: any;
}

type ExtendedOption =
  | 'useInlinedReferences'
  | 'useHttpErrors'
  | 'testRun'
  | 'includeNewFields'
  | 'skipValidation';

export interface AlertRequestOptions {
  extendedOptions?: ExtendedOption[];
}
