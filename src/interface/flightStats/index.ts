import { InitialFlightStatsAlert, OperationalTime } from './alert';
import { FlightInfo } from '../lumo';

interface Airline {
  fs: string;
  iata: string;
  icao: string;
  name: string;
  phoneNumber: string;
  active: boolean;
}

interface Airport {
  fs: string;
  iata: string;
  icao: string;
  faa: string;
  name: string;
  city: string;
  countryCode: string;
  countryName: string;
  regionName: string;
  timeZoneRegionName: string;
  weatherZone: string;
  localTime: string;
  utcOffsetHours: number;
  latitude: number;
  longitude: number;
  elevationFeet: number;
  classification: number;
  active: boolean;
  weatherUrl: string;
  delayIndexUrl: string;
  street1?: string;
  cityCode?: string;
  stateCode?: string;
  postalCode?: string;
}

interface Equipment {
  iata: string;
  name: string;
  turboProp: boolean;
  jet: boolean;
  widebody: boolean;
  regional: boolean;
}

interface FlightStatsDateTime {
  dateUtc: string;
  dateLocal: string;
}

export interface Schedule {
  flightType: string;
  serviceClasses: string;
  restrictions: string;
  uplines: any[];
  downlines: any[];
}

export interface FlightStatus {
  // example: 1038417465
  flightId: number;
  carrierFsCode: string;
  flightNumber: string;
  operatingCarrier?: {
    iata: string;
  };
  carrier?: {
    iata: string;
    name?: string;
  };
  primaryCarrier?: {
    iata: string;
  };
  departureAirportFsCode: string;
  departureAirport?: Airport;
  arrivalAirportFsCode: string;
  arrivalAirport?: Airport;
  divertedAirport?: Airport;
  departureDate: FlightStatsDateTime;
  arrivalDate: FlightStatsDateTime;
  status: 'L' | 'C' | 'A' | 'D' | 'DN' | 'NO' | 'R' | 'S' | 'U';
  schedule: Schedule;
  operationalTimes: {
    publishedDeparture: OperationalTime;
    scheduledGateDeparture: OperationalTime;
    estimatedGateDeparture: OperationalTime;
    actualGateDeparture: OperationalTime;
    publishedArrival: OperationalTime;
    scheduledGateArrival: OperationalTime;
    estimatedGateArrival: OperationalTime;
    estimatedRunwayArrival: OperationalTime;
  };
  // no info about  types
  codeshares: any[];
  // no info about  types
  delays: any;
  flightDurations: {
    scheduledBlockMinutes: number;
    scheduledAirMinutes: number;
    scheduledTaxiOutMinutes: number;
    scheduledTaxiInMinutes: number;
  };
  airportResources: {
    departureTerminal?: string;
    departureGate?: string;
    arrivalGate?: string;
    arrivalTerminal: string;
    baggage?: string;
  };
  flightEquipment: {
    scheduledEquipmentIataCode: string;
    actualEquipmentIataCode: string;
    tailNumber?: string;
    actualEquipment?: {
      iata: string;
    };
    scheduledEquipment?: {
      iata: string;
    };
  };
}

export interface Appendix {
  airlines: Airline[];
  airports: Airport[];
  equipments: Equipment[];
}

export interface FlightStatsBaseResponse {
  // requested url params
  request: any;
  appendix: Appendix;
  error?: {
    httpStatusCode: number;
    errorId: string;
    errorMessage: string;
    errorCode: string;
  };
}

export interface FlightStatsResponse extends FlightStatsBaseResponse {
  flightStatus: FlightStatus;
}

export interface FlightStatusesResponse extends FlightStatsBaseResponse {
  flightStatuses: FlightStatus[];
}

export enum AirportOrigin {
  arrival = 'arr',
  departure = 'dep',
}

export type AirportOriginType = AirportOrigin.arrival | AirportOrigin.departure;

interface MaxFlightLimit {
  // The maximum number of unique flights to return status documents for.
  // This is used to restrict output size for the API documentation;
  // if it is omitted, all flights are returned.
  maxFlights?: number;
}

interface CodeType {
  // Type of any given codes: 'IATA', 'ICAO', or 'FS'.
  // If not specified all domains will be searched in the order stated.
  codeType?: string;
}

interface UtcTime {
  // Time given as UTC instead of local (default is false)?
  utc?: boolean;
}

interface FlightStatusCommon {
  // Time given as UTC instead of local (default is false)?
  utc?: boolean;

  // format YYYY-MM-DD
  date: string;

  // arrival or departure airport
  airportType: AirportOriginType;
}

export interface FlightsByAirportOptions
  extends FlightStatusCommon,
    MaxFlightLimit,
    CodeType {
  //  airport code
  airport: string;

  // Hour of day (0-23)
  hourOfDay: number;

  // Number of hours' worth of arrivals to include (up to 6; default is 1)
  numHours?: number;

  // Filter results to include only the carrier indicated
  carrier?: string;
}

export interface FlightStatusesOptions
  extends FlightStatusCommon,
    CodeType,
    UtcTime {
  // Flight number
  flight: string | number;

  // Carrier (airline) code
  carrier: string;

  // arrival or departure airport code, depends on on airportType option
  airport?: string;
}

export interface FlightStatusByRouteOptions
  extends FlightStatusCommon,
    MaxFlightLimit,
    CodeType {
  // Arrival airport code
  arrivalAirport: string;

  // Departure airport code
  departureAirport: string;

  // Hour of day (0-23)
  hourOfDay?: number;
}

export interface FlightStatsResults<T> {
  results: T | null;
  error: boolean;
  message: string;
}

export interface InitialSubscriptionPayload {
  lumo: FlightInfo;
  alert: InitialFlightStatsAlert;
}
