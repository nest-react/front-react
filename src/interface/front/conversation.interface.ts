export interface Conversation {
  id: string;
  subject: string;
  status: string;
  tags: [];
  created_at: number;
  is_private: boolean;
}
