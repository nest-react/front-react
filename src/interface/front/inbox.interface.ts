export interface Inbox {
  id: string;
  name: string;
  is_private: boolean;
  address: string;
  send_as: string;
  type: string;
}
