interface Message {
  subject?: string;
  body: string;
  attachments?: string[];
}

export interface ConversationMessage extends Message {
  to?: string[];
  cc?: string[];
  bcc?: string[];
  sender_name?: string;
  author_id?: string;
  text?: string;
  options?: {
    tag_ids: string[];
    archive: boolean;
  };
}

export interface ChannelMessage extends Message {
  metadata: {
    external_id: string;
    external_conversation_id: string;
  };
  delivered_at?: number;
}

export interface InboundMessage extends ChannelMessage {
  sender: {
    name?: string;
    handle: string;
  };
}

export interface OutboundMessage extends ChannelMessage {
  to: {
    name?: string;
    handle: string;
  };
}
