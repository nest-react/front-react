export interface Front<T> {
  findAll(): Promise<FrontResponse<T[]>>;
  findById(id: string): Promise<FrontResponse<T>>;
  create(data: T): Promise<FrontResponse<T>>;
  update(id: string, data: T): Promise<FrontResponse<T>>;
}

export interface FrontAlias {
  key: string;
  value: string;
}

export interface FrontResponse<T> {
  data: {
    _pagination: {
      prev?: string;
      next?: string;
    };
    _links: {
      self: string;
    };
    _results: T;
  };
  success: boolean;
  error: any;
}

export interface FrontError {
  response: {
    data: {
      _error: {
        status: number;
        title: string;
        message: string;
      };
    };
  };
}
