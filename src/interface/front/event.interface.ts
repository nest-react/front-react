import { Conversation } from './conversation.interface';

// eslint-disable-next-line @typescript-eslint/no-empty-interface
interface Event {}

export interface MessageEvent extends Event {
  id: string;
  type: string;
  emitted_at: number;
  conversation: Conversation;
}
