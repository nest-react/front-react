interface Handle {
  handle: string;
  source: string;
}

export interface Contact {
  name?: string;
  description?: string;
  is_spammer?: boolean;
  links?: string[];
  group_names?: string[];
  handles: Handle[];
}
