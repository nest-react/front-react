import { FlightInfoExtended } from './';

export interface AlternatesResponse {
  count: number;
  flights: FlightInfoExtended[];
}
