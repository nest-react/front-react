export interface Alert {
  booking_reference?: string;
  change: string;
  itinerary_id: string;
  timestamp: string;
}

export interface Summaries {
  short?: {
    title: string;
    message: string;
    timestamp: string;
  };
}

type DelayCause =
  | 'flight-cancelled'
  | 'late-incoming-flight'
  | 'arrival-airport-conditions'
  | 'arrival-airport-constraints'
  | 'departure-airport-constraints'
  | 'latest-available-information';

export type DelayIndex = 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10;

export type PredictionRisk = 'LOW' | 'MODERATE' | 'HIGH' | 'SEVERE' | 'UNKNOWN';

export interface Prediction {
  delay_index: DelayIndex;
  risk: PredictionRisk;
  distribution: number[];
  causes?: DelayCause[];
  is_override: boolean;
}

export interface FlightInfo {
  aircraft_type: {
    generic?: string;
    generic_description?: string;
    specific?: string;
    specific_description?: string;
  };
  carrier: {
    iata: string;
    name: string;
  };
  destination: {
    iata: string;
    city: string;
    name: string;
  };
  id: string;
  number: number;
  operating_carrier?: {
    iata?: string;
  };
  origin: {
    iata: string;
    city: string;
    name: string;
  };
  scheduled_arrival: string;
  scheduled_departure: string;
  uuid: string;
}

type FlightStatusText =
  | 'Scheduled'
  | 'Departed'
  | 'Enroute'
  | 'Landed'
  | 'Arrived'
  | 'Expected'
  | 'Delayed'
  | 'Cancelled'
  | 'Diverted'
  | 'No Recent Info - Call Airline';

type StatusType = 'scheduled' | 'estimated' | 'actual';

export interface Status {
  text: FlightStatusText;
  departure: {
    scheduled: string;
    latest: string;
    type: StatusType;
    terminal?: string;
    gate?: string;
  };
  arrival?: {
    scheduled: string;
    latest: string;
    type: StatusType;
    terminal?: string;
    gate?: string;
    baggage_claim?: string;
  };
  diversion?: {
    airport?: string;
  };
  cancelled: boolean;
  source: 'flightview' | 'schedule' | 'swim';
  last_updated: string;
}

type IssuesBy =
  | 'Government of Canada'
  | 'Australian Department of Foreign Affairs and Trade'
  | 'US Department of State'
  | 'US Dept of State Overseas Security Advisory Council (OSAC)';

type Scope = 'global' | 'country' | 'city';

interface TravelAdvisory {
  issued_by: IssuesBy;
  advisory?: string;
  scope: Scope;
  url: string;
  last_updated: string;
}

type WeatherIcon =
  | 'clear-day'
  | 'clear-night'
  | 'rain'
  | 'snow'
  | 'sleet'
  | 'wind'
  | 'fog'
  | 'cloudy'
  | 'partly-cloudy-day'
  | 'partly-cloudy-night';

export interface Weather {
  icon: WeatherIcon;
  summary: string;
  temperature: number;
  precipitation: number;
  hourly: boolean;
}

type AlertType =
  | 'civil_unrest'
  | 'computer_systems'
  | 'construction'
  | 'industrial_action'
  | 'natural_disaster'
  | 'other'
  | 'severe_weather'
  | 'terrorism'
  | 'test'
  | 'us_dept_state_advisory'
  | 'vip_movement'
  | 'waiver';

export interface SpecialAlerts {
  id: number;
  location: {
    countries: [string];
    airports: [string];
  };
  airline: {
    carrier_codes: [string];
  };
  period: {
    start: string;
    end: string;
    tz: string;
  };
  operation: {
    arrivals: boolean;
    departures: boolean;
  };
  override_probs: boolean;
  alert: {
    type: AlertType;
    summary: string;
    description: string;
    url: string;
  };
  send_alerts: 'new' | 'all' | 'none';
  date_restrictions: {
    ticket_issued_on_before: string;
    new_travel_begin: string;
    new_travel_end: string;
  };
  dom_intl: 'D' | 'I' | 'A';
  remarks: string;
}

export interface TravelAdvisories {
  origin: TravelAdvisory[];
  destination: TravelAdvisory[];
}

export interface WeatherStatus {
  origin: Weather;
  destination: Weather;
}

export interface ConnectionData {
  airport: string;
  prob_miss: number;
  connection_time: number;
}

export interface FlightResponse {
  alert: Alert;
  flight: FlightInfo;
  inbound?: FlightResponse | null;
  prediction: {
    delay_index: DelayIndex;
    risk: PredictionRisk;
    distribution: [number];
    causes: DelayCause[];
    is_override: boolean;
    connections: [
      {
        airport: string;
        prob_miss: number;
        connection_time: number;
      },
    ];
  };
  special_alerts?: SpecialAlerts[];
  status?: Status;
  summaries?: Summaries;
  travel_advisories?: TravelAdvisories;
  weather?: WeatherStatus;
}

export type RiskDistribution = [number, number, number, number];

export interface FlightInfoExtended {
  alert: Alert;
  prediction: {
    delay_index: DelayIndex;
    risk: PredictionRisk;
    distribution: RiskDistribution;
    causes: DelayCause[];
    is_override: boolean;
    connections: [
      {
        airport: string;
        prob_miss: number;
        connection_time: number;
      },
    ];
  };
  status: Status;
  historical_performance: {
    // An array of 60 values representing the historical arrival performance of this flight over the last 60 days.
    // The first value is 60 days ago and the last value in the array is yesterday.
    arrival_delay: number[];
    last_updated: string;
  };
  weather: WeatherStatus;
  inbound: FlightResponse | null;
  special_alerts: SpecialAlerts[];
  travel_advisories: TravelAdvisories | null;
}

export interface ValidateRequest {
  carrier: string;
  flightNumber: number;
  // YYYY-MM-DD
  date: string;
}

export interface ValidationResults {
  isValid: boolean;
  origin?: string;
  destination?: string;
}
