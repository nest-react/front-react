/*
 * API reference
 * https://developer.thinklumo.com/#tag/flights/paths/~1flights~1v1~1status/get
 * Required query parameters for status search: 'carrier', 'flight_number', 'date'
 * */
export interface StatusOptions {
  // Example: carrier=DL
  // filters by carrier IATA code
  carrier: string;

  // Example: flight_number=709
  // filters by flight number
  flight_number: number;

  // Example: date=2018-06-02
  // filter flights that depart on this date (departure airport local time)
  date: string;

  // Example: origin=BOS
  // filters by departure airport
  origin?: string;

  // Example: destination=DEN
  // filters by arrival airport
  destination?: string;

  // Example: inbound=false
  // include inbound flight status
  inbound?: boolean;

  // Default: false
  // include weather data
  weather?: boolean;

  // Default: false
  // include historical performance data
  history?: boolean;

  // round the delay distribution.
  // We recommend using the rounded distribution to better communicate delay to end users.
  round?: boolean;
}
