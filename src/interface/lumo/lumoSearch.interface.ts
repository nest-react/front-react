/*
API reference
// https://developer.thinklumo.com/#tag/flights/paths/~1flights~1v1~1search/get

Search must be more specific:
we restrict the number of flights that can be queried at once.
Please make the query more specific by including either
1) an origin/destination and date
2) a carrier, flight_number and date
3) departing_after and departing_before to limit the departing interval to 1 hour,
4) arriving_after and arriving_before to limit the arriving interval to 1 hour
*/
export interface SearchOptions {
  // Example: origin=BOS
  // filters by departure airport
  origin?: string;

  // Example: destination=DEN
  // filters by arrival airport
  destination?: string;

  // Example: carrier=DL
  // filters by carrier IATA code
  carrier?: string;

  // Example: flight_number=709
  // filters by flight number
  flight_number?: number;

  // Example: date=2018-06-02
  // filter flights that depart on this date (departure airport local time)
  date?: string;

  // Default: false
  // includes nearby airports when origin or destination filters are specified
  nearby?: boolean;

  // Default: false
  // include codeshare flights - default is false
  codeshare?: boolean;

  // Example: departing_after=2018-06-24T18:30:00Z
  // filters flights that depart after the specified time (inclusive)
  departing_after?: string;

  // Example: departing_before=2018-06-24T18:30:00Z
  // filters flights that depart before the specified time (exclusive)
  departing_before?: string;

  // Example: changed_after=2018-06-24T18:30:00Z
  // filters flights whose delay predictions were changed after the specified time (inclusive)
  changed_after?: string;

  // Example: changed_before=2018-06-24T18:30:00Z
  // filters flights whose delay predictions were changed before the specified time (exclusive)
  changed_before?: string;

  // Default: false
  // include realtime status data
  status?: boolean;

  // Default: false
  // include historical performance data
  history?: boolean;

  // Default: false
  // include weather data
  weather?: boolean;

  // round the delay distribution.
  // We recommend using the rounded distribution to better communicate delay to end users.
  round?: boolean;
}

export interface SearchResponse<T> {
  count: number;
  results: T[];
}
