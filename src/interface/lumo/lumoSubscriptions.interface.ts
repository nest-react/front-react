import { StatusOptions } from './lumoStatus.interface';

export interface SubscriptionOptions {
  flight_ids?: string[];

  flights?: StatusOptions[];
  // webhook URL - this is where updates to flight status are sent as a POST request
  target: string;
}

export interface SubscriptionResponse {
  itinerary_id: string;
  flight_ids: string[];
  target: string;
  created: string;
  updated: string;
}
