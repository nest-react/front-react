import { FlightInfoExtended } from './';

export interface ConnectionResponse {
  connection: {
    airport: string;
    prob_miss: number;
    risk: 'LOW' | 'MODERATE' | 'HIGH' | 'SEVERE' | 'UNKNOWN';
    connection_time: number;
  };
  flights: FlightInfoExtended[];
}
