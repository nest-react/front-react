export enum Collections {
  passengers = 'passengers',
  queueFlight = 'queue_flight',
}

export interface Passenger {
  firstName: string;
  lastName: string;
  email: string;
  mobile: string;
  has_whatsapp?: boolean;
}

export interface QueueFlightTicket {
  airline1: string;
  depdatestring: string;
  email: string;
  flight1airport: string;
  flight2airport?: string;
  flightno1: string;
  passengerRef: string;
}

export interface QueueFlightPick {
  depdatestring: string;
  email: string;
  flight1airport: string;
  flight2airport?: string;
  passengerRef: string;
}

export enum Flight {
  ticket = 'ticket',
  pick = 'pick',
}

interface RegisterPayload {
  firstName: string;
  lastName: string;
  email: string;
  phone: string;
  haveFlight: string;
  haveWhatsApp: boolean;
  haveConnecting: boolean;
  haveReturnFlight: boolean;
  carrier: string;
  flightNumber: string;
  airport: string;
  date: string;
  airportFrom: string;
  airportTo: string;
  connectingCarrier: string;
  connectingFlightNumber: string;
  departingAirport: string;
  returnCarrier: string;
  returnFlightNumber: string;
  returnAirport: string;
  returnDate: string;
  returnConnectingCarrier: string;
  returnConnectingFlightNumber: string;
  returnConnectingAirport: string;
  haveReturnConnecting: boolean;
}

export interface PassengerResponse extends Passenger {
  id?: string;
}

export type RegisterDto = RegisterPayload & Passenger;
