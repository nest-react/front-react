interface Disease {
  common_name: string;
  created_at: string;
  id: string;
  occurs_where: string;
  scientific_name: string;
  updated_at: string;
}

interface Banner {
  large: string;
  medium: string;
  original: string;
  small: string;
}

interface Flag {
  list: string;
  main: string;
  original: string;
}

export interface RestrictionResponse {
  affected_countries: any[];
  affected_country_division_ids: any[];
  affected_country_divisions: any[];
  affected_country_ids: any[];
  affected_country_region_ids: any[];
  affected_country_regions: any[];
  airline: any;
  airline_id: any;
  categories: any[];
  closed: boolean;
  comment: string;
  created_at: string;
  disease: Disease;
  disease_id: string;
  effective_as_of: string;
  finish: any;
  id: string;
  origin_country: {
    banner: Banner;
    center_lat: number;
    center_lng: number;
    country_code: string;
    country_code_3: string;
    emerg_numbers: any[];
    flag: Flag;
    flag_emoji: string;
    geographic_region_id: string;
    geojson_url: string;
    id: string;
    name: string;
    ne_bound_lat: number;
    ne_bound_lng: number;
    population: number;
    sec_emer_num: string;
    sw_bound_lat: number;
    sw_bound_lng: number;
    topojson_url: string;
    travel_status: number;
    updated_at: string;
  };
  origin_country_id: string;
  references: string[];
  start: string;
  // Value	Description
  // 0	  Airline
  // 1	  Border Closure or Change
  // 2	  Curfew
  // 3	  Social Distancing
  // 4	  Transportation (usually internal)
  // 99	  Other
  // 201	Recommended Border App
  // 401	Non-essential shops open?
  // 402	Accomodations open?
  // 403	Restaurants open?
  // 404	Bars and cafes open?
  // 405	Beaches and tourism sites open?
  // 406	Museums and heritage sites open?
  // 407	Personal care services open? (e.g. nail salon, barber)
  // 408	Places of worship open?
  // 409	Events allowed?
  // 410	Masks required in public?
  type: number;
  updated_at: string;
  value: number;
}

export interface RestrictionsList {
  airline?: RestrictionResponse[];
  border?: RestrictionResponse[];
  curfew?: RestrictionResponse[];
  social?: RestrictionResponse[];
  transportation?: RestrictionResponse[];
  other?: RestrictionResponse[];
  borderApp?: RestrictionResponse[];
  nonEssentialShops?: RestrictionResponse[];
  restaurants?: RestrictionResponse[];
  accommodation?: RestrictionResponse[];
  barsAndCafes?: RestrictionResponse[];
  beaches?: RestrictionResponse[];
  museums?: RestrictionResponse[];
  personalCare?: RestrictionResponse[];
  placesOfWorship?: RestrictionResponse[];
  events?: RestrictionResponse[];
  masks?: RestrictionResponse[];
}

interface RestrictionsMap {
  [key: number]: keyof RestrictionsList;
}

export const restrictionsTypeMap: RestrictionsMap = {
  0: 'airline',
  1: 'border',
  2: 'curfew',
  3: 'social',
  4: 'transportation',
  99: 'other',
  201: 'borderApp',
  401: 'nonEssentialShops',
  402: 'accommodation',
  403: 'restaurants',
  404: 'barsAndCafes',
  405: 'beaches',
  406: 'museums',
  407: 'personalCare',
  408: 'placesOfWorship',
  409: 'events',
  410: 'masks',
};

export interface DivisionTravel {
  id: string;
  travel_status: number;
  name: string;
  topojson_url: string;
  geojson_url: string;
}

export interface TravelStatusResponse {
  travel_status: number;
  id: string;
  regions: any[];
  divisions: DivisionTravel[];
}

export const travelStatusMap: StatusMap = {
  0: 'Normal',
  1: 'Caution',
  2: 'Avoid',
};

export interface StatusMap {
  [key: number]: string;
}

export interface TravelStatus extends TravelStatusResponse {
  description: string;
}

export interface Country {
  country_code: string;
  id: string;
  population: number;
  topojson_url: string;
}

export interface CavidSummaryResponse {
  country: Country;
  summary: {
    active_count: number;
    active_density: number;
    case_count: number;
    case_density: number;
    death_count: number;
    death_density: number;
    irregular: boolean;
    recovered_count: number;
    recovered_density: number;
    reported_on: string;
    sitata_risk: number;
  };
}
