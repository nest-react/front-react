export default interface twillliosms {
  From: string;
  To: string;
  Body: string;
  Source?: string;
}
