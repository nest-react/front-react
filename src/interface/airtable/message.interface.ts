export enum Direction {
  arrive = 'Arrive',
  depart = 'Depart',
}

interface IMessage {
  airportCode: string;
  message: string;
  active: boolean;
}

export interface IAirportMessage extends IMessage {
  terminal: string;
  direction: Direction;
  date: Date;
}

export interface IAirlineMessage extends IMessage {
  AirLine: string;
  Link: string;
}

export interface iPickupMessage extends IMessage {
  'IATA Code': string;
  City: string;
}

export interface IPassengerMessage extends IMessage {
  email: string;
  eventName: string;
  sent: boolean;
}
