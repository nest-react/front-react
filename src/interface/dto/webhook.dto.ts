export interface FrontDTO {
  conversation: {
    id: string;
  };

  target: {
    data: {
      text: string;
    };
  };
}

export interface MuteBotDTO {
  mute: boolean;
}

export interface MuteBotParams {
  phone: string;
}

export interface GetUserDTO {
  email?: string;
  phone?: string;
}

export interface RegisterFlightDTO {
  mobile: string;
}

export interface RegisterUserDTO {
  firstName: string;
  lastName: string;
  email: string;
  mobile: string;
  haveWhatsApp: boolean;
  whatsapp?: string;
}

export interface FrontParamsDTO {
  object: string;
}
