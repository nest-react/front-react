import { EngineResult } from 'json-rules-engine';
import Flight from '../../model/Flight';
import { RuleEngineServices } from '../webhook-server.interface';
import { AlertResponse } from '../../interface/flightStats/alert';
import { FlightResponse } from '../../interface/lumo';
interface Condition {
  fact: string;

  operator: string;

  value: any;
}

interface Event {
  type: string;
  params: {
    message: string;
  };
}

export interface Rule {
  conditions: {
    any: [
      {
        all: Condition[];
      },
    ];
  };
  event: Event;
}

export interface BaseHandler {
  handle(
    result: EngineResult,
    services?: RuleEngineServices,
    flight?: Flight,
  ): Promise<BaseHandler>;

  setNext(handler: BaseHandler): BaseHandler;
}

export interface FactRequest {
  source: string;
  oldFlight: Flight;
  newFlight: AlertResponse | FlightResponse;
}
