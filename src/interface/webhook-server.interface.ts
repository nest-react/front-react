import { Connection } from 'typeorm/index';
import BeeQueue from 'bee-queue/index';
import { FlightService } from '../service/Flight.service';
import { UserService } from '../service/User.service';
import { BeeQueueService } from '../service/BeeQueue.service';
import { CommsEventService } from '../service/CommsEvent.service';

export interface WebhookServices {
  db: Connection;
  bq: BeeQueue;
}

export interface RuleEngineServices extends WebhookServices {
  flightService: FlightService;
  userService: UserService;
  beeQueueService: BeeQueueService;
  commsEventService: CommsEventService;
}
