// The Front object is loaded through the Front script added in the header of the main HTML.
// This object can be used to listen to conversation event data as it occurs on Front, request information from Front, and perform actions on Front.
// See the full plugin API documentation here: https://dev.frontapp.com/plugin.html

String.prototype.toProperCase = function () {
  return this.replace(/\w\S*/g, function (txt) {
    return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
  });
};

const firebaseConfig = {
  apiKey: 'AIzaSyCZg6RoDQtZ4E5-AJo8BPzKpUFwCE36gPQ',
  authDomain: 'zenner-epomeno.firebaseapp.com',
  databaseURL: 'https://zenner-epomeno.firebaseio.com',
  projectId: 'zenner-epomeno',
  storageBucket: 'zenner-epomeno.appspot.com',
  messagingSenderId: '553211875065',
  appId: '1:553211875065:web:aff24add44954e95111c28',
};

// Initialize Firebase
firebase.initializeApp(firebaseConfig);

const token = firebaseToken;
const secret = frontSecret;

firebase
  .auth()
  .signInWithCustomToken(token)
  .catch(function (error) {
    console.log(error);
  });

const provider = new firebase.auth.GoogleAuthProvider();
const db = firebase.firestore();
let googleUser = null;
let pendingContact = null;

firebase.auth().onAuthStateChanged(function (user) {
  if (user === null) {
    console.log('Not logged in!');
    // firebase.auth().signInWithPopup(provider).then(function(result) {
    //   // This gives you a Google Access Token. You can use it to access the Google API.
    //   var token = result.credential.accessToken;
    //   // The signed-in user info.
    //   googleUser = result.user;
    //   console.log(googleUser);
    //   // ...
    // }).catch(function(error) {
    //   // Handle Errors here.
    //   var errorCode = error.code;
    //   var errorMessage = error.message;
    //   // The email of the user's account used.
    //   var email = error.email;
    //   // The firebase.auth.AuthCredential type that was used.
    //   var credential = error.credential;
    //   // ...
    //   console.log(error);
    // });
  } else {
    console.log('User logged in');
    googleUser = user;
    if (pendingContact !== null) {
      loadContact(pendingContact);
    }
  }
});

// This keeps track of if Front has returned a conversation to the plugin.
let hasConversation;

// Listen for the `conversation` event from Front and print its contents, then load the contact to the plugin.
Front.on('conversation', function (data) {
  //console.log('Event data', data);
  // Set the conversation state.
  hasConversation = true;

  // Load the Contact information based off of the event data. And set tab to 'Info'.
  loadContact(data.contact);
  //showInfo();
});

// Listen for the `no_conversation` event.  This can happen when opened to Inbox Zero.
Front.on('no_conversation', function () {
  console.log('No conversation');

  // Set the conversation state.
  hasConversation = false;

  // Display `No Contact` data and clear the notes and set the tab to 'Info'.
  displayContactInfo();
  //displayCRMInfo();
  //clearNotes();
  showInfo();
});

// Asynchronously loads the contact through our mocked CRM service once the body of the plugin is loaded.
// This will call our mocked CRM service for data and then add the contact information and notes to the page.
async function loadContact(contact) {
  clearNotes();

  console.log('Need to load ' + contact.handle);
  if (googleUser !== null) {
    const response = await loadUserData(contact.source, contact.handle);
    if (response && response.status === 200) {
      const user = await response.json();
      await updateCustomerInfo(user.id, user);
      if (user.flights && user.flights.length >= 1) {
        user.flights.forEach((flight) => {
          displayFlight(flight);
        });
      }
    }
    pendingContact = null;
  } else {
    pendingContact = contact;
  }

  // Display Front contact info.
  // displayContactInfo(contact.display_name || contact.handle, contact.handle);

  // Build and display our CRM data.
  // const crmData = await mockQueryCRM(contact.handle);
  // displayCRMInfo(crmData.info.id, crmData.info.location, crmData.info.status);

  //  Load the notes from our CRM data.
  // displayNotes(crmData.notes);
}

async function loadUserData(source, handle) {
  if (['email', 'phone'].includes(source)) {
    return await fetch(`/user?${source}=${handle}`, {
      method: 'GET',
      headers: {
        Authorization: secret,
      },
    });
  }
}

async function loadFlights(passengerRef) {
  db.collection('flights')
    .where('_passenger', '==', passengerRef)
    .where('_active', '==', true)
    .orderBy('times.estimated_departure_dt', 'desc')
    .get()
    .then((querySnapshot) => {
      querySnapshot.forEach((doc) => {
        let flight = doc.data();
        displayFlight(flight);
      });
    });
}

async function updateCustomerInfo(ref, passenger) {
  document.getElementById('name').textContent = passenger.name;
  document.getElementById('handle').textContent = ref;
  document.getElementById('phone').textContent = '+' + passenger.mobile;
  document.getElementById('email').textContent = passenger.email;

  let muteButton = document.getElementById('mute_bot');
  let unmuteButton = document.getElementById('unmute_bot');

  if (passenger && passenger.muteBot) {
    // Bot is muted
    if (unmuteButton.classList.contains('w3-disabled')) {
      unmuteButton.classList.remove('w3-disabled');
    }
    muteButton.classList.add('w3-disabled');
  } else {
    // Bot is active
    if (muteButton.classList.contains('w3-disabled')) {
      muteButton.classList.remove('w3-disabled');
    }
    unmuteButton.classList.add('w3-disabled');
  }

  showNotes();
}

async function muteBot() {
  let muteButton = document.getElementById('mute_bot');
  let unmuteButton = document.getElementById('unmute_bot');
  const phoneNumber = document.querySelector('#phone').textContent;
  if (unmuteButton.classList.contains('w3-disabled')) {
    unmuteButton.classList.remove('w3-disabled');
    unmuteButton.disabled = false;
  }

  muteButton.classList.add('w3-disabled');
  muteButton.disabled = true;

  await fetch(`/bot/mute/${phoneNumber}`, {
    method: 'PUT',
    body: JSON.stringify({
      mute: true,
    }),
    headers: {
      Authorization: secret,
      'Content-Type': 'application/json',
    },
  });
}

async function unmuteBot() {
  let muteButton = document.getElementById('mute_bot');
  let unmuteButton = document.getElementById('unmute_bot');

  const phoneNumber = document.querySelector('#phone').textContent;

  if (muteButton.classList.contains('w3-disabled')) {
    muteButton.classList.remove('w3-disabled');
    muteButton.disabled = false;
  }

  unmuteButton.classList.add('w3-disabled');
  unmuteButton.disabled = true;

  await fetch(`/bot/mute/${phoneNumber}`, {
    method: 'PUT',
    body: JSON.stringify({
      mute: false,
    }),
    headers: {
      Authorization: secret,
      'Content-Type': 'application/json',
    },
  });
}

// Asynchronously create another note through our mocked CRM service to add to the list.
async function createNote() {
  if (!hasConversation) {
    console.log('No conversation selected.');
    return;
  }

  const note = await mockPostNote();
  displayNote(note);
}

// Displays Front contact information.
function displayContactInfo(display_name = 'No Contact', handle = '-') {
  const nameElement = document.getElementById('name');
  const handleElement = document.getElementById('handle');

  nameElement.textContent = display_name;
  handleElement.textContent = handle;
}

// Displays mocked CRM Info.
function displayCRMInfo(id = '-', location = '-', status = '-') {
  const idElement = document.getElementById('id');
  const locationElement = document.getElementById('location');
  const statusElement = document.getElementById('status');

  idElement.textContent = id;
  locationElement.textContent = location;
  statusElement.textContent = status;
}

// Displays the mocked CRM notes.
function displayNotes(notes) {
  // Reset the Notes column to make room for the newly found notes.
  clearNotes();

  // Add each Note to the Notes Column object.
  notes.forEach((note) => {
    displayNote(note);
  });
}

// Removes the currently displayed Notes.
function clearNotes() {
  const noteColumns = document.getElementById('notes');
  noteColumns.innerHTML = null;
}

// Set the tab to Info and hide Notes.
function showInfo() {
  const infoButton = document.getElementById('infoButton');
  const notesButton = document.getElementById('notesButton');
  infoButton.classList.add('selected');
  notesButton.classList.remove('selected');

  const infoSection = document.getElementById('infoSection');
  infoSection.classList.remove('displayNone');
  const notesSection = document.getElementById('notesSection');
  notesSection.classList.add('displayNone');
}

// Set the tab to Notes and hide Info.
function showNotes() {
  const infoButton = document.getElementById('infoButton');
  const notesButton = document.getElementById('notesButton');
  //infoButton.classList.remove('selected');
  notesButton.classList.add('selected');

  const infoSection = document.getElementById('infoSection');
  infoSection.classList.add('displayNone');
  const notesSection = document.getElementById('notesSection');
  notesSection.classList.remove('displayNone');
}

function displayFlight(flight) {
  const noteColumns = document.getElementById('notes');
  // Build the shadowed backdrop for the Note.
  let noteBlock = document.createElement('div');
  noteBlock.classList.add('noteBlock');

  // Build the Header of the note containing the author and the time written.
  let noteHeader = document.createElement('p');
  noteHeader.classList.add('row');

  let noteHeaderAuthor = document.createElement('div');
  noteHeaderAuthor.textContent = flight.flightNo;
  noteHeaderAuthor.classList.add('font', 'noteKey');

  let noteHeaderTime = document.createElement('div');
  noteHeaderTime.textContent = new Date(
    flight.scheduledDeparture,
  ).toLocaleDateString('en-US', {
    weekday: 'long',
    year: 'numeric',
    month: 'long',
    day: 'numeric',
  });
  noteHeaderTime.classList.add('font', 'noteValue');

  noteHeader.appendChild(noteHeaderAuthor);
  noteHeader.appendChild(noteHeaderTime);

  // Build the Blurb of the note;
  let now = new Date();
  let depdate = new Date(flight.scheduledDeparture);
  let diffMs = depdate - now; // milliseconds between now & date
  let diffDays = Math.floor(diffMs / 86400000); // days
  let diffHrs = Math.floor((diffMs % 86400000) / 3600000); // hours
  let diffMins = Math.round(((diffMs % 86400000) % 3600000) / 60000); // minutes
  let noteBlurb = document.createElement('p');
  noteBlurb.innerHTML =
    'Departs in ' +
    diffDays +
    'd, ' +
    diffHrs +
    'h, ' +
    diffMins +
    'm.<br>' +
    'Carrier: ' +
    flight.operatingCarrier +
    '<br><br>' +
    'From: ' +
    flight.initialLumoData.flight.origin.city +
    ' (' +
    flight.origin +
    ')<br>' +
    'Terminal: ' +
    (flight.initialFlightStatsData
      ? flight.initialFlightStatsData.airportResources.destinationTerminal
      : '') +
    ' ' +
    'Gate: ' +
    flight.destinationGate +
    '<br>' +
    'On: ' +
    new Date(flight.scheduledDeparture).toLocaleDateString('en-GB', {
      weekday: 'long',
      month: 'long',
      day: 'numeric',
    }) +
    ' @ ' +
    flight.scheduledDeparture.substr(11, 5) +
    '<br><br>' +
    'To: ' +
    flight.initialLumoData.flight.destination.city +
    ' (' +
    flight.destination +
    ')<br>' +
    'Terminal: ' +
    flight.destinationTerminal +
    ' ' +
    'Gate: ' +
    flight.destinationGate +
    ' ' +
    'Baggage: ' +
    flight.destinationBaggage +
    '<br>' +
    'On: ' +
    new Date(flight.scheduledArrival).toLocaleDateString('en-GB', {
      weekday: 'long',
      month: 'long',
      day: 'numeric',
    }) +
    ' @ ' +
    flight.scheduledArrival.substr(11, 5) +
    '<br><br>' +
    'Status: ' +
    flight.statusName +
    '<br>' +
    'Risk: ' +
    flight.lumoRisk.toProperCase() +
    '<br>';

  noteBlurb.classList.add('row', 'font');

  // Append the Header and the Blurb to the Note block.
  noteBlock.appendChild(noteHeader);
  noteBlock.appendChild(noteBlurb);
  noteColumns.appendChild(noteBlock);
}
