import { FastifyRequest } from 'fastify';
import { Connection, Repository } from 'typeorm';

import User from '../model/User';
import { ConfigService, LumoService } from '../service';
import { trackConnectionFlight, trackSingleFlight } from '../bot-server';
import { ValidateRequest } from '../interface/lumo';
import { FlightService } from '../service/Flight.service';
import { FlightStatsService } from '../service/FlightStats.service';
import {
  //   FirestoreService,
  Flight as FlightType,
  RegisterDto,
} from '../interface/firestore.interface';
import { TrackService } from '../service/Track.service';
import { AirportOrigin } from '../interface/flightStats';
import * as axios from 'axios';

export default class FlightController {
  constructor(
    //private readonly firestoreService: FirestoreService,
    private readonly lumoService: LumoService,
    private readonly flightStatsService: FlightStatsService,
    private readonly db: Connection,
  ) {}

  public registerForFlight = async (
    request: FastifyRequest<{
      Body: RegisterDto;
    }>,
    reply: any,
  ): Promise<any> => {
    const { email, firstName, lastName, mobile } = request.body;
    /*
    let passengerId = await this.firestoreService.getPassengerByEmail(email);
    if (!passengerId) {
      const payload = {
        email,
        firstName,
        lastName,
        mobile,
        has_whatsapp: haveWhatsApp,
      };
      passengerId = await this.firestoreService.addPassenger(payload);
    }
    */
    /* START OF A TERRIBLE HACKS. dkontorovskyy is not proud of this. at all */
    /* I specifically not proud of next line */
    /* istanbul ignore file */
    const flightService = new FlightService(this.db);
    const flightStatsService = new FlightStatsService(this.db);
    const trackService: TrackService = new TrackService(
      this.lumoService,
      flightStatsService,
      flightService,
    );
    let outboundFlight, outboundConnection, inboundFlight, inboundConnection;
    const userRepository: Repository<User> = this.db.getRepository(User);

    let user = await userRepository.findOne({
      where: {
        mobile,
      },
      select: ['id'],
    });
    if (!user) {
      user = await userRepository.save({
        name: `${firstName} ${lastName}`,
        firstName,
        lastName,
        email,
        mobile,
        whatsapp: request.body.haveWhatsApp ? mobile : null,
      });
    }

    if (request.body.haveFlight === FlightType.pick) {
      // Hardcoded path for demo purposes
      const now = new Date();
      const inFourHours = new Date();

      // Depart should be 4 hours +
      inFourHours.setHours(inFourHours.getHours() + 4);

      // !!! HACK !!!
      // For changing date or month or year situation
      if (
        inFourHours.getUTCDate() > now.getUTCDate() ||
        inFourHours.getUTCMonth() > now.getUTCMonth() ||
        inFourHours.getUTCFullYear() > now.getUTCFullYear()
      ) {
        if (inFourHours.getUTCDate() > now.getUTCDate()) {
          now.setUTCDate(inFourHours.getUTCDate());
        }

        if (inFourHours.getUTCMonth() > now.getUTCMonth()) {
          now.setUTCMonth(inFourHours.getUTCMonth());
          now.setUTCDate(1);
        }

        if (inFourHours.getUTCFullYear() > now.getUTCFullYear()) {
          now.setUTCFullYear(inFourHours.getUTCFullYear());
          now.setUTCMonth(0);
          now.setUTCDate(1);
        }
      }

      const flightDate = now.toISOString().split('T')[0];
      const departAfter = inFourHours.toISOString();

      const { results } = await this.lumoService.search({
        origin: 'JFK',
        destination: 'LAX',
        date: flightDate,
        departing_after: departAfter,
      });

      await trackSingleFlight(results[0], user, trackService);
    } else if (request.body.haveFlight === FlightType.ticket) {
      const { results: resultsOutbound } = await this.lumoService.search({
        origin: request.body.airport,
        carrier: request.body.carrier,
        flight_number: Number(request.body.flightNumber),
        date: request.body.date,
      });
      outboundFlight = resultsOutbound[0];
      if (!resultsOutbound.length) {
        const { results } = await this.flightStatsService.status(
          {
            flight: request.body.flightNumber,
            carrier: request.body.carrier,
            date: request.body.date,
            airportType: AirportOrigin.departure,
            airport: request.body.airport,
          },
          { extendedOptions: ['useInlinedReferences', 'includeNewFields'] },
        );
        if (results.length) {
          await trackSingleFlight(results[0], user, trackService);
        }
      }

      if (request.body.haveConnecting) {
        const {
          results: resultsOutboundConnection,
        } = await this.lumoService.search({
          origin: request.body.departingAirport,
          carrier: request.body.connectingCarrier,
          flight_number: Number(request.body.connectingFlightNumber),
          date: resultsOutbound[0].flight.scheduled_arrival.split('T')[0],
          departing_after: new Date(
            resultsOutbound[0].flight.scheduled_arrival,
          ).toISOString(),
        });

        outboundConnection = resultsOutboundConnection[0];
        if (
          new Date(resultsOutboundConnection[0].flight.scheduled_departure) <
          new Date(resultsOutbound[0].flight.scheduled_arrival)
        ) {
          const dateAfter = new Date(
            resultsOutbound[0].flight.scheduled_arrival,
          );
          dateAfter.setDate(
            new Date(resultsOutbound[0].flight.scheduled_arrival).getDate() + 1,
          );

          const {
            results: resultsOutgoingConnection,
          } = await this.lumoService.search({
            origin: request.body.returnConnectingAirport,
            carrier: request.body.returnConnectingCarrier,
            flight_number: Number(request.body.returnConnectingFlightNumber),
            date: dateAfter.toISOString(),
            departing_after: dateAfter.toISOString(),
          });
          outboundConnection = resultsOutgoingConnection[0];
        }
        await trackConnectionFlight(
          [outboundFlight, outboundConnection],
          user,
          trackService,
        );
      }

      if (request.body.haveReturnFlight) {
        const { results: resultsInbound } = await this.lumoService.search({
          origin: request.body.returnAirport,
          carrier: request.body.returnCarrier,
          flight_number: Number(request.body.returnFlightNumber),
          date: request.body.returnDate,
        });
        inboundFlight = resultsInbound[0];
        if (request.body.haveReturnConnecting) {
          const {
            results: resultsInboundConnection,
          } = await this.lumoService.search({
            origin: request.body.returnConnectingAirport,
            carrier: request.body.returnConnectingCarrier,
            flight_number: Number(request.body.returnConnectingFlightNumber),
            date: request.body.returnDate,
            departing_after: new Date(
              resultsInbound[0].flight.scheduled_arrival,
            ).toISOString(),
          });

          inboundConnection = resultsInboundConnection[0];
          if (
            new Date(resultsInboundConnection[0].flight.scheduled_departure) <
            new Date(resultsInbound[0].flight.scheduled_arrival)
          ) {
            const dateAfter = new Date(
              resultsInbound[0].flight.scheduled_arrival,
            );
            dateAfter.setDate(
              new Date(resultsInbound[0].flight.scheduled_arrival).getDate() +
                1,
            );

            const {
              results: resultsInboundConnection,
            } = await this.lumoService.search({
              origin: request.body.returnConnectingAirport,
              carrier: request.body.returnConnectingCarrier,
              flight_number: Number(request.body.returnConnectingFlightNumber),
              date: dateAfter.toISOString(),
              departing_after: dateAfter.toISOString(),
            });
            inboundConnection = resultsInboundConnection[0];
          }
          await trackConnectionFlight(
            [inboundFlight, inboundConnection],
            user,
            trackService,
          );
        } else {
          await trackSingleFlight(resultsInbound[0], user, trackService);
        }
      }
      if (!request.body.haveConnecting && resultsOutbound.length) {
        await trackSingleFlight(resultsOutbound[0], user, trackService);
      }
    }
    /* END OF TERRIBLE HACKS THAT dkontorovskyy IS NOT PROUD OFF */
    let slackMessage = `Timestamp: ${new Date(
      new Date().getTime() + 7200000,
    )} \nRegister Event: ${
      request.body.haveFlight === 'ticket'
        ? 'Register to flight'
        : 'Flight for later'
    } 
    Name: ${request.body.firstName} ${request.body.lastName}
    Phone: +${request.body.mobile} (${
      request.body.haveWhatsApp ? 'Whatsapp' : 'SMS'
    })
    Email: ${request.body.email}
    `;
    if (outboundFlight) {
      slackMessage += `\nFlight Date: ${request.body.date}
      Outbound: ${request.body.carrier}${request.body.flightNumber}
      Route: ${outboundFlight?.flight?.origin.iata} ===> ${outboundFlight?.flight?.destination.iata}
      departure time: ${outboundFlight?.flight?.scheduled_departure} 
      `;
    }
    if (outboundConnection) {
      slackMessage += `\nOutbound Connection: ${request.body.connectingCarrier}${request.body.connectingFlightNumber}
      Route: ${outboundConnection?.flight?.origin.iata} ===> ${outboundConnection?.flight?.destination.iata}
      departure time: ${outboundConnection?.flight?.scheduled_departure} 
      `;
    }
    if (inboundFlight) {
      slackMessage += `\nReturn Date: ${request.body.returnDate}
      Inbound: ${request.body.returnCarrier}${request.body.returnFlightNumber}
      Route: ${inboundFlight?.flight?.origin.iata} ===> ${inboundFlight?.flight?.destination.iata}
      departure time: ${inboundFlight?.flight?.scheduled_departure} 
      `;
    }
    if (inboundConnection) {
      slackMessage += `\nInbound Connection:${request.body.returnConnectingCarrier}${request.body.returnConnectingFlightNumber}
      Route: ${inboundConnection?.flight?.origin.iata} ===> ${inboundConnection?.flight?.destination.iata}
      departure time: ${inboundConnection?.flight?.scheduled_departure} `;
    }
    await axios.default.post(ConfigService.get('SLACK_ONBOARDING_SHU'), {
      text: slackMessage,
    });

    return reply.code(200).send({});
  };

  public validate = async (
    payload: ValidateRequest,
    reply: any,
  ): Promise<any> => {
    const lumoResult = await this.lumoService.validateFlights(payload);
    if (lumoResult.isValid) {
      return reply.code(200).send(lumoResult);
    }
    const flightStatsResults = await this.flightStatsService.validateFlights(
      payload,
    );
    return reply.code(200).send(flightStatsResults);
  };
}
