import { FastifyRequest } from 'fastify';

import CarrierService, { CarrierMeta } from '../service/Carrier.service';
import AirportService, { AirportMeta } from '../service/Airport.service';

interface AutocompleteRequest {
  q: string;
  limit: string;
}

class AutoCompleteController {
  public airlines(
    request: FastifyRequest<{
      Querystring: AutocompleteRequest;
    }>,
    reply: any,
  ): Promise<CarrierMeta[]> {
    const { q, limit = 5 } = request.query;
    const results = CarrierService.search(q, Number(limit));

    return reply.code(200).send(results);
  }

  public airports(
    request: FastifyRequest<{
      Querystring: AutocompleteRequest;
    }>,
    reply: any,
  ): Promise<AirportMeta[]> {
    const { q, limit = 5 } = request.query;
    const results = AirportService.search(q, Number(limit));

    return reply.code(200).send(results);
  }
}

export default new AutoCompleteController();
