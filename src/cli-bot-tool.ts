// Libraries
import * as dotenv from 'dotenv'; // Environment vars
import BQ from 'bee-queue'; // Queue management via Redis
import ConfigService from './service/Config.service';
dotenv.config();

// Serverline doesn't support import statements
const fs = require('fs');
const path = require('path');
const serverline = require('serverline');
const chalk = require('chalk');

const { Command } = require('commander');
const program = new Command();

program
  .version('0.0.1')
  .description('A tool to interact with Zenny from your desktop')
  .option('-p, --phone <number>', 'Set your phone number', '1234567890')
  // Mesages From Zenny
  .option(
    '-iq, --incomingq <string>',
    'Set your phone number',
    'INCOMING_TWILIO',
  )
  // Messages To Zenny
  .option(
    '-oq, --outgoingq <string>',
    'Set your phone number',
    'OUTGOING_MESSAGE',
  )
  .option('-m, --message <string>', 'Send a message manually')
  .option('-t, --timeout <number>', 'How long to wait for replies', 1000);

const redisConfig = {
  host: ConfigService.get('REDIS_HOST'),
  port: Number(ConfigService.get('REDIS_PORT')),
};

//creating instance of Queue class
let bqMessagesToBot: BQ; // Messages received via WhatsApp and/or SMS
let bqRepliesFromZenny: BQ; // Send messages for dispatch

let messageLog = '';

/* instanbul ignore next*/
async function main() {
  program.parse(process.argv); // Check for command line options
  const phnumber = 'test:+' + program.phone; // Set our "phone" number

  const interactive = typeof program.message === 'undefined';

  if (interactive) {
    process.stdout.write('\x1Bc');
    serverline.init();
    console.log(chalk.green('This is Zenny! How can I help?'));
    serverline.setPrompt('> ');
    serverline.on('line', async function (line: string) {
      const job = bqMessagesToBot.createJob({
        To: 'test:1234567890',
        From: phnumber,
        source: 'test',
        human: false,
        Body: line,
        server: 'localhost',
      });
      await job.save();
      messageLog +=
        new Date().toISOString().substr(11, 8) + ' HUMAN >> ' + line + '\n';
    });
  }

  try {
    bqMessagesToBot = new BQ(program.incomingq, {
      isWorker: false,
      redis: redisConfig,
    });

    bqRepliesFromZenny = new BQ(program.outgoingq, {
      isWorker: true,
      redis: redisConfig,
    });
  } catch (err) /* istanbul ignore next */ {
    console.error(chalk.red('SEVERE: Could not connect to Bee-Queue'));
    process.exit(2);
  }

  bqMessagesToBot.on('ready', async () => {
    if (interactive) {
      console.log(
        chalk.grey(ConfigService.get('QUEUE_NAME_TWILIO') + ' Queue now ready'),
      );
    }

    if (!interactive) {
      const job = bqMessagesToBot.createJob({
        To: 'test:1234567890',
        From: phnumber,
        source: 'test',
        human: false,
        Body: program.message,
        server: 'localhost',
      });
      await job.save();

      setTimeout(function () {
        process.exit(0);
      }, program.timeout);
    }
  });

  bqRepliesFromZenny.on('ready', () => {
    if (interactive) {
      console.log(
        chalk.grey(
          ConfigService.get('QUEUE_NAME_OUTGOING') + ' Queue now ready',
        ),
      );
    }
  });

  bqRepliesFromZenny.on('error', (err: any) => {
    console.log(err, `A queue error happened: ${err.message}`);
  });

  bqRepliesFromZenny.on('retrying', (job: any, err: any) => {
    console.warn(
      `Job ${job.id} failed with error ${err.message} but is being retried!`,
    );
  });

  bqRepliesFromZenny.on('failed', (job: any, err: any) => {
    console.log(`Job ${job.id} failed with error ${err.message}`);
  });

  bqRepliesFromZenny.on('stalled', (jobId: any) => {
    console.log(`Job ${jobId} stalled and will be reprocessed`);
  });

  // bqRepliesFromZenny.on('succeeded', (job: any, result: any) => {
  //   console.logs(chalk.grey(`Job ${job.id} succeeded with result: ${result}`));
  // });

  bqRepliesFromZenny.process(async function (job: any, done: any) {
    console.log(chalk.green(job.data.message));
    messageLog += prePrender('Zenny') + formatLine(job.data.message) + '\n';
    return done(null, 'done');
  });

  serverline.on('SIGINT', function () {
    if (!interactive) {
      process.exit(0);
    }
    const now: string = new Date().toISOString();
    const filename =
      'cli-chat-' +
      now.substr(0, 10) +
      '-' +
      now.substr(11, 2) +
      '-' +
      now.substr(14, 2) +
      '-' +
      now.substr(17, 2) +
      '.txt';
    fs.writeFileSync(path.join(__dirname, '../logs/' + filename), messageLog);
    process.exit(0);
  });
}

function prePrender(who: string) {
  return (
    new Date().toISOString().substr(11, 8) + ' ' + who.toUpperCase() + ' >> '
  );
}

function formatLine(line: string) {
  return line.split('\n').join('\n                  ');
}

main();
