import { Server, IncomingMessage, ServerResponse } from 'http';
import { Connection } from 'typeorm/index';

declare module 'fastify' {
  // @ts-ignore
  export interface FastifyInstance<
    HttpServer = Server,
    HttpRequest = IncomingMessage,
    HttpResponse = ServerResponse
  > {
    db: {
      connection: Connection;
    };
  }
}
