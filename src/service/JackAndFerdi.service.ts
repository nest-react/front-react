import { AxiosInstance } from 'axios';
import ConfigService from './Config.service';
import { HttpClient } from './HttpClient';
import Il8nService from './Il8n.service';
import { Cacheable } from '@type-cacheable/core';
import { CustomStrategy } from './CacheStrategy';
import * as qs from 'qs';
import { Connection, Repository } from 'typeorm';
import { CityTable } from '../model/CityTable';

const strategy = new CustomStrategy();

export class JackAndFerdiService extends HttpClient {
  private http: AxiosInstance;
  private db: Connection;
  private cityTable: Repository<CityTable>;
  constructor(db: Connection) {
    super();
    this.http = this.create({
      baseURL: ConfigService.get('JACKANDFERDI_API'),
      headers: {
        Authorization: ConfigService.get('JACKANDFERDI_TOKEN'),
      },
    });
    if (db) {
      this.db = db;
      this.cityTable = db.getRepository(CityTable);
    }
  }

  @Cacheable({
    cacheKey: qs.stringify,
    hashKey: 'getCities',
    strategy,
    // 1h
    ttlSeconds: 60 * 60 * 24,
  })
  public async getCities(): Promise<Array<any>> {
    const results = await this.http.get('/city');
    return results.data;
  }

  @Cacheable({
    cacheKey: qs.stringify,
    hashKey: 'cityPlacesCall',
    strategy,
    ttlSeconds: 60 * 60 * 24,
  })
  public async cityPlacesCall(params: any) {
    return (await this.http.get('place/search/', { params: params })).data;
  }

  @Cacheable({
    cacheKey: qs.stringify,
    hashKey: 'cityDetailsCall',
    strategy,
    ttlSeconds: 60 * 60 * 24,
  })
  public async cityDetailsCall(id: number) {
    return (await this.http.get(`/city/${id}/detail`)).data;
  }

  public cleanArray(arr: any): any[] {
    if (!Array.isArray(arr)) {
      return Object.keys(arr).map((key) => arr[key]);
    }
    return arr;
  }

  public async getCitiesNames(): Promise<Array<string>> {
    let cities: Array<any> = await this.getCities();
    cities = this.cleanArray(cities);
    return cities.map((a: any) => a.name);
  }

  public async getCityId(city: string): Promise<number> {
    let cities = await this.getCities();
    cities = this.cleanArray(cities);
    const results = cities.filter(
      (item) => item.name.toLowerCase() === city.toLowerCase(),
    );
    return results[0].id;
  }

  public async getCityName(city: string): Promise<string> {
    const cities: Array<string> = await this.getCitiesNames();
    const cityT =
      (await this.cityTable.findOne({
        where: {
          city: city,
        },
        order: {
          ranking: 'ASC',
          density: 'DESC',
        },
      })) ||
      (await this.cityTable.findOne({
        where: {
          city_ascii: city,
        },
        order: {
          ranking: 'ASC',
          density: 'DESC',
        },
      })) ||
      (await this.cityTable.findOne({
        where: { admin_name: city },
        order: { ranking: 'ASC', density: 'DESC' },
      }));
    try {
      return (
        cities.find(
          (a) =>
            a.toLowerCase().includes(city.toLowerCase()) ||
            city.toLowerCase().includes(a.toLowerCase()),
        ) ||
        cities.find(
          (a) =>
            a.toLowerCase().includes(cityT.city_ascii.toLowerCase()) ||
            cityT.city_ascii.toLowerCase().includes(a.toLowerCase()),
        )
      );
    } catch {
      return undefined;
    }
  }

  private placesFilter(
    cityName: string,
    tags: Array<string>,
    results: Array<any>,
  ): Array<string> {
    return results
      .filter((item: any) => {
        return (
          (item.city_ref.name.toLowerCase() === cityName.toLowerCase() ||
            cityName === '') &&
          tags.every((tag) => item.tags.includes(tag))
        );
      })
      .slice(0, 3)
      .map((itemData: any) => {
        const google_place_id = itemData.google_place_id;
        return Il8nService.get('zenny:jackandferdi:place', {
          templateVars: {
            title: itemData.title,
            sum: itemData.description || itemData.tagline,
            link: google_place_id
              ? `\nhttps://www.google.com/maps/place/?q=place_id:` +
                google_place_id
              : '',
          },
        });
      });
  }

  public async getCityData(cityName: string, type: string): Promise<string> {
    const id = await this.getCityId(await this.getCityName(cityName));
    let data = (await this.cityDetailsCall(id))[type];
    data =
      type === 'manners'
        ? (data = data.filter(
            (item: any) =>
              !['Men & Women', 'After Work'].includes(item.category),
          ))
        : data;
    if (data.length === 0) {
      /*istanbul ignore next*/ return Il8nService.get(`zenny:notfound:${type}`);
    }
    if (data.length >= 2) {
      let message = Il8nService.get(`zenny:jackandferdi:${type}count`, {
        templateVars: {
          city: cityName,
        },
      });
      const rand = Math.floor(Math.random() * data.length);
      let rand2 = Math.floor(Math.random() * data.length);
      while (rand2 === rand) {
        rand2 = Math.floor(Math.random() * data.length);
      }
      message += data[rand].description + '\n' + data[rand2].description;
      return message;
    }
    return data[Math.floor(Math.random() * data.length)].description;
  }

  public async getFunFact(cityName: string): Promise<string> {
    const id = await this.getCityId(await this.getCityName(cityName));
    const funfacts = (await this.cityDetailsCall(id)).funfacts;
    if (funfacts.length === 0) {
      return Il8nService.get('zenny:notfound:funfact');
    }
    return funfacts.filter(
      (item: any) => item.category.toLowerCase() !== 'health',
    )[Math.floor(Math.random() * funfacts.length)].description;
  }

  public async getPlacesInAirport(airport: string): Promise<string> {
    const params = {
      tags: airport,
    };
    const { results } = await this.cityPlacesCall(params);
    const res = this.placesFilter('', [airport], results);
    return res.length === 0
      ? Il8nService.get('zenny:jackandferdi:notfound', {
          templateVars: {
            city: airport,
          },
        })
      : res.length > 1
      ? Il8nService.get('zenny:jackandferdi:airportcount', {
          templateVars: {
            counter: res.length,
            airport: airport,
          },
        }) + res.map((line, index) => `|||${index + 1}. ${line}`).join('')
      : res[0];
  }

  public async getWorkPlaces(city: string): Promise<string> {
    const cityName = await this.getCityName(city);
    let data;
    if (cityName === undefined) {
      /*istanbul ignore next*/ return Il8nService.get('zenny:notfound:city');
    }
    const params = {
      search: cityName.toLowerCase(),
      type: 'WORK',
    };
    const { results } = await this.cityPlacesCall(params);
    const res = results.slice(0, 3).map((itemData: any) => {
      return Il8nService.get('zenny:jackandferdi:place', {
        templateVars: {
          title: itemData.title,
          sum: itemData.tagline
            ? itemData.tagline
            : itemData.description.split('.')[0] + '.',
        },
      });
    });
    if (res.length > 1) {
      data = Il8nService.get(`zenny:jackandferdi:workcount`, {
        templateVars: {
          counter: res.length,
          city: cityName,
        },
      });
    } else {
      return Il8nService.get('zenny:jackandferdi:notfound', {
        templateVars: { city: cityName },
      });
    }
    return `${data}${res.join('\n')}`;
  }

  public async getPlaces(
    cityName: string,
    tags: Array<string>,
  ): Promise<string> {
    let summary = '';
    let data = '';
    let res: Array<any> = [];
    const city = await this.getCityName(cityName);
    if (city === undefined) {
      return Il8nService.get('zenny:notfound:city');
    }
    await Promise.all(
      tags.map(async (tag: string) => {
        const params = {
          search: city.toLowerCase(),
          tags: tag,
        };
        const { results } = await this.cityPlacesCall(params);
        switch (tag.toLowerCase()) {
          case 'meditation': {
            res = this.placesFilter(
              city,
              ['Meditation', 'Outdoors', 'Stunning'],
              results,
            );
            break;
          }
          case 'drinks': {
            res = this.placesFilter(city, ['Drinks'], results);
            break;
          }
          case 'dancing': {
            res = this.placesFilter(city, ['Dancing'], results);
            break;
          }
          case 'food': {
            res = this.placesFilter(city, ['Food'], results);
            break;
          }
          case 'unwind': {
            res = this.placesFilter(city, ['Unwind'], results);
            break;
          }
        }
        if (res.length > 1) {
          data = Il8nService.get(
            `zenny:jackandferdi:${tag.toLowerCase()}count`,
            {
              templateVars: {
                counter: res.length,
                city: city,
              },
            },
          );
        } else {
          data = '';
        }
        if (summary !== '') {
          summary += '\n\n';
        }
        summary += `${data}${
          res.length > 1
            ? res.map((line, index) => `|||${index + 1}. ${line}`).join('')
            : res
        }`;
        if (res.length === 0) {
          summary += Il8nService.get('zenny:jackandferdi:notfound', {
            templateVars: { city: city },
          });
        }
      }),
    );
    return summary;
  }
}
