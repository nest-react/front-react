import axios, { AxiosInstance, AxiosRequestConfig, AxiosResponse } from 'axios';
import { hostname } from 'os';

interface CustomAxiosConfig extends AxiosRequestConfig {
  metadata: {
    start: [number, number];
  };
}

interface CustomAxiosResponse extends AxiosResponse<any> {
  config: CustomAxiosConfig;
}

export interface KeyValuePair<K, V> extends Array<any> {
  0: K;
  1: V;
}

type Callback = (payload?: any) => void;

export type Event = 'onResponse' | 'onRequest';

export interface onResponseMeta {
  hostname: string;
  outgoing: boolean;
  response: Record<string, any>;
  request?: Record<string, any>;
  serviceClass: string;
  duration: number;
  method: string;
  url: string;
}

export class HttpClient {
  private subscriptions: KeyValuePair<string, Callback>[] = [];

  public on(event: Event, callback: Callback): void {
    this.subscriptions.push([event, callback]);
  }

  public off(event: Event, callback: Callback): void {
    this.subscriptions = this.subscriptions.filter(
      ([subscriptionEvent, subscriptionCallback]) => {
        return event === subscriptionEvent && callback === subscriptionCallback;
      },
    );
  }

  private notify(event: Event, payload?: any): void {
    this.subscriptions.forEach(([e, callback]) => {
      if (e === event) {
        callback(payload);
      }
    });
  }

  public create(config?: AxiosRequestConfig): AxiosInstance {
    const instance = axios.create(config);
    instance.interceptors.request.use(this.interceptRequest, (error) =>
      Promise.reject(error),
    );

    instance.interceptors.response.use(this.interceptResponse, (error) =>
      Promise.reject(error),
    );

    return instance;
  }

  private interceptResponse = (
    response: CustomAxiosResponse,
  ): CustomAxiosResponse => {
    const [, end] = process.hrtime(response.config?.metadata?.start);
    const duration = end / 1000000; // convert nanoseconds to ms
    const record: onResponseMeta = {
      hostname: hostname(),
      outgoing: true,
      response: response.data,
      serviceClass: this.constructor.name,
      duration,
      method: response.config.method,
      url: response.request?.path,
    };
    this.notify('onResponse', record);
    return response;
  };

  private interceptRequest = (config: CustomAxiosConfig): CustomAxiosConfig => {
    config.metadata = { start: process.hrtime() };
    return config;
  };
}
