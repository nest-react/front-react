//Libraries
import Twilio from 'twilio';
import { MessagingInstance } from 'twilio/lib/rest/pricing/v1/messaging';
import axios from 'axios';
import * as qs from 'qs';
import { createLogger } from '../util/logger';
import ConfigService from './Config.service';
import { PhoneNumberInstance } from 'twilio/lib/rest/lookups/v1/phoneNumber';

export interface SendArgInterface {
  from: string;
  to: string;
  body: string;
}

export class TwilioService {
  private twilio: any;
  private logger = createLogger('zenner', 'twilio', 'zenner-comms');

  constructor() {
    this.twilio = Twilio(
      ConfigService.get('TWILIO_SID'),
      ConfigService.get('TWILIO_TOKEN'),
    );
  }
  public split(message: string): Array<string> {
    const splittedMessages = [];
    const limit = 1500;
    let lines = 0,
      slicer = 0;
    let curr = 0,
      top = limit;
    do {
      let currMessage = message.slice(curr, top);
      slicer = limit;
      if (curr + limit <= message.length) {
        lines = currMessage.split('\n').length;
        slicer = limit - currMessage.split('\n')[lines - 1].length;
        currMessage = currMessage.slice(0, slicer);
      }
      curr += slicer;
      top += slicer;
      splittedMessages.push(currMessage);
    } while (curr <= message.length);
    return splittedMessages;
  }

  public splitOnSubject(message: string): Array<string> {
    const splittedMessages: string[] = [];
    const splitMessageOnSubjects = message.split('|||').filter(Boolean);
    for (let i = 0; i < splitMessageOnSubjects.length; i++) {
      splittedMessages.push(...this.split(splitMessageOnSubjects[i]));
    }
    return splittedMessages;
  }

  async send(obj: SendArgInterface): Promise<MessagingInstance> {
    const messages = this.splitOnSubject(obj.body);
    let result: any;
    for (let i = 0; i < messages.length; i++) {
      const message = {
        from: obj.from,
        to: obj.to,
        body: messages[i],
      };

      try {
        result = await this.twilio.messages.create(message);
        this.logger.info(
          `Sent message via Twilio from ${message.from} to ${message.to}`,
          {
            sms: message,
            result: result,
          },
        );
      } catch (err) {
        this.logger.error(
          `Failed to send message via Twilio from ${message.from} to ${message.to}`,
          {
            sms: message,
            error: err,
          },
        );
      }

      if (
        ConfigService.get('FRONT_TWILIO') !== 'undefined' &&
        ConfigService.get('FRONT_TWILIO') !== ''
      ) {
        try {
          await axios({
            method: 'post',
            url: ConfigService.get('FRONT_TWILIO'),
            data: qs.stringify({
              From: result.from,
              To: result.to,
              Body: result.body,
              MessageSid: result.sid,
            }),
            headers: {
              'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
            },
          });
        } catch (err) {
          this.logger.error(`Failed to post message to Front`, { error: err });
        }
      }
    }

    return result;
  }

  async validatePhoneNumberWithTwilio(mobile: string): Promise<string> {
    mobile = mobile.replace(/\D/g, '');
    return this.twilio.lookups
      .phoneNumbers(mobile)
      .fetch()
      .then((phone: PhoneNumberInstance) => {
        return phone.phoneNumber.replace(/\D/g, '');
      })
      .catch((err: any) => {
        console.log('err', err.status);
      });
  }
}
