import {
  Client,
  TravelMode,
  UnitSystem,
} from '@googlemaps/google-maps-services-js/dist';
import { DirectionsResponseData } from '@googlemaps/google-maps-services-js/dist/directions';
import { DistanceMatrixRowElement } from '../interface/DistanceMatrixRowElement.interface';

export class DirectionService {
  constructor(private readonly client: Client, private readonly key: string) {}

  getDirection(
    origin: string,
    destination: string,
    mode: TravelMode = TravelMode.driving,
    departure_time?: number,
  ): Promise<DirectionsResponseData> {
    return this.client
      .directions({
        params: {
          key: this.key,
          origin: origin,
          destination: destination,
          mode: mode,
          alternatives: true,
          departure_time: departure_time,
        },
      })
      .then((response) => response.data);
  }

  getFastestWay(
    airport: string,
    city: string,
    mode: TravelMode = TravelMode.driving,
    departure_time?: number,
  ): Promise<DistanceMatrixRowElement> {
    return this.client
      .distancematrix({
        params: {
          key: this.key,
          units: UnitSystem.imperial,
          origins: [airport],
          destinations: [city],
          mode: mode,
          departure_time: departure_time,
        },
      })
      .then((response) => {
        return {
          mode,
          ...response.data.rows[0].elements[0],
        };
      });
  }

  getFastestWayOfModes(
    airport: string,
    city: string,
    modes: TravelMode[],
  ): Promise<DistanceMatrixRowElement[]> {
    const promises: Promise<DistanceMatrixRowElement>[] = modes.map((mode) => {
      return this.getFastestWay(airport, city, mode);
    });
    return Promise.all(promises);
  }
}
