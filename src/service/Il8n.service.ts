import yaml from 'js-yaml';
import * as hbs from 'handlebars';
import * as fs from 'fs';
import * as path from 'path';

interface il8nGetOpts {
  language?: string;
  templateVars?: any;
}
class Il8nService {
  private readonly il8nDictionary: any;

  constructor() {
    const il8n: any = {};
    const directoryPath = path.join(__dirname, '../resources/strings');
    const files = fs.readdirSync(directoryPath);

    for (let i = 0; i < files.length; i++) {
      // Keep the variables, it makes it easier to read
      const fileName = files[i];
      const filePath = path.join(directoryPath, fileName);
      const lang = fileName.split('.')[0];

      // Load the file
      try {
        il8n[lang] = yaml.safeLoad(fs.readFileSync(filePath, 'utf8'));
      } catch (err) /* istanbul ignore next */ {
        throw new Error(
          `Could not load translations for ${lang} because il8n ${err.message}`,
        );
      }
    }

    this.il8nDictionary = il8n;
  }

  private resolve(
    translationLocation: string,
    obj: any,
    separator = ':',
  ): string {
    const properties = Array.isArray(translationLocation)
      ? translationLocation
      : translationLocation.split(separator);
    return properties.reduce((prev: any, curr: any) => prev && prev[curr], obj);
  }

  public get(path: string, opts?: il8nGetOpts) {
    const language =
      typeof opts !== 'undefined' && typeof opts.language !== 'undefined'
        ? opts.language
        : 'en';
    const str = this.lookupString(path, language);

    if (
      typeof opts !== 'undefined' &&
      typeof opts.templateVars !== 'undefined' &&
      str.indexOf('{{') >= 0
    ) {
      let compiledString = hbs.compile(str, { noEscape: true })(
        opts.templateVars,
      );

      // Facebook compares strings on a byte level, so we need to make sure we are uniform
      // in not presenting the non-breaking space &nbsp; instead of a regular space character ' '
      compiledString = compiledString.replace(/\u00A0/g, '\u0020');
      return compiledString;
    } else {
      return str;
    }
  }

  private lookupString(path: string, language: string) {
    if (
      typeof this.resolve(path, this.il8nDictionary[language]) !== 'undefined'
    ) {
      return this.resolve(path, this.il8nDictionary[language]);
    } else if (
      typeof this.resolve(path, this.il8nDictionary['en']) !== 'undefined'
    ) {
      return this.resolve(path, this.il8nDictionary['en']);
    } else {
      return null;
    }
  }
}

export default new Il8nService();
