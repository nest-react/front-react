import { Connection, Repository } from 'typeorm';
import axios from 'axios';
import { FlightstatsRecord } from '../model/FlightstatsRecord';
import { LumoRecord } from '../model/LumoRecord';
import { AlertPayload, AlertResponse } from '../interface/flightStats/alert';
import { FlightResponse } from '../interface/lumo';
import Flight from '../model/Flight';
import { FlightstatsSubscription } from '../model/FlightstatsSubscription';
import { LumoSubscription } from '../model/LumoSubscription';
import { createLogger } from '../util/logger';
import {
  getAdditionalAlertPayload,
  getAlertPayload,
  getLumoPayload,
  tConvert,
} from '../util/flight.utils';
import { ConfigService } from './index';
import { MONTH_NAMES } from '../interface/bot-server.interface';
import { gateChanged } from '../util/Subscription.util';
import { lumoUpdateToSlack } from '../util/slackNotifications';

export default class SubscriptionService {
  private alertRecords: Repository<FlightstatsRecord>;

  private lumoRecords: Repository<LumoRecord>;

  private flights: Repository<Flight>;

  private readonly logger: any;

  constructor(db: Connection) {
    this.alertRecords = db.getRepository(FlightstatsRecord);
    this.flights = db.getRepository(Flight);
    this.lumoRecords = db.getRepository(LumoRecord);
    this.logger = createLogger(
      'zenner',
      'subscription.service',
      'zenner-common',
    );
  }

  async processAlertSubscription(
    payload: AlertResponse,
  ): Promise<Flight | null> {
    // Eventually we may want to move to remove this
    // but for debugging - let's post to Slack
    try {
      const departureTime = tConvert(
        payload.alert.flightStatus.departureDate.dateLocal.substr(11, 5),
      );
      const departureDate = payload.alert.flightStatus.departureDate.dateLocal;
      const departureMonth = new Date(departureDate).getMonth() + 1;
      const departureDay = new Date(departureDate).getDay() + 1;
      const timestamp = new Date().toISOString().split('T')[1];
      await axios.post(ConfigService.get('SLACK_FLIGHTEVENTS_SHU'), {
        text: `Timestamp: ${timestamp} \nFlightStats Event: ${payload.alert.event.type.toLowerCase()} \nFlight: ${
          payload.alert.rule.carrierFsCode
        } ${payload.alert.rule.flightNumber} (${
          payload.alert.rule.departureAirportFsCode
        } > ${payload.alert.rule.arrivalAirportFsCode}) departing ${
          MONTH_NAMES[departureMonth]
        } ${departureDay} @ ${departureTime}
        \nChange:${
          '\nDelay: ' +
            payload.alert.flightStatus.delays.departureGateDelayMinutes ??
          '\nBaggage: ' + payload.alert.flightStatus.airportResources.baggage ??
          '\nDeparture gate:' +
            payload.alert.flightStatus.airportResources.departureGate ??
          '\nDeparture terminal: ' +
            payload.alert.flightStatus.airportResources.departureTerminal ??
          '\nArrival Terminal: ' +
            payload.alert.flightStatus.airportResources.arrivalTerminal ??
          'None'
        }`,
      });
    } catch (e: any) {
      this.logger.error('An error occurred posting a flight to slack', {
        error: e,
        payload,
      });
    }

    try {
      const alert = await this.alertRecords.save(
        new FlightstatsRecord(payload.alert),
      );
      const alertWithRelations = await this.getAlertWithFlight(alert.id);
      if (alertWithRelations.subscription) {
        await this.updateFlightStatsFields(alertWithRelations, payload.alert);
      }
      return alertWithRelations?.subscription?.flight ?? null;
    } catch (e) {
      this.logger.error(
        'An error occurred processing an update from FlightStats',
        { err: e },
      );
      return null;
    }
  }

  private async updateFlightStatsFields(
    alert: FlightstatsRecord,
    payload: AlertPayload,
  ): Promise<boolean | null> {
    try {
      const flight = await this.flights.findOne(alert.subscription.flight.id);
      const FieldPayload: Partial<Flight> = getAlertPayload(payload);

      //TODO: Remove this logging line
      this.logger.info(
        `Temporary marker: Before FlightStats gateChanged(${FieldPayload.originGate}, ${flight.originGate});`,
      );

      if (gateChanged(flight.originGate, FieldPayload.originGate)) {
        //TODO: Remove this logging line
        this.logger.info(
          `gateChanged === true - Set gate to ${FieldPayload.originGate}`,
        );
      } else {
        //TODO: Remove this logging line
        FieldPayload.originGate = flight.originGate;
        this.logger.info(
          `gateChanged === false - Keep gate as ${FieldPayload.originGate}`,
        );
      }

      const additionalFieldPayload: Partial<Flight> = getAdditionalAlertPayload(
        alert.flightStatus,
      );
      await this.flights.update(
        {
          id: flight.id,
        },
        {
          ...FieldPayload,
        },
      );
      await this.flights.update(
        {
          id: flight.id,
        },
        {
          ...additionalFieldPayload,
        },
      );
      return true;
    } catch (e) {
      return null;
    }
  }

  private async getAlertWithFlight(alertId: number) {
    return this.alertRecords
      .createQueryBuilder('alert')
      .leftJoinAndMapOne(
        'alert.subscription',
        FlightstatsSubscription,
        'sub',
        'sub.subscription_id=alert.subscription_id',
      )
      .leftJoinAndMapOne(
        'sub.flight',
        Flight,
        'flight',
        'sub.flight_id=flight.id',
      )
      .where('alert.id = :id', { id: alertId })
      .getOne();
  }

  private async getLumoRecordWithFlight(lumoId: string) {
    return this.lumoRecords
      .createQueryBuilder('lumo')
      .leftJoinAndMapOne(
        'lumo.subscription',
        LumoSubscription,
        'sub',
        'sub.subscription_id=lumo.itinerary_id',
      )
      .leftJoinAndMapOne(
        'sub.flight',
        Flight,
        'flight',
        'sub.flight_id=flight.id',
      )
      .where('lumo.itinerary_id = :id', { id: lumoId })
      .orderBy('lumo.id', 'DESC')
      .limit(1)
      .getOne();
  }

  async processLumoSubscription(
    payload: FlightResponse,
  ): Promise<Flight | null> {
    await lumoUpdateToSlack(payload);
    // Eventually we may want to move to remove this
    // but for debugging - let's post to Slack

    try {
      const payloadWithDate = Object.assign(payload, {
        flightNumber: payload.flight.carrier.iata + payload.flight.number,
        departureDate: payload.flight.scheduled_departure,
      });
      await this.lumoRecords.save(new LumoRecord(payloadWithDate));

      this.logger.info(
        `Got Lumo Update for ${payload.flight.id} which is ${payload.status.text}`,
      );

      const lumo = await this.getLumoRecordWithFlight(
        payload.alert.itinerary_id,
      );

      this.logger.info(
        `Attempting to update flight ${lumo.subscription.flightId} from update for ${payload.flight.id}`,
      );

      if (lumo && (lumo.flight || lumo.subscription)) {
        // It's important to note that lumo.flight is a Lumo flight object
        // so we need to get the actual flight from the database, and a reference
        // is stored in the subscription object

        //TODO: Remove this logging line
        this.logger.info(
          `Temporary marker: Before this.flights.findOne(${lumo.subscription.flightId});`,
        );
        const flight = await this.flights.findOne(lumo.subscription.flightId);

        //TODO: Remove this logging line
        this.logger.info(`Temporary marker: Before getLumoPayload;`, {
          lumoPayload: payload,
        });
        const fieldsToUpdate = getLumoPayload(payload);

        //TODO: Remove this logging line
        this.logger.info(
          `Temporary marker: Before Lumo gateChanged(${fieldsToUpdate?.originGate}, ${flight?.originGate});`,
        );

        if (gateChanged(flight?.originGate, fieldsToUpdate?.originGate)) {
          //TODO: Remove this logging line
          this.logger.info(
            `gateChanged === true - Set gate to ${fieldsToUpdate?.originGate}`,
          );
        } else {
          //TODO: Remove this logging line
          fieldsToUpdate.originGate = flight?.originGate;
          this.logger.info(
            `gateChanged === false - Keep gate as ${fieldsToUpdate?.originGate}`,
          );
        }

        this.logger.info(`Updating flight record from lumo`, {
          lumoFields: fieldsToUpdate,
        });
        await this.flights.update(
          {
            lumoId: String(lumo.flight.id),
          },
          {
            ...fieldsToUpdate,
          },
        );
        return lumo.subscription?.flight ?? null;
      }
      return null;
    } catch (e) {
      this.logger.error('An error occurred processing an update from Lumo', {
        err: e,
      });
      return null;
    }
  }
}
