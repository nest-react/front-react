//Libraries
import * as dialogflow from '@google-cloud/dialogflow';
import { google } from '@google-cloud/dialogflow/build/protos/protos';
import IQueryResult = google.cloud.dialogflow.v2.IQueryResult;
import { SessionsClient } from '@google-cloud/dialogflow/build/src/v2';
import { ContextsClient } from '@google-cloud/dialogflow/build/src/v2';
import * as dotenv from 'dotenv';
import ConfigService from './Config.service';
import { createLogger } from '../util/logger';
import axios from 'axios';

const logger = createLogger('zenner', 'Diaologflow.service', 'zenner-common');

dotenv.config();
//Config
const dfConfig = {
  projectId: ConfigService.get('DF_PROJECT_ID'),
  credentials: {
    private_key_id: ConfigService.get('DF_PRIVATE_KEY_ID'),
    private_key: ConfigService.get('DF_PRIVATE_KEY'),
    client_email: ConfigService.get('DF_CLIENT_EMAIL'),
    client_id: ConfigService.get('DF_CLIENT_ID'),
  },
};

const dfSessionClient: SessionsClient = new dialogflow.v2.SessionsClient(
  dfConfig,
);

const dfContextClient: ContextsClient = new dialogflow.v2.ContextsClient(
  dfConfig,
);

export class DialogflowService {
  async getResponse(
    from: string,
    to: string,
    message: string,
    src: string,
    identifier: string,
  ): Promise<IQueryResult> {
    const sessionPath = this.getSessionPath(identifier);
    try {
      const dfResponses = await dfSessionClient.detectIntent({
        session: sessionPath,
        queryInput: {
          text: {
            text: message,
            languageCode: 'en-US',
          },
        },
      });
      logger.info(
        `Intent: ${
          dfResponses[0].queryResult.intent.displayName
        }\nOriginal Message: ${message}\nParameters: ${JSON.stringify(
          dfResponses[0].queryResult.parameters.fields,
        )}`,
      );
      try {
        await axios.post(ConfigService.get('SLACK_DIALOGFLOW_SHU'), {
          text: `Intent: ${
            dfResponses[0].queryResult.intent.displayName
          }\nOriginal Message: ${message}\nParameters: ${JSON.stringify(
            dfResponses[0].queryResult.parameters.fields,
            null,
            2,
          )}`,
        });
      } catch (e: any) {
        logger.error('An error occurred posting a detect intent to slack', {
          error: e,
        });
      }

      return dfResponses[0].queryResult;
    } catch (e) {
      logger.error(e);
    }
  }
  get contextClient(): ContextsClient {
    return dfContextClient;
  }

  get sessionClient(): SessionsClient {
    return dfSessionClient;
  }

  public getSessionPath(identifier: string): string {
    return this.sessionClient.projectAgentSessionPath(
      dfConfig.projectId,
      identifier,
    );
  }

  public getContextPath(identifier: string, context: string): string {
    return this.contextClient.projectAgentSessionContextPath(
      dfConfig.projectId,
      identifier,
      context,
    );
  }
}
