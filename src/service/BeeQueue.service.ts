import BeeQueue from 'bee-queue';
import { hostname } from 'os';

export class BeeQueueService {
  async createResponseJob(
    bq: BeeQueue,
    from: string,
    to: string,
    source: string,
    message: string,
  ): Promise<void> {
    const outboundMessage = {
      human: false,
      from: from,
      to: to,
      source: source,
      server: hostname(),
      message: message,
      outbound: true,
    };
    await bq.createJob(outboundMessage).save();
  }
}
