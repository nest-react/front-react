import { config } from 'dotenv';
import { resolve } from 'path';
import { Config } from '../interface/config.interface';
import { createLogger } from '../util/logger';
const logger = createLogger('zenner', 'config.service', 'zenner-common');
interface EnvKeyValue {
  [key: string]: any;
}

class ConfigService implements Config {
  private readonly env: EnvKeyValue | undefined;

  constructor() {
    this.env = config({
      path: resolve(__dirname, '../../.env'),
    }).parsed;
  }
  public get(key: string, defaultValue?: any): string {
    /* istanbul ignore next */
    if (this.env?.[key]) {
      return this.env[key];
    }

    if (process.env[key]) {
      return process.env[key];
    }

    logger.error(`Could not find config value for ${key}`);

    /* istanbul ignore next */
    if (defaultValue) {
      return defaultValue;
    }

    return undefined;
  }
}

export default new ConfigService();
