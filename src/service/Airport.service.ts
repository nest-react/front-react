import airportData from '../resources/airports.json';
import Fuse from 'fuse.js';

export interface AirportMeta {
  id: number;
  type:
    | 'large_airport'
    | 'medium_airport'
    | 'small_airport'
    | 'metropolitan_airport';
  name: string;
  iso_country: string;
  municipality: string;
  iata_code: string;
  rank: number;
}

const options: Fuse.IFuseOptions<AirportMeta> = {
  includeScore: true,
  threshold: 0.1,
  location: 0,
  distance: 100,
  minMatchCharLength: 2,
  keys: [
    { name: 'iata_code', weight: 0.8 },
    { name: 'name', weight: 0.3 },
    { name: 'municipality', weight: 0.8 },
  ],
};

class AirportService {
  private list: Fuse<AirportMeta>;

  constructor() {
    this.list = new Fuse<AirportMeta>(airportData as AirportMeta[], options);
  }

  public findByCityName(cityName: string, limit = 5): AirportMeta[] {
    const airport = metropolitanAirports.find(
      (s) => s.municipality === cityName,
    );

    if (airport) {
      return [airport];
    }

    return this.list
      .search<AirportMeta>(cityName, { limit })
      .map(parseResult)
      .sort(sortByAirportSize)
      .sort(sortByRank);
  }

  public search(pattern: string, limit = 5): AirportMeta[] {
    return this.list
      .search<AirportMeta>(pattern, { limit })
      .map(parseResult)
      .sort(sortByAirportSize)
      .sort(sortByRank);
  }
}

export default new AirportService();

export type AirportType = Pick<AirportMeta, 'type'>;

export const sortByRank = (a: AirportMeta, b: AirportMeta): number =>
  a.rank - b.rank;

export const parseResult = (result: Fuse.FuseResult<any>): any => result.item;

export const sortByAirportSize = (a: AirportType, b: AirportType): number => {
  if (a.type === b.type) {
    return 0;
  }

  if (
    a.type === 'large_airport' &&
    (b.type === 'medium_airport' || b.type === 'small_airport')
  ) {
    return -1;
  }

  if (
    (a.type === 'medium_airport' || a.type === 'small_airport') &&
    b.type === 'large_airport'
  ) {
    return 1;
  }
};

const metropolitanAirports: AirportMeta[] = [
  {
    id: 1,
    type: 'metropolitan_airport',
    name: 'New York City Airports',
    iso_country: 'US',
    municipality: 'New York',
    iata_code: 'NYC',
    rank: 10,
  },
];
