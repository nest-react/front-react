import axios, { AxiosInstance } from 'axios';
import { ConfigService } from './';
import { createLogger } from '../util/logger';

const logger = createLogger('zenner', 'sitata.service', 'zenner-common');
// interfaces
import {
  RestrictionResponse,
  TravelStatusResponse,
  CavidSummaryResponse,
  TravelStatus,
  RestrictionsList,
  travelStatusMap,
  restrictionsTypeMap,
} from '../interface/sitata.interface';

// utils
import { parseData } from '../util';
import { CustomStrategy } from './CacheStrategy';
import { Cacheable } from '@type-cacheable/core';
import * as qs from 'qs';

const strategy = new CustomStrategy();

export class SitataService {
  private http: AxiosInstance;

  private handleError(err: any): null {
    logger.error('sitata error', { error: err });
    return null;
  }

  constructor() {
    this.http = axios.create({
      baseURL: ConfigService.get('SITATA_URL'),
      headers: {
        Authorization: ConfigService.get('SITATA_AUTHORIZATION'),
        Organization: ConfigService.get('SITATA_ORGANIZATION'),
      },
    });
  }

  @Cacheable({
    cacheKey: qs.stringify,
    hashKey: 'getRestrictions',
    strategy,
    // 1h
    ttlSeconds: 60 * 60,
  })
  getRestrictions(countryCode: string): Promise<RestrictionsList> {
    return this.http
      .get<RestrictionResponse[]>(`${countryCode}/travel_restrictions`)
      .then<RestrictionResponse[]>(parseData)
      .then((restrictions) => {
        return restrictions.reduce<RestrictionsList>((acc, restriction) => {
          const type = restrictionsTypeMap[restriction.type];
          if (type) {
            if (acc[type] && Array.isArray(acc[type])) {
              acc[type].push(restriction);
            } else {
              acc[type] = [restriction];
            }
          }
          return acc;
        }, {});
      })
      .catch(this.handleError);
  }

  @Cacheable({
    cacheKey: qs.stringify,
    hashKey: 'getCovidSummary',
    strategy,
    // 1h
    ttlSeconds: 60 * 60,
  })
  getCovidSummary(countryCode: string): Promise<CavidSummaryResponse | null> {
    return this.http
      .get<CavidSummaryResponse>(`${countryCode}/covid19_summary`)
      .then(parseData)
      .catch(this.handleError);
  }
  // Status	  Value	Description
  // Normal	  0	    Travellers should use normal safety precautions.
  // Caution  1	    Travellers should use heightened safety precautions.
  // Avoid	  2	    Travellers should avoid travel to this area.
  @Cacheable({
    cacheKey: qs.stringify,
    hashKey: 'getTravelStatus',
    strategy,
    // 1h
    ttlSeconds: 60 * 60,
  })
  getTravelStatus(countryCode: string): Promise<TravelStatus | null> {
    return this.http
      .get<TravelStatusResponse>(`${countryCode}/travel_status`)
      .then<TravelStatusResponse>(parseData)
      .then(
        (response): TravelStatus => ({
          ...response,
          description: travelStatusMap[response.travel_status],
        }),
      )
      .catch(this.handleError);
  }
  getAvailableCodes(travelStatus: RestrictionsList): string[] {
    const IS_UNDEFINED = 'undefined';
    const reply = [];
    if (
      typeof travelStatus.airline !== IS_UNDEFINED &&
      typeof travelStatus.airline[0] !== typeof IS_UNDEFINED
    ) {
      reply.push('0');
    }
    if (
      typeof travelStatus.border !== IS_UNDEFINED &&
      typeof travelStatus.border[0] !== typeof IS_UNDEFINED
    ) {
      reply.push('1');
    }
    if (
      typeof travelStatus.curfew !== IS_UNDEFINED &&
      typeof travelStatus.curfew[0] !== typeof IS_UNDEFINED
    ) {
      reply.push('2');
    }
    if (
      typeof travelStatus.social !== IS_UNDEFINED &&
      typeof travelStatus.social[0] !== typeof IS_UNDEFINED
    ) {
      reply.push('3');
    }
    if (
      typeof travelStatus.transportation !== IS_UNDEFINED &&
      typeof travelStatus.transportation[0] !== typeof IS_UNDEFINED
    ) {
      reply.push('4');
    }
    if (
      typeof travelStatus.borderApp !== IS_UNDEFINED &&
      typeof travelStatus.borderApp[0] !== typeof IS_UNDEFINED
    ) {
      reply.push('201');
    }
    if (
      typeof travelStatus.nonEssentialShops !== IS_UNDEFINED &&
      typeof travelStatus.nonEssentialShops[0] !== typeof IS_UNDEFINED
    ) {
      reply.push('401');
    }
    if (
      typeof travelStatus.accommodation !== IS_UNDEFINED &&
      typeof travelStatus.accommodation[0] !== typeof IS_UNDEFINED
    ) {
      reply.push('402');
    }
    if (
      typeof travelStatus.restaurants !== IS_UNDEFINED &&
      typeof travelStatus.restaurants[0] !== typeof IS_UNDEFINED
    ) {
      reply.push('403');
    }
    if (
      typeof travelStatus.barsAndCafes !== IS_UNDEFINED &&
      typeof travelStatus.barsAndCafes[0] !== typeof IS_UNDEFINED
    ) {
      reply.push('404');
    }
    if (
      typeof travelStatus.beaches !== IS_UNDEFINED &&
      typeof travelStatus.beaches[0] !== typeof IS_UNDEFINED
    ) {
      reply.push('405');
    }
    if (
      typeof travelStatus.museums !== IS_UNDEFINED &&
      typeof travelStatus.museums[0] !== typeof IS_UNDEFINED
    ) {
      reply.push('406');
    }
    if (
      typeof travelStatus.personalCare !== IS_UNDEFINED &&
      typeof travelStatus.personalCare[0] !== typeof IS_UNDEFINED
    ) {
      reply.push('407');
    }
    if (
      typeof travelStatus.placesOfWorship !== IS_UNDEFINED &&
      typeof travelStatus.placesOfWorship[0] !== typeof IS_UNDEFINED
    ) {
      reply.push('408');
    }
    if (
      typeof travelStatus.events !== IS_UNDEFINED &&
      typeof travelStatus.events[0] !== typeof IS_UNDEFINED
    ) {
      reply.push('409');
    }
    if (
      typeof travelStatus.masks !== IS_UNDEFINED &&
      typeof travelStatus.masks[0] !== typeof IS_UNDEFINED
    ) {
      reply.push('410');
    }
    return reply;
  }
}
