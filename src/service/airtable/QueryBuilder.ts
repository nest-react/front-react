interface ICompareStatement {
  name: string;
  value: any;
  type: StatementType;
  operator: StatementOperator;
}

interface ICondition {
  statements: Array<ICompareStatement | ICondition>;
  operator: LogicOperator;
}

export enum StatementType {
  scalar = 'scalar',
  date = 'date',
}

export enum StatementOperator {
  eq = 'eq',
}

export enum LogicOperator {
  AND = ', ',
}

export class QueryBuilder {
  protected static readonly DEFAULT_TEMPLATE: string = `%name = '%value'`;
  protected static readonly TEMPLATES: { [key: string]: any } = {
    date: {
      eq: `IS_SAME(%name, DATETIME_FORMAT('%value', 'YYYY-MM-DD'))`,
    },
  };

  public static renderStatement(statement: ICompareStatement): string {
    let template = QueryBuilder.DEFAULT_TEMPLATE;
    if (
      QueryBuilder.TEMPLATES.hasOwnProperty(statement.type) &&
      QueryBuilder.TEMPLATES[statement.type].hasOwnProperty(statement.operator)
    ) {
      template = QueryBuilder.TEMPLATES[statement.type][statement.operator];
    }
    return template
      .replace('%name', statement.name)
      .replace('%value', statement.value);
  }

  public static renderCondition(condition: ICondition): string {
    return condition.statements
      .map((statement) => {
        if (statement.hasOwnProperty('statements')) {
          return QueryBuilder.renderCondition(statement as ICondition);
        }
        return QueryBuilder.renderStatement(statement as ICompareStatement);
      })
      .join(condition.operator);
  }

  public static prepareStatements(params: {
    [key: string]: any;
  }): Array<ICompareStatement> {
    const statements: Array<ICompareStatement> = [];
    for (const item in params) {
      if (params.hasOwnProperty(item)) {
        statements.push({
          name: item,
          value: params[item],
          type:
            params[item] instanceof Date
              ? StatementType.date
              : StatementType.scalar,
          operator: StatementOperator.eq,
        });
      }
    }
    return statements.filter((item) => item.value);
  }
}
