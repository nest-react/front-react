import Airtable from 'airtable';
import { Config } from '../../interface/config.interface';
import {
  IAirlineMessage,
  IAirportMessage,
  IPassengerMessage,
  iPickupMessage,
} from '../../interface/airtable/message.interface';
import { QueryBuilder, LogicOperator } from './QueryBuilder';

export class AirtableService {
  private readonly base: Airtable.Base;

  constructor(
    private readonly airtable: Airtable,
    private readonly configService: Config,
  ) {
    this.base = this.airtable.base(this.configService.get('AIRTABLE_APP_ID'));
  }

  getMessageForAirport(
    params: Partial<IAirportMessage>,
  ): Promise<Airtable.Records<IAirportMessage>> {
    const statements = QueryBuilder.prepareStatements({
      ...params,
      active: 1,
    });
    return this.base(this.configService.get('AIRTABLE_AIRPORTS_TABLE'))
      .select({
        filterByFormula: `AND(${QueryBuilder.renderCondition({
          operator: LogicOperator.AND,
          statements,
        })})`,
      })
      .all()
      .catch(() => {
        return null;
      });
  }

  getMessageForPassenger(
    params: Partial<IPassengerMessage>,
  ): Promise<Airtable.Records<IPassengerMessage>> {
    const statements = QueryBuilder.prepareStatements({
      ...params,
      active: 1,
    });
    return this.base(this.configService.get('AIRTABLE_PASSENGERS_TABLE'))
      .select({
        filterByFormula: `AND(${QueryBuilder.renderCondition({
          operator: LogicOperator.AND,
          statements,
        })})`,
      })
      .all()
      .catch(() => {
        return null;
      });
  }

  getAirlinesWebsitesLink(
    params: Partial<IAirlineMessage>,
  ): Promise<Airtable.Records<IAirlineMessage>> {
    return this.base(this.configService.get('AIRTABLE_AIRLINES_TABLE'))
      .select({
        filterByFormula: `{AirLine} = "${params.AirLine}"`,
        maxRecords: 1,
      })
      .all()
      .catch(() => {
        return null;
      });
  }

  getValidPickups(
    params: Partial<iPickupMessage>,
  ): Promise<Airtable.Records<iPickupMessage>> {
    return this.base(this.configService.get('AIRTABLE_PICKUP_TABLE'))
      .select({
        filterByFormula: `{IATA Code} = "${params['IATA Code']}"`,
        maxRecords: 1,
      })
      .all()
      .catch(() => {
        return null;
      });
  }
}
