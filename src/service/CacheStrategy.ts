import { CacheStrategy, CacheStrategyContext } from '@type-cacheable/core';
import redis, { RedisClient } from 'redis';
import { useAdapter } from '@type-cacheable/redis-adapter';

export const redisClient: RedisClient = redis.createClient();

useAdapter(redisClient);

// default cache reference
// https://github.com/joshuaslate/type-cacheable/blob/master/packages/core/lib/strategies/DefaultStrategy.ts
export class CustomStrategy implements CacheStrategy {
  async handle(context: CacheStrategyContext): Promise<any> {
    try {
      const cachedValue = await context.client.get(context.key);
      // If a value for the cacheKey was found in cache.
      if (cachedValue) {
        if (typeof cachedValue === 'object') {
          return Object.fromEntries(
            Object.entries(cachedValue).map(([key, value]) => {
              if (typeof value === 'string') {
                try {
                  const parsedValue = JSON.parse(value);
                  return [key, parsedValue];
                } catch {
                  console.error(
                    'CustomStrategy - JSON parse fail, key - ' + context.key,
                  );
                }
              }

              return [key, value];
            }),
          );
        }
        return cachedValue;
      }
    } catch (err) {
      if (context.debug) {
        console.warn(
          `type-cacheable Cacheable cache miss due to client error: ${err.message}`,
        );
      }
    }

    // On a cache miss, run the decorated function and cache its return value.
    let result: any;
    if (context.originalMethod) {
      result = await context.originalMethod.apply(
        context.originalMethodScope,
        context.originalMethodArgs,
      );
    }
    try {
      await context.client.set(context.key, result, context.ttl);
    } catch (err) {
      if (context.debug) {
        console.warn(
          `type-cacheable Cacheable set cache failure due to client error: ${err.message}`,
        );
      }
    }

    return result;
  }
}
