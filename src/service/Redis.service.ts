import redis, { RedisClient } from 'redis';
import { useAdapter } from '@type-cacheable/redis-adapter';

const redisClient = redis.createClient();

export const client: RedisClient = redisClient;

useAdapter(redisClient);
