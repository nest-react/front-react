import User from '../model/User';
import { Repository, Connection, In } from 'typeorm';

import Flight from '../model/Flight';
import { UserTrip } from '../model/UserTrip';
import { Trip } from '../model/Trip';
import { FlightPayload } from '../interface/bot-server.interface';
import { UpdateResult } from 'typeorm/browser';
import { FlightResponse, ConnectionData } from '../interface/lumo';
import { InitialFlightStatsAlert } from '../interface/flightStats/alert';
import { createLogger } from '../util/logger';

const logger = createLogger('zenner', 'flight.service', 'zenner-common');

export class FlightService {
  public userRepository: Repository<User>;
  public flightRepository: Repository<Flight>;
  public userTripRepository: Repository<UserTrip>;
  public tripRepository: Repository<Trip>;

  /* istanbul ignore next */
  constructor(db: Connection) {
    if (db) {
      this.userRepository = db.getRepository(User);
      this.flightRepository = db.getRepository(Flight);
      this.userTripRepository = db.getRepository(UserTrip);
      this.tripRepository = db.getRepository(Trip);
    }
  }

  // user and passenger are the same
  async getPassengerIdForUser(userId: number): Promise<number> {
    return userId;
  }

  async getUpcomingFlightsByUserForFrontend(userId: number): Promise<Flight[]> {
    let result;
    try {
      const userTrips = await this.userTripRepository.find({
        where: {
          user_id: userId,
        },
      });
      const tripIds: number[] = userTrips.map((curr) => curr.trip_id);
      const trips = await this.tripRepository.find({
        relations: ['flight'],
        where: {
          trip_id: In(tripIds),
        },
      });
      const flights = trips.map((trip) => {
        trip.flight.tripId = trip.trip_id;
        if (trip.seq === 0) {
          // This variable is used on the front end
          trip.flight.isSeq = true;
          trip.flight.conn_time = trip.conn_time;
        }
        return trip.flight;
      });
      result = flights.filter(
        (flight) =>
          !['cancelled', 'landed', 'arrived', 'diverted'].includes(
            flight?.statusName ?? null,
          ) && !flight?.isOutOfDate,
      );
      result = result.sort((a, b) => {
        const c = new Date(a.scheduledDeparture);
        const d = new Date(b.scheduledDeparture);
        return c.getTime() - d.getTime();
      });
    } catch {
      return [];
    }

    /* istanbul ignore next */
    if (!result) {
      return [];
    }

    return result;
  }

  async getLandedOrCancelledFlightsByUser(
    userId: number,
    hasFlight: boolean,
  ): Promise<Flight[]> {
    let result;
    try {
      const userTrips = await this.userTripRepository.find({
        where: {
          user_id: userId,
        },
      });
      const tripIds: number[] = userTrips.map((curr) => curr.trip_id);
      const trips = await this.tripRepository.find({
        relations: ['flight'],
        where: {
          trip_id: In(tripIds),
        },
      });
      result = hasFlight
        ? trips
            .map((curr) => curr.flight)
            .filter(
              (flight) =>
                flight.statusName === 'landed' ||
                flight.statusName === 'arrived' ||
                flight.statusName === 'cancelled' ||
                flight.statusName === 'diverted',
            )
        : trips.map((curr) => curr.flight);
      result = result.sort((firstFlight, secondFlight) => {
        const firstDepartureTime = new Date(firstFlight.scheduledDeparture);
        const secondDepartureTime = new Date(secondFlight.scheduledDeparture);
        return secondDepartureTime.getTime() - firstDepartureTime.getTime();
      });
    } catch {
      return [];
    }
    /* istanbul ignore next */
    if (!result) {
      return [];
    }
    return result;
  }

  async getUserFlightsForFrontApp(userId: number): Promise<Flight[]> {
    const result = await this.getUpcomingFlightsByUserForFrontend(userId);
    const hasFlight = result.length > 0 ? true : false;
    const temp = await this.getLandedOrCancelledFlightsByUser(
      userId,
      hasFlight,
    );
    if (temp.length > 0) {
      result.unshift(
        (await this.getLandedOrCancelledFlightsByUser(userId, hasFlight))[0],
      );
    }

    return result;
  }

  async getAllFlightsByUser(userId: number): Promise<Flight[]> {
    let result;
    try {
      const userTrips = await this.userTripRepository.find({
        where: {
          user_id: userId,
        },
      });
      const tripIds: number[] = userTrips.map((curr) => curr.trip_id);
      const trips = await this.tripRepository.find({
        relations: ['flight'],
        where: {
          trip_id: In(tripIds),
        },
      });
      const flights = trips.map((trip) => {
        const flight = trip.flight;
        if (trip.seq === 0) {
          flight.seq = trip.seq;
        }
        return flight;
      });
      result = flights.filter(
        (flight) =>
          !['cancelled', 'landed', 'arrived', 'diverted'].includes(
            flight?.statusName ?? null,
          ) && !flight?.isOutOfDate,
      );
      result = result.sort((a, b) => {
        const c = new Date(a.scheduledDeparture);
        const d = new Date(b.scheduledDeparture);
        return c.getTime() - d.getTime();
      });
    } catch {
      return undefined;
    }

    /* istanbul ignore next */
    if (!result) {
      return undefined;
    }

    return result;
  }

  async getLandedFlightsByUser(userId: number): Promise<Flight[]> {
    let result;
    try {
      const userTrips = await this.userTripRepository.find({
        where: {
          user_id: userId,
        },
      });
      const tripIds: number[] = userTrips.map((curr) => curr.trip_id);
      const trips = await this.tripRepository.find({
        relations: ['flight'],
        where: {
          trip_id: In(tripIds),
        },
      });
      const flights = trips
        .map((curr) => curr.flight)
        .filter((flight) => flight.statusName === 'landed');
      result = flights.filter((x) => x.isOutOfDate);
      result = result.sort((firstFlight, secondFlight) => {
        const firstDepartureTime = new Date(firstFlight.scheduledDeparture);
        const secondDepartureTime = new Date(secondFlight.scheduledDeparture);
        return secondDepartureTime.getTime() - firstDepartureTime.getTime();
      });
    } catch {
      return undefined;
    }

    /* istanbul ignore next */
    if (!result) {
      return undefined;
    }

    return result;
  }

  async getFlightByData(
    flightNumber: string,
    origin: string,
    date: string,
  ): Promise<Flight> {
    const flights = await this.flightRepository.find({
      where: {
        flightNo: flightNumber,
        scheduledDepartureDateLocal: date,
      },
    });

    return flights.length
      ? flights.find(
          (flight) => flight.initialLumoData?.flight.origin.iata === origin,
        )
      : undefined;
  }
  async updateInitialLumoInfo(
    flightId: number,
    lumoId: string,
    lumoPayload: FlightResponse,
  ): Promise<UpdateResult> {
    return this.flightRepository.update(
      {
        id: flightId,
      },
      {
        lumoId,
        initialLumoData: lumoPayload,
      },
    );
  }

  async updateInitialFlightStatsInfo(
    flightId: number,
    flightStatsId: number,
    flightStatsPayload: InitialFlightStatsAlert,
  ): Promise<UpdateResult> {
    return this.flightRepository.update(
      {
        id: flightId,
      },
      {
        flightstatsId: String(flightStatsId),
        initialFlightStatsData: flightStatsPayload,
      },
    );
  }

  async getFlightByLumoId(lumoId: string): Promise<Flight> {
    return this.flightRepository.findOne({
      where: {
        lumoId,
      },
    });
  }

  async saveFlight(payload: FlightPayload): Promise<Flight> {
    const { lumoId } = payload;
    const flight = await this.getFlightByLumoId(lumoId);
    if (flight) return flight;
    return this.flightRepository.save(new Flight(payload));
  }

  async updateFlight(
    flightId: number,
    payload: Partial<Flight>,
  ): Promise<UpdateResult> {
    try {
      await this.flightRepository.findOneOrFail(flightId);
      return this.flightRepository.update(
        {
          id: flightId,
        },
        { ...payload },
      );
    } catch (e) {
      logger.info('Unable to find flight with id', flightId);
    }
  }
  async updateTrip(
    tripId: number,
    payload: Partial<Trip>,
  ): Promise<UpdateResult> {
    try {
      await this.tripRepository.findOneOrFail(tripId);
      return this.tripRepository.update(
        {
          trip_id: tripId,
        },
        { ...payload },
      );
    } catch (e) {
      logger.info('Unable to find Trip with id', tripId);
    }
  }

  /*  OLD DB STRUCTURE, KEEP UNTILL FULLY TESTED
  async createUserFlight(
    userId: number,
    flightId: number,
  ): Promise<UserFlight> {
    const payload = {
      user_id: userId,
      flightId,
    };
    return this.userFlightRepository.save(payload);
  }
*/
  async createUserTrip(userId: number): Promise<UserTrip> {
    const payload = {
      user_id: userId,
    };
    return this.userTripRepository.save(payload);
  }

  async getFlightById(id: number): Promise<Flight> {
    return await this.flightRepository.findOne({
      relations: ['trips'],
      where: { id: id },
    });
  }
  async createTrip(
    tripId: number,
    seq: number,
    flight_fk: number,
    conn_time: number,
    conn_data: ConnectionData,
  ): Promise<Trip> {
    const payload = {
      trip_id: tripId,
      seq: seq,
      flight: await this.getFlightById(flight_fk),
      conn_time: conn_time,
      conn_data: conn_data,
    };
    return this.tripRepository.save(payload);
  }

  async getNextFlightByUser(userId: number): Promise<Flight> {
    const results = await this.getAllFlightsByUser(userId);
    if (!results) {
      return undefined;
    }

    results.sort((a: Flight, b: Flight) => {
      const aTime = a.estimatedDeparture.getTime();
      const bTime = b.estimatedDeparture.getTime();
      return aTime - bTime;
    });

    /* istanbul ignore next */
    if (!results.length) {
      return undefined;
    } else {
      return results[0];
    }
  }

  async getNextArrivalFlight(userId: number): Promise<Flight> {
    const allFlights = await this.getAllFlightsByUser(userId);

    /* istanbul ignore next */
    if (!allFlights || !allFlights.length) {
      return undefined;
    }

    allFlights.sort((a: Flight, b: Flight) => {
      const aTime = new Date(a.estimatedArrival).getTime();
      const bTime = new Date(b.estimatedArrival).getTime();
      return aTime - bTime;
    });

    /* istanbul ignore next */
    return allFlights[0];
  }

  async getNextDepartureFlight(userId: number): Promise<Flight> {
    const nextflight = await this.getNextFlightByUser(userId);

    /* istanbul ignore next */
    if (!nextflight) {
      return undefined;
    }
    return nextflight;
  }

  async getUsersByFlight(flightId: number): Promise<User[]> {
    const flight = await this.getFlightById(flightId);
    const userTrips = await this.userTripRepository.find({
      where: {
        trip_id: In(flight.trips.map((trip) => trip.trip_id)),
      },
    });
    return this.userRepository.findByIds(
      userTrips.map((usertrip) => usertrip.user_id),
    );
  }

  async updateFlightTrip(trip: Partial<Trip>): Promise<Trip> {
    return this.tripRepository.save(trip);
  }
}
