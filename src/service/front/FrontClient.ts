import { Method } from 'axios';
import { HttpClient } from '../HttpClient';
import {
  FrontResponse,
  FrontError,
} from '../../interface/front/front.interface';
import ConfigService from '../Config.service';

// Helper middleware class for validating Socket.IO
export const validateSocketAuth = (
  socket: any,
  next: (err?: any) => any,
): void => {
  if (socket.handshake.query && socket.handshake.query.authorization) {
    if (
      socket.handshake.query.authorization !==
      ConfigService.get('FRONT_PLUGIN_SECRET')
    ) {
      next(new Error('You are not authorized to connect to the server.'));
    } else {
      next();
    }
  } else {
    next(new Error('You are not authorized to connect to the server.'));
  }
};

// The actual, featured, Front client
export class FrontClient extends HttpClient {
  public execute<T>(
    url: string,
    method: Method = 'GET',
    httpData?: T,
  ): Promise<FrontResponse<T>> {
    return this.create({
      baseURL: ConfigService.get('FRONT_API_URL'),
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${ConfigService.get('FRONT_API_TOKEN')}`,
      },
    })
      .request({
        url,
        method,
        data: httpData,
      })
      .then(({ data }) => {
        return {
          data: data,
          success: true,
          error: null,
        };
      })
      .catch(({ response }: FrontError) => {
        return {
          data: null,
          success: false,
          error: response.data._error,
        };
      });
  }
}
