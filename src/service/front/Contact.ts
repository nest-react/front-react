import { FrontBase } from './FrontBase';
import { Contact as ContactInterface } from '../../interface/front/contact.interface';
import { FrontResponse } from '../../interface/front/front.interface';

export class Contact extends FrontBase<Partial<ContactInterface>> {
  protected readonly TARGET: string = 'contacts';

  public updateByEmail(
    email: string,
    data: Partial<ContactInterface>,
  ): Promise<FrontResponse<Partial<ContactInterface>>> {
    return this.update(
      this.createAlias({
        key: 'email',
        value: email,
      }),
      data,
    );
  }

  public updateByPhone(
    phone: string,
    data: Partial<ContactInterface>,
  ): Promise<FrontResponse<Partial<ContactInterface>>> {
    return this.update(
      this.createAlias({
        key: 'phone',
        value: phone,
      }),
      data,
    );
  }
}
