import {
  Front,
  FrontAlias,
  FrontResponse,
} from '../../interface/front/front.interface';
import { FrontClient } from './FrontClient';

export abstract class FrontBase<T> implements Front<T> {
  protected readonly TARGET: string;
  protected readonly ALIAS_PREFIX: string = 'alt';

  public static apiClient: FrontClient;

  constructor() {
    FrontBase.getApiClient();
  }

  public static getApiClient(): FrontClient {
    if (!FrontBase.apiClient) {
      FrontBase.apiClient = new FrontClient();
    }
    return FrontBase.apiClient;
  }

  public findAll(): Promise<FrontResponse<T[]>> {
    return FrontBase.apiClient.execute<T[]>(this.TARGET);
  }

  public findById(id: string): Promise<FrontResponse<T>> {
    return FrontBase.apiClient.execute<T>(`${this.TARGET}/${id}`);
  }

  public create(data: T): Promise<FrontResponse<T>> {
    return FrontBase.apiClient.execute<T>(this.TARGET, 'POST', data);
  }

  public update(id: string, data: T): Promise<FrontResponse<T>> {
    return FrontBase.apiClient.execute<T>(
      `${this.TARGET}/${id}`,
      'PATCH',
      data,
    );
  }

  public createAlias(data: FrontAlias): string {
    return `${this.ALIAS_PREFIX}:${data.key}:${data.value}`;
  }
}
