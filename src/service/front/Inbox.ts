import { FrontBase } from './FrontBase';
import { FrontResponse } from '../../interface/front/front.interface';
import { Inbox as InboxInterface } from '../../interface/front/inbox.interface';
import { Conversation } from '../../interface/front/conversation.interface';

export class Inbox extends FrontBase<InboxInterface> {
  protected readonly TARGET: string = 'inboxes';

  public findByName(name: string): Promise<InboxInterface> {
    return super.findAll().then((response) => {
      return response.data._results.find((item) => item.name === name);
    });
  }

  public getConversations(id: string): Promise<FrontResponse<Conversation>> {
    return Inbox.apiClient.execute(`${this.TARGET}/${id}/conversations`);
  }
}
