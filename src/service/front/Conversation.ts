import { FrontBase } from './FrontBase';
import { FrontResponse } from '../../interface/front/front.interface';
import { ConversationMessage } from '../../interface/front/message.interface';

export class Conversation extends FrontBase<ConversationMessage> {
  protected readonly TARGET: string = 'conversations';

  public reply(
    id: string,
    data: ConversationMessage,
  ): Promise<FrontResponse<ConversationMessage>> {
    return Conversation.apiClient.execute(
      `${this.TARGET}/${id}/messages`,
      'POST',
      data,
    );
  }
}
