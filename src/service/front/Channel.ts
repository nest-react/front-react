import { FrontBase } from './FrontBase';
import { FrontResponse } from '../../interface/front/front.interface';
import {
  ChannelMessage,
  InboundMessage,
  OutboundMessage,
} from '../../interface/front/message.interface';

export class Channel extends FrontBase<ChannelMessage> {
  protected readonly TARGET: string = 'channels';

  public receiveInboundMessage(
    id: string,
    data: InboundMessage,
  ): Promise<FrontResponse<InboundMessage>> {
    return Channel.apiClient.execute(
      `${this.TARGET}/${id}/inbound_messages`,
      'POST',
      data,
    );
  }

  receiveOutboundMessage(
    id: string,
    data: OutboundMessage,
  ): Promise<FrontResponse<OutboundMessage>> {
    return Channel.apiClient.execute(
      `${this.TARGET}/${id}/outbound_messages`,
      'POST',
      data,
    );
  }
}
