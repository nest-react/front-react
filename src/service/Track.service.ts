import {
  AirportOrigin,
  FlightStatsResults,
  FlightStatus,
} from '../interface/flightStats';
import {
  getAdditionalAlertPayload,
  getLumoPayload,
} from '../util/flight.utils';
import * as axios from 'axios';
import { FlightInfo, FlightResponse, ConnectionData } from '../interface/lumo';
import User from '../model/User';
import { LumoService } from './Lumo.service';
import { FlightStatsService } from './FlightStats.service';
import { FlightService } from './Flight.service';
import Flight from '../model/Flight';
import { ConnectionResponse } from '../interface/lumo/lumoConnection.interface';
import { UserTrip } from '../model/UserTrip';
import ConfigService from './Config.service';

export class TrackService {
  public constructor(
    private readonly lumoService: LumoService,
    private readonly flightStatsService: FlightStatsService,
    private readonly flightService: FlightService,
  ) {}

  public async trackSingleFlight(
    record: FlightResponse | FlightStatus,
    passenger: User,
  ): Promise<void> {
    const dbRecord = await this.flightService.createUserTrip(passenger.id);
    let slackMessage = `Timestamp: ${new Date(
      new Date().getTime() + 7200000,
    )} \nRegister Event: New flight
    Name: ${passenger.firstName} ${passenger.lastName}
    Phone: +${passenger.mobile} (${passenger.whatsapp ? 'Whatsapp' : 'SMS'})
    Email: ${passenger.email}
    `;
    if (record.hasOwnProperty('airportResources')) {
      if (record as FlightStatus) {
        slackMessage += `\nFlight Date: ${
          (record as FlightStatus).departureDate.dateLocal
        }
        Outbound: ${(record as FlightStatus).carrier.iata}${
          (record as FlightStatus).flightNumber
        }
        Route: ${(record as FlightStatus).departureAirport.iata} ===> ${
          (record as FlightStatus).arrivalAirport.iata
        }
        departure time: ${(record as FlightStatus).departureDate.dateLocal} 
        `;
      }

      if (process.env.NODE_ENV === 'production') {
        await axios.default.post(ConfigService.get('SLACK_NEWFLIGHT_SHU'), {
          text: slackMessage,
        });
      }
      return this.processAlertRecord(record as FlightStatus, dbRecord);
    }

    if (record as FlightResponse) {
      slackMessage += `\nFlight Date: ${
        (record as FlightResponse).flight.scheduled_departure
      }
      Outbound: ${(record as FlightResponse).flight.carrier.iata}${
        (record as FlightResponse).flight.number
      }
      Route: ${(record as FlightResponse).flight.origin.iata} ===> ${
        (record as FlightResponse).flight.destination.iata
      }
      departure time: ${(record as FlightResponse).flight.scheduled_departure} 
      `;
    }
    if (process.env.NODE_ENV === 'production') {
      await axios.default.post(ConfigService.get('SLACK_NEWFLIGHT_SHU'), {
        text: slackMessage,
      });
    }
    return this.processLumoRecord(record as FlightResponse, dbRecord);
  }

  private async processAlertRecord(record: FlightStatus, userTrip: UserTrip) {
    const departureDate = record.departureDate.dateLocal.split('T')[0];
    const flightNumber = `${record.carrier.iata}${record.flightNumber}`;
    const flightStatus = await this.flightStatsService.status(
      {
        flight: record.flightNumber,
        carrier: record.carrier.iata,
        date: departureDate,
        airportType: AirportOrigin.departure,
        airport: record.departureAirport.iata,
      },
      { extendedOptions: ['useInlinedReferences', 'includeNewFields'] },
    );

    const payload = {
      flightNo: flightNumber,
      active: true,
      flightStatsId: record.flightId,
      departureDate,
    };
    const flightRecord = await this.flightService.saveFlight(payload);

    await this.flightService.createTrip(
      userTrip.trip_id,
      null,
      flightRecord.id,
      null,
      null,
    );
    await this.updateSubscriptionFields(
      record,
      flightRecord,
      departureDate,
      null,
      flightStatus,
    );
  }

  private async processLumoRecord(lumoRecord: FlightResponse, trip: UserTrip) {
    const departureDate = lumoRecord.flight.scheduled_departure.substr(0, 10);
    const flightNumber = `${lumoRecord.flight.carrier.iata}${lumoRecord.flight.number}`;
    const flight = lumoRecord.flight;
    const flightStatus = await this.flightStatsService.status(
      {
        flight: flight.number,
        carrier: flight.carrier.iata,
        date: departureDate,
        airportType: AirportOrigin.arrival,
      },
      { extendedOptions: ['useInlinedReferences', 'includeNewFields'] },
    );
    const lumoStatus = await this.lumoService.status({
      carrier: flight.carrier.iata,
      flight_number: flight.number,
      origin: flight.origin.iata,
      destination: flight.destination.iata,
      date: departureDate,
      weather: true,
      inbound: true,
    });

    const payload = {
      flightNo: flightNumber,
      active: true,
      lumoId: flight.id,
      departureDate,
    };

    const flightRecord = await this.flightService.saveFlight(payload);

    const allFlights = await this.flightService.getAllFlightsByUser(
      trip.user_id,
    );
    allFlights.map(async (x) => {
      if (x.destination === flightRecord.origin) {
        if (
          x.estimatedArrival >
          new Date(
            flightRecord.estimatedDeparture.setHours(
              flightRecord.estimatedDeparture.getHours() - 12,
            ),
          )
        ) {
          const tripRecord = await this.flightService.userTripRepository.find({
            where: {
              user_id: trip.user_id,
            },
          });
          await this.flightService.createTrip(
            tripRecord[0].trip_id,
            1,
            flightRecord.id,
            null,
            null,
          );
          await this.flightService.updateTrip(tripRecord[0].trip_id, {
            seq: 0,
          });
          await this.updateSubscriptionFields(
            flight,
            flightRecord,
            departureDate,
            lumoStatus,
            flightStatus,
          );
          return;
        }
      }
    });

    await this.flightService.createTrip(
      trip.trip_id,
      null,
      flightRecord.id,
      null,
      null,
    );
    await this.updateSubscriptionFields(
      flight,
      flightRecord,
      departureDate,
      lumoStatus,
      flightStatus,
    );
  }

  public async trackConnectionFlight(
    lumoRecords: FlightResponse[],
    passenger: User,
  ): Promise<void> {
    let flightRecord: Flight;
    const dbRecord = await this.flightService.createUserTrip(passenger.id);

    for (let i = 0; i < lumoRecords.length; i++) {
      const lumoRecord = lumoRecords[i];
      let connection: ConnectionResponse = null,
        connectionData: ConnectionData = null,
        connectionTime: number = null;
      const departureDate = lumoRecord.flight.scheduled_departure.substr(0, 10);
      const flightNumber = `${lumoRecord.flight.carrier.iata}${lumoRecord.flight.number}`;
      const { flight } = lumoRecord;
      const flightStatus = await this.flightStatsService.status(
        {
          flight: flight.number,
          carrier: flight.carrier.iata,
          date: departureDate,
          airportType: AirportOrigin.arrival,
        },
        { extendedOptions: ['useInlinedReferences', 'includeNewFields'] },
      );
      const lumoStatus = await this.lumoService.status({
        carrier: flight.carrier.iata,
        flight_number: flight.number,
        origin: flight.origin.iata,
        destination: flight.destination.iata,
        date: departureDate,
        weather: true,
        inbound: true,
      });
      if (lumoRecords[i + 1]) {
        //This magick trick will save the date as requested in the Lumo connection API in the format: YYMMDD
        const thisFlightDate = this.createLumoDate(
          lumoRecord.flight.scheduled_departure,
        );
        const nextFlightDate = this.createLumoDate(
          lumoRecords[i + 1].flight.scheduled_departure,
        );
        //The next magick trick will abide to Lumo's weird format for flight ID for the connection querry
        const thisFlightId = this.createLumoConnectionId(
          lumoRecord,
          thisFlightDate,
        );
        const nextFlightId = this.createLumoConnectionId(
          lumoRecords[i + 1],
          nextFlightDate,
        );

        try {
          connection = await this.lumoService.connection(
            thisFlightId,
            nextFlightId,
          );
        } catch {
          connection = null;
        }

        if (connection) {
          connectionData = connection.connection;
          connectionTime = connection.connection.connection_time;
        }
      }

      const payload = {
        flightNo: flightNumber,
        active: true,
        lumoId: flight.id,
        departureDate,
      };

      flightRecord = await this.flightService.saveFlight(payload);

      await this.flightService.createTrip(
        dbRecord.trip_id,
        i,
        flightRecord.id,
        connectionTime,
        connectionData,
      );

      await this.updateSubscriptionFields(
        flight,
        flightRecord,
        departureDate,
        lumoStatus,
        flightStatus,
      );
    }
  }

  private createLumoDate(scheduled_departure: string) {
    const d = new Date(scheduled_departure);
    const dateFormat = [
      d.getFullYear().toString().replace('20', ''),
      ('0' + (d.getMonth() + 1)).slice(-2),
      ('0' + d.getDate()).slice(-2),
    ].join('');
    return dateFormat;
  }

  private createLumoConnectionId(lumoRecord: FlightResponse, date: string) {
    const FlightId =
      lumoRecord.flight.carrier.iata +
      lumoRecord.flight.number +
      lumoRecord.flight.origin.iata +
      lumoRecord.flight.destination.iata +
      date;
    return FlightId;
  }

  private async updateSubscriptionFields(
    record: FlightInfo | FlightStatus,
    flight: Flight,
    departureDate: string,
    lumoStatus?: FlightResponse[],
    flightStatus?: FlightStatsResults<FlightStatus[]>,
  ) {
    if (lumoStatus) {
      await this.processLumoSubscription(
        lumoStatus[0],
        flight,
        record as FlightInfo,
      );
    }
    if (
      flightStatus &&
      !flightStatus.error &&
      flightStatus.results.length >= 1
    ) {
      const status = flightStatus.results[0];
      if (!lumoStatus) {
        await this.flightService.updateFlight(
          flight.id,
          getAdditionalAlertPayload(status),
        );
      }
      await this.processFlightStatsSubscription(status, departureDate, flight);
    }
  }

  private async processLumoSubscription(
    lumoStatus: FlightResponse,
    dbRecord: Flight,
    flight: FlightInfo,
  ) {
    const additionalLumoFields = getLumoPayload(lumoStatus);
    await this.flightService.updateInitialLumoInfo(
      dbRecord.id,
      flight.id,
      lumoStatus,
    );
    await this.flightService.updateFlight(dbRecord.id, additionalLumoFields);
    await this.lumoService.subscription(dbRecord.id);
  }

  private async processFlightStatsSubscription(
    flightStatus: FlightStatus,
    departureDate: string,
    flight: Flight,
  ) {
    await this.flightService.updateInitialFlightStatsInfo(
      flight.id,
      flightStatus.flightId,
      flightStatus,
    );
    const additionalFlightStatsFields = await this.getAlertPayload(
      flightStatus,
    );

    await this.flightService.updateFlight(
      flight.id,
      additionalFlightStatsFields,
    );

    await this.flightStatsService.createAlertByDeparture(
      {
        carrier: flightStatus.carrier.iata,
        flightNumber: Number(flightStatus.flightNumber),
        departureAirport: flightStatus.departureAirport.iata,
        arrivalAirport: flightStatus.arrivalAirport.iata,
        date: departureDate,
        events:
          'dep,arr,can,div,depDelay1,arrDelay1,depGate,arrGate,bag,tailNumber',
        status: flightStatus,
      },
      flight.id,
    );
  }

  private async getAlertPayload(flightStatus: FlightStatus) {
    const marketedCarrier = flightStatus.codeshares[0]?.carrier?.iata;

    return {
      realFlightNo:
        flightStatus.operatingCarrier.iata + flightStatus.flightNumber,
      operatingCarrier: flightStatus.operatingCarrier.iata,
      marketedCarrier,
      cancelled: flightStatus.status === 'C',
      diverted: flightStatus.status === 'D',
      planeType: flightStatus.flightEquipment?.actualEquipment?.iata
        ? flightStatus.flightEquipment?.actualEquipment?.iata
        : flightStatus.flightEquipment?.scheduledEquipment?.iata,
      tailNo: flightStatus.flightEquipment?.tailNumber,
      origin: flightStatus.departureAirport.iata,
      destination: flightStatus.arrivalAirport.iata,
      diversionAirport: flightStatus.divertedAirport?.iata,
      originTerminal: flightStatus.airportResources?.departureTerminal,
      originGate: flightStatus.airportResources?.departureGate,
      destinationTerminal: flightStatus.airportResources?.arrivalTerminal,
      destinationGate: flightStatus.airportResources?.arrivalGate,
      destinationBaggage: flightStatus.airportResources?.baggage,
    };
  }
}
