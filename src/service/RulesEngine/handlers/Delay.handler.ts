import { EngineResult } from 'json-rules-engine';
import Handler from '../Handler';
import { BaseHandler } from '../../../interface/rulesEngine/RulesEngine';
import { RuleEngineServices } from '../../../interface/webhook-server.interface';
import Flight from '../../../model/Flight';
import Il8nService from '../../../service/Il8n.service';
export default class DelayHandler extends Handler {
  async handle(
    result: EngineResult,
    services: RuleEngineServices,
    flight: Flight,
  ): Promise<BaseHandler> {
    const shouldBeHandled = result.events.some(
      (event) => event.type === 'departure_delay',
    );
    if (shouldBeHandled) {
      const users = await services.flightService.getUsersByFlight(flight.id);
      users.forEach((user) => {
        if (user.confirmed_messaging === false) {
          return;
        }
        if (user.whatsapp) {
          this.sendMessage(
            flight,
            `whatsapp:+${this.phone}`,
            `whatsapp:+${user.mobile}`,
            'whatsapp',
            services,
          );
        } else {
          this.sendMessage(
            flight,
            `+${this.phone}`,
            `+${user.mobile}`,
            'sms',
            services,
          );
        }
      });
      return super.handle(result);
    }
  }

  private async sendMessage(
    flight: Flight,
    from: string,
    to: string,
    source: string,
    services: RuleEngineServices,
  ) {
    let message = undefined;
    if (!flight.updatedEstimatedDeparture) {
      if (
        new Date(flight.estimatedDeparture.getTime() - 20 * 60000) >=
        flight.scheduledDeparture
      ) {
        await services.db.getRepository('flight').update(flight, {
          updatedEstimatedDeparture: flight.estimatedDeparture,
        });
      } else {
        return 1;
      }
    } else if (
      new Date(flight.estimatedDeparture.getTime() - 20 * 60000) >=
      flight.updatedEstimatedDeparture
    ) {
      await services.db.getRepository('flight').update(flight, {
        updatedEstimatedDeparture: flight.estimatedDeparture,
      });
    } else {
      return 1;
    }

    message = Il8nService.get('zenny:flight:delay', {
      templateVars: {
        delay: flight.updatedEstimatedDeparture,
      },
    });
    await services.beeQueueService.createResponseJob(
      services.bq,
      from,
      to,
      source,
      message,
    );
    await services.commsEventService.addEvent({
      from,
      to,
      message: message,
      server: require('os').hostname(),
      source: source,
      created: new Date(),
      result: 'success',
    });
  }
}
