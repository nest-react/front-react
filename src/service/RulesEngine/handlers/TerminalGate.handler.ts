import { EngineResult } from 'json-rules-engine';
import Handler from '../Handler';
import { BaseHandler } from '../../../interface/rulesEngine/RulesEngine';
import { RuleEngineServices } from '../../../interface/webhook-server.interface';
import Flight from '../../../model/Flight';
export default class TerminalGateHandler extends Handler {
  async handle(
    result: EngineResult,
    services: RuleEngineServices,
    flight: Flight,
  ): Promise<BaseHandler> {
    const shouldBeHandled = result.events.some(
      (event) => event.type === 'gate_changed',
    );
    if (shouldBeHandled) {
      const today = new Date();
      const users = await services.flightService.getUsersByFlight(flight.id);
      if (
        new Date(flight.scheduledDeparture).getTime() - today.getTime() >= 0 &&
        new Date(flight.scheduledDeparture).getTime() - today.getTime() <=
          14400000
      ) {
        users.forEach((user) => {
          if (user.confirmed_messaging === false) {
            return;
          }
          if (user.whatsapp) {
            this.sendMessage(
              flight,
              `whatsapp:+${this.phone}`,
              `whatsapp:+${user.mobile}`,
              'whatsapp',
              services,
            );
          } else {
            this.sendMessage(
              flight,
              `+${this.phone}`,
              `+${user.mobile}`,
              'sms',
              services,
            );
          }
        });
      }

      return super.handle(result);
    }
  }

  private async sendMessage(
    flight: Flight,
    from: string,
    to: string,
    source: string,
    services: RuleEngineServices,
  ) {
    let message = null;
    if (flight.originTerminal && flight.originGate) {
      message = `I just found out that flight from ${flight.origin} to ${flight.destination} will depart from terminal ${flight.originTerminal} gate ${flight.originGate}`;
    } else if (flight.originGate) {
      message = `I just found out that your flight will leave from gate ${flight.originGate}`;
    }
    if (message) {
      await services.beeQueueService.createResponseJob(
        services.bq,
        from,
        to,
        source,
        message,
      );
      await services.commsEventService.addEvent({
        from,
        to,
        message: message,
        server: require('os').hostname(),
        source: source,
        created: new Date(),
        result: 'success',
      });
    }
  }
}
