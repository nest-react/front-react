import { EngineResult } from 'json-rules-engine';
import Handler from '../Handler';
import { BaseHandler } from '../../../interface/rulesEngine/RulesEngine';
import { RuleEngineServices } from '../../../interface/webhook-server.interface';
import Flight from '../../../model/Flight';
export default class BaggageClaimHandler extends Handler {
  async handle(
    result: EngineResult,
    services: RuleEngineServices,
    flight: Flight,
  ): Promise<BaseHandler> {
    const shouldBeHandled = result.events.some(
      (event) => event.type === 'baggage_changed',
    );
    if (shouldBeHandled) {
      const users = await services.flightService.getUsersByFlight(flight.id);
      users.forEach((user) => {
        if (user.confirmed_messaging === false) {
          return;
        }
        if (user.whatsapp) {
          this.sendMessage(
            flight,
            `whatsapp:+${this.phone}`,
            `whatsapp:+${user.mobile}`,
            'whatsapp',
            services,
          );
        } else {
          this.sendMessage(
            flight,
            `+${this.phone}`,
            `+${user.mobile}`,
            'sms',
            services,
          );
        }
      });
      return super.handle(result);
    }
  }

  private async sendMessage(
    flight: Flight,
    from: string,
    to: string,
    source: string,
    services: RuleEngineServices,
  ) {
    let message = undefined;
    if (
      flight.destinationBaggage &&
      ['landed', 'arrived'].includes(flight.statusName)
    ) {
      message = `Just found out that there was a change and the bags are on their way to carousel ${flight.destinationBaggage}. Go get 'em.`;
      await services.beeQueueService.createResponseJob(
        services.bq,
        from,
        to,
        source,
        message,
      );
      await services.commsEventService.addEvent({
        from,
        to,
        message: message,
        server: require('os').hostname(),
        source: source,
        created: new Date(),
        result: 'success',
      });
    }
  }
}
