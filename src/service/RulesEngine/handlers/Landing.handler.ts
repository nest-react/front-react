import { EngineResult } from 'json-rules-engine';
import Handler from '../Handler';
import { BaseHandler } from '../../../interface/rulesEngine/RulesEngine';
import { RuleEngineServices } from '../../../interface/webhook-server.interface';
import { AirtableFactory } from '../../../factory/Airtable.factory';
import Flight from '../../../model/Flight';
import Il8nService from '../../../service/Il8n.service';
import AirportService from '../../../service/Airport.service';
import { WeatherTime } from '../../../service/WeatherTime.service';
import { createLogger } from '../../../util/logger';
import { kelvinToCelsius, kelvinToFahrenheit } from '../../../bot-server';
import { Trip } from '../../../model/Trip';
import { JackAndFerdiService } from '../../../service/JackAndFerdi.service';

export default class LandingHandler extends Handler {
  async handle(
    result: EngineResult,
    services: RuleEngineServices,
    flight: Flight,
  ): Promise<BaseHandler> {
    /**
     * Logging object unique to the landing handler
     */
    const logger = createLogger('zenner', 'landing.handler', 'rules-engine');

    const shouldBeHandled = result.events.some(
      (event) => event.type === 'flight_landed',
    );

    if (shouldBeHandled) {
      const userFlight = await services.flightService.getUsersByFlight(
        flight.id,
      );
      const flightWithTrips = await services.flightService.getFlightById(
        flight.id,
      );

      logger.info(
        `Landing handler triggered for ${
          flight.flightNo
        } and shouldBeHandled=${String(shouldBeHandled)}`,
        {
          userFlight,
          flightWithTrips,
        },
      );
      let secondLeg: Flight;

      let trip: Trip[];

      //Two connectiong flights will always have sequential trip_flight id's

      const airtableService = AirtableFactory.create();
      const airport = flight.destination;
      const terminal = flight.destinationTerminal;
      const airportStruct = AirportService.search(flight.destination)[0];
      const time = new WeatherTime();
      const temperature = await time.getTemperature(airportStruct.municipality);

      const landingMessage = Il8nService.get('zenny:landing:landing', {
        templateVars: {
          airport: airportStruct.name,
          city: airportStruct.municipality,
          local_time: await time.getTime(airportStruct.municipality),
          weather_description: await time.getWeatherDescription(
            airportStruct.municipality,
          ),
          ftemp: Math.round(kelvinToFahrenheit(temperature)),
          ctemp: Math.round(kelvinToCelsius(temperature)),
        },
      });
      let connectionMessage = '';
      let airportMessage = '';
      let tipsMessage = '';
      let tips = await airtableService.getMessageForAirport({
        airportCode: airport,
        active: true,
        terminal: terminal,
      });
      if (terminal !== null) {
        tips = tips.concat(
          await airtableService.getMessageForAirport({
            airportCode: airport,
            active: true,
            terminal: '_',
          }),
        );
      }

      if (tips[0] !== undefined) {
        tipsMessage = `Here are some tips for ${airport} ${
          !terminal ? '' : `in terminal ${terminal}`
        }: \n`;
        tips.forEach((tip: any) => {
          tipsMessage += `* ${tip.fields.message}\n`;
        });
      } else {
        tipsMessage = null;
      }
      let i = 0;
      userFlight.map(async (user) => {
        if (user.confirmed_messaging === false) {
          return;
        }
        if (flightWithTrips.trips[i].seq === 0) {
          trip = await services.db.getRepository(Trip).find({
            where: {
              trip_id: flightWithTrips.tripId,
            },

            order: {
              flight_trip_id: 'DESC',
            },
          });

          secondLeg = trip[0].flight;
        }

        if (secondLeg) {
          const diffTime = Math.floor(
            Math.abs(
              flight.estimatedArrival.getTime() -
                flight.scheduledArrival.getTime(),
            ) /
              1000 /
              60,
          );

          if (
            !(
              (await new JackAndFerdiService(null).getPlacesInAirport(
                flight.destination,
              )) ===
              Il8nService.get('zenny:jackandferdi:notfound', {
                templateVars: {
                  city: flight.destination,
                },
              })
            ) &&
            flight.conn_time >= 180
          ) {
            airportMessage = Il8nService.get('zenny:notifications:2hrs', {
              templateVars: { airport: flight.destination },
            });
          }

          connectionMessage = Il8nService.get('zenny:landing:firstLeg', {
            templateVars: {
              airport: flight.destination,

              isDelay:
                flight.estimatedArrival > flight.scheduledArrival
                  ? 'unfortunately'
                  : 'fortunately',

              delayString:
                diffTime == 0
                  ? 'on time'
                  : flight.estimatedArrival > flight.scheduledArrival
                  ? `${diffTime} late`
                  : `${diffTime} early`,

              conn_time: flight.conn_time,

              landingTerminalGateString: flight.destinationTerminal
                ? `at terminal ${flight.destinationTerminal}` +
                  flight.destinationGate
                  ? `gate ${flight.destinationGate}`
                  : ''
                : flight.destinationGate
                ? `gate ${flight.destinationGate}`
                : flight.destination,

              terminalGateString: secondLeg.originTerminal
                ? `at terminal ${secondLeg.originTerminal}` +
                  secondLeg.originGate
                  ? `gate ${secondLeg.originGate}`
                  : ''
                : secondLeg.originGate
                ? `gate ${secondLeg.originGate}`
                : secondLeg.origin,

              baggage: `(${flight.destinationBaggage})`,
            },
          });
        } else {
          connectionMessage = null;
        }

        if (!flightWithTrips.trips[i].sent_landed) {
          if (user.whatsapp) {
            this.sendMessage(
              `whatsapp:+${this.phone}`,
              `whatsapp:+${user.mobile}`,
              'whatsapp',
              services,
              landingMessage,
            );
            if (tipsMessage) {
              this.sendMessage(
                `whatsapp:+${this.phone}`,
                `whatsapp:+${user.mobile}`,
                'whatsapp',
                services,
                tipsMessage,
              );
            }
            if (connectionMessage) {
              this.sendMessage(
                `whatsapp:+${this.phone}`,
                `whatsapp:+${user.mobile}`,
                'whatsapp',
                services,
                connectionMessage,
              );
            }
            if (airportMessage) {
              this.sendMessage(
                `whatsapp:+${this.phone}`,
                `whatsapp:+${user.mobile}`,
                'whatsapp',
                services,
                airportMessage,
              );
            }
          } else {
            this.sendMessage(
              `+${this.phone}`,
              `+${user.mobile}`,
              'sms',
              services,
              landingMessage,
            );
            if (tipsMessage) {
              this.sendMessage(
                `+${this.phone}`,
                `+${user.mobile}`,
                'sms',
                services,
                tipsMessage,
              );
            }
            if (connectionMessage) {
              this.sendMessage(
                `+${this.phone}`,

                `+${user.mobile}`,

                'sms',

                services,

                connectionMessage,
              );
            }

            if (airportMessage) {
              this.sendMessage(
                `+${this.phone}`,
                `+${user.mobile}`,
                'sms',
                services,
                airportMessage,
              );
            }
          }
        }

        i++;
      });
      flightWithTrips.trips.forEach(async (trip) => {
        trip.sent_landed = true;
        await services.flightService.updateFlightTrip(trip);
      });

      return super.handle(result);
    }
  }

  private async sendMessage(
    from: string,
    to: string,
    source: string,
    services: RuleEngineServices,
    message?: string,
  ) {
    await services.beeQueueService.createResponseJob(
      services.bq,
      from,
      to,
      source,
      message,
    );
    await services.commsEventService.addEvent({
      from,
      to,
      message: message,
      server: require('os').hostname(),
      source: source,
      created: new Date(),
      result: 'success',
    });
  }
}
