import { EngineResult } from 'json-rules-engine';
import Flight from '../../model/Flight';
import { BaseHandler } from '../../interface/rulesEngine/RulesEngine';
import { RuleEngineServices } from '../../interface/webhook-server.interface';
import ConfigService from '../Config.service';

export default abstract class Handler implements BaseHandler {
  private nextHandler: BaseHandler;
  public phone: string = ConfigService.get('PRIMARY_PHONE');
  public setNext(handler: BaseHandler): BaseHandler {
    this.nextHandler = handler;
    return handler;
  }

  public async handle(
    result: EngineResult,
    services?: RuleEngineServices,
    flight?: Flight,
  ): Promise<BaseHandler> {
    if (this.nextHandler) {
      return this.nextHandler.handle(result, services, flight);
    }
    return null;
  }
}
