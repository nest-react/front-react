import { Engine, EngineResult, Rule, RuleProperties } from 'json-rules-engine';
import ErrnoException = NodeJS.ErrnoException;
const fs = require('fs');
import path from 'path';

import { createLogger } from '../../util/logger';
import { FactRequest } from '../../interface/rulesEngine/RulesEngine';

export default class RulesEngine {
  private readonly logger = createLogger(
    'zenner',
    'Rules engine',
    'zenner-comms',
  );
  private readonly engine: Engine;

  private readonly RULES_PATH: string = path.join(
    __dirname,
    '/../../resources/rules',
  );

  private rules: RuleProperties[] = [];

  constructor() {
    this.engine = new Engine();
    this.parseRules();
  }

  private parseRules() {
    fs.readdir(
      this.RULES_PATH,
      (err: ErrnoException | null, files: string[]) => {
        if (err) {
          this.rules = [];
          return;
        }
        files.forEach((file) => {
          const json = fs.readFileSync(`${this.RULES_PATH}/${file}`);
          const rule = new Rule(JSON.parse(json));
          this.engine.addRule(rule);
          this.rules.push(rule);
        });
      },
    );
  }

  public async runRules(facts: FactRequest): Promise<EngineResult | null> {
    try {
      if (this.rules.length >= 1) {
        return await this.engine.run(facts);
      }
      return null;
    } catch (e) {
      this.logger.error('An error occurred');
      this.logger.error(e);
      return null;
    }
  }
}
