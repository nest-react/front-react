import { AxiosInstance, AxiosResponse } from 'axios';
// interfaces
import {
  AirportOrigin,
  FlightsByAirportOptions,
  FlightStatsBaseResponse,
  FlightStatsResponse,
  FlightStatsResults,
  FlightStatus,
  FlightStatusByRouteOptions,
  FlightStatusesOptions,
  FlightStatusesResponse,
} from '../interface/flightStats';
import { FlightstatsSubscription } from '../model/FlightstatsSubscription';
import {
  AlertIdsResponse,
  AlertOptions,
  AlertRequestOptions,
  AlertRouteOptions,
  FlightStatsAlertResponse,
} from '../interface/flightStats/alert';
import { FlightstatsRecord } from '../model/FlightstatsRecord';

// services
import { ConfigService } from './';
import { HttpClient } from './HttpClient';
import {
  getMetropolitanAirports,
  isMetropolitanAirport,
} from '../util/metropolitanAirportCodes.utils';
import { parseData } from '../util';
import { Connection, Repository } from 'typeorm';
import Flight from '../model/Flight';
import { createLogger } from '../util/logger';

const logger = createLogger('zenner', 'FlightStats.service', 'zenner-common');
import { ValidateRequest, ValidationResults } from '../interface/lumo';

export class FlightStatsService extends HttpClient {
  private http: AxiosInstance;

  private readonly FLIGHT_STATUS_URL = 'flightstatus/rest/v2/json';

  private readonly alertSubscriptionRepo: Repository<FlightstatsSubscription>;

  private readonly flightRepo: Repository<Flight>;

  private readonly alertRecordsRepo: Repository<FlightstatsRecord>;

  private readonly ALERT_URL = 'alerts/rest/v1/json';

  constructor(db: Connection) {
    super();
    this.http = this.create({
      baseURL: ConfigService.get('FLIGHTSTATS_API_URL'),
    });

    this.http.interceptors.request.use((config) => {
      config.params = config.params || {};
      config.params.appId = ConfigService.get('FLIGHTSTATS_APP_ID');
      config.params.appKey = ConfigService.get('FLIGHTSTATS_APP_KEY');
      config.params.type = 'JSON';
      config.params.deliverTo = ConfigService.get('FLIGHTSTATS_WEBHOOK_URL');
      return config;
    });

    this.alertSubscriptionRepo = db.getRepository(FlightstatsSubscription);
    this.alertRecordsRepo = db.getRepository(FlightstatsRecord);
    this.flightRepo = db.getRepository(Flight);
  }

  public getFlightStatusById(
    id: number,
  ): Promise<FlightStatsResults<FlightStatus>> {
    return this.http
      .get<FlightStatsResponse>(
        `${this.FLIGHT_STATUS_URL}/flight/status/` + String(id),
      )
      .then((data) => this.handleErrors<FlightStatsResponse>(data))
      .then((result) => result.data.flightStatus)
      .then((results) => this.response<FlightStatus>(results))
      .catch(this.responseError);
  }

  public async createAlertByArrival(
    options: AlertOptions,
    flightId: number,
  ): Promise<FlightStatsResults<FlightStatsAlertResponse> | undefined> {
    const { arrivalAirport, date } = options;
    const arrivalDate = this.format(date);
    const url = this.getAlertUrlWithFlightInfo(options);
    const flightNumber = this.getFlightNumber(options);
    if (await this.checkIsSubscribed(flightNumber, date)) return;
    const result = await this.http
      .get<FlightStatsAlertResponse>(
        `${url}/to/${arrivalAirport}/arriving/${arrivalDate}`,
        { params: options },
      )
      .then((data) => this.handleErrors<FlightStatsAlertResponse>(data))
      .then(parseData)
      .then((results) => this.response<FlightStatsAlertResponse>(results))
      .catch(this.responseError);
    if (result.results) {
      await this.createAlertSubscription(result.results, flightId);
      await this.saveAlertRecord(result.results);
    }
    return result;
  }

  public async createAlertByDeparture(
    options: AlertRouteOptions,
    flightId: number,
  ): Promise<FlightStatsResults<FlightStatsAlertResponse> | undefined> {
    const { departureAirport, date } = options;
    const departureDate = this.format(date);
    const url = this.getAlertUrlWithFlightInfo(options);
    const flightNumber = this.getFlightNumber(options);
    if (await this.checkIsSubscribed(flightNumber, date)) return;
    const result = await this.http
      .get<FlightStatsAlertResponse>(
        `${url}/from/${departureAirport}/departing/${departureDate}`,
        { params: options },
      )
      .then((data) => this.handleErrors<FlightStatsAlertResponse>(data))
      .then(parseData)
      .then((results) => this.response<FlightStatsAlertResponse>(results))
      .catch(this.responseError);
    if (!result.error) {
      await this.createAlertSubscription(result.results, flightId);
      await this.saveAlertRecord(result.results, options.status);
    }
    return result;
  }

  private async saveAlertRecord(
    payload: FlightStatsAlertResponse,
    status?: FlightStatus,
  ) {
    let additionalPayload = {
      departureDate: payload.rule.departure,
      flightNumber: payload.rule.carrierFsCode + payload.rule.flightNumber,
    };
    if (status) {
      additionalPayload = Object.assign(additionalPayload, {
        flightStatus: status,
        event: 'INITIAL',
        dataSource: 'Bot',
        dateTimeRecorded: new Date().toISOString(),
      });
    }
    const dataForSave = Object.assign(payload, additionalPayload);
    return this.alertRecordsRepo.insert(new FlightstatsRecord(dataForSave));
  }

  public async createAlertForRouteByArrival(
    options: AlertRouteOptions,
    flightId: number,
  ): Promise<FlightStatsResults<FlightStatsAlertResponse> | undefined> {
    const { arrivalAirport, date, departureAirport } = options;
    const arrivingDate = this.format(date);
    const url = this.getAlertUrlWithFlightInfo(options);
    const flightNumber = this.getFlightNumber(options);
    if (await this.checkIsSubscribed(flightNumber, date)) return;
    const result = await this.http
      .get<FlightStatsAlertResponse>(
        `${url}/from/${departureAirport}/to/${arrivalAirport}/arriving/${arrivingDate}`,
        { params: options },
      )
      .then((data) => this.handleErrors<FlightStatsAlertResponse>(data))
      .then(parseData)
      .then((results) => this.response<FlightStatsAlertResponse>(results))
      .catch(this.responseError);
    await this.createAlertSubscription(result.results, flightId);
    await this.saveAlertRecord(result.results);
    return result;
  }

  public async createAlertForRouteByDeparture(
    options: AlertRouteOptions,
    flightId: number,
  ): Promise<FlightStatsResults<FlightStatsAlertResponse> | undefined> {
    const { arrivalAirport, date, departureAirport } = options;
    const departureDate = this.format(date);
    const url = this.getAlertUrlWithFlightInfo(options);
    const flightNumber = this.getFlightNumber(options);
    if (await this.checkIsSubscribed(flightNumber, date)) return;
    const result = await this.http
      .get<FlightStatsAlertResponse>(
        `${url}/from/${departureAirport}/to/${arrivalAirport}/departing/${departureDate}`,
        { params: options },
      )
      .then((data) => this.handleErrors<FlightStatsAlertResponse>(data))
      .then(parseData)
      .then((results) => this.response<FlightStatsAlertResponse>(results))
      .catch(this.responseError);
    await this.createAlertSubscription(result.results, flightId);
    await this.saveAlertRecord(result.results);
    return result;
  }

  public async deleteAlertById(
    alertId: string,
  ): Promise<FlightStatsResults<FlightStatsAlertResponse>> {
    const alert = await this.http
      .get<FlightStatsAlertResponse>(`${this.ALERT_URL}/delete/${alertId}`)
      .then((data) => this.handleErrors<FlightStatsAlertResponse>(data))
      .then(parseData)
      .then((results) => this.response<FlightStatsAlertResponse>(results))
      .catch(this.responseError);
    if (!alert.error) {
      await this.alertSubscriptionRepo.delete({ subscriptionId: alertId });
      await this.alertRecordsRepo.delete({ subscriptionId: alertId });
    }
    return alert;
  }

  public async getAlertById(
    alertId: number,
  ): Promise<FlightStatsResults<FlightStatsAlertResponse>> {
    return this.http
      .get<FlightStatsAlertResponse>(`${this.ALERT_URL}/get/${alertId}`)
      .then((data) => this.handleErrors<FlightStatsAlertResponse>(data))
      .then(parseData)
      .then((results) => this.response<FlightStatsAlertResponse>(results))
      .catch(this.responseError);
  }

  public async getAlertIds(): Promise<FlightStatsResults<AlertIdsResponse>> {
    return this.http
      .get<AlertIdsResponse>(`${this.ALERT_URL}/list`)
      .then((data) => this.handleErrors<AlertIdsResponse>(data))
      .then(parseData)
      .then((results) => this.response<AlertIdsResponse>(results))
      .catch(this.responseError);
  }

  public async getAlertIdsLesserThanId(
    alertId: number,
  ): Promise<FlightStatsResults<AlertIdsResponse>> {
    return this.http
      .get<AlertIdsResponse>(`${this.ALERT_URL}/list/${alertId}`)
      .then((data) => this.handleErrors<AlertIdsResponse>(data))
      .then(parseData)
      .then((results) => this.response<AlertIdsResponse>(results))
      .catch(this.responseError);
  }

  private getAlertUrlWithFlightInfo(options: AlertOptions): string {
    const { carrier, flightNumber } = options;
    return `${this.ALERT_URL}/create/${carrier}/${flightNumber}`;
  }

  public validateFlights = async (
    options: ValidateRequest,
  ): Promise<ValidationResults> => {
    try {
      const response = await this.status({
        carrier: options.carrier,
        date: options.date,
        flight: options.flightNumber,
        airportType: AirportOrigin.departure,
      });
      const isValid = response.results.length > 0;
      return {
        isValid,
        origin: response.results[0].departureAirportFsCode ?? null,
        destination: response.results[0].arrivalAirportFsCode ?? null,
      };
    } catch {
      return {
        isValid: false,
      };
    }
  };

  public status(
    options: FlightStatusesOptions,
    requestOption?: AlertRequestOptions,
  ): Promise<FlightStatsResults<FlightStatus[]>> {
    const { carrier, airportType, date, flight } = options;
    const params = requestOption
      ? {
          extendedOptions: requestOption?.extendedOptions.join(','),
        }
      : {
          ...options,
        };
    return this.http
      .get<FlightStatusesResponse>(
        `${
          this.FLIGHT_STATUS_URL
        }/flight/status/${carrier}/${flight}/${airportType}/${this.format(
          date,
        )}`,
        {
          params,
        },
      )
      .then((data) => this.handleErrors<FlightStatusesResponse>(data))
      .then(this.parseStatuses)
      .then((results) => this.response<FlightStatus[]>(results))
      .catch(this.responseError);
  }

  /*
   * Returns the status of all flights arriving/departing (or having arrived/departed)
   * at an airport within the specified hour, or within a window up to 6 hours wide
   * if numHours is specified.
   */
  public findFlightsByAirport(
    {
      airport,
      date,
      hourOfDay,
      airportType,
      ...params
    }: FlightsByAirportOptions,
    requestOption?: AlertRequestOptions,
  ): Promise<FlightStatsResults<FlightStatus[]>> {
    const requestParams = requestOption
      ? {
          extendedOptions: requestOption?.extendedOptions.join(','),
          ...params,
        }
      : {
          ...params,
        };
    return this.http
      .get<FlightStatusesResponse>(
        `${
          this.FLIGHT_STATUS_URL
        }/airport/status/${airport}/${airportType}/${this.format(
          date,
        )}/${hourOfDay}`,
        { params: requestParams },
      )
      .then((data) => this.handleErrors<FlightStatusesResponse>(data))
      .then(this.parseStatuses)
      .then((results) => this.response<FlightStatus[]>(results))
      .catch(this.responseError);
  }

  private setAirportProperty(
    airports: string[],
    place: 'departureAirport' | 'arrivalAirport',
    searchOptions: FlightStatusByRouteOptions,
  ): FlightStatusByRouteOptions[] {
    return airports.map<FlightStatusByRouteOptions>((airport) => {
      return {
        ...searchOptions,
        [place]: airport,
      };
    });
  }

  private getSearchCombinations(
    options: FlightStatusByRouteOptions,
  ): FlightStatusByRouteOptions[] {
    const { departureAirport, arrivalAirport } = options;
    const hasMultipleOrigin: boolean = isMetropolitanAirport(departureAirport);
    const hasMultipleDestination: boolean = isMetropolitanAirport(
      arrivalAirport,
    );

    if (hasMultipleOrigin || hasMultipleDestination) {
      const origins = hasMultipleOrigin
        ? getMetropolitanAirports(departureAirport)
        : [departureAirport];

      const destinations = hasMultipleDestination
        ? getMetropolitanAirports(arrivalAirport)
        : [arrivalAirport];

      const searchOptionsOrigin = this.setAirportProperty(
        origins,
        'departureAirport',
        options,
      );

      return searchOptionsOrigin.reduce<FlightStatusByRouteOptions[]>(
        (acc, searchOptions) => {
          return acc.concat(
            this.setAirportProperty(
              destinations,
              'arrivalAirport',
              searchOptions,
            ),
          );
        },
        [],
      );
    }

    return [options];
  }

  /*
  Returns the status of all flights for a given route
  departing/arriving on the specified date depends on airportType options
* */
  public findFlightsByRoute(
    options: FlightStatusByRouteOptions,
    requestOption?: AlertRequestOptions,
  ): Promise<FlightStatsResults<FlightStatus[]>> {
    const searchOptions: FlightStatusByRouteOptions[] = this.getSearchCombinations(
      options,
    );

    const flightsByAllPossibleWays = searchOptions.map((params) => {
      const {
        departureAirport,
        arrivalAirport,
        date,
        airportType,
        ...rest
      } = params;
      const requestParams = requestOption
        ? {
            extendedOptions: requestOption?.extendedOptions.join(','),
            ...rest,
          }
        : {
            ...rest,
          };
      return this.http
        .get<FlightStatusesResponse>(
          `${
            this.FLIGHT_STATUS_URL
          }/route/status/${departureAirport}/${arrivalAirport}/${airportType}/${this.format(
            date,
          )}`,
          { params: requestParams },
        )
        .then((data) => this.handleErrors<FlightStatusesResponse>(data))
        .then(this.parseStatuses);
    });

    return Promise.all(flightsByAllPossibleWays)
      .then((results: FlightStatus[][]) => {
        return results.reduce(
          (list, statuses) => list.concat(statuses),
          [] as FlightStatus[],
        );
      })
      .then((results) => this.response<FlightStatus[]>(results))
      .catch(this.responseError);
  }

  private format(dateString: string): string {
    const dtISOString: string = new Date(Date.parse(dateString)).toISOString();

    // Sometimes there is a timezone jump, so this manually parses an ISO string format
    // in which the elements of the date will always appear in the exact same location.
    const dtUrlString: string =
      dtISOString.substr(0, 4) +
      '/' +
      Number(dtISOString.substr(5, 2)).toString() +
      '/' +
      Number(dtISOString.substr(8, 2)).toString();

    return dtUrlString;
  }

  private parseStatuses(
    response: AxiosResponse<FlightStatusesResponse>,
  ): FlightStatus[] {
    return response.data.flightStatuses;
  }

  private handleErrors<T extends FlightStatsBaseResponse>(
    results: AxiosResponse<T>,
  ): AxiosResponse<T> {
    if (results.data.error) {
      throw new Error(results.data?.error.errorMessage);
    }

    return results;
  }

  private responseError = (error: any): FlightStatsResults<null> => {
    logger.error('Flightstats error', { error: error });
    return this.response<null>(null, true, error?.message);
  };

  private async checkIsSubscribed(flightNumber: string, date: string) {
    const flight = await this.flightRepo.findOne({
      where: {
        flightNo: flightNumber,
        departureDate: date,
      },
    });
    if (flight) {
      const res = await this.alertSubscriptionRepo.findOne({
        where: { flightId: flight.id },
      });
      return !!res;
    }
    return false;
  }

  private async createAlertSubscription(
    result: FlightStatsAlertResponse,
    flightId: number,
  ): Promise<boolean> {
    try {
      const alertId = result.rule.id;
      await this.alertSubscriptionRepo.insert({
        flightId,
        subscriptionId: alertId,
      });
    } catch (e) {
      logger.error('An error occurred', { error: e });
      return false;
    }
    return true;
  }

  private getFlightNumber(options: AlertOptions) {
    return `${options.carrier}${options.flightNumber}`;
  }

  private response<T>(
    results: T,
    error?: boolean,
    message?: string,
  ): FlightStatsResults<T> {
    return {
      results,
      error,
      message,
    };
  }
}
