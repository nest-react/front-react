import { Repository, Connection, In } from 'typeorm';
import Flight from '../model/Flight';
import { UserTrip } from '../model/UserTrip';
import { Trip } from '../model/Trip';
import User from '../model/User';
import {
  sendMessage,
  getRiskWord,
  kelvinToCelsius,
  kelvinToFahrenheit,
  getCity,
} from '../bot-server';
import BQ from 'bee-queue';
import Il8nService from './Il8n.service';
import { getDiffMinutes } from '../util/datetime.utils';
import { Client, TravelMode } from '@googlemaps/google-maps-services-js/dist';
import { DirectionService } from './Direction.service';
import { createLogger } from '../util/logger';
import ConfigService from './Config.service';
import { AirtableFactory } from '../factory/Airtable.factory';
import { FlightService } from './Flight.service';
import { JackAndFerdiService } from './JackAndFerdi.service';
import CarrierService from './Carrier.service';
import { WeatherTime } from './WeatherTime.service';
import { DialogflowService } from './Dialogflow.service';
import { FlightHelper } from '../helpers/flight.helper';

const logger = createLogger(
  'zenner',
  'pushNotifications.service',
  'zenner-common',
);
export class pushNotificationsService {
  private DFService = new DialogflowService();
  private minuteInMS = 60000;
  private secondsInMS = 1000;
  private hourInMS = 3600000;
  private dayInMS = this.hourInMS * 24;
  public userRepository: Repository<User>;
  public flightRepository: Repository<Flight>;
  public userTripRepository: Repository<UserTrip>;
  public tripRepository: Repository<Trip>;
  public flightService: FlightService;
  public jackAndFerdiService: JackAndFerdiService;
  public bqOutgoing: BQ;
  public db: Connection;
  public services: any;
  private weatherTimeService;
  private weatherAPIDescriptionSwitch: { [key: string]: string } = {
    'clear sky': 'sunny',
    'broken clouds': 'cloudy',
    'scattered clouds': 'cloudy',
    'few clouds': 'cloudy',
    'overcast clouds': 'cloudy',
    'light rain': 'rainy',
    'moderate rain': 'rainy',
    'light intensity drizzle': 'rainy',
    'heavy intensity rain': 'rainy',
    'shower rain': 'rainy',
    'light intensity shower rain': 'rainy',
    'light intensity drizzle rain': 'rainy',
    'light snow': 'snowy',
    snow: 'snowy',
  };
  private weatherDescriptionItemSwitch: { [key: string]: string } = {
    sunny: 'sunglasses 🕶️', //23+
    snowy: 'gloves 🧤',
    rainy: 'umbrella ☂️',
    cloudy: 'scarf 🧣', //20-
  };
  constructor(db: Connection, setCron = false) {
    if (db) {
      /*istanbul ignore next*/ this.userRepository = db.getRepository(User);
      this.flightRepository = db.getRepository(Flight);
      this.userTripRepository = db.getRepository(UserTrip);
      this.tripRepository = db.getRepository(Trip);
    }
    this.jackAndFerdiService = new JackAndFerdiService(db);
    this.weatherTimeService = new WeatherTime();
    const redisConfig = {
      host: ConfigService.get('REDIS_HOST'),
      port: Number(ConfigService.get('REDIS_PORT')),
    };
    this.bqOutgoing = new BQ('OUTGOING_MESSAGE', {
      isWorker: false,
      redis: redisConfig,
    });
    this.db = db;
    this.services = {
      db: this.db,
      bq: this.bqOutgoing,
    };
    this.flightService = new FlightService(db);
    if (setCron === true) {
      setInterval(this.chrono.bind(this), 1000 * 60 * 2); //every 2 minutes
    }
  }
  public async getFlights(): Promise<Flight[]> {
    const date = new Date();
    date.setDate(date.getDate() - 5);

    return await this.flightRepository.find({ relations: ['trips'] });
  }
  public async getUsersThatNotConfirmed(): Promise<User[]> {
    return await this.userRepository.find({
      where: {
        confirmed_messaging: false,
      },
    });
  }

  private async UserNotifcationSendMessage(user: User, msg: string) {
    if (user.whatsapp) {
      sendMessage(
        'whatsapp:+' + user.mobile,
        'whatsapp:+' + ConfigService.get('PRIMARY_PHONE'),
        'whatsapp',
        msg,
        this.services,
      );
    } /*istanbul ignore next*/ else {
      sendMessage(
        '+' + user.mobile,
        '+' + ConfigService.get('PRIMARY_PHONE'),
        'sms',
        msg,
        this.services,
      );
    }
  }
  private async sendMessage(user: User, msg: string) {
    if (!user.confirmed_messaging) {
      return;
    }
    if (user.whatsapp) {
      sendMessage(
        'whatsapp:+' + user.mobile,
        'whatsapp:+' + ConfigService.get('PRIMARY_PHONE'),
        'whatsapp',
        msg,
        this.services,
      );
    } /*istanbul ignore next*/ else {
      sendMessage(
        '+' + user.mobile,
        '+' + ConfigService.get('PRIMARY_PHONE'),
        'sms',
        msg,
        this.services,
      );
    }
  }
  public async FeedBackNotifications(
    allFlights: Flight[],
    today: Date,
  ): Promise<Flight[]> {
    allFlights = allFlights.filter(
      (flight) =>
        ['arrived', 'landed'].includes(flight.statusName) &&
        today.getTime() - new Date(flight.estimatedArrival).getTime() >=
          60 * this.minuteInMS &&
        today.getTime() - new Date(flight.estimatedArrival).getTime() <=
          70 * this.minuteInMS,
    );
    return await Promise.all(
      allFlights.map(async (flight) => {
        if (!flight.trips.some((trip) => trip.sent_feedback === false)) {
          return;
        }
        await Promise.all(
          flight.trips.map(async (trip) => {
            if (trip.sent_feedback) {
              return;
            }
            const userTrips = await this.userTripRepository.find({
              where: {
                trip_id: trip.trip_id,
              },
            });
            if (userTrips.length > 0) {
              const users = await this.userRepository.find({
                where: {
                  id: In(userTrips.map((userTrip) => userTrip.user_id)),
                },
              });
              logger.info(
                `Sending the feedback message to ${users.map(
                  (user) => user.name,
                )}`,
              );
              await Promise.all(
                users.map(async (user) => {
                  const nextFlight = await this.flightService.getNextFlightByUser(
                    user.id,
                  );
                  if (
                    !(
                      !nextFlight ||
                      nextFlight.scheduledDeparture.getTime() -
                        today.getTime() >
                        this.dayInMS * 7
                    )
                  ) {
                    return;
                  }
                  const msg = Il8nService.get('zenny:feedback:first', {
                    templateVars: {
                      name: user.firstName,
                    },
                  });

                  const from =
                    'whatsapp:+' + ConfigService.get('PRIMARY_PHONE');
                  const to = 'whatsapp:+' + user.mobile;
                  const identifier = from + '_' + to;
                  const sessionPath = this.DFService.getSessionPath(identifier);
                  const request = {
                    parent: sessionPath,
                    context: {
                      name: sessionPath + '/contexts/first_feedback',
                      lifespanCount: 2,
                    },
                  };
                  await this.DFService.contextClient.createContext(request);
                  await this.sendMessage(user, msg);
                }),
              );
            }
            trip.sent_feedback = true;
            await this.tripRepository.save(trip);
          }),
        );
        return flight;
      }),
    );
  }
  public async twoDaysBeforeNotifications(
    allFlights: Flight[],
    today: Date,
  ): Promise<Flight[]> {
    allFlights = allFlights.filter(
      (flight) =>
        new Date(flight.scheduledDeparture).getTime() - today.getTime() >=
          47.5 * this.hourInMS &&
        new Date(flight.scheduledDeparture).getTime() - today.getTime() <=
          48 * this.hourInMS,
    );
    return await Promise.all(
      allFlights.map(async (flight) => {
        await Promise.all(
          flight.trips.map(async (trip) => {
            if (trip.sent_48hrs === false) {
              logger.info(
                `Trip ${trip.trip_id} may need to send T-48 notification`,
              );
              const userTrips = await this.userTripRepository.find({
                where: {
                  trip_id: trip.trip_id,
                },
              });
              if (userTrips.length > 0) {
                const users = await this.userRepository.find({
                  where: {
                    id: In(userTrips.map((userTrip) => userTrip.user_id)),
                  },
                });
                await Promise.all(
                  users.map(async (user) => {
                    logger.info(
                      `Prepare T-48 message for User #${user.id} on ${flight.flightNo}`,
                    );
                    let msg = '';
                    const airtableService = AirtableFactory.create();
                    const destinationAirport = flight.destination;
                    try {
                      const airport = await airtableService.getValidPickups({
                        'IATA Code': destinationAirport,
                      });
                      if (airport[0]?.fields['IATA Code']) {
                        msg = Il8nService.get('zenny:notifications:48hrs', {
                          templateVars: {
                            name: user.firstName,
                            destination: destinationAirport,
                          },
                        });
                        this.sendMessage(user, msg);
                        logger.info(`SENT 48 ${flight.flightNo}`);
                      }
                    } catch (err) {
                      logger.error(err);
                    }
                  }),
                );
              }
              trip.sent_48hrs = true;
              await this.tripRepository.save(trip);
            }
          }),
        );
        return flight;
      }),
    );
  }

  public async dayBeforeNotifications(
    allFlights: Flight[],
    today: Date,
  ): Promise<Flight[]> {
    allFlights = allFlights.filter(
      (flight) =>
        new Date(flight.scheduledDeparture).getTime() - today.getTime() >=
          82800000 &&
        new Date(flight.scheduledDeparture).getTime() - today.getTime() <=
          86400000,
    );
    return await Promise.all(
      allFlights.map(async (flight) => {
        await Promise.all(
          flight.trips.map(async (trip) => {
            if (trip.sent_24hrs === false) {
              logger.info(
                `Trip ${trip.trip_id} may need to send T-24 notification`,
              );
              let tripFlight;
              let finalFlight: Flight, currentFlight: Flight;
              //Two connectiong flights will always have sequential trip_flight id's
              logger.info(`SEQ of flight :${flight.seq}`);
              if (trip.seq === 1) {
                trip.sent_24hrs = true;

                await this.tripRepository.save(trip);
                return flight;
              }
              if (trip.seq === 0) {
                trip.sent_JNF = true;
                trip.sent_directions = true; //we dont need these messages on the first leg

                await this.tripRepository.save(trip);
                tripFlight = await this.tripRepository.find({
                  where: {
                    trip_id: trip.trip_id,
                  },
                  order: {
                    flight_trip_id: 'DESC',
                  },
                });
                finalFlight = tripFlight[0].flight;

                currentFlight = flight;
              } else {
                finalFlight = flight;
                currentFlight = flight;
              }
              const userTrips = await this.userTripRepository.find({
                where: {
                  trip_id: trip.trip_id,
                },
              });
              if (userTrips.length > 0) {
                const users = await this.userRepository.find({
                  where: {
                    id: In(userTrips.map((userTrip) => userTrip.user_id)),
                  },
                });
                await Promise.all(
                  users.map(async (user) => {
                    logger.info(
                      `Prepare T-24 message for User #${user.id} on ${flight.flightNo}`,
                    );

                    const msg = Il8nService.get('zenny:notifications:24hrs', {
                      templateVars: {
                        name: user.firstName,
                        city:
                          currentFlight.initialLumoData?.flight?.destination
                            ?.city ||
                          currentFlight.initialFlightStatsData?.arrivalAirport
                            ?.city,
                        sentence: Il8nService.get(
                          `zenny:riskmessage:${getRiskWord(
                            parseInt(currentFlight.lumoRisk) * 10,
                          )}`,
                        ),
                      },
                    });
                    let msg2 = '';
                    const airtabelService = AirtableFactory.create();
                    const airport = currentFlight.origin;
                    const terminal = currentFlight.originTerminal;

                    try {
                      const airline = await airtabelService.getAirlinesWebsitesLink(
                        {
                          AirLine:
                            currentFlight.operatingCarrier ||
                            currentFlight.marketedCarrier ||
                            currentFlight.flightNo.substr(0, 2),
                        },
                      );

                      const link = airline[0].fields.Link;
                      msg2 = Il8nService.get('zenny:notifications:24hrs3', {
                        templateVars: {
                          airline: CarrierService.search(
                            currentFlight.operatingCarrier,
                          )[0].name,
                          link: link,
                        },
                      });
                    } catch (err) {
                      logger.error(err);
                    }

                    let msg3 = '';
                    /* istanbul ignore next */
                    try {
                      let tips = await airtabelService.getMessageForAirport({
                        airportCode: airport,
                        active: true,
                        terminal: terminal,
                      });
                      if (terminal !== null) {
                        tips = tips.concat(
                          await airtabelService.getMessageForAirport({
                            airportCode: airport,
                            active: true,
                            terminal: '_',
                          }),
                        );
                      }
                      if (tips[0] !== undefined) {
                        msg3 = `Here are some tips for ${airport} ${
                          !terminal ? '' : `in terminal ${terminal}`
                        }: \n`;
                        tips.forEach((tip: any) => {
                          msg3 += `* ${tip.fields.message}\n`;
                        });
                      } else {
                        msg3 = null;
                      }
                    } catch (e) {
                      logger.error(e);
                    }

                    let msg4 = '';
                    try {
                      let secondSentence = '';
                      const city =
                        finalFlight.initialLumoData?.flight?.destination
                          ?.city ||
                        finalFlight.initialFlightStatsData?.arrivalAirport
                          ?.city;
                      let description = await this.weatherTimeService.getWeatherDescription(
                        city,
                      );
                      const temperature = await this.weatherTimeService.getTemperature(
                        city,
                      );
                      if (!temperature || !description) {
                        throw 'missing temparture or descripiton for city';
                      }
                      if (
                        Object.keys(this.weatherAPIDescriptionSwitch).includes(
                          description,
                        )
                      ) {
                        description = this.weatherAPIDescriptionSwitch[
                          description
                        ];
                        const item = this.weatherDescriptionItemSwitch[
                          description
                        ];
                        secondSentence = Il8nService.get(
                          'zenny:notifications:24hrsWeather2',
                          {
                            templateVars: {
                              item,
                            },
                          },
                        );
                      }
                      msg4 = Il8nService.get(
                        'zenny:notifications:24hrsWeather',
                        {
                          templateVars: {
                            city,
                            description,
                            ftemp: Math.round(kelvinToFahrenheit(temperature)),
                            ctemp: Math.round(kelvinToCelsius(temperature)),
                            sentence: secondSentence,
                          },
                        },
                      );
                    } catch (e) {
                      logger.error(e);
                    }

                    this.sendMessage(user, msg);
                    if (msg2) {
                      this.sendMessage(user, msg2);
                    }
                    if (msg3) {
                      this.sendMessage(user, msg3);
                    }
                    if (msg4) {
                      this.sendMessage(user, msg4);
                    }
                  }),
                );
              }
              trip.sent_24hrs = true;
              await this.tripRepository.save(trip);
            }
          }),
        );
        return flight;
      }),
    );
  }

  public async predepartureMessages(
    allFlights: Flight[],
    today: Date,
  ): Promise<Flight[]> {
    allFlights = allFlights.filter(
      (flight) =>
        new Date(flight.scheduledDeparture).getTime() - today.getTime() >=
          12600000 &&
        new Date(flight.scheduledDeparture).getTime() - today.getTime() <=
          14460000,
    );
    return await Promise.all(
      allFlights.map(async (flight) => {
        if (!flight.trips.some((trip) => trip.sent_4hrs === false)) {
          return;
        }
        let currentFlight: Flight, finalFlight: Flight;

        const minutesDiff = getDiffMinutes(
          flight.scheduledDeparture.toJSON(),
          flight.estimatedDeparture.toISOString(),
        );

        const status =
          minutesDiff == 0
            ? 'on time'
            : minutesDiff < 0
            ? -minutesDiff + ' minutes late'
            : minutesDiff + ' minutes early';
        const city =
          flight.initialLumoData?.flight?.destination?.city ||
          flight.initialFlightStatsData?.arrivalAirport?.city;
        const weatherDescription =
          (await this.weatherTimeService.getWeatherDescription(city)) ?? null;
        const msg = Il8nService.get('zenny:notifications:4hrs', {
          templateVars: {
            message: status,
            city: city,
            flight_no: flight.flightNo,
            time_until_departure: FlightHelper.roundTimeUntilDeparture(flight),
            weather:
              flight?.initialLumoData?.weather?.destination?.summary ??
              weatherDescription,
          },
        });
        const msg2 =
          flight.originTerminal && flight.originGate
            ? `Your flight will depart from terminal ${flight.originTerminal} gate ${flight.originGate}`
            : flight.originTerminal
            ? `Your flight will depart from terminal ${flight.originTerminal}`
            : flight.originGate
            ? `Your flight will depart from gate ${flight.originGate}`
            : null;

        let directionmessage = '';

        await Promise.all(
          flight.trips.map(async (trip) => {
            if (trip.seq === 1) {
              trip.sent_4hrs = true;
              await this.tripRepository.save(trip);
            }
            if (trip.seq === 0) {
              const tripFlight = await this.tripRepository.find({
                where: {
                  trip_id: trip.trip_id,
                },
                order: {
                  flight_trip_id: 'DESC',
                },
              });
              finalFlight = tripFlight[0].flight;

              currentFlight = flight;
            } else {
              finalFlight = flight;
              currentFlight = flight;
            }
            const distance = new DirectionService(
              new Client(),
              ConfigService.get('GOOGLE_API_KEY'),
            );
            try {
              const city =
                currentFlight.initialLumoData?.flight?.origin?.city ||
                currentFlight.initialFlightStatsData?.departureAirport?.city;
              const fromLocation =
                city +
                  ', ' +
                  currentFlight.initialFlightStatsData?.departureAirport
                    ?.countryName ||
                (await getCity({ db: this.db, bq: null }, city)).country;
              const toLocation =
                currentFlight.initialFlightStatsData?.departureAirport?.name ||
                currentFlight.initialLumoData?.flight?.origin?.name +
                  '(' +
                  currentFlight.origin +
                  ')';
              const car = await distance.getFastestWay(
                fromLocation,
                toLocation,
                TravelMode.driving,
              );

              if (car.status === 'OK') {
                directionmessage =
                  '\n\n' +
                  Il8nService.get('zenny:notifications:4hrsdirection', {
                    templateVars: {
                      city: fromLocation,
                      airport: toLocation,
                      link: `https://www.google.com/maps/dir/?api=1&origin=${encodeURI(
                        fromLocation,
                      )}&destination=${encodeURI(toLocation)}`,
                      time: car.duration.text,
                    },
                  });
              }
            } catch (err) {
              logger.error(err);
            }

            if (trip.sent_4hrs === false) {
              const userTrips = await this.userTripRepository.find({
                where: {
                  trip_id: trip.trip_id,
                },
              });
              if (userTrips.length > 0) {
                const users = await this.userRepository.find({
                  where: {
                    id: In(userTrips.map((userTrip) => userTrip.user_id)),
                  },
                });
                users.map(async (user) => {
                  logger.info(
                    `Need to send T-4 to ${user.id} for trip ${
                      trip.trip_id
                    } on ${finalFlight.flightNo}\nthis flight is ${
                      trip.seq
                        ? `a connection(seq=${trip.seq}`
                        : 'not a connection'
                    }`,
                  );
                  this.sendMessage(user, msg + directionmessage);
                  this.sendMessage(user, msg2);
                });
              }
              trip.sent_4hrs = true;
              await this.tripRepository.save(trip);
            }
          }),
        );
        return flight;
      }),
    );
  }

  public async baggageNotification(
    allFlights: Flight[],
    today: Date,
  ): Promise<Flight[]> {
    allFlights = allFlights.filter(
      (flight) =>
        ['arrived', 'landed'].includes(flight.statusName) &&
        today.getTime() - new Date(flight.estimatedArrival).getTime() >=
          1 * this.minuteInMS &&
        today.getTime() - new Date(flight.estimatedArrival).getTime() <=
          4 * this.minuteInMS,
    );
    return await Promise.all(
      allFlights.map(async (flight) => {
        if (!flight.destinationBaggage) {
          return;
        }
        const msg = Il8nService.get('zenny:landing:baggage', {
          templateVars: {
            bag: flight.destinationBaggage,
          },
        });
        await Promise.all(
          flight.trips.map(async (trip) => {
            if (trip.sent_baggage) {
              return;
            }
            const userTrips = await this.userTripRepository.find({
              where: {
                trip_id: trip.trip_id,
              },
            });
            if (userTrips.length > 0) {
              const users = await this.userRepository.find({
                where: {
                  id: In(userTrips.map((userTrip) => userTrip.user_id)),
                },
              });
              logger.info(
                `Sending the baggage message to ${users.map(
                  (user) => user.name,
                )}`,
              );
              await Promise.all(
                users.map(async (user) => {
                  await this.sendMessage(user, msg);
                }),
              );
            }
            trip.sent_baggage = true;
            await this.tripRepository.save(trip);
          }),
        );
        return flight;
      }),
    );
  }

  public async directionsNotification(
    allFlights: Flight[],
    today: Date,
  ): Promise<Flight[]> {
    allFlights = allFlights.filter(
      (flight) =>
        ['arrived', 'landed'].includes(flight.statusName) &&
        today.getTime() - new Date(flight.estimatedArrival).getTime() >=
          15 * this.minuteInMS &&
        today.getTime() - new Date(flight.estimatedArrival).getTime() <=
          18 * this.minuteInMS,
    );
    return await Promise.all(
      allFlights.map(async (flight) => {
        if (!flight.trips.some((trip) => trip.sent_directions === false)) {
          return;
        }
        let msg = '';
        const distance = new DirectionService(
          new Client(),
          ConfigService.get('GOOGLE_API_KEY'),
        );
        try {
          const city =
            flight.initialLumoData?.flight?.destination?.city ||
            flight.initialFlightStatsData?.arrivalAirport?.city;
          const airport =
            flight.initialFlightStatsData?.arrivalAirport?.name ||
            flight.initialLumoData?.flight?.destination.name;
          const fromLocation = airport + '(' + flight.destination + ')';
          const toLocation =
            city +
              ', ' +
              flight.initialFlightStatsData?.arrivalAirport?.countryName ||
            (await getCity({ bq: null, db: this.db }, city)).country;

          const [car, transit] = await Promise.all([
            distance.getFastestWay(
              fromLocation,
              toLocation,
              TravelMode.driving,
            ),
            distance.getFastestWay(
              fromLocation,
              toLocation,
              TravelMode.transit,
            ),
          ]);
          if (car.status === 'OK' && transit.status === 'OK') {
            msg = Il8nService.get('zenny:landing:directions', {
              templateVars: {
                city: city,
                airport: airport,
                taxi_time: car.duration.text,
                public_transportation_time: transit.duration.text,
                link: `https://www.google.com/maps/dir/?api=1&origin=${encodeURI(
                  fromLocation,
                )}&destination=${encodeURI(toLocation)}`,
                time: car.duration.text,
              },
            });
          }
        } catch (err) {
          logger.error(err);
        }
        if (!msg) {
          return;
        }
        await Promise.all(
          flight.trips.map(async (trip) => {
            if (trip.sent_directions) {
              return;
            }
            const userTrips = await this.userTripRepository.find({
              where: {
                trip_id: trip.trip_id,
              },
            });
            if (userTrips.length > 0) {
              const users = await this.userRepository.find({
                where: {
                  id: In(userTrips.map((userTrip) => userTrip.user_id)),
                },
              });
              logger.info(
                `Sending the directions message to ${users.map(
                  (user) => user.name,
                )}`,
              );
              users.map((user) => {
                this.sendMessage(user, msg);
              });
            }
            trip.sent_directions = true;
            await this.tripRepository.save(trip);
          }),
        );
        return flight;
      }),
    );
  }

  public async JNFNotification(
    allFlights: Flight[],
    today: Date,
  ): Promise<Flight[]> {
    allFlights = allFlights.filter(
      (flight) =>
        ['arrived', 'landed'].includes(flight.statusName) &&
        today.getTime() - new Date(flight.estimatedArrival).getTime() >=
          16 * this.minuteInMS &&
        today.getTime() - new Date(flight.estimatedArrival).getTime() <=
          19 * this.minuteInMS,
    );
    return await Promise.all(
      allFlights.map(async (flight) => {
        if (
          !(await this.jackAndFerdiService.getCityName(
            flight.initialLumoData.flight.destination.city ||
              flight.initialFlightStatsData.arrivalAirport?.city,
          ))
        ) {
          return;
        }
        const msg = Il8nService.get('zenny:landing:jnf', {
          templateVars: {
            city:
              flight.initialLumoData.flight.destination.city ||
              flight.initialFlightStatsData.arrivalAirport?.city,
          },
        });
        await Promise.all(
          flight.trips.map(async (trip) => {
            if (trip.sent_JNF) {
              return;
            }
            const userTrips = await this.userTripRepository.find({
              where: {
                trip_id: trip.trip_id,
              },
            });

            if (userTrips.length > 0) {
              const users = await this.userRepository.find({
                where: {
                  id: In(userTrips.map((userTrip) => userTrip.user_id)),
                },
              });
              logger.info(
                `Sending the JNF message to ${users.map((user) => user.name)}`,
              );
              users.map((user) => {
                this.sendMessage(user, msg);
              });
            }
            trip.sent_JNF = true;
            await this.tripRepository.save(trip);
          }),
        );
        return flight;
      }),
    );
  }

  public async dayAfterRegisterNotification(
    allUsers: User[],
    today: Date,
  ): Promise<User[]> {
    allUsers = allUsers.filter(
      (user) =>
        user.sent_24afterRegister === false &&
        today.getTime() - new Date(user.created).getTime() >
          24 * this.hourInMS &&
        today.getTime() - new Date(user.created).getTime() < 26 * this.hourInMS,
    );
    return Promise.all(
      allUsers.map(async (user) => {
        this.UserNotifcationSendMessage(
          user,
          Il8nService.get('zenny:user:24HrAfterRegister'),
        );
        user.sent_24afterRegister = true;
        await this.userRepository.save(user);
        return user;
      }),
    );
  }

  public async dayBeforeFlightUserNotConfirmed(
    allUsers: User[],
    today: Date,
  ): Promise<User[]> {
    allUsers = await Promise.all(
      allUsers.map(async (user) => {
        const nextDeparture = await this.flightService.getNextDepartureFlight(
          user.id,
        );
        if (nextDeparture === undefined) {
          return undefined;
        } else {
          return nextDeparture.scheduledDeparture.getTime() - today.getTime() <
            25 * this.hourInMS &&
            nextDeparture.scheduledDeparture.getTime() - today.getTime() >
              23 * this.hourInMS &&
            user.sent_24beforeFlight === false
            ? user
            : undefined;
        }
      }),
    );
    allUsers = allUsers.filter((user) => user !== undefined);
    return await Promise.all(
      allUsers.map(async (user) => {
        if (
          (
            await this.flightService.getNextDepartureFlight(user.id)
          ).scheduledDeparture.getTime() -
            user.created.getTime() <
          27 * this.hourInMS
        ) {
          if (
            today.getTime() - user.created.getTime() > 2 * this.hourInMS &&
            !user.sent_24afterRegister
          ) {
            await this.UserNotifcationSendMessage(
              user,
              Il8nService.get('zenny:user:24HrAfterRegister'),
            );
            user.sent_24afterRegister = true;
          }
        } else {
          await this.UserNotifcationSendMessage(
            user,
            Il8nService.get('zenny:user:24HrBeforeFlight'),
          );
        }

        user.sent_24beforeFlight = true;
        await this.userRepository.save(user);
        return user;
      }),
    );
  }

  public async airportPlacesNotification(allFlights: Flight[], today: Date) {
    allFlights = allFlights.filter(
      (flight) =>
        new Date(flight.scheduledDeparture).getTime() - today.getTime() >=
          1.5 * this.hourInMS &&
        new Date(flight.scheduledDeparture).getTime() - today.getTime() <=
          2 * this.hourInMS,
    );
    return await Promise.all(
      allFlights.map(async (flight) => {
        if (!flight.trips.some((trip) => trip.sent_JNFAirport === false)) {
          return;
        }
        if (
          (await this.jackAndFerdiService.getPlacesInAirport(flight.origin)) ===
          Il8nService.get('zenny:jackandferdi:notfound', {
            templateVars: {
              city: flight.origin,
            },
          })
        ) {
          return;
        }
        await Promise.all(
          flight.trips.map(async (trip) => {
            if (trip.sent_JNFAirport) {
              return;
            }
            const userTrips = await this.userTripRepository.find({
              where: {
                trip_id: trip.trip_id,
              },
            });

            if (userTrips.length === 0) {
              return;
            }
            const msg = Il8nService.get('zenny:notifications:2hrs', {
              templateVars: { airport: flight.origin },
            });
            const users = await this.userRepository.find({
              where: {
                id: In(userTrips.map((userTrip) => userTrip.user_id)),
              },
            });
            logger.info(
              `Sending the 2hrs message to ${users.map((user) => user.name)}`,
            );
            users.map((user) => {
              const from = 'whatsapp:+' + ConfigService.get('PRIMARY_PHONE');
              const to = 'whatsapp:+' + user.mobile;
              const identifier = from + '_' + to;
              const sessionPath = this.DFService.getSessionPath(identifier);
              const request = {
                parent: sessionPath,
                context: {
                  name: sessionPath + '/contexts/jnf_airport',
                  lifespanCount: 1,
                },
              };
              this.DFService.contextClient.createContext(request);
              this.sendMessage(user, msg);
            });
            trip.sent_JNFAirport = true;
            await this.tripRepository.save(trip);
          }),
        );
        return flight;
      }),
    );
  }

  public async chrono(): Promise<void> {
    const startTime = new Date();
    logger.debug('Chrono function running');
    //get all flights
    const allFlights = await this.getFlights();
    const allUsers = await this.getUsersThatNotConfirmed();
    await this.twoDaysBeforeNotifications(allFlights, startTime);
    await this.dayBeforeNotifications(allFlights, startTime);
    await this.predepartureMessages(allFlights, startTime);
    await this.baggageNotification(allFlights, startTime);
    await this.directionsNotification(allFlights, startTime);
    await this.JNFNotification(allFlights, startTime);
    await this.FeedBackNotifications(allFlights, startTime);
    await this.dayAfterRegisterNotification(allUsers, startTime);
    await this.dayBeforeFlightUserNotConfirmed(allUsers, startTime);
    await this.airportPlacesNotification(allFlights, startTime);
    const finishTime = new Date();
    logger.info(
      `finished chrono function after ${
        (finishTime.getTime() - startTime.getTime()) / this.secondsInMS
      } seconds`,
    );
  }
}
