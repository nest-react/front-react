import { Connection, Repository } from 'typeorm';
import ApiAudit from '../model/ApiAudit';

export interface AuditRecord {
  serviceClass: string;

  hostname: string;

  url: string;

  request?: Record<string, any>;

  response?: Record<string, any>;

  outgoing: boolean;

  duration: number;

  method: string;
}

export class ApiAuditService {
  private readonly repository: Repository<ApiAudit>;

  constructor(db: Connection) {
    this.repository = db.getRepository(ApiAudit);
  }

  public async log(record: AuditRecord): Promise<boolean> {
    try {
      await this.repository.save(record);
      return true;
    } catch {
      return false;
    }
  }
}
