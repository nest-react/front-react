import axios from 'axios';
import { createLogger } from '../util/logger';
import ConfigService from './Config.service';

interface OpenWeatherArg {
  dt: string;
  weather: [{ description: string }];
  main: { temp: number };
}

const logger = createLogger('zenner', 'weathertime.service', 'zenner-common');

export class WeatherTime {
  private url = `https://api.openweathermap.org/data/2.5/weather?appid=${ConfigService.get(
    'OPENWEATEHRMAP_API_KEY',
  )}&q=`;

  private handleError(err: any): null {
    logger.error('An error occurred', { error: err });
    return null;
  }

  buildURL(city: string): string {
    return `${this.url}${city}`;
  }
  async getTemperature(
    city: string,
  ): Promise<OpenWeatherArg['main']['temp']> | null {
    try {
      const uri = this.buildURL(city);
      const reply = await axios.get(uri);
      const result = <OpenWeatherArg>reply.data;
      return Math.round(result.main.temp);
    } catch (err) {
      logger.error(`Failed to get temperature`, { error: err });
    }
    return null;
  }

  async getWeatherDescription(
    city: string,
  ): Promise<OpenWeatherArg['weather'][0]['description']> | null {
    const uri = this.buildURL(city);
    const reply = await axios.get(uri).catch(this.handleError);
    const result = <OpenWeatherArg>reply.data;
    return result.weather[0].description;
  }

  async getDateForCity(city: string): Promise<Date> | null {
    const serverResponse = (
      await axios
        .get(
          `https://maps.googleapis.com/maps/api/geocode/json?&address=${city}&key=${process.env.GOOGLE_API_KEY}`,
        )
        .catch(this.handleError)
    ).data;

    if (typeof serverResponse.error_message !== 'undefined') {
      throw new Error(
        serverResponse.status + ' - ' + serverResponse.error_message,
      );
    }

    const location = serverResponse.results[0].geometry.location;
    const coords = location.lat + ',' + location.lng;
    const timestamp = Math.floor(Date.now() / 1000);

    const time = (
      await axios
        .get(
          `https://maps.googleapis.com/maps/api/timezone/json` +
            `?location=${coords}` +
            `&timestamp=${timestamp}` +
            `&sensor=false&key=${process.env.GOOGLE_API_KEY}`,
        )
        .catch(this.handleError)
    ).data;

    return new Date(
      new Date().toLocaleString('en-US', {
        timeZone: time.timeZoneId,
      }),
    );
  }

  async getTime(city: string): Promise<OpenWeatherArg['dt']> | null {
    let Response = '';
    const serverResponse = (
      await axios
        .get(
          `https://maps.googleapis.com/maps/api/geocode/json?&address=${city}&key=${ConfigService.get(
            'GOOGLE_API_KEY',
          )}`,
        )
        .catch(this.handleError)
    ).data;

    if (typeof serverResponse.error_message !== 'undefined') {
      throw new Error(
        serverResponse.status + ' - ' + serverResponse.error_message,
      );
    }

    const location = serverResponse.results[0].geometry.location;
    const coords = location.lat + ',' + location.lng;
    const timestamp = Math.floor(Date.now() / 1000);

    const time = (
      await axios
        .get(
          `https://maps.googleapis.com/maps/api/timezone/json` +
            `?location=${coords}` +
            `&timestamp=${timestamp}` +
            `&sensor=false&key=${ConfigService.get('GOOGLE_API_KEY')}`,
        )
        .catch(this.handleError)
    ).data;

    const dateObj = new Date().toLocaleString('en-US', {
      timeZone: time.timeZoneId,
    });
    Response = String(dateObj.split(',')[1]).trim();
    Response =
      Response.split(':')[0] +
      ':' +
      Response.split(':')[1] +
      ' ' +
      Response.slice(8);
    return Response;
  }
}
