import CommsEvent from '../model/CommsEvent';
import { Repository, Connection } from 'typeorm';

export interface AddEventInterface {
  created: Date;
  from: string;
  to: string;
  human?: boolean;
  message: string;
  source: string;
  result: string;
  server: string;
  html?: string;
  subject?: string;
  intent?: string;
}

export class CommsEventService {
  eventRepository: Repository<CommsEvent>;

  constructor(db: Connection) {
    this.eventRepository = db.getRepository(CommsEvent);
  }

  async getLastEvent(from: string, to: string): Promise<CommsEvent> {
    let event;
    try {
      event = await this.eventRepository.findOne({
        where: {
          from,
          to,
        },
        order: {
          created: 'DESC',
        },
      });
      if (!event) {
        return null;
      }
      return event;
    } catch (err) {
      return null;
    }
  }

  async getLastMessageDateFromUser(userPhone: string): Promise<Date> {
    let event;
    try {
      event = await this.eventRepository.findOne({
        where: {
          from: userPhone,
        },
        order: {
          created: 'DESC',
        },
      });
    } catch (err) {
      return null;
    }
    if (!event) {
      return null;
    }
    return event.created;
  }

  async addEvent(record: AddEventInterface): Promise<CommsEvent> {
    const {
      created,
      from,
      to,
      human,
      message,
      source,
      result,
      server,
      intent,
    } = record;
    return await this.eventRepository.save({
      created,
      from,
      to,
      human,
      message,
      source,
      result,
      intent,
      server,
    });
  }
}
