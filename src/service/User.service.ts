import User from '../model/User';
import {
  Repository,
  Connection,
  UpdateResult,
  MoreThan,
  Not,
  LessThan,
} from 'typeorm';
import Flight from '../model/Flight';
import { UserTrip } from '../model/UserTrip';
import { Trip } from '../model/Trip';
import { RegisterUserDTO } from '../interface/dto/webhook.dto';

// Not currently used - should be removed
// interface UserInterface {
//   id: number;
//   created: Date;
//   name: string;
//   firstName: string;
//   middleName: string;
//   lastName: string;
//   gender: string;
//   email: string;
//   mobile: string;
//   whatsapp: string;
// }

export class UserService {
  userRepository: Repository<User>;

  constructor(db: Connection) {
    /* istanbul ignore next */
    if (db) {
      this.userRepository = db.getRepository(User);
    }
    setInterval(this.autoUnmute.bind(this), 1000 * 60 * 1); //every 1 minute
  }

  async getAllNewUsers(): Promise<User[]> {
    const yesterday = new Date();
    yesterday.setDate(new Date().getDate() - 1);
    return await this.userRepository.find({
      where: {
        created: MoreThan(yesterday),
      },
    });
  }

  async findByEmail(email: string): Promise<User> {
    const user = await this.userRepository.findOne({
      where: { email: email },
    });
    return user;
  }

  async getUserWithFlightsByField(
    field: keyof User,
    value: string | number | boolean,
  ): Promise<User> {
    return this.userRepository
      .createQueryBuilder('user')
      .leftJoinAndMapMany(
        'user.trip',
        UserTrip,
        'user_trip',
        'user_trip.user_id=user.id',
      )
      .leftJoinAndMapMany(
        'user.trip',
        Trip,
        'trip',
        'trip.id=user_trip.trip_id',
      )
      .leftJoinAndMapMany(
        'trip.flights',
        Flight,
        'flights',
        'flights.id=trip_flight.flight_id',
      )
      .where(`${field}=:${field}`, { [field]: value })
      .getOne();
  }

  async autoUnmute() {
    const HourAgo = new Date();
    HourAgo.setHours(HourAgo.getDate() - 1);
    await this.userRepository.update(
      {
        muteBot: true,
        mutedAt: LessThan(HourAgo),
      },
      {
        muteBot: false,
      },
    );
  }

  async findByMobile(mobile: string): Promise<User> {
    const user = await this.userRepository.findOne({
      where: { mobile: mobile },
    });
    return user;
  }

  async muteBot(mobile: string, isMuted: boolean): Promise<UpdateResult> {
    return this.userRepository.update(
      {
        mobile,
      },
      {
        muteBot: isMuted,
        mutedAt: new Date(),
      },
    );
  }

  async createUser(payload: any): Promise<User> {
    return this.userRepository.save(new User(payload));
  }

  async findById(id: number): Promise<User> {
    const user = await this.userRepository.findOne({
      where: { id: id },
    });
    return user;
  }

  //returns back the users that fit the parameters
  async checkIfDuplicateEmail(userDTO: RegisterUserDTO): Promise<User[]> {
    const query = this.userRepository
      .createQueryBuilder()
      .where({ mobile: Not(userDTO.mobile), email: userDTO.email });

    return query.getMany();
  }

  async updateUser(user: User): Promise<User> {
    return this.userRepository.save(user);
  }
}
