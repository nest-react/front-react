import { AxiosInstance } from 'axios';
import { Cacheable } from '@type-cacheable/core';
import * as qs from 'qs';
import { Connection, In, Repository } from 'typeorm';

import { CustomStrategy } from './CacheStrategy';
import { HttpClient } from './HttpClient';
import { ConfigService } from './';
// interfaces
import {
  FlightResponse,
  ValidateRequest,
  ValidationResults,
} from '../interface/lumo';
import {
  SearchOptions,
  SearchResponse,
} from '../interface/lumo/lumoSearch.interface';
import { StatusOptions } from '../interface/lumo/lumoStatus.interface';
import { AlternatesResponse } from '../interface/lumo/lumoAlternates.interface';
import { ConnectionResponse } from '../interface/lumo/lumoConnection.interface';
// utils
import { parseData } from '../util';
import {
  getMetropolitanAirports,
  isMetropolitanAirport,
} from '../util/metropolitanAirportCodes.utils';
import {
  SubscriptionOptions,
  SubscriptionResponse,
} from '../interface/lumo/lumoSubscriptions.interface';
import { LumoSubscription } from '../model/LumoSubscription';
import Flight from '../model/Flight';
import { createLogger } from '../util/logger';

const logger = createLogger('zenner', 'Lumo.service', 'zenner-common');

const strategy = new CustomStrategy();

export class LumoService extends HttpClient {
  private http: AxiosInstance;
  subscriptionRepository: Repository<LumoSubscription>;
  flightRepository: Repository<Flight>;
  private readonly WEBHOOK_URL: string;

  constructor(db: Connection) {
    super();
    this.subscriptionRepository = db.getRepository(LumoSubscription);
    this.flightRepository = db.getRepository(Flight);
    this.WEBHOOK_URL = ConfigService.get('LUMO_WEBHOOK_URL');
    this.http = this.create({
      baseURL: ConfigService.get('LUMO_API'),
      headers: {
        Token: ConfigService.get('LUMO_TOKEN'),
      },
    });
  }

  private setAirportProperty(
    airports: string[],
    place: 'destination' | 'origin',
    searchOptions: SearchOptions,
  ): SearchOptions[] {
    return airports.map<SearchOptions>((airport) => {
      return {
        ...searchOptions,
        [place]: airport,
      };
    });
  }

  private getSearchCombinations = (
    options: SearchOptions | SearchOptions[],
  ): SearchOptions[] => {
    if (Array.isArray(options)) {
      const combinations: SearchOptions[] = [];

      options.forEach((option) => {
        combinations.push(...this.getCombination(option));
      });

      return combinations;
    }
    return this.getCombination(options);
  };

  private getCombination(options: SearchOptions): SearchOptions[] {
    const { origin, destination, ...searchOptions } = options;
    const hasMultipleOrigin: boolean = origin && isMetropolitanAirport(origin);
    const hasMultipleDestination: boolean =
      destination && isMetropolitanAirport(destination);
    // multiple search and combine result
    if (hasMultipleOrigin || hasMultipleDestination) {
      //
      const origins = hasMultipleOrigin
        ? getMetropolitanAirports(origin)
        : [origin];
      const destinations = hasMultipleDestination
        ? getMetropolitanAirports(destination)
        : [destination];

      // Possible cases with origin and destination:
      // (case 1) origin and destination airport is metropolitan
      //
      // (case 2) destination  is metropolitan and origin is airport
      // (case 3) origin is metropolitan and destination is airport
      //
      // (case 4) destination is not specified and origin metropolitan
      // (case 5) origin is not specified  and destination metropolitan

      // case 4, case 5
      if (!origin || !destination) {
        if (origin) {
          return this.setAirportProperty(origins, 'origin', searchOptions);
        }

        if (destination) {
          return this.setAirportProperty(
            destinations,
            'destination',
            searchOptions,
          );
        }
      }

      // case 1, case 2, case 3
      // origin is simple airport or metropolitan
      const searchOptionsOrigin: SearchOptions[] = this.setAirportProperty(
        origins,
        'origin',
        searchOptions,
      );

      // destination is airport or metropolitan
      return searchOptionsOrigin.reduce(
        (acc, options) =>
          acc.concat(
            this.setAirportProperty(destinations, 'destination', options),
          ),
        [],
      );
    }
    return [options];
  }

  private mergeSearchResponse(
    response: SearchResponse<FlightResponse>[],
  ): SearchResponse<FlightResponse> {
    return response.reduce(
      (searchResponse, { results, count }) => {
        searchResponse.results.push(...results);
        searchResponse.count += count;
        return searchResponse;
      },
      { count: 0, results: [] } as SearchResponse<FlightResponse>,
    );
  }

  private async getFlightIdsForSubscription(ids: number[]): Promise<string[]> {
    const flights = await this.flightRepository.findByIds(ids, {
      select: ['lumoId'],
    });
    return flights.map((flight) => flight.lumoId);
  }

  // fix the date from UTC to local (2020-05-16T09:30:00-04:00)
  private getFlightId({ flight }: FlightResponse): string {
    // slice timestamp;
    const baseId: string = flight.id.slice(0, -10);
    const scheduledDeparture: string = flight.scheduled_departure
      .substr(2, 8)
      .replace(/\D/g, '');
    return baseId + scheduledDeparture;
  }

  public status(flightID: string): Promise<FlightResponse>;
  public status(options: StatusOptions): Promise<FlightResponse[]>;
  /*
  @Cacheable({
    cacheKey: qs.stringify,
    hashKey: 'flightStatus',
    strategy,
    // 10 min
    ttlSeconds: 60 * 10,
  })
    */

  public status(
    params: string | StatusOptions,
  ): Promise<FlightResponse | FlightResponse[]> {
    if (typeof params === 'string') {
      return this.http
        .get<FlightResponse>(`status/${params}`)
        .then(parseData)
        .catch((e) => {
          logger.error('e', { error: e });
        });
    }

    return this.http
      .get<FlightResponse[]>(`status?${qs.stringify(params)}`)
      .then(parseData)
      .catch(() => null);
  }

  @Cacheable({
    cacheKey: qs.stringify,
    hashKey: 'flightSearch',
    strategy,
    // 10 min
    ttlSeconds: 60 * 10,
  })
  public search(
    options: SearchOptions | SearchOptions[],
  ): Promise<SearchResponse<FlightResponse>> {
    const searchOptions = this.getSearchCombinations(options);
    const promises = searchOptions.map<Promise<SearchResponse<FlightResponse>>>(
      (params) => {
        return this.http
          .get<SearchResponse<FlightResponse>>(`search?${qs.stringify(params)}`)
          .then(parseData);
      },
    );
    return Promise.all(promises).then(this.mergeSearchResponse);
  }

  @Cacheable({
    cacheKey: qs.stringify,
    hashKey: 'flightAlternates',
    strategy,
    // 10 min
    ttlSeconds: 60 * 10,
  })
  public alternates(flightId: string): Promise<AlternatesResponse> {
    return this.http
      .get<AlternatesResponse>(`alternates/${flightId}`)
      .then(parseData);
  }

  public getFutureDate(dateString: string, daysToAdd: number): string {
    const dt: Date = new Date(Date.parse(dateString));
    dt.setDate(dt.getDate() + daysToAdd);
    return dt.toISOString().substr(0, 10);
  }

  private async findDestination(
    flight: string,
    startDate: string,
    departingAfter: string,
    daysToCheck: number,
  ): Promise<FlightResponse | undefined> {
    const days: number[] = [...Array(daysToCheck).keys()];
    for (const day of days) {
      const [carrier, flight_number] = parseFlightID(flight);

      const destinationOptions = {
        carrier,
        flight_number,
        departing_after:
          new Date(departingAfter).toISOString().split('.')[0] + 'Z',
        date: this.getFutureDate(startDate, day),
      };

      const destinationResponse = await this.search(destinationOptions);
      /* istanbul ignore next */
      if (destinationResponse.count === 1) {
        const [destination] = destinationResponse.results;
        return destination;
      }
    }

    /* istanbul ignore next */
    return undefined;
  }

  public connection(
    originFlight: string,
    destinationFlight: string,
  ): Promise<ConnectionResponse>;
  public connection(
    originFlight: string,
    destinationFlight: string,
    date: string,
    originAirport?: string,
  ): Promise<ConnectionResponse>;

  // flight ids of two connecting flights separated by a comma.
  @Cacheable({
    cacheKey: qs.stringify,
    hashKey: 'flightRisk',
    strategy,
    // 10 min
    ttlSeconds: 60 * 10,
  })
  public async connection(
    originFlight: string,
    destinationFlight: string,
    date?: string,
    originAirport?: string,
  ): Promise<ConnectionResponse> {
    if (typeof date !== 'string') {
      return this.http
        .get<ConnectionResponse>(
          `connection?flights=${originFlight},${destinationFlight}`,
        )
        .then(parseData);
    }

    const [carrier, flight_number] = parseFlightID(originFlight);
    const originResponse = await this.search({
      carrier,
      flight_number,
      date,
      // optional origin airport
      /* istanbul ignore next */
      ...(originAirport ? { origin: originAirport } : {}),
    });

    /* istanbul ignore next */
    if (originResponse.count !== 1) {
      throw new Error('Find more than one origin flight');
    }

    const [origin] = originResponse.results;
    const destination = await this.findDestination(
      destinationFlight,
      date,
      origin.flight.scheduled_arrival,
      2,
    );

    /* istanbul ignore next */
    if (!destination) {
      throw new Error('Destination not found');
    }

    return this.connection(
      this.getFlightId(origin),
      this.getFlightId(destination),
    );
  }

  public validateFlights = async (
    options: ValidateRequest,
  ): Promise<ValidationResults> => {
    try {
      const response = await this.search({
        carrier: options.carrier,
        date: options.date,
        flight_number: options.flightNumber,
      });
      const isValid = response.results.length > 0;
      return {
        isValid,
        origin: response.results[0].flight.origin.iata ?? null,
        destination: response.results[0].flight.destination.iata ?? null,
      };
    } catch {
      return {
        isValid: false,
      };
    }
  };

  public async subscription(flightId: number): Promise<boolean>;
  public async subscription(flightId: number[]): Promise<boolean>;
  public async subscription(flightId: number | number[]): Promise<boolean> {
    const flightIds = await this.getIdsForSubscription(flightId);
    if (flightIds.length === 0) return true;
    const flightNumbers = await this.getFlightIdsForSubscription(flightIds);
    try {
      const options: SubscriptionOptions = {
        flight_ids: flightNumbers,
        target: this.WEBHOOK_URL,
      };
      return this.sendLumoRequestAndSaveRecords(flightIds, options);
    } catch (e) {
      logger.error(e);
    }
  }

  public async subscribeByStatusOptions(
    statusOptions: StatusOptions[],
    target: string,
    ids: number | number[],
  ): Promise<boolean> {
    const flightIds = await this.getIdsForSubscription(ids);
    if (flightIds.length === 0) return true;

    const options: SubscriptionOptions = {
      flights: statusOptions,
      target,
    };
    return await this.sendLumoRequestAndSaveRecords(flightIds, options);
  }

  private async sendLumoRequestAndSaveRecords(
    ids: number[],
    options: SubscriptionOptions,
  ) {
    try {
      const status = await this.http.post<SubscriptionResponse>(
        'subscriptions',
        options,
      );
      for (const id of ids) {
        await this.saveLumoSubscription(id, status.data.itinerary_id);
      }
      return true;
    } catch (e) {
      return false;
    }
  }

  private async getIdsForSubscription(
    id: number[] | number,
  ): Promise<number[]> {
    if (Array.isArray(id)) {
      const subscriptions = await this.subscriptionRepository.find({
        where: {
          flightId: In(id),
        },
      });
      if (subscriptions.length) {
        return id.filter((i) => !subscriptions.some((s) => s.flightId === i));
      }
      return id;
    } else {
      const subscription = await this.subscriptionRepository.findOne({
        flightId: id,
      });
      if (!subscription) return [id];
      return [];
    }
  }

  private async saveLumoSubscription(flightId: number, subscriptionId: string) {
    await this.subscriptionRepository.save(
      new LumoSubscription({
        flightId,
        subscriptionId,
      }),
    );
  }
}

// flight -  KL1794
// IATA code - consist from 2 characters
const parseFlightID = (id: string): [string, number] => {
  const [, carrier, flightNumber] = id.match(/([A-Za-z0-9]{2})(\d+)/);
  return [carrier, Number(flightNumber)];
};
