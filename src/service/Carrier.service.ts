import airlinesData from '../resources/airlines.json';
import Fuse from 'fuse.js';

export interface CarrierMeta {
  id: number;
  name: string;
  country: string;
  iata: string;
}

const options = {
  location: 0,
  threshold: 0.2,
  keys: ['name', 'country', 'iata'],
};

const airlinesFuse = new Fuse(
  (airlinesData as unknown) as CarrierMeta[],
  options,
);

class CarrierService {
  public search(pattern: string, limit = 5): CarrierMeta[] {
    return airlinesFuse.search(pattern, { limit }).map((s) => s.item);
  }
}

export default new CarrierService();
