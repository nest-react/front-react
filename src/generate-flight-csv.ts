//TODO: This is temporary until we have a dashboard

import { FlightService } from './service/Flight.service';
import { UserService } from './service/User.service';
import { createConnection } from 'typeorm';
import User from './model/User';
import Flight from './model/Flight';
import { getNiceTimeDiffString } from './util/datetime.utils';
import { GmailService } from './service/Gmail.service';

const createCsvWriter = require('csv-writer').createObjectCsvWriter;
const csvWriter = createCsvWriter({
  path: 'flights.csv',
  header: [
    { id: 'name', title: 'Name' },
    { id: 'phone', title: 'Phone' },
    { id: 'email', title: 'Email' },
    { id: 'outbound', title: 'Outbound' },
    { id: 'inbound', title: 'Inbound' },
    { id: 'route', title: 'Route' },
    { id: 'departure time', title: 'Departure Time' },
    { id: 'departure time in israel', title: 'Departure Time In Israel' },
    { id: 'time till departure', title: 'Time Until Departure' },
    { id: 'date', title: 'Date' },
    { id: 'days to flight', title: 'Days To Flight' },
    { id: 'today', title: 'Today' },
  ],
});

function generateRow(user: User, flight: Flight, type: string) {
  const one_day = 1000 * 60 * 60 * 24;
  const today = new Date();
  const row = {
    name: user.name,
    phone: user.mobile,
    email: user.email,
    outbound: '',
    inbound: '',
    route: flight.origin + '===>' + flight.destination,
    'departure time': flight.scheduledDeparture,
    'departure time in israel': flight.scheduledDeparture.toLocaleString(
      'en-US',
      { timeZone: 'Asia/Jerusalem' },
    ),
    'time till departure': getNiceTimeDiffString(
      flight.estimatedDeparture.toISOString(),
      new Date().toISOString(),
    ),
    date: flight.scheduledDeparture.toDateString(),
    'days to flight': Math.ceil(
      (flight.scheduledDeparture.getTime() - today.getTime()) / one_day,
    ),
    today: new Date().toLocaleDateString('en-US', {
      timeZone: 'Asia/Jerusalem',
    }),
  };
  type === 'inbound'
    ? (row.inbound = flight.flightNo)
    : (row.outbound = flight.flightNo);
  return row;
}

async function main() {
  const db = await createConnection();
  const userService = new UserService(db);
  const flightService = new FlightService(db);
  const ignored_names = [
    'Daniel Kfir',
    'Lidor Ben Yosef',
    'Daniel Green',
    'Motti Bebchuk',
    'Elad Schaffer',
    'Nathan Bletel',
    'Nathan Starkman',
  ];
  const users = (await userService.getAllNewUsers()).filter(
    (user) => !ignored_names.includes(user.firstName),
  );
  const data: any[] = [];
  await Promise.all(
    users.map(async (user) => {
      let type = 'outbound';
      const flights = await flightService.getAllFlightsByUser(user.id);

      if (!flights || flights.length === 0) {
        return;
      }
      let index = 0;
      while (index < flights.length) {
        if ([1, 2].includes(index)) {
          type = 'inbound';
        }
        const currFlight = flights[index];
        data.push(generateRow(user, currFlight, type));
        index += 1;

        if (currFlight.seq === 0) {
          const connFlight = flights[index];
          data.push(generateRow(user, connFlight, type));
          index += 1;
        }
      }
    }),
  );
  await csvWriter.writeRecords(data);

  const mailService: GmailService = new GmailService();

  await mailService.send({
    to: 'motti@zenner.ai',
    subject: 'Flights Spreadsheet',
    text: 'Please see attachment',
    attachments: [
      {
        filename: 'flights.csv',
        path: 'flights.csv',
      },
    ],
  });
}

main();
