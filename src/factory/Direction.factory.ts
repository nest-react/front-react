import { Client } from '@googlemaps/google-maps-services-js/dist';
import { DirectionService } from '../service/Direction.service';

export class DirectionFactory {
  static create(key: string): DirectionService {
    return new DirectionService(new Client(), key);
  }
}
