import Airtable from 'airtable';
import { AirtableService } from '../service/airtable/Airtable';
import ConfigService from '../service/Config.service';

export class AirtableFactory {
  static create(): AirtableService {
    const airtable = new Airtable({
      apiKey: ConfigService.get('AIRTABLE_API_KEY'),
    });
    return new AirtableService(airtable, ConfigService);
  }
}
