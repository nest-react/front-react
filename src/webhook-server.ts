import * as dotenv from 'dotenv';
import * as axios from 'axios';

// Load env vars
dotenv.config();
if (process.env.NODE_ENV === 'production') {
  require('newrelic');
}

// 3rd party
import {
  FastifyInstance,
  FastifyReply,
  FastifyRequest,
  FastifyServerOptions,
} from 'fastify';

import {
  FrontDTO,
  RegisterUserDTO,
  GetUserDTO,
  MuteBotDTO,
  MuteBotParams,
} from './interface/dto/webhook.dto';

import * as path from 'path';
import BeeQueue from 'bee-queue';
import fastifyCors from 'fastify-cors';
import fastifyStatic from 'fastify-static';
import fastifyFormBody from 'fastify-formbody';
import { Server, IncomingMessage, ServerResponse } from 'http';

// Interfaces
import { AlertResponse } from './interface/flightStats/alert';
import { FlightResponse, ValidateRequest } from './interface/lumo';

// Services
import ConfigService from './service/Config.service';
import { LumoService } from './service';
import { Conversation } from './service/front/Conversation';
import SubscriptionService from './service/Subscription.service';
import RulesEngine from './service/RulesEngine/RulesEngine';
import { BeeQueueService } from './service/BeeQueue.service';
import { CommsEventService } from './service/CommsEvent.service';
import { UserService } from './service/User.service';
import { FlightService } from './service/Flight.service';
import Il8nService from './service/Il8n.service';
import { TwilioService } from './service/Twilio.service';

// Controllers
import FlightController from './controllers/Flight.controller';
import AutocompleteController from './controllers/AutoComplete.controller';

import db from './plugins/db';
import { createLogger } from './util/logger';
import { parsePhoneNumber } from './util/botServer.utils';
import LandingHandler from './service/RulesEngine/handlers/Landing.handler';
import DelayHandler from './service/RulesEngine/handlers/Delay.handler';
import Flight from './model/Flight';
import { FlightStatsService } from './service/FlightStats.service';
import TerminalGateHandler from './service/RulesEngine/handlers/TerminalGate.handler';
import BaggageClaimHandler from './service/RulesEngine/handlers/BaggageClaim.handler';
import { validateSocketAuth } from './service/front/FrontClient';
import { Contact } from './service/front/Contact';
import { FactRequest } from './interface/rulesEngine/RulesEngine';
import { gateChanged } from './util/Subscription.util';

dotenv.config();

const redisConfig = {
  host: ConfigService.get('REDIS_HOST'),
  port: Number(ConfigService.get('REDIS_HOST')),
};

const logger = createLogger('zenner', 'webhook', 'zenner-webhooks');
const engine = new RulesEngine();

logger.info('Starting Web Server');

const bq = new BeeQueue(ConfigService.get('QUEUE_NAME_TWILIO'), {
  isWorker: false,
  redis: redisConfig,
});

const outgoingBQ = new BeeQueue('OUTGOING_MESSAGE', {
  isWorker: false,
  redis: redisConfig,
});

let subscriptionService: SubscriptionService;

bq.on('ready', () => {
  logger.info('Queue now ready', {
    queue: ConfigService.get('QUEUE_NAME_TWILIO'),
  });
});

export async function twilio(
  request: FastifyRequest,
  reply: FastifyReply<any>,
): Promise<FastifyReply<any>> {
  const job = bq.createJob(request.body);
  await job.save();

  return reply
    .code(200)
    .type('application/xml')
    .send('<?xml version="1.0" encoding="UTF-8"?><Response></Response>');
}

const serverOptions: FastifyServerOptions = {
  // Logger only for production
  logger: process.env.NODE_ENV !== 'development',
};

export const app: FastifyInstance<
  Server,
  IncomingMessage,
  ServerResponse
> = require('fastify')(serverOptions);

app.register(require('fastify-express'));
app.register(fastifyFormBody);

app.register(fastifyCors, {
  origin: '*',
});

// first plugin
app.register(fastifyStatic, {
  root: path.join(__dirname, 'public'),
});

app.register(db);

// Socket IO Server
const io = require('socket.io')(app.server, {
  transports: ['websocket', 'polling'],
  cors: {
    origin: 'https://heuristic-goldberg-c74378.netlify.app',
    credentials: true,
    methods: ['GET', 'POST'],
  },
});

io.use(validateSocketAuth).on('connection', () => {
  // We do nothing here, but you can't make the
  // middleware load without passing in a function
});

// app.use('api')
// Middleware: Router
app.register((fastifyInstance: FastifyInstance, opts: any, next) => {
  try {
    // This webhook is covered by tests for its individual components
    /* istanbul ignore next */
    fastifyInstance.post<{ Body: FrontDTO }>(
      '/front',
      async (request, reply) => {
        const conversationId = request.body.conversation.id;
        const conversationContent = request.body.target.data.text;
        logger.info(`Front chat ${conversationId} said ${conversationContent}`);
        const response = await new Conversation().reply(conversationId, {
          body: 'Test reply',
        });
        reply.send(response);
      },
    );

    // This webhook is covered by tests for its individual components
    /* istanbul ignore next */
    fastifyInstance.post('/twilio', twilio);
  } catch (err) /*istanbul ignore next*/ {
    logger.error(`Failed to post Twilio webhook to message queue.`, {
      error: err,
    });
  }
  next();
});

app.post<{
  Body: RegisterUserDTO;
}>('/register/user', async (req, res) => {
  try {
    const connection = app.db.connection;
    const userService = new UserService(connection);
    const bqService = new BeeQueueService();

    const twilioService = new TwilioService();
    req.body.mobile = await twilioService.validatePhoneNumberWithTwilio(
      req.body.mobile,
    );
    //update whatsup number if phone is valid and user approved whatsup
    if (req.body.mobile && req.body.whatsapp) {
      req.body.whatsapp = req.body.mobile;
    }
    //return an error if the phone is invalid we dont want to save a user we can't contact
    if (req.body.mobile == null) {
      return res.code(400).send('Phone is invalid');
    }

    //validate that the email is unique
    const duplicateEmailUser = await userService.checkIfDuplicateEmail(
      req.body,
    );
    if (duplicateEmailUser && duplicateEmailUser.length > 0) {
      return res.code(400).send('Email is already registered in the system');
    }

    // Step One: see if this is a returning user
    const user = await userService.findByMobile(req.body.mobile);
    if (user) {
      // check if we email is not the same and update the email
      if (user.email != req.body.email) {
        user.email = req.body.email;
        await userService.updateUser(user);
      }

      // User already registered, nothing more to do here.
      return res.code(200).send();
    }

    // if not - create them!
    const createdUser = await userService.createUser(req.body);

    // and update Front
    try {
      const contact: Contact = new Contact();

      contact
        .create({
          name: req.body.firstName + ' ' + req.body.lastName,
          handles: [
            {
              source: 'email',
              handle: req.body.email,
            },
            {
              source: 'phone',
              handle: '+' + req.body.mobile,
            },
          ],
        })
        .then((result) => {
          console.log(result);
        });
    } catch (err: any) {
      logger.error(`Failed to post user info to Front`, { err: err });
    }

    // Step Two: notify everyone on Slack
    try {
      const slackMessage = `Timestamp: ${new Date(
        new Date().getTime() + 7200000,
      )} \nRegister Event: ${'Register'} 
    Name: ${createdUser.firstName} ${createdUser.lastName}
    Phone: +${createdUser.mobile} (${createdUser.whatsapp ? 'Whatsapp' : 'SMS'})
    Email: ${createdUser.email}
    `;
      await axios.default.post(ConfigService.get('SLACK_ONBOARDING_SHU'), {
        text: slackMessage,
      });
    } catch (err: any) {
      logger.error(`Failed to post user announcement to Slack`, { err: err });
    }

    // Step Three: prepare a welcome message
    const servicePrefix = createdUser.whatsapp ? 'whatsapp:' : '';
    const source = createdUser.whatsapp ? 'whatsapp' : 'sms';
    const message = Il8nService.get('zenny:user:register', {
      templateVars: {
        name: createdUser.firstName,
      },
    });

    // Send a message back to the user (confirmation flow)
    await bqService.createResponseJob(
      outgoingBQ,
      `${servicePrefix}+${process.env.PRIMARY_PHONE}`,
      `${servicePrefix}+${createdUser.mobile}`,
      source,
      message,
    );

    return res.code(200).send();
  } catch (e) {
    logger.error('User register error', { e });
    res.code(500).send('An error occurred');
  }
});

const frontValidation = async (
  request: FastifyRequest,
  reply: FastifyReply,
) => {
  const { authorization } = request.headers;
  if (authorization !== ConfigService.get('FRONT_PLUGIN_SECRET')) {
    reply.code(401).send();
  }
};

app.get('/autocomplete/carrier', AutocompleteController.airlines);
app.get('/autocomplete/airports', AutocompleteController.airports);

app.route<{
  Params: MuteBotParams;
  Body: MuteBotDTO;
}>({
  method: 'PUT',
  url: '/bot/mute/:phone',
  preValidation: frontValidation,
  handler: async (req, res) => {
    try {
      const payload = req.body;
      if (!req.params.phone || !Object.keys(payload)) {
        return res.code(400).send();
      }
      const userService = new UserService(app.db.connection);
      const phone = parsePhoneNumber(req.params.phone);
      const user = await userService.findByMobile(phone);
      if (!user) {
        return res.code(404).send();
      }
      await userService.muteBot(user.mobile, payload.mute);
      io.emit('botStateChanged', { isMuted: payload.mute, phone: phone }); // If anyone is subscribed to mute state via Socket.IO ... update
      return res.code(200).send();
    } catch (e) {
      logger.error('An error occurred', e);
      return res.code(500).send();
    }
  },
});

app.route<{
  Querystring: GetUserDTO;
}>({
  method: 'GET',
  url: '/user',
  preValidation: frontValidation,
  handler: async (req, res) => {
    if (!Object.values(req.query)) {
      return res.code(400).send();
    }
    try {
      const payload = req.query as GetUserDTO;
      let user;
      const userService = new UserService(app.db.connection);
      const flightService = new FlightService(app.db.connection);
      const commsEventService = new CommsEventService(app.db.connection);
      if (payload.email) {
        user = await userService.findByEmail(payload.email);
      } else {
        const phone = parsePhoneNumber(payload.phone).trim();
        user = await userService.findByMobile(phone);
        // user = await userService.getUserWithFlightsByField('mobile', phone);
      }
      if (!user) {
        return res.code(404).send();
      }
      user.flights = await flightService.getUserFlightsForFrontApp(user.id);
      user.lastMessageDate = await commsEventService.getLastMessageDateFromUser(
        `whatsapp:+${user.mobile}`,
      );

      return res.code(200).send(user);
    } catch (e) {
      logger.error('An error occurred', e);
      return res.code(500).send();
    }
  },
});

app.post('/flight/register', async (req: any, res: any) => {
  const connection = app.db.connection;
  const flightController = new FlightController(
    new LumoService(connection),
    new FlightStatsService(connection),
    connection,
  );
  const userService = new UserService(connection);
  const bqService = new BeeQueueService();
  const commsService = new CommsEventService(connection);
  const twilioService = new TwilioService();

  req.body.mobile = await twilioService.validatePhoneNumberWithTwilio(
    req.body.mobile,
  );
  //update whatsup number if phone is valid and user approved whatsup
  if (req.body.mobile && req.body.whatsapp) {
    req.body.whatsapp = req.body.mobile;
  }

  const user = await userService.findByMobile(req.body.mobile);
  let source;
  let servicePrefix;
  let message;
  if (user) {
    servicePrefix = user.whatsapp !== null ? 'whatsapp:' : '';
    source = user.whatsapp !== null ? 'whatsapp' : 'sms';
    message = Il8nService.get('zenny:welcome');
  } else {
    servicePrefix = req.body.haveWhatsApp !== null ? 'whatsapp:' : '';
    source = req.body.haveWhatsApp !== null ? 'whatsapp' : 'sms';
    message = Il8nService.get('zenny:user:register', {
      templateVars: {
        name: req.body.firstName,
      },
    });
  }

  //TODO: This welcome message should really come from the il8n file
  const code = await flightController.registerForFlight(req, res);

  await bqService.createResponseJob(
    outgoingBQ,
    `${servicePrefix}+${process.env.PRIMARY_PHONE}`,
    `${servicePrefix}+${req.body.mobile}`,
    source,
    message,
  );
  await commsService.addEvent({
    from: process.env.PRIMARY_PHONE,
    to: req.body.mobile,
    message,
    server: require('os').hostname(),
    source,
    created: new Date(),
    result: 'success',
  });

  return code;
});

app.get<{
  Querystring: ValidateRequest;
}>('/flight/validate', async (req, res) => {
  const connection = app.db.connection;
  const flightController = new FlightController(
    new LumoService(connection),
    new FlightStatsService(connection),
    connection,
  );
  return await flightController.validate(req.query, res);
});

// This webhook is covered by tests for its individual components
app.post<{
  Body: AlertResponse;
}>('/alerts/webhook', async (request, reply) => {
  try {
    const payload = request.body;
    const dbFlight = await subscriptionService.processAlertSubscription(
      payload,
    );
    if (dbFlight) {
      await runRules('FlightStats', dbFlight, payload);
    }
    return reply.code(204).send();
  } catch (e) {
    logger.error('Fail to save FlightStats update', {
      error: e,
      payload: request.body,
    });
    return reply.code(500).send();
  }
});

app.post<{
  Body: FlightResponse;
}>('/lumo/webhook', async (request, reply) => {
  try {
    const payload = request.body;
    const dbFlight = await subscriptionService.processLumoSubscription(payload);
    if (dbFlight) {
      await runRules('Lumo', dbFlight, payload);
    }
    return reply.code(204).send();
  } catch (e) {
    logger.error('Fail to save Lumo update', {
      error: e,
      payload: request.body,
    });
    return reply.code(500).send();
  }
});

async function runRules(
  source: string,
  dbFlight: Flight,
  payload: AlertResponse | FlightResponse,
) {
  const fact: FactRequest = {
    source: source,
    oldFlight: dbFlight,
    newFlight: payload,
  };
  const res = await engine.runRules(fact);
  const services = {
    db: app.db.connection,
    bq: outgoingBQ,
    userService: new UserService(app.db.connection),
    flightService: new FlightService(app.db.connection),
    beeQueueService: new BeeQueueService(),
    commsEventService: new CommsEventService(app.db.connection),
  };
  const newGate =
    source === 'Lumo'
      ? (payload as FlightResponse)?.status?.departure?.gate
      : (payload as AlertResponse)?.alert?.flightStatus?.airportResources
          .departureGate;
  if (gateChanged(dbFlight.originGate, newGate)) {
    res.events.push({ type: 'gate_changed' });
    dbFlight.originGate = newGate;
    services.flightService.flightRepository.update(dbFlight.id, dbFlight);
  }

  const Loginhandler = new LandingHandler();
  await Loginhandler.handle(res, services, dbFlight);
  const Gatehandler = new TerminalGateHandler();
  await Gatehandler.handle(res, services, dbFlight);
  const BaggageHandler = new BaggageClaimHandler();
  await BaggageHandler.handle(res, services, dbFlight);
  const Delayhandler = new DelayHandler();
  await Delayhandler.handle(res, services, dbFlight);
}

const FASTIFY_PORT = Number(ConfigService.get('FASTIFY_PORT')) || 8080;

async function start() {
  try {
    await app.listen(FASTIFY_PORT, '0.0.0.0');

    subscriptionService = new SubscriptionService(app.db.connection);
    logger.info(`Fastify server running on port ${FASTIFY_PORT}`);
  } catch (e) {
    logger.error('Fastify server failed with error', { error: e });
    process.exit(1);
  }
}

start();
