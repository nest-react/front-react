import Il8nService from '../service/Il8n.service';
import AirportService from '../service/Airport.service';
import { createLogger } from '../util/logger';
const logger = createLogger('zenner', 'flight', 'zenner-common');
import {
  getNiceDateString,
  getNiceTimeDiffString,
} from '../util/datetime.utils';
import Flight from '../model/Flight';

export class FlightHelper {
  static getOutOfDateStatus(flight: Flight) {
    const scheduledArrival = flight.scheduledArrival;
    const latestArrival = flight.estimatedArrival;
    const nowEpoch = new Date().getTime();
    // 43200s = 12hours
    if (scheduledArrival && latestArrival) {
      flight.isOutOfDate =
        nowEpoch - latestArrival.getTime() > 43200 &&
        nowEpoch - scheduledArrival.getTime() > 43200;
    }
  }

  static getOriginCity(flight: Flight): string {
    try {
      return flight.initialLumoData.flight.origin.city;
    } catch (error) {
      logger.error(error);
      return AirportService.search(flight.origin)[0].municipality;
    }
  }

  /**
   * Produces a short summary of the flight
   */
  static toShortSummary(printDeparture: boolean, flight: Flight): string {
    let summary: string;

    // We wrap this in a try/catch mostly because of il8n and string errors
    try {
      summary = `${flight.flightNo} (${flight.origin} to ${flight.destination}). `;

      if (this.isBeforeDeparture(flight)) {
        // Flight hasn't departed yet
        if (printDeparture) {
          summary += `departing in ${this.timeUntilDeparture(
            flight,
          )} at ${flight.estimatedDepartureTimeLocal.slice(0, 5)} `;
          summary += `on ${getNiceDateString(
            flight.estimatedDepartureDateLocal,
          )}.`;
        }

        // We won't always have a risk rating from Lumo, but it we do, append it.
        if (flight.lumoRisk !== undefined) {
          summary +=
            '\n' +
            Il8nService.get('zenny:flightdata:risk', {
              templateVars: { risk: flight.lumoRisk },
            });
        }
      } else {
        // Flight is in the air - so we can show a much shorter version
        summary = `${flight.flightNo} (${flight.origin} to ${flight.destination}) `;

        summary += `landing in ${this.timeUntilArrival(
          flight,
        )} at ${flight.estimatedArrivalTimeLocal?.slice(0, 5)} `;
        summary += `on ${getNiceDateString(flight.estimatedArrivalDateLocal)}.`;
      }
    } catch (err) {
      logger.error("Couldn't get flight data", { error: err });
      return 'I was not able to load this flight';
    }

    return summary;
  }

  static timeUntilArrival(flight: Flight): string {
    const now = new Date();
    if (flight.estimatedArrival) {
      return getNiceTimeDiffString(
        now.toJSON(),
        flight.estimatedArrival.toISOString(),
      );
    }
    return '';
  }

  static roundMinutesForPredepartureMessages(date: Date): Date {
    //copy the date
    const now = new Date();
    now.setHours(now.getHours() + 4);
    // This will give difference in milliseconds
    const difference = date.getTime() - now.getTime();
    // convert to minutes
    const resultInMinutes = Math.abs(Math.round(difference / 60000));
    if (resultInMinutes > 0 && resultInMinutes <= 5) {
      date = now;
    }
    return date;
  }

  static roundTimeUntilDeparture(flight: Flight): string {
    const now = new Date();
    return getNiceTimeDiffString(
      now.toJSON(),
      this.roundMinutesForPredepartureMessages(
        flight.estimatedDeparture,
      ).toISOString(),
      true,
    );
  }

  static timeUntilDeparture(flight: Flight): string {
    const now = new Date();
    return getNiceTimeDiffString(
      now.toJSON(),
      flight.estimatedDeparture.toISOString(),
      true,
    );
  }

  /**
   * Check that it is before the departure time and flight status hasn't change
   */
  static isBeforeDeparture(flight: Flight): boolean {
    if (this.isLanded(flight) || flight.statusName === 'enroute') {
      return false;
    }
    return flight.estimatedDeparture > new Date();
  }

  /**
   * Check is flight departure was in the past
   */
  static isPastDepartureTime(flight: Flight): boolean {
    if (flight.statusName === 'enroute') return true;
    return flight.estimatedDeparture < new Date();
  }

  /**
   * Check is flight has a landed or arrived status
   */
  static isLanded(flight: Flight): boolean {
    return flight.statusName === 'landed' || flight.statusName === 'arrived';
  }
}
