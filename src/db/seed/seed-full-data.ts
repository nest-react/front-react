import { UserTrip } from '../../model/UserTrip';
import { Connection, createConnection } from 'typeorm';
import { Flights } from './seed-test-flight';
import { Users } from './seed-test-user';
import { Trip } from '../../model/Trip';

async function main(): Promise<void> {
  let db: Connection;

  try {
    db = await createConnection();
  } catch (err) {
    console.log(err);
    console.log('DB connection failed');
    process.exit(1);
  }

  const now = new Date();
  const user = await new Users().createUser(db);
  const flight1 = await new Flights().createFlight(
    db,
    now, //flight departs soons
    1,
    'enroute',
    'JFK',
    'TLV',
  );
  const flight2 = await new Flights().createFlight(
    db,
    new Date(now.getTime() + 8 * 60 * 60 * 1000), //flight departs in 8 hours
    1,
    'scheduled',
    'TLV',
    'CDG',
  );
  const flight3 = await new Flights().createFlight(
    db,
    new Date(now.getTime() - 8 * 60 * 60 * 1000), //flight departed 8 hours ago
    1,
    'landed',
    'EWR',
    'LAX',
  );

  const userTripRepo = db.getRepository(UserTrip);
  const tripRepo = db.getRepository(Trip);
  const trip1 = await tripRepo.save(
    new Trip({ flight: flight1, trip_id: 1, seq: 0, conn_time: 120 }),
  );
  await tripRepo.save(new Trip({ flight: flight2, trip_id: 1, seq: 1 }));
  const trip2 = await tripRepo.save(new Trip({ flight: flight3, trip_id: 2 }));
  await userTripRepo.save(
    new UserTrip({ user_id: user.id, trip_id: trip1.trip_id }),
  );
  await userTripRepo.save(
    new UserTrip({ user_id: user.id, trip_id: trip2.trip_id }),
  );
}
main();
