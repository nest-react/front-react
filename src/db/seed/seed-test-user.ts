import ConfigService from '../../service/Config.service';

import { Connection } from 'typeorm';

import User from '../../model/User';
import Company from '../../model/Company';

export class Users {
  async createUser(db: Connection) {
    const companyRepo = db.getRepository(Company);
    const userRepo = db.getRepository(User);

    let testCompany = new Company();
    let testUser = new User();

    const testUsers = await userRepo.find({ name: 'Testing McTester' });

    if (testUsers.length > 0) {
      console.log('Looks like the test user is already in place!');
    } else {
      // Create the test company
      testCompany.name = 'Acme Labs';
      testCompany = await companyRepo.save(testCompany);

      // Create the test user
      testUser.name = 'Testing McTester';
      testUser.firstName = 'Testing';
      testUser.lastName = 'McTester';
      testUser.email = 'test@gozenner.com';
      testUser.mobile = '1234567890';
      testUser.whatsapp = '1234567890';
      testUser = await userRepo.save(testUser);

      // Make the user the admin of the company
      testUser.company = testCompany;
      testCompany.administrator = testUser;
      await userRepo.save(testUser);
      await companyRepo.save(testCompany);
    }

    return testUser;
  }
}
