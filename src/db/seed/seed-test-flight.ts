import { Connection, createConnection } from 'typeorm';

import Flight from '../../model/Flight';
import { FlightstatsRecord } from '../../model/FlightstatsRecord';
import { AlertPayload } from '../../interface/flightStats/alert';
import { FlightstatsSubscription } from '../../model/FlightstatsSubscription';
import { LumoSubscription } from '../../model/LumoSubscription';
import { LumoRecord } from '../../model/LumoRecord';
let LumoData = require('./InitialLumo.json');
let FlightstatsData = require('./InitialFlightStats.json');

export class Flights {
  async createFlight(
    db: Connection,
    date: Date,
    id: number,
    status: string,
    origin: string,
    destination: string,
  ) {
    const flightRepo = db.getRepository(Flight);
    const alertSubscriptionRepo = db.getRepository(FlightstatsSubscription);
    let payload: AlertPayload;
    payload = FlightstatsData;

    const now = new Date();
    let lumoSubPayload = {
      id: id,
      subscription_id: 'e04cad13-ad17-42a1-a1f9-ef1dd0b8e039',
      created: now,
      flight_id: id,
    };
    let subPayload = {
      id: id,
      flightId: id,
      subscriptionId: 1,
      created: now,
    };

    let alertSubscription = new FlightstatsSubscription(subPayload);

    let lumoRecord = new LumoRecord(LumoData);

    let lumoSubscription = new LumoSubscription(lumoSubPayload);

    const testFlight = new Flight();

    let departureTime = new Date(date.getTime() + 4 * 60 * 60 * 1000); //current time plus 4 hours

    let arrivalDate = new Date(date.getTime() + 6 * 60 * 60 * 1000); //2 hour fligt

    testFlight.active = true;
    testFlight.created = date;
    testFlight.updated = date;
    testFlight.flightNo = '90';
    testFlight.statusName = status;
    testFlight.departureDate = departureTime;
    testFlight.lumoId = '79';
    testFlight.flightstatsId = null;
    testFlight.firestoreId = null;
    testFlight.firestorePassenger = null;
    testFlight.firestoreData = 'DATA';
    testFlight.realFlightNo = '90';
    testFlight.operatingCarrier = 'UA';
    testFlight.marketedCarrier = 'UA';
    testFlight.lumoRisk = '1';
    testFlight.cancelled = false;
    testFlight.diverted = false;
    testFlight.scheduledDeparture = departureTime;
    testFlight.estimatedDeparture = departureTime;
    testFlight.scheduledDepartureDateLocal = departureTime
      .toISOString()
      .split('T')[0];
    testFlight.scheduledDepartureTimeLocal = departureTime
      .toISOString()
      .split('T')[0];
    testFlight.estimatedDepartureDateLocal = departureTime
      .toISOString()
      .split('T')[0];
    testFlight.estimatedDepartureTimeLocal = departureTime
      .toISOString()
      .split('T')[0];
    testFlight.scheduledArrival = arrivalDate;
    testFlight.estimatedArrival = arrivalDate;
    testFlight.scheduledArrivalDateLocal = arrivalDate
      .toISOString()
      .split('T')[0];
    testFlight.scheduledArrivalTimeLocal = arrivalDate
      .toISOString()
      .split('T')[0];
    testFlight.estimatedArrivalDateLocal = arrivalDate
      .toISOString()
      .split('T')[0];
    testFlight.estimatedArrivalTimeLocal = arrivalDate
      .toISOString()
      .split('T')[0];
    testFlight.origin = origin;
    testFlight.destination = destination;
    testFlight.diversionAirport = null;
    testFlight.originTerminal = 'C';
    testFlight.originGate = 'C123';
    testFlight.destinationTerminal = '3';
    testFlight.destinationGate = 'B6';
    testFlight.destinationBaggage = '5';
    testFlight.planeType = '787';
    testFlight.tailNo = '13013';
    testFlight.initialLumoData = LumoData;
    testFlight.initialFlightStatsData = FlightstatsData;

    await alertSubscriptionRepo.save(alertSubscription);

    await flightRepo.save(testFlight);

    return testFlight;
  }
}
