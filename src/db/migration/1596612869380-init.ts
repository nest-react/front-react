import { MigrationInterface, QueryRunner } from 'typeorm';

export class init1596612869380 implements MigrationInterface {
  name = 'init1596612869380';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "alert_record" ("id" SERIAL NOT NULL, "alert_id" character varying NOT NULL, "alert" jsonb, "alert_capabilities" jsonb, CONSTRAINT "PK_e7f70ff237b0fb5bc09e02cf0d5" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TABLE "alert_subscription" ("id" SERIAL NOT NULL, "flight_id" character varying NOT NULL, "subscription_id" character varying NOT NULL, "created" TIMESTAMP NOT NULL DEFAULT now(), CONSTRAINT "PK_32cbf375c12e1a65d03eb604ae5" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TABLE "api_audit" ("id" SERIAL NOT NULL, "created" TIMESTAMP NOT NULL DEFAULT now(), "service_class" character varying NOT NULL, "hostname" character varying NOT NULL, "url" character varying NOT NULL, "outgoing" boolean, "duration" double precision NOT NULL, "request" jsonb, "response" jsonb, CONSTRAINT "PK_fec5cab64c6eee696bc6db95587" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TABLE "comms_event" ("id" SERIAL NOT NULL, "created" TIMESTAMP NOT NULL DEFAULT now(), "from" character varying NOT NULL, "to" character varying NOT NULL, "source" character varying NOT NULL, "outbound" boolean, "human" boolean, "result" character varying, "server" character varying, "message" character varying NOT NULL, "passenger_id" numeric, "sentiment_score" double precision, "sentiment_magnitude" double precision, "metadata" jsonb, CONSTRAINT "PK_3010f971d4c75fa27a2f488cb7e" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TABLE "flight" ("id" SERIAL NOT NULL, "created" TIMESTAMP NOT NULL DEFAULT now(), "updated" TIMESTAMP NOT NULL DEFAULT now(), "flight_no" character varying, "status_name" character varying, "active" boolean NOT NULL DEFAULT true, "departure_date" date, "lumo_id" character varying, "flightstats_id" character varying, "firestore_id" character varying, "firestore_passenger" character varying, "firestore_data" character varying, CONSTRAINT "PK_bf571ce6731cf071fc51b94df03" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TABLE "user" ("id" SERIAL NOT NULL, "created" TIMESTAMP NOT NULL DEFAULT now(), "name" character varying NOT NULL, "first_name" character varying, "last_name" character varying, "email" character varying, "mobile" character varying, "whatsapp" character varying, "firestore_id" character varying, "firestore_data" character varying, "company_id" integer, CONSTRAINT "PK_cace4a159ff9f2512dd42373760" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TABLE "company" ("id" SERIAL NOT NULL, "created" TIMESTAMP NOT NULL DEFAULT now(), "name" character varying NOT NULL, "credits" integer, "firestore_id" character varying, "firestore_data" character varying, "administrator_id" integer, CONSTRAINT "PK_056f7854a7afdba7cbd6d45fc20" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TABLE "lumo_record" ("id" SERIAL NOT NULL, "flight_id" character varying NOT NULL, "timestamp" character varying NOT NULL, "change" character varying NOT NULL, "firestore_id" character varying, "alert" jsonb, "summaries" jsonb, "flight" jsonb NOT NULL, "prediction" jsonb, "status" jsonb NOT NULL, "weather" jsonb, "inbound" jsonb, "special_alerts" jsonb, "travel_advisories" jsonb, CONSTRAINT "PK_46baa2ecb4276a0b50f8fc213bc" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TABLE "lumo_subscription" ("id" SERIAL NOT NULL, "flight_id" character varying NOT NULL, "subscription_id" character varying NOT NULL, "created" TIMESTAMP NOT NULL DEFAULT now(), CONSTRAINT "PK_63aa69bf524f02879a7fb75804f" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TABLE "user_flight" ("user_id" integer NOT NULL, "flight_id" integer NOT NULL, CONSTRAINT "PK_52d910ef7f5bc7c2bc2b9c72bab" PRIMARY KEY ("user_id", "flight_id"))`,
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_3b4a857f032a78b8d9a4ea3450" ON "user_flight" ("user_id") `,
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_e3fe0681510204d9cf81be71aa" ON "user_flight" ("flight_id") `,
    );
    await queryRunner.query(
      `ALTER TABLE "user" ADD CONSTRAINT "FK_9e70b5f9d7095018e86970c7874" FOREIGN KEY ("company_id") REFERENCES "company"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "company" ADD CONSTRAINT "FK_758630945258337cade1ef75a65" FOREIGN KEY ("administrator_id") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "user_flight" ADD CONSTRAINT "FK_3b4a857f032a78b8d9a4ea34502" FOREIGN KEY ("user_id") REFERENCES "user"("id") ON DELETE CASCADE ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "user_flight" ADD CONSTRAINT "FK_e3fe0681510204d9cf81be71aac" FOREIGN KEY ("flight_id") REFERENCES "flight"("id") ON DELETE CASCADE ON UPDATE NO ACTION`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "user_flight" DROP CONSTRAINT "FK_e3fe0681510204d9cf81be71aac"`,
    );
    await queryRunner.query(
      `ALTER TABLE "user_flight" DROP CONSTRAINT "FK_3b4a857f032a78b8d9a4ea34502"`,
    );
    await queryRunner.query(
      `ALTER TABLE "company" DROP CONSTRAINT "FK_758630945258337cade1ef75a65"`,
    );
    await queryRunner.query(
      `ALTER TABLE "user" DROP CONSTRAINT "FK_9e70b5f9d7095018e86970c7874"`,
    );
    await queryRunner.query(`DROP INDEX "IDX_e3fe0681510204d9cf81be71aa"`);
    await queryRunner.query(`DROP INDEX "IDX_3b4a857f032a78b8d9a4ea3450"`);
    await queryRunner.query(`DROP TABLE "user_flight"`);
    await queryRunner.query(`DROP TABLE "lumo_subscription"`);
    await queryRunner.query(`DROP TABLE "lumo_record"`);
    await queryRunner.query(`DROP TABLE "company"`);
    await queryRunner.query(`DROP TABLE "user"`);
    await queryRunner.query(`DROP TABLE "flight"`);
    await queryRunner.query(`DROP TABLE "comms_event"`);
    await queryRunner.query(`DROP TABLE "api_audit"`);
    await queryRunner.query(`DROP TABLE "alert_subscription"`);
    await queryRunner.query(`DROP TABLE "alert_record"`);
  }
}
