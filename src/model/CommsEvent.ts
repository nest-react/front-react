import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
} from 'typeorm';

@Entity()
export default class CommsEvent {
  // Primary column
  @PrimaryGeneratedColumn()
  id: number;

  // Creation timestamp
  @CreateDateColumn()
  created: Date;

  // Who sent the message (phone no, email address)
  @Column()
  from: string;

  // Who should receive the message?
  @Column()
  to: string;

  // Do we know the transmission method (WhatsApp, SMS, email, etc...)
  @Column()
  source: string;

  // True if going from Zenner to a user
  @Column({ nullable: true })
  outbound: boolean;

  // Did a human type the message?
  @Column({ nullable: true })
  human: boolean;

  // Was there an error?
  @Column({ nullable: true })
  result: string;

  // What server handled this message?
  @Column({ nullable: true })
  server: string;

  // Message body
  @Column()
  message: string;

  // Optional link to a passenger record
  @Column({ type: 'numeric', nullable: true })
  passenger_id: number;

  // Optional sentiment score
  @Column({ type: 'float', nullable: true })
  sentiment_score: number;

  // Optional sentiment magnitude
  @Column({ type: 'float', nullable: true })
  sentiment_magnitude: number;

  // Optional server metadata
  @Column({ type: 'jsonb', nullable: true })
  metadata: string;

  @Column({ nullable: true })
  intent: string;
}
