import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  OneToOne,
} from 'typeorm';
import Flight from './Flight';
import { FlightstatsRecord } from './FlightstatsRecord';

@Entity()
export class FlightstatsSubscription {
  constructor(payload: any) {
    Object.assign(this, payload);
  }

  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  flightId: number;

  @Column()
  subscriptionId: string;

  @CreateDateColumn()
  created: Date;

  @OneToOne(() => Flight, (flight) => flight.alertSubscription)
  flight: Flight;

  @OneToOne(() => FlightstatsRecord, (alert) => alert.subscription)
  alert: FlightstatsRecord;
}
