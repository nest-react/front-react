import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  ManyToOne,
  JoinColumn,
} from 'typeorm';

@Entity()
export class CityTable {
  constructor(payload: any) {
    Object.assign(this, payload);
  }

  @PrimaryGeneratedColumn()
  city_id: number;

  @Column({ nullable: true })
  city: string;

  @Column({ nullable: true })
  city_ascii: string;

  @Column({ nullable: true })
  city_alt: string;

  @Column({ nullable: true })
  lat: string;

  @Column({ nullable: true })
  lng: string;

  @Column({ nullable: true })
  country: string;

  @Column({ nullable: true })
  iso2: string;

  @Column({ nullable: true })
  iso3: string;

  @Column({ nullable: true })
  admin_name: string;

  @Column({ nullable: true })
  admin_code: string;

  @Column({ nullable: true })
  admin_type: string;

  @Column({ nullable: true })
  capital: string;

  @Column({ nullable: true })
  density: string;

  @Column({ nullable: true })
  population: string;

  @Column({ nullable: true })
  population_proper: string;

  @Column({ nullable: true })
  ranking: string;

  @Column({ nullable: true })
  timezone: string;

  @Column({ nullable: true })
  id: string;
}
