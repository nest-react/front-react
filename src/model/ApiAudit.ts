import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
} from 'typeorm';

@Entity()
export default class ApiAudit {
  @PrimaryGeneratedColumn()
  id: number;

  @CreateDateColumn()
  created: Date;

  @Column()
  serviceClass: string;

  @Column()
  hostname: string;

  @Column()
  url: string;

  @Column({ nullable: true })
  outgoing: boolean;

  // execution time
  @Column({ type: 'float' })
  duration: number;

  @Column({ type: 'jsonb', nullable: true })
  request: Record<string, any>;

  @Column({ type: 'jsonb', nullable: true })
  response: Record<string, any>;
}
