import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  PrimaryColumn,
  OneToOne,
} from 'typeorm';

import {
  Alert,
  FlightInfo,
  FlightResponse,
  Prediction,
  SpecialAlerts,
  Status,
  Summaries,
  TravelAdvisories,
  WeatherStatus,
} from '../interface/lumo';
import { LumoSubscription } from './LumoSubscription';
import Flight from './Flight';

@Entity()
export class LumoRecord {
  constructor(payload: FlightResponse) {
    this.flightId = payload?.flight?.id;
    this.timestamp = payload?.alert?.timestamp;
    this.itinerary_id = payload?.alert?.itinerary_id;
    this.change = payload?.alert?.change;
    this.flightNumber =
      payload?.flight?.carrier?.iata + payload?.flight?.number;
    this.departureDate = payload?.flight?.scheduled_departure;
    Object.assign(this, payload);
  }

  @PrimaryGeneratedColumn()
  id?: number;

  @PrimaryColumn()
  flightId: string;

  @Column()
  timestamp: string;

  @Column()
  change: string;

  @Column({ nullable: true })
  firestoreId?: string;

  @Column({ type: 'jsonb', nullable: true })
  alert?: Alert;

  @Column({ type: 'jsonb', nullable: true })
  summaries?: Summaries;

  @Column({ type: 'jsonb' })
  flight?: FlightInfo;

  @Column({ type: 'jsonb', nullable: true })
  prediction?: Prediction;

  @Column({ type: 'jsonb' })
  status: Status;

  @Column({ type: 'jsonb', nullable: true })
  weather?: WeatherStatus;

  @Column({ type: 'jsonb', nullable: true })
  inbound?: FlightResponse;

  @Column({ type: 'jsonb', nullable: true })
  special_alerts?: SpecialAlerts[];

  @Column({ type: 'jsonb', nullable: true })
  travel_advisories?: TravelAdvisories;

  @Column({ type: 'varchar', nullable: true })
  flightNumber?: string;

  @Column({ type: 'varchar', nullable: true })
  itinerary_id?: string;

  @Column({ type: 'timestamp without time zone', nullable: true })
  departureDate?: string;

  @OneToOne(() => LumoSubscription, (sub) => sub.lumo)
  subscription?: LumoSubscription;

  flightRelation?: Flight;
}
