import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  OneToOne,
  AfterLoad,
  OneToMany,
} from 'typeorm';
import {
  getNiceDateString,
  getNiceTimeDiffString,
} from '../util/datetime.utils';
import { FlightResponse } from '../interface/lumo';
import { FlightstatsSubscription } from './FlightstatsSubscription';
import { LumoSubscription } from './LumoSubscription';
import { LumoRecord } from './LumoRecord';
import Il8nService from '../service/Il8n.service';
import { InitialFlightStatsAlert } from '../interface/flightStats/alert';
import { Trip } from './Trip';
import { createLogger } from '../util/logger';
import AirportService from '../service/Airport.service';
const logger = createLogger('zenner', 'flight', 'zenner-common');

@Entity()
export default class Flight {
  constructor(payload?: any) {
    Object.assign(this, payload);
  }

  @PrimaryGeneratedColumn()
  id: number;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  @Column({ nullable: true })
  flightNo: string;

  @Column({ nullable: true })
  realFlightNo: string;

  @Column({ nullable: true })
  operatingCarrier: string;

  @Column({ nullable: true })
  marketedCarrier: string;

  @Column({ nullable: true })
  statusName: string;

  @Column({ nullable: true })
  lumoRisk: string;

  @Column({ default: true })
  active: boolean;

  @Column({ default: false })
  cancelled: boolean;

  @Column({ default: false })
  diverted: boolean;

  @Column({ type: 'date', nullable: true })
  departureDate: Date;

  @Column({ type: 'timestamp with time zone', nullable: true })
  scheduledDeparture: Date;

  @Column({ type: 'timestamp with time zone', nullable: true })
  estimatedDeparture: Date;

  @Column({ type: 'timestamp with time zone', nullable: true })
  updatedEstimatedDeparture: Date;

  @Column({ nullable: true })
  scheduledDepartureDateLocal: string;

  @Column({ nullable: true })
  scheduledDepartureTimeLocal: string;

  @Column({ nullable: true })
  estimatedDepartureDateLocal: string;

  @Column({ nullable: true })
  estimatedDepartureTimeLocal: string;

  @Column({ type: 'timestamp with time zone', nullable: true })
  scheduledArrival: Date;

  @Column({ type: 'timestamp with time zone', nullable: true })
  estimatedArrival: Date;

  @Column({ type: 'timestamp with time zone', nullable: true })
  updatedEstimatedArrival: Date;

  @Column({ nullable: true })
  scheduledArrivalDateLocal: string;

  @Column({ nullable: true })
  scheduledArrivalTimeLocal: string;

  @Column({ nullable: true })
  estimatedArrivalDateLocal: string;

  @Column({ nullable: true })
  estimatedArrivalTimeLocal: string;

  @Column({ nullable: true })
  origin: string;

  @Column({ nullable: true })
  destination: string;

  @Column({ nullable: true })
  diversionAirport: string;

  @Column({ nullable: true })
  originTerminal: string;

  @Column({ nullable: true })
  originGate: string;

  @Column({ nullable: true })
  destinationTerminal: string;

  @Column({ nullable: true })
  destinationGate: string;

  @Column({ nullable: true })
  destinationBaggage: string;

  @Column({ nullable: true })
  planeType: string;

  @Column({ nullable: true })
  tailNo: string;

  @Column({ nullable: true })
  lumoId: string;

  @Column({ nullable: true })
  flightstatsId: string;

  @Column({ nullable: true })
  firestoreId: string;

  @Column({ nullable: true })
  firestorePassenger: string;

  @Column({ nullable: true })
  firestoreData: string;

  @Column({ type: 'jsonb', nullable: true })
  initialLumoData: FlightResponse;

  @Column({ type: 'jsonb', nullable: true })
  initialFlightStatsData: InitialFlightStatsAlert;

  @OneToOne(() => FlightstatsSubscription, (sub) => sub.flight)
  alertSubscription: FlightstatsSubscription;

  @OneToOne(() => LumoSubscription, (sub) => sub.flight)
  lumoSubscription: LumoSubscription;

  lumoRecords: LumoRecord[];
  @OneToMany((type) => Trip, (trip) => trip.flight) // note: we will create author property in the Photo class below
  trips: Trip[];

  isOutOfDate: boolean;

  //Hotfix as variable doesn't seem to be used, possibly should delete
  //isSeq: boolean = false;

  conn_time: Number = null;
  tripId: Number = null;
  isSeq: Boolean = false;

  @AfterLoad()
  getOutOfDateStatus() {
    const scheduledArrival = this.scheduledArrival;
    const latestArrival = this.estimatedArrival;
    const nowEpoch = new Date().getTime();
    // 43200s = 12hours
    if (scheduledArrival && latestArrival) {
      this.isOutOfDate =
        nowEpoch - latestArrival.getTime() > 43200 &&
        nowEpoch - scheduledArrival.getTime() > 43200;
    }
  }

  seq: number;
}
