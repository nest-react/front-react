import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  ManyToMany,
  JoinTable,
  ManyToOne,
  OneToOne,
} from 'typeorm';
import Flight from './Flight';
import Company from './Company';

@Entity()
export default class User {
  constructor(payload?: any) {
    Object.assign(this, payload);
  }

  @PrimaryGeneratedColumn()
  @JoinTable({ name: 'user_trip' })
  id: number;

  @CreateDateColumn()
  created: Date;

  @Column({ nullable: true })
  mutedAt: Date;

  @Column()
  name: string;

  @Column({ nullable: true })
  firstName: string;

  @Column({ nullable: true })
  lastName: string;

  @Column({ nullable: true })
  email: string;

  @Column({ nullable: true })
  mobile: string;

  @Column({ nullable: true })
  whatsapp: string;

  @Column({ nullable: true })
  firestoreId: string;

  @Column({ nullable: true })
  firestoreData: string;

  @Column({ type: 'boolean', nullable: true, default: false })
  muteBot: boolean;

  @Column({ type: 'boolean', nullable: true, default: false })
  confirmed_messaging: boolean;

  @Column({ type: 'boolean', nullable: true, default: false })
  sent_24afterRegister: boolean;

  @Column({ type: 'boolean', nullable: true, default: false })
  sent_24beforeFlight: boolean;

  @ManyToOne((type) => Company, (company) => company.users, {
    nullable: true,
  })
  company?: Company;

  lastMessageDate: Date = null;

  @ManyToMany((type) => Flight)
  @JoinTable({ name: 'user_flight' })
  flights: Flight[];
}
