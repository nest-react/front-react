import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  OneToOne,
  CreateDateColumn,
} from 'typeorm';
import Flight from './Flight';
import { LumoRecord } from './LumoRecord';

@Entity()
export class LumoSubscription {
  constructor(payload: any) {
    Object.assign(this, payload);
  }

  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  flightId: number;

  @Column()
  subscriptionId: string;

  @CreateDateColumn()
  created: Date;

  @OneToOne(() => Flight, (flight) => flight.lumoSubscription)
  flight: Flight;

  @OneToOne(() => LumoRecord, (lumo) => lumo.subscription)
  lumo: LumoRecord;
}
