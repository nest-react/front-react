import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  OneToMany,
  ManyToOne,
} from 'typeorm';
import User from './User';

@Entity()
export default class Company {
  @PrimaryGeneratedColumn()
  id: number;

  @CreateDateColumn()
  created: Date;

  @Column()
  name: string;

  @Column({ nullable: true })
  credits: number;

  @Column({ nullable: true })
  firestoreId: string;

  @Column({ nullable: true })
  firestoreData: string;

  @ManyToOne((type) => User, { nullable: true })
  administrator: User;

  @OneToMany((type) => User, (user) => user.company, {
    nullable: true,
  })
  users: User[];
}
