import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity()
export class UserTrip {
  constructor(payload: any) {
    Object.assign(this, payload);
  }

  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  user_id: number;

  @Column()
  trip_id: number;
}
