import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  ManyToOne,
  JoinColumn,
} from 'typeorm';
import { ConnectionData } from '../interface/lumo';
import Flight from './Flight';
@Entity()
export class Trip {
  constructor(payload: any) {
    Object.assign(this, payload);
  }
  @PrimaryGeneratedColumn()
  flight_trip_id: number;

  @Column()
  trip_id: number;

  @ManyToOne((type) => Flight, (flight) => flight.trips, { cascade: true })
  @JoinColumn({ name: 'flight_id' })
  flight: Flight;

  @Column({ nullable: true })
  seq: number;

  @Column({ nullable: true })
  conn_time: number;

  @Column({ type: 'jsonb', nullable: true })
  conn_data: ConnectionData;

  @Column({ default: false })
  sent_landed: boolean;

  @Column({ default: false })
  sent_4hrs: boolean;

  @Column({ default: false })
  sent_24hrs: boolean;

  @Column({ default: false })
  sent_48hrs: boolean;

  @Column({ default: false })
  sent_baggage: boolean;

  @Column({ default: false })
  sent_JNF: boolean;

  @Column({ default: false })
  sent_directions: boolean;

  @Column({ default: false })
  sent_feedback: boolean;

  @Column({ default: false })
  sent_JNFAirport: boolean;
}
