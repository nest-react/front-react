import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';
import {
  AlertPayload,
  FlightStatsAlert,
  RuleEvent,
} from '../interface/flightStats/alert';
import { FlightStatus } from '../interface/flightStats';
import { OneToOne } from 'typeorm';
import { FlightstatsSubscription } from './FlightstatsSubscription';

@Entity()
export class FlightstatsRecord {
  constructor(payload?: AlertPayload) {
    this.subscriptionId = payload?.rule?.id;
    this.alert = payload?.rule;
    this.departureDate = payload?.rule.departure;
    this.event = payload?.event.type;
    this.flightNumber =
      payload?.rule.carrierFsCode + payload?.rule.flightNumber;
    if (payload?.event.type) {
      const { event, ...payloadWithoutEvent } = payload;
      Object.assign(this, payloadWithoutEvent);
      return;
    }
    Object.assign(this, payload);
  }

  @PrimaryGeneratedColumn()
  id?: number;

  @Column()
  subscriptionId: string;

  @Column({ type: 'jsonb', nullable: true })
  alert: FlightStatsAlert;

  @Column({ type: 'jsonb', nullable: true })
  flightStatus?: FlightStatus;

  @Column({ type: 'varchar', nullable: true })
  event?: string;

  @Column({ type: 'varchar', nullable: true })
  dataSource: string;

  @Column({ type: 'timestamp without time zone', nullable: true })
  dateTimeRecorded: string;

  @Column({ type: 'varchar', nullable: true })
  flightNumber: string;

  @Column({ type: 'timestamp without time zone', nullable: true })
  departureDate: string;

  @OneToOne(() => FlightstatsSubscription, (sub) => sub.alert)
  subscription: FlightstatsSubscription;
}
