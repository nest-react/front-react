/**
 * Shu Comms Dispatch Server
 *
 * This file represents a worker function which is constantly monitoring a
 * Redis queue (via the Bee-Queue) library and recieving messages which should
 * be sent out to a user via an 3rd party service (i.e. Twilio for SMS/Whatsapp
 * or Google Mail for email).
 * @file listens for messages from redis and dispatches them out via Gmail or Twilio
 */

// Load environment variables first
import * as dotenv from 'dotenv';
dotenv.config();

// Add NewRelic monitoring if it's a production environment
if (process.env.NODE_ENV === 'production') {
  require('newrelic');
}

// Libraries
import BQ from 'bee-queue'; // Queue management via Redis
import { Connection, createConnection } from 'typeorm'; // Database connections

// Services
import { GmailService } from './service/Gmail.service';
import { SendArgInterface, TwilioService } from './service/Twilio.service';
import {
  AddEventInterface,
  CommsEventService,
} from './service/CommsEvent.service';
import ConfigService from './service/Config.service';

// Logging
import { createLogger } from './util/logger';
import BeeQueue from 'bee-queue';
import { pushNotificationsService } from './service/pushNotifications.service';
const logger = createLogger('zenner', 'comms', 'zenner-comms');
logger.info('Starting Comms Worker');

// Interfaces
interface message {
  from?: string;
  source?: string;
  to: string;
  message: string;
  result?: string;
}

interface emailMessage extends message {
  subject: string;
}

/**
 * Process a new message or job from Bee-Queue
 *
 * @param id Bee-Queue job id
 * @param data Content of message to send including envelope
 * @param db Connection to the database for auditing/logging tables
 */
async function bqProcesshandler(
  id: number,
  data: Partial<message>,
  db: Connection,
): Promise<string> {
  logger.info(`Comms worker processing BQ ${data.source} Job ${id}`, {
    job: data,
  });

  let result: string; // Result of actual dispatch which is normally SUCCESS or FAIL

  if (data.source === 'whatsapp' || data.source === 'sms') {
    result = await twilioJobHandler(<message>data);
  } /* istanbul ignore next */ else if (data.source === 'gmail') {
    result = await gmailJobHandler(<emailMessage>data);
  } else if (data.source === 'test') {
    result = await testToolJobHandler(data);
  } /* istanbul ignore next */ else {
    logger.error(`Comms worker failed to find a route named ${data.source}`);
    return 'FAIL';
  }

  // Save this message in the audit/logging database
  await saveEventHandler({ ...data, result }, db);
  return result;
}

/**
 * Handles a message intended for a testing or cli tool
 * @param jobData message content
 */
async function testToolJobHandler(jobData: any): Promise<string> {
  logger.info(`Received a message via cli-tool`, { jobData });
  return 'SUCCESS';
}

/**
 * Send an SMS or WhatsApp with Twilio from a Bee-Queue
 * @param messageEnvelope the message including address information
 */
async function twilioJobHandler(messageEnvelope: message): Promise<string> {
  const twilioService = new TwilioService();

  // We wrap this in a try-catch in case we're missing critical information
  // needed to send out the message.
  try {
    const twilioEnvelope: SendArgInterface = {
      from: messageEnvelope.from,
      to: messageEnvelope.to,
      body: messageEnvelope.message,
    };

    await twilioService.send(twilioEnvelope);
    return 'SUCCESS';
  } catch (err: any) /* istanbul ignore next */ {
    logger.error(`twilioJobHandler() failed`, { error: err });
    return 'FAIL';
  }
}

/**
 * Helper function to pass a job to GMail via Nodemailer
 * @param jobMessage the message including address information
 */
async function gmailJobHandler(jobMessage: emailMessage): Promise<string> {
  const gmailService = new GmailService();

  try {
    const gmailEnvelope = {
      to: jobMessage.to,
      subject: jobMessage.subject,
      html: '',
      text: jobMessage.message,
    };
    const gmailResult = await gmailService.send(gmailEnvelope);
    logger.info(`Sent message via Gmail`, gmailResult);
    return 'SUCCESS';
  } catch (err) {
    logger.error(`gmailJobHandler() failed`, { error: err });
    return 'FAIL';
  }
}

/**
 * Helper function to save a comms event to the database for audit/logging
 * @param event message to be sent to a user
 * @param db TypeORM database connection
 */
async function saveEventHandler(
  event: Partial<message>,
  db: Connection,
): Promise<void> {
  /* istanbul ignore next */
  const commsEventService = new CommsEventService(db);

  const result = await commsEventService.addEvent(<AddEventInterface>event);
  logger.debug(`Added comms dispatch event record ${result.id} to database`);
}

/* istanbul ignore next */
/**
 * Main function
 */
async function main(): Promise<void> {
  // db connection
  let db: Connection;
  try {
    db = await createConnection();
    logger.info('Comms server connected to Database');
  } catch (err) {
    logger.error('SEVERE: TypeORM failed to connect', { error: err });
    process.exit(1);
  }

  //creating instance of Queue class
  let bq: BeeQueue;

  try {
    bq = new BQ(ConfigService.get('QUEUE_NAME_OUTGOING'), {
      isWorker: true, // This is important, as we only need to read from this queue
      redis: {
        host: ConfigService.get('REDIS_HOST'),
        port: Number(ConfigService.get('REDIS_PORT')),
      },
    });

    // Initialize the push notification cron job here.
    const pushNotiService = new pushNotificationsService(db, true);
    await pushNotiService.chrono();
  } catch (err) {
    logger.error('SEVERE: Comms worker could not start Bee-Queue', {
      error: err,
    });
    process.exit(2);
  }

  bq.on('ready', () => {
    logger.info(`Comms worker ready to dispatch messages`, {
      queue: ConfigService.get('QUEUE_NAME_OUTGOING'),
    });
  });

  bq.on('retrying', (job: any, err: any) => {
    logger.warn(
      `Job for comms worker to dispatch failed but is being retried!`,
      {
        id: job.id,
        error: err,
      },
    );
  });

  bq.on('failed', (job: any, err: any) => {
    logger.error(`Job for comms worker to dispatch failed with error`, {
      id: job.id,
      error: err,
    });
  });

  // We do this inside main so the database connection is in place
  bq.process((job: any) => bqProcesshandler(job.id, job.data, db));
}

// Launch the worker
main().then(() => {
  console.log(`Comms worker main() completed`); // We shouldn't get here
});

// Note - we only export these classes so we can test them
export = {
  bqProcesshandler,
  gmailJobHandler,
  twilioJobHandler,
  saveEventHandler,
};
