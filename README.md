### Git Flow

All pull requests to the `develop` branch will trigger Staging environment deploy

All pull requests to the `master` branch will trigger Production environment deploy

Other branches pushes will trigger only terraform Linter.

### Local Environment

#### Requirements:

For the local environment setup, please use `docker` and `docker-compose`

You can install `docker` with the https://www.docker.com/products/docker-desktop installation.

To install docker-compose, please use this instructions: https://docs.docker.com/compose/install/#install-compose

#### Spin Up DEV Environment

############ All commands should be executed in the root directory of your project #################

`docker-compose build` - rebuilds the container
`docker-compose up -d` - spinns up CORE, DB and Redis containers

`src` directory linked to the container with volume setup

#### Docker-Compose Environment Variables

```
environment:
      - NODE_ENV=dev
      - REDIS_HOST=redis
      - DB_HOST=db
      - DB_NAME=zennerdb
      - DB_USER=zenneruser
      - DB_PASS=zennerpass
```

## Deployment Environment

### Terraform

############ All commands should be executed in the root directory of your project #################

You should export AWS credentials to work with Terraform. I recommend to use `aws-vault` https://github.com/99designs/aws-vault to keep your secrets secure.

#### Commands

`docker-compose -f deploy/docker-compose.yml run --rm terraform workspace list` - Lists environments that we use

`staging` - environment for the staging Infrastructure
`production` - environment for the PRODUCTION Infrastructure deployments

`docker-compose -f deploy/docker-compose.yml run --rm terraform workspace select NAME` - Select Desired Workspace

`docker-compose -f deploy/docker-compose.yml run --rm terraform plan` - Plans resources or changes for the infrastructure

`docker-compose -f deploy/docker-compose.yml run --rm terraform apply` - Apply changes to the infrastructure
