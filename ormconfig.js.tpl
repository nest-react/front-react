const SnakeNamingStrategy = require("typeorm-naming-strategies")
  .SnakeNamingStrategy;

module.exports = {
  type: "postgres",
  host: "${DB_HOST}",
  port: 5432,
  username: "${DB_USER}",
  password: "${DB_PASS}",
  database: "${DB_NAME}",
  synchronize: true,
  logging: false,
  namingStrategy: new SnakeNamingStrategy(),
  entities: ["src/model/**/*.ts"],
  migrations: ["src/db/migration/**/*.ts"],
  subscribers: ["src/subscriber/**/*.ts"],
  cli: {
    entitiesDir: "src/model",
    migrationsDir: "src/db/migration",
    subscribersDir: "src/subscriber",
  },
};
