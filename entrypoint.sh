#!/bin/sh

set -e

envsubst < /usr/src/app/.env.tpl > /usr/src/app/.env
envsubst < /usr/src/app/ormconfig.js.tpl > /usr/src/app/ormconfig.js

cd /usr/src/app && npm start
