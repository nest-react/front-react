import * as Yup from 'yup';

export enum Flight {
  ticket = 'ticket',
  pick = 'pick',
}

export const StepOneSchema = Yup.object().shape({
  firstName: Yup.string()
    .min(2, 'Too Short!')
    .required('First name is a required field'),
  lastName: Yup.string()
    .min(2, 'Too Short!')
    .required('Last name is a required field'),
  email: Yup.string()
    .email('Please enter a valid email address.')
    .required('Email is a required field'),
  phone: Yup.string().required('Phone number is a required field'),
  haveFlight: Yup.string().required('Required'),
  haveWhatsApp: Yup.boolean().required('Required'),
});

export const RegisterSchema = Yup.object().shape({
  firstName: Yup.string()
    .min(2, 'Too Short!')
    .required('First name is a required field'),
  lastName: Yup.string()
    .min(2, 'Too Short!')
    .required('Last name is a required field'),
  email: Yup.string()
    .email('Please enter a valid email address.')
    .required('Required'),
  phone: Yup.string().required('Required'),
  haveFlight: Yup.string().required('Required'),
  date: Yup.date().required(),
  // connecting flight validation
  connectingCarrier: Yup.string().when('haveConnecting', {
    is: true,
    then: Yup.string()
      .required('Connecting carrier is a required field')
      .min(2),
    otherwise: Yup.string(),
  }),
  connectingFlightNumber: Yup.number().when('haveConnecting', {
    is: true,
    then: Yup.number()
      .required('Connecting flight number is a required field')
      .min(2),
    otherwise: Yup.number(),
  }),
  departingAirport: Yup.string().when('haveConnecting', {
    is: true,
    then: Yup.string()
      .required('Connecting airport is a required field')
      .min(2),
    otherwise: Yup.string(),
  }),
  airport: Yup.string().required('Airport is a required field').min(3),
  flightNumber: Yup.number()
    .required('Flight number is a required field')
    .min(2),
  carrier: Yup.string().required('Carrier is a required field'),
});

export const ReturnFlightValidationSchema = Yup.object().shape({
  returnAirport: Yup.string().required('Airport is a required field').min(3),
  returnFlightNumber: Yup.number()
    .required('Flight number is a required field')
    .min(2),
  returnCarrier: Yup.string().required('Carrier is a required field'),
  returnDate: Yup.date().required('Date is a required field'),
});
