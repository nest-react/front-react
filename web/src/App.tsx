import React, { useState } from 'react';
import { useFormik } from 'formik';
import {
  Alert,
  Button,
  Checkbox,
  Container,
  ControlLabel,
  DatePicker,
  ErrorMessage,
  Form,
  FormControl,
  FormGroup,
  Panel,
  Steps,
} from 'rsuite';
import PhoneInput from 'react-phone-input-2';

import { addDays, format, isPast } from 'date-fns';
// styles
import './App.css';
import 'react-phone-input-2/lib/style.css';
//
import {
  Flight,
  RegisterSchema,
  ReturnFlightValidationSchema,
  StepOneSchema,
} from './register/validation';

import { useAirport } from './hooks/useAirport';
import {
  useFlightRegister,
  useUserRegister,
  validateFlight,
  ValidateResponse,
} from './hooks/useFlightRegister';
import { AppAutoComplete } from './components/AppAutoComplete';
import FeedbackCard from './components/FeedbackCard/FeedbackCard';
import { useCarrier } from './hooks/useCarrier';
import { DisplayValues, RegisterPayload, Titles } from './interfaces';

type Step = 0 | 1 | 2 | 3 | number;

const ValidationRules = [
  StepOneSchema,
  RegisterSchema,
  ReturnFlightValidationSchema,
];

interface AppProps {}

const App: React.FC<AppProps> = () => {
  const [step, setStep] = useState<Step>(0);
  const [shouldDisplayFlight, setShouldDisplayFlight] = useState<boolean>(true);
  const airports = useAirport();
  const carrier = useCarrier();
  const onChange = (nextStep: Step) => {
    setStep(nextStep < 0 ? 0 : nextStep > 3 ? 3 : nextStep);
  };

  const onNext = async () => {
    const hasErrors = !!Object.keys(await formik.validateForm()).length;
    if (step !== 0 && !hasErrors) {
      const isFlightValid = await validateFlightData(formik.values);
      if (!isFlightValid.isValid) {
        return;
      }
    }
    if (!hasErrors) {
      if (formik.values.haveFlight === Flight.pick || step === 2) {
        await formik.submitForm();
        onChange(3);
      } else onChange(step + 1);
    }
  };

  const titlesMap: Titles = {
    0: { title: 'Sign up', paper: '' },
    1: { title: 'Add flight', paper: '' },
  };

  const onPrevious = async () => {
    if (step === 2) {
      formik.values.haveReturnFlight = false;
    }
    onChange(step - 1);
  };

  const onAddFlightLater = async () => {
    const hasErrors = !!Object.keys(await formik.validateForm()).length;
    if (!hasErrors) {
      setShouldDisplayFlight(false);
      try {
        await registerUser.request({
          firstName: formik.values.firstName,
          lastName: formik.values.lastName,
          name: `${formik.values.firstName} ${formik.values.lastName}`,
          email: formik.values.email,
          mobile: formik.values.phone,
          whatsapp: formik.values.haveWhatsApp ? formik.values.phone : null,
        });
        setStep(3);
      } catch (e) {
        Alert.error('Something went wrong');
        return;
      }
    }
  };

  const onPickAFlight = async () => {
    const hasErrors = !!Object.keys(await formik.validateForm()).length;
    if (!hasErrors) {
      formik.setFieldValue('haveFlight', Flight.pick);
      await formik.submitForm();
      setStep(3);
    }
  };

  const handleSave = async () => {
    const hasErrors = !!Object.keys(await formik.validateForm()).length;
    if (!hasErrors) {
      const isFlightsValid = await validateFlightData(formik.values);
      if (isFlightsValid.isValid) {
        await formik.submitForm();
        setStep(3);
      }
    }
  };

  const handleReturnFlightSave = async () => {
    formik.values.haveReturnFlight = true;
    await handleSave();
  };

  const validateReturnFlight = async (values: RegisterPayload) => {
    if (
      values.returnDate &&
      values.returnCarrier &&
      values.returnFlightNumber
    ) {
      const { returnFlightNumber, returnDate, returnCarrier } = values;
      const res = await validateFlight({
        carrier: returnCarrier,
        date: format(returnDate, 'yyyy-MM-dd'),
        flightNumber: Number(returnFlightNumber),
      });
      const isValid = displayValidationError(res);
      if (isValid) {
        formik.setFieldValue('returnAirport', res.origin);
        formikDisplayValues.setFieldValue('returnAirport', res.origin);
      }
    }
  };

  const validateConnectingFlight = async (values: RegisterPayload) => {
    if (
      values.connectingCarrier &&
      values.connectingFlightNumber &&
      values.date
    ) {
      const { connectingCarrier, connectingFlightNumber, date } = values;
      const res = await validateFlight({
        carrier: connectingCarrier,
        date: format(date, 'yyyy-MM-dd'),
        flightNumber: Number(connectingFlightNumber),
      });
      const isValid = displayValidationError(res);
      if (isValid) {
        formik.setFieldValue('departingAirport', res.origin);
        formikDisplayValues.setFieldValue('departingAirport', res.origin);
      }
    }
  };

  const displayValidationError = (
    result: ValidateResponse | ValidateResponse[],
  ): boolean => {
    if (Array.isArray(result)) {
      if (result.some((res) => !res.isValid)) {
        Alert.error(
          'Flight not found. Try to change flight number or carrier ',
          0,
        );
        return false;
      }
      return true;
    } else if (!result.isValid) {
      Alert.error(
        'Flight not found. Try to change flight number or carrier ',
        0,
      );
      return false;
    }
    return true;
  };

  const validateFlightData = async (
    values: RegisterPayload,
    field?: string,
  ): Promise<ValidateResponse> => {
    if (
      values.haveFlight === Flight.ticket &&
      values.carrier &&
      values.flightNumber &&
      values.date
    ) {
      const {
        carrier,
        date,
        flightNumber,
        haveReturnFlight,
        haveConnecting,
      } = values;
      const validationResults: ValidateResponse[] = [];
      const formattedDate = format(date, 'yyyy-MM-dd');
      const response = await validateFlight({
        carrier,
        date: formattedDate,
        flightNumber: Number(flightNumber),
      });
      validationResults.push(response);
      if (haveConnecting) {
        const response = await validateFlight({
          carrier: values.connectingCarrier,
          date: format(date, 'yyyy-MM-dd'),
          flightNumber: Number(values.connectingFlightNumber),
        });
        validationResults.push(response);
      }
      if (haveReturnFlight) {
        const response = await validateFlight({
          carrier: values.returnCarrier,
          date: format(values.returnDate, 'yyyy-MM-dd'),
          flightNumber: Number(values.returnFlightNumber),
        });
        validationResults.push(response);
      }
      const isValid = displayValidationError(validationResults);
      if (isValid) {
        if (field) formik.setFieldValue(field, response.origin);
        return {
          isValid,
          origin: response.origin,
          destination: response.destination,
        };
      }
      return {
        isValid: false,
      };
    }
    return {
      isValid: false,
    };
  };

  const validateMainFlight = async () => {
    const values = await validateFlightData(formik.values, 'airport');
    if (values && values.isValid) {
      formik.setFieldValue('departureAirport', values.destination);
      formikDisplayValues.setFieldValue('airport', values.origin);
    }
  };

  const StepsItems = Object.entries(titlesMap).map(([key, value], index) => (
    <Steps.Item
      title={value.title}
      key={key}
      className={index === step ? 'stepsTitle' : undefined}
    />
  ));

  const register = useFlightRegister();

  const registerUser = useUserRegister();

  const formikDisplayValues = useFormik<DisplayValues>({
    initialValues: {
      carrier: '',
      connectingCarrier: '',
      returnCarrier: '',
      airport: '',
      departingAirport: '',
      returnAirport: '',
    },
    onSubmit: () => {},
  });

  const formik = useFormik<RegisterPayload>({
    validateOnBlur: true,
    validateOnChange: false,
    initialValues: {
      firstName: '',
      lastName: '',
      email: '',
      phone: '',
      haveFlight: Flight.ticket,
      haveWhatsApp: true,
      haveConnecting: false,
      haveReturnFlight: false,
      carrier: '',
      flightNumber: '',
      airport: '',
      departureAirport: '',
      date: new Date(),
      airportFrom: '',
      airportTo: '',
      connectingCarrier: '',
      connectingFlightNumber: '',
      departingAirport: '',
      returnCarrier: '',
      returnFlightNumber: '',
      returnAirport: '',
      returnDate: new Date(),
    },
    validationSchema: ValidationRules[step],
    onSubmit: async (values) => {
      Alert.closeAll();

      const formattedDate = format(values.date, 'yyyy-MM-dd');

      try {
        const validationResults: ValidateResponse[] = [];
        if (values.haveFlight === Flight.ticket) {
          const res = await validateFlight({
            carrier: values.carrier,
            date: formattedDate,
            flightNumber: Number(values.flightNumber),
          });
          validationResults.push(res);

          if (values.haveConnecting) {
            const connectingRes = await validateFlight({
              carrier: values.connectingCarrier,
              date: formattedDate,
              flightNumber: Number(values.connectingFlightNumber),
            });
            validationResults.push(connectingRes);
          }

          if (values.haveReturnFlight) {
            const connectingRes = await validateFlight({
              carrier: values.returnCarrier,
              date: format(values.returnDate, 'yyyy-MM-dd'),
              flightNumber: Number(values.returnFlightNumber),
            });
            validationResults.push(connectingRes);
          }

          displayValidationError(validationResults);
        }
        await register.request({
          ...values,
          mobile: values.phone,
          date: formattedDate,
        });
        setStep(2);
      } catch (e) {
        Alert.error('Unhandled error');
        console.error('Error', e);
      }
    },
  });

  return (
    <>
      <Container className="container mt-5 mb-5">
        <Steps current={step === 2 ? 1 : step}>{StepsItems}</Steps>
        <Container className="container mt-4">
          <Panel header={titlesMap[step]?.paper} shaded>
            <div className="card-body">
              {step === 0 && (
                <>
                  <Form fluid>
                    <FormGroup>
                      <ControlLabel>First name</ControlLabel>
                      <FormControl
                        name="firstName"
                        placeholder="First name"
                        value={formik.values.firstName}
                        onChange={(v) => {
                          if (formik.errors) formik.setErrors({});
                          formik.setFieldValue('firstName', v);
                        }}
                      />
                      <ErrorMessage
                        show={!!formik.errors.firstName}
                        className="errorMessage"
                      >
                        {formik.errors.firstName}
                      </ErrorMessage>
                    </FormGroup>
                    <FormGroup>
                      <ControlLabel>Last name</ControlLabel>
                      <FormControl
                        name="lastName"
                        placeholder="Last name"
                        value={formik.values.lastName}
                        onChange={(v) => {
                          if (formik.errors) formik.setErrors({});
                          formik.setFieldValue('lastName', v);
                        }}
                      />
                      <ErrorMessage
                        show={!!formik.errors.lastName}
                        className="errorMessage"
                      >
                        {formik.errors.lastName}
                      </ErrorMessage>
                    </FormGroup>
                    <FormGroup>
                      <ControlLabel>Email address</ControlLabel>
                      <FormControl
                        type="email"
                        name="email"
                        className="form-control"
                        placeholder="Enter email"
                        value={formik.values.email}
                        onChange={(v) => {
                          if (formik.errors) formik.setErrors({});
                          formik.setFieldValue('email', v);
                        }}
                      />
                      <ErrorMessage
                        show={!!formik.errors.email}
                        className="errorMessage"
                      >
                        {formik.errors.email}
                      </ErrorMessage>
                      <small id="emailHelp" className="form-text text-muted">
                        We'll never share your email with anyone else.
                      </small>
                    </FormGroup>
                    <FormGroup>
                      <ControlLabel>Phone Number</ControlLabel>
                      <PhoneInput
                        country={'us'}
                        value={formik.values.phone}
                        isValid={!formik.errors.phone}
                        inputStyle={{
                          width: '100%',
                        }}
                        onChange={(v: string) => {
                          if (formik.errors) formik.setErrors({});
                          formik.setFieldValue('phone', v);
                        }}
                      />
                      <ErrorMessage
                        show={!!formik.errors.phone}
                        className="errorMessage"
                      >
                        {formik.errors.phone}
                      </ErrorMessage>
                    </FormGroup>
                    <div className="mt-3 mb-3">
                      <Checkbox
                        checked={formik.values.haveWhatsApp}
                        onChange={(v, checked) =>
                          formik.setFieldValue('haveWhatsApp', checked)
                        }
                        size="lg"
                      >
                        I am using WhatsApp
                      </Checkbox>
                    </div>
                  </Form>
                </>
              )}
              {step === 1 && (
                <Form fluid>
                  {formik.values.haveFlight &&
                    formik.values.haveFlight === Flight.ticket && (
                      <div>
                        <div className="form-group">
                          <label>Date</label>
                          <DatePicker
                            block
                            placement="bottomStart"
                            ranges={[]}
                            value={formik.values.date}
                            onChange={(v) => {
                              formik.setFieldValue('date', v);
                              validateFlightData(
                                {
                                  ...formik.values,
                                  date: v,
                                },
                                'airport',
                              );
                            }}
                            disabledDate={(date) =>
                              Boolean(date && isPast(addDays(date, 1)))
                            }
                            showWeekNumbers
                          />
                          <ErrorMessage
                            show={!!formik.errors.date}
                            className="errorMessage"
                          >
                            {formik.errors.date}
                          </ErrorMessage>
                        </div>
                        <div className="form-group">
                          <label>Carrier or Airline</label>
                          <AppAutoComplete
                            labelKey="name"
                            valueKey="iata"
                            value={formikDisplayValues.values.carrier}
                            data={carrier.data}
                            placeholder="For example: British Airways, or BA"
                            onChange={(value) => {
                              carrier.setQuery(value);
                              formikDisplayValues.setFieldValue(
                                'carrier',
                                value,
                              );
                            }}
                            onSelect={(value) => {
                              if (formik.errors) formik.setErrors({});
                              formik.setFieldValue('carrier', value.label);
                            }}
                          />
                          <ErrorMessage
                            show={!!formik.errors.carrier}
                            className="errorMessage"
                          >
                            {formik.errors.carrier}
                          </ErrorMessage>
                        </div>
                        <FormGroup>
                          <ControlLabel> Flight Number</ControlLabel>
                          <FormControl
                            type="text"
                            className="form-control"
                            name="number"
                            placeholder="1692"
                            value={formik.values.flightNumber}
                            onBlur={validateMainFlight}
                            onChange={(v) => {
                              if (formik.errors) formik.setErrors({});
                              formik.setFieldValue('flightNumber', v);
                            }}
                          />
                          <ErrorMessage
                            show={!!formik.errors.flightNumber}
                            className="errorMessage"
                          >
                            {formik.errors.flightNumber}
                          </ErrorMessage>
                        </FormGroup>
                        <div className="form-group">
                          <label>Departure airport</label>
                          <AppAutoComplete
                            data={airports.data}
                            labelKey="name"
                            value={formikDisplayValues.values.airport}
                            valueKey="iata_code"
                            placeholder="For example: JFK"
                            onChange={(value) => {
                              formikDisplayValues.setFieldValue(
                                'airport',
                                value,
                              );
                              airports.setQuery(value);
                            }}
                            onSelect={(value) => {
                              if (formik.errors) formik.setErrors({});
                              formik.setFieldValue('airport', value.label);
                            }}
                          />
                          <ErrorMessage
                            show={!!formik.errors.airport}
                            className="errorMessage"
                          >
                            {formik.errors.airport}
                          </ErrorMessage>
                        </div>
                        <div className="mt-3 mb-3">
                          <Checkbox
                            checked={formik.values.haveConnecting}
                            onChange={(v, checked) =>
                              formik.setFieldValue('haveConnecting', checked)
                            }
                            size="lg"
                          >
                            I have a connecting flight
                          </Checkbox>
                        </div>
                        {formik.values.haveConnecting && (
                          <>
                            <div className="form-group">
                              <label>Connecting carrier or Airline</label>
                              <AppAutoComplete
                                labelKey="name"
                                valueKey="iata"
                                data={carrier.data}
                                value={
                                  formikDisplayValues.values.connectingCarrier
                                }
                                placeholder="For example: British Airways, or BA"
                                onChange={(value) => {
                                  carrier.setQuery(value);
                                  formikDisplayValues.setFieldValue(
                                    'connectingCarrier',
                                    value,
                                  );
                                }}
                                onSelect={(value) => {
                                  if (formik.errors) formik.setErrors({});
                                  formik.setFieldValue(
                                    'connectingCarrier',
                                    value.label,
                                  );
                                }}
                              />
                              <ErrorMessage
                                show={!!formik.errors.connectingCarrier}
                                className="errorMessage"
                              >
                                {formik.errors.connectingCarrier}
                              </ErrorMessage>
                            </div>
                            <FormGroup>
                              <ControlLabel>
                                {' '}
                                Connecting flight number
                              </ControlLabel>
                              <FormControl
                                type="text"
                                className="form-control"
                                name="connecting_number"
                                placeholder="1692"
                                value={formik.values.connectingFlightNumber}
                                onBlur={() => {
                                  validateConnectingFlight(formik.values);
                                }}
                                onChange={(v) => {
                                  if (formik.errors) formik.setErrors({});
                                  formik.setFieldValue(
                                    'connectingFlightNumber',
                                    v,
                                  );
                                }}
                              />
                              <ErrorMessage
                                show={!!formik.errors.connectingFlightNumber}
                                className="errorMessage"
                              >
                                {formik.errors.connectingFlightNumber}
                              </ErrorMessage>
                            </FormGroup>
                            <div className="form-group">
                              <label>Departure airport</label>
                              <AppAutoComplete
                                data={airports.data}
                                labelKey="name"
                                value={
                                  formikDisplayValues.values.departingAirport
                                }
                                valueKey="iata_code"
                                onChange={(value) => {
                                  airports.setQuery(value);
                                  formikDisplayValues.setFieldValue(
                                    'departingAirport',
                                    value,
                                  );
                                }}
                                placeholder="For example: JFK"
                                onSelect={(value) => {
                                  if (formik.errors) formik.setErrors({});
                                  formik.setFieldValue(
                                    'departingAirport',
                                    value.label,
                                  );
                                }}
                              />
                              <ErrorMessage
                                show={!!formik.errors.departingAirport}
                                className="errorMessage"
                              >
                                {formik.errors.departingAirport}
                              </ErrorMessage>
                            </div>
                          </>
                        )}
                      </div>
                    )}
                </Form>
              )}
              {step === 2 && (
                <Form fluid>
                  <div>
                    <div className="form-group">
                      <label>Date</label>
                      <DatePicker
                        block
                        placement="topStart"
                        ranges={[]}
                        value={formik.values.returnDate}
                        onChange={(v) => {
                          if (formik.errors) formik.setErrors({});
                          formik.setFieldValue('returnDate', v);
                          validateFlightData(
                            {
                              ...formik.values,
                              returnDate: v,
                            },
                            'returnAirport',
                          );
                        }}
                        disabledDate={(date) =>
                          Boolean(date && isPast(addDays(date, 1)))
                        }
                        showWeekNumbers
                      />
                    </div>
                    <ErrorMessage
                      show={!!formik.errors.returnDate}
                      className="errorMessage"
                    >
                      {formik.errors.returnDate}
                    </ErrorMessage>
                    <div className="form-group">
                      <label>Return carrier or Airline</label>
                      <AppAutoComplete
                        labelKey="name"
                        valueKey="iata"
                        data={carrier.data}
                        value={formikDisplayValues.values.returnCarrier}
                        onChange={(value) => {
                          carrier.setQuery(value);
                          formikDisplayValues.setFieldValue(
                            'returnCarrier',
                            value,
                          );
                        }}
                        placeholder="For example: British Airways, or BA"
                        onSelect={(value) => {
                          if (formik.errors) formik.setErrors({});
                          formik.setFieldValue('returnCarrier', value.label);
                        }}
                      />
                      <ErrorMessage
                        show={!!formik.errors.returnCarrier}
                        className="errorMessage"
                      >
                        {formik.errors.returnCarrier}
                      </ErrorMessage>
                    </div>
                    <FormGroup>
                      <ControlLabel>Flight Number</ControlLabel>
                      <FormControl
                        type="text"
                        className="form-control"
                        name="number"
                        placeholder="1692"
                        value={formik.values.returnFlightNumber}
                        onBlur={() => validateReturnFlight(formik.values)}
                        onChange={(v) => {
                          if (formik.errors) formik.setErrors({});
                          formik.setFieldValue('returnFlightNumber', v);
                        }}
                      />
                      <ErrorMessage
                        show={!!formik.errors.returnFlightNumber}
                        className="errorMessage"
                      >
                        {formik.errors.returnFlightNumber}
                      </ErrorMessage>
                    </FormGroup>
                    <div className="form-group">
                      <label>Airport</label>
                      <AppAutoComplete
                        data={airports.data}
                        labelKey="name"
                        value={formikDisplayValues.values.returnAirport}
                        valueKey="iata_code"
                        placeholder="For example: JFK"
                        onChange={(value) => {
                          formikDisplayValues.setFieldValue(
                            'returnAirport',
                            value,
                          );
                          formik.setFieldValue('returnAirport', value);
                          airports.setQuery(value);
                        }}
                        onSelect={(value) => {
                          if (formik.errors) formik.setErrors({});
                          formik.setFieldValue('returnAirport', value.label);
                        }}
                      />
                      <ErrorMessage
                        show={!!formik.errors.returnAirport}
                        className="errorMessage"
                      >
                        {formik.errors.returnAirport}
                      </ErrorMessage>
                    </div>
                  </div>
                </Form>
              )}
              {step === 3 && (
                <FeedbackCard
                  displayFlight={shouldDisplayFlight}
                  firstName={formik.values.firstName}
                  airportFrom={formik.values.airport}
                  airportTo={formik.values.departureAirport}
                  date={formik.values.date}
                />
              )}
              {step === 0 && (
                <>
                  <Button
                    className="mr-4 mb-2"
                    onClick={onNext}
                    active
                    appearance="primary"
                  >
                    I have a flight
                  </Button>
                  <Button
                    onClick={onPickAFlight}
                    active
                    className="mr-4 mb-2"
                    appearance="primary"
                    loading={register.loading || formik.isSubmitting}
                  >
                    Pick a flight for me
                  </Button>
                  <Button
                    onClick={onAddFlightLater}
                    active
                    className="mr-4 mb-2"
                    appearance="primary"
                  >
                    I'll add a flight later
                  </Button>
                </>
              )}
              {step === 1 && (
                <>
                  <Button
                    className="mr-4 mb-2"
                    onClick={onPrevious}
                    active
                    appearance="primary"
                  >
                    Back
                  </Button>
                  <Button
                    className="mr-4 mb-2"
                    onClick={onNext}
                    active
                    appearance="primary"
                  >
                    I have a return flight
                  </Button>
                  <Button
                    className="mr-4 mb-2"
                    onClick={handleSave}
                    active
                    color="green"
                    loading={register.loading || formik.isSubmitting}
                    appearance="primary"
                  >
                    Submit
                  </Button>
                </>
              )}
              {step === 2 && (
                <>
                  <Button
                    className="mr-4 mb-2"
                    onClick={onPrevious}
                    active
                    appearance="primary"
                  >
                    Back
                  </Button>
                  <Button
                    className="mr-4 mb-2"
                    onClick={handleReturnFlightSave}
                    active
                    color="green"
                    loading={register.loading || formik.isSubmitting}
                    appearance="primary"
                  >
                    Submit
                  </Button>
                </>
              )}
            </div>
          </Panel>
        </Container>
      </Container>
    </>
  );
};

export default App;
