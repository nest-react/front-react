import React from 'react';
import { AutoCompleteProps } from 'rsuite/lib/AutoComplete/AutoComplete';
import { AutoComplete } from 'rsuite';


interface AppAutoComplete extends AutoCompleteProps {
  labelKey: string
  valueKey: string;
}


export const AppAutoComplete: React.FC<AppAutoComplete> = ({ labelKey, valueKey, ...props }) => {

  const data = Array.isArray(props.data) ? props.data.map(item => {
    return {
      label: item[valueKey],
      value: getLabel({ label: item[labelKey], value: item[valueKey] }),
    };
  }) : [];

  return (
    <AutoComplete
      {...props}
      filterBy={showAll}
      data={data}
      renderItem={item => item.value}
    />
  );
};

interface DataItem {
  label: string;
  value: string;
}

const getLabel = ({ value, label }: DataItem): string => {
  return value ? `${label} (${value})` : '';
};

const showAll = (): boolean => true;