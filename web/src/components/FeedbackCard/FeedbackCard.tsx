import React, { FC, memo } from 'react';
import { format } from 'date-fns';

type FeedbackCardProps = {
  date: Date;
  firstName: string;
  displayFlight: boolean;
  airportFrom?: string;
  airportTo?: string;
};

const FeedbackCard: FC<FeedbackCardProps> = ({
  firstName,
  airportFrom,
  airportTo,
  date,
  displayFlight,
}) => {
  if (airportTo === '') {
    const dtToday = new Date();
    const dtTomorrow = new Date(dtToday);
    dtTomorrow.setDate(dtTomorrow.getDate() + 1);
    date = dtTomorrow;
  }

  return (
    <>
      <h6 className="card-title">
        <p className="mt-3 mb-3">You’re all set, {firstName}!</p>
        {displayFlight && (
          <>
            <br />I am now tracking your flight from{' '}
            {airportFrom !== '' ? airportFrom : 'JFK'} to
            {airportTo !== '' ? ` ${airportTo}` : ' LAX'} at{' '}
            {format(date, 'yyyy-MM-dd')}. I’ll be in touch shortly with updates
            on your flight, safe travels!
            <br />
          </>
        )}
        {!displayFlight && (
          <p>
            Your account has been created and I will be available to assist you
            via Whatsapp or SMS shortly.
          </p>
        )}
        <p className="mt-3 mb-3">Zenny</p>
        <p>Your personal travel assistant </p>
      </h6>
    </>
  );
};

export default memo(FeedbackCard);
