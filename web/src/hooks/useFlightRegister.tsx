import { useCallback, useState } from 'react';
import qs from 'qs';
import { api } from './Api';

import { Flight } from '../register/validation';

export interface ValidateDto {
  carrier: string;
  flightNumber: number;
  // YYYY-MM-DD
  date: string;
}

export interface ValidateResponse {
  isValid: boolean;
  origin?: string;
  destination?: string;
}

export const validateFlight = async (
  request: ValidateDto,
): Promise<ValidateResponse> => {
  return api
    .get<ValidateResponse>('flight/validate?' + qs.stringify(request))
    .then((response) => response.data);
};

export interface Passenger {
  firstName: string;
  lastName: string;
  email: string;
  mobile: string;
}

interface PassengerPayload extends Passenger {
  whatsapp: string | null;
  name: string;
}

export const useFlightRegister = () => {
  const [loading, setLoading] = useState<boolean>(false);
  const [error, setError] = useState<boolean>(false);
  const [data, setData] = useState<string>();

  const request = useCallback(
    async (payload: RegisterDto) => {
      setLoading(true);
      setError(false);
      try {
        const response = await api.post<{ id: string }>(
          'flight/register',
          payload,
        );
        setData(response.data.id);

        return response.data.id;
      } catch (e) {
        setError(true);
      } finally {
        setLoading(false);
      }
    },
    [setLoading, setData, setError],
  );

  return {
    loading,
    error,
    data,
    request,
  };
};

export const useUserRegister = () => {
  const [data, setData] = useState<number>();

  const request = useCallback(
    async (payload: PassengerPayload) => {
      const response = await api.post<{ id: number }>(
        '/register/user',
        payload,
      );
      setData(response.data.id);
      return response.data.id;
    },
    [setData],
  );

  return {
    data,
    request,
  };
};

interface RegisterPayload {
  firstName: string;
  lastName: string;
  email: string;
  haveFlight: Flight.pick | Flight.ticket;
  carrier?: string;
  flightNumber?: string;
  airport?: string;
  date: string;
  airportFrom?: string;
  airportTo?: string;
}

export type RegisterDto = RegisterPayload & Passenger;
