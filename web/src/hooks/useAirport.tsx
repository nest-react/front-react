import { useEffect, useState } from 'react';
import { api } from './Api';

export interface AirportMeta {
  id: number;
  type:
    | 'large_airport'
    | 'medium_airport'
    | 'small_airport'
    | 'metropolitan_airport';
  name: string;
  iso_country: string;
  municipality: string;
  iata_code: string;
  rank: number;
}

export const useAirport = () => {
  const [data, setData] = useState<AirportMeta[]>([]);
  const [loading, setLoading] = useState<boolean>(false);
  const [error, setError] = useState<boolean>(false);
  const [q, setQuery] = useState<string>('');

  useEffect(() => {
    if (!q) {
      return;
    }

    setLoading(true);
    setError(false);
    try {
      api.get(('autocomplete/airports?q=' + q)).then(response => {
        setData(response.data);
      });
    } catch (e) {
      console.error('useAirport error', e);
      setError(true);
    } finally {
      setLoading(false);
    }

  }, [q]);

  return {
    loading,
    data,
    error,
    setQuery,
  };

};