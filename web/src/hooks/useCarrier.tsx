import { useEffect, useState } from 'react';
import { api } from './Api';

export interface CarrierMeta {
  id: number;
  name: string;
  country: string;
  iata: string;
}

export const useCarrier = () => {
  const [data, setData] = useState<CarrierMeta[]>([]);
  const [loading, setLoading] = useState<boolean>(false);
  const [error, setError] = useState<boolean>(false);
  const [q, setQuery] = useState<string>('');
  useEffect(() => {
    if (!q) {
      return;
    }

    setLoading(true);
    setError(false);
    try {
      api.get(('autocomplete/carrier?q=' + q)).then(response => {
        setData(response.data);
      });
    } catch (e) {
      console.error('useCarrier error', e);
      setError(true);
    } finally {
      setLoading(false);
    }

  }, [q]);

  return {
    loading,
    data,
    error,
    setQuery,
  };

};