import { Flight } from '../register/validation';

export interface Passenger {
  name: string;
  email: string;
  mobile: string;
}

export interface RegisterPayload {
  firstName: string;
  lastName: string;
  email: string;
  phone: string;
  haveFlight: Flight.pick | Flight.ticket;
  haveWhatsApp: boolean;
  haveConnecting: boolean;
  haveReturnFlight: boolean;
  connectingCarrier: string;
  connectingFlightNumber: string;
  departingAirport: string;
  carrier: string;
  flightNumber: string;
  airport: string;
  departureAirport: string;
  date: Date;
  airportFrom?: string;
  airportTo?: string;
  returnCarrier: string;
  returnFlightNumber: string;
  returnAirport: string;
  returnDate: Date;
}

export interface DisplayValues {
  carrier: string;
  connectingCarrier: string;
  returnCarrier: string;
  airport: string;
  departingAirport: string;
  returnAirport: string;
}

export type Titles = Record<number, { title: string; paper: string }>;
